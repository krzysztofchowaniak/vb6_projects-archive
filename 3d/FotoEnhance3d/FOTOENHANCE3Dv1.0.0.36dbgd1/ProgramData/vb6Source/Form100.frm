VERSION 5.00
Object = "{08216199-47EA-11D3-9479-00AA006C473C}#2.1#0"; "rmcontrol.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form Form1 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   0  'None
   Caption         =   "bmp AND jpg DXVIEWER"
   ClientHeight    =   9885
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   15360
   Icon            =   "Form100.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   659
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   1024
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.TextBox Text1 
      Height          =   615
      Left            =   0
      MultiLine       =   -1  'True
      ScrollBars      =   1  'Horizontal
      TabIndex        =   38
      Text            =   "Form100.frx":0CCA
      Top             =   0
      Visible         =   0   'False
      Width           =   8775
   End
   Begin VB.PictureBox Picture6 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   9615
      Left            =   3600
      Picture         =   "Form100.frx":0CD0
      ScaleHeight     =   9615
      ScaleWidth      =   3615
      TabIndex        =   11
      Top             =   240
      Width           =   3615
      Begin VB.HScrollBar HScroll4 
         Height          =   255
         Index           =   2
         Left            =   0
         Max             =   10
         TabIndex        =   37
         Top             =   8640
         Width           =   2055
      End
      Begin VB.HScrollBar HScroll4 
         Height          =   255
         Index           =   1
         Left            =   0
         Max             =   10
         TabIndex        =   36
         Top             =   8280
         Width           =   2055
      End
      Begin VB.HScrollBar HScroll4 
         Height          =   255
         Index           =   0
         Left            =   0
         Max             =   10
         TabIndex        =   35
         Top             =   7920
         Width           =   2055
      End
      Begin VB.VScrollBar VScroll2 
         Height          =   2055
         Left            =   2160
         Max             =   1000
         TabIndex        =   33
         Top             =   5040
         Value           =   1
         Width           =   255
      End
      Begin VB.CommandButton Command6 
         Caption         =   "Fog Color and Back"
         Height          =   255
         Left            =   2640
         TabIndex        =   32
         Top             =   7080
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.HScrollBar HScroll3 
         Height          =   255
         Index           =   2
         Left            =   0
         Max             =   255
         TabIndex        =   30
         Top             =   6120
         Width           =   2055
      End
      Begin VB.HScrollBar HScroll3 
         Height          =   255
         Index           =   1
         Left            =   0
         Max             =   255
         TabIndex        =   29
         Top             =   6480
         Width           =   2055
      End
      Begin VB.HScrollBar HScroll3 
         Height          =   255
         Index           =   0
         Left            =   0
         Max             =   255
         TabIndex        =   28
         Top             =   6840
         Width           =   2055
      End
      Begin VB.CommandButton Command5 
         Caption         =   "save picture"
         BeginProperty Font 
            Name            =   "Terminal"
            Size            =   9
            Charset         =   255
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   0
         TabIndex        =   27
         Top             =   360
         Width           =   2055
      End
      Begin VB.CommandButton Command7 
         Caption         =   "show screen"
         BeginProperty Font 
            Name            =   "Terminal"
            Size            =   9
            Charset         =   255
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   0
         TabIndex        =   26
         Top             =   0
         Width           =   2055
      End
      Begin VB.HScrollBar HScroll1 
         Height          =   255
         Index           =   0
         Left            =   0
         Max             =   10
         TabIndex        =   21
         Top             =   2760
         Value           =   10
         Width           =   2055
      End
      Begin VB.HScrollBar HScroll1 
         Height          =   255
         Index           =   1
         Left            =   0
         Max             =   10
         TabIndex        =   20
         Top             =   3120
         Value           =   10
         Width           =   2055
      End
      Begin VB.HScrollBar HScroll1 
         Height          =   255
         Index           =   2
         Left            =   0
         Max             =   10
         TabIndex        =   19
         Top             =   3480
         Value           =   10
         Width           =   2055
      End
      Begin VB.CommandButton Command3 
         Caption         =   "save color"
         BeginProperty Font 
            Name            =   "Terminal"
            Size            =   9
            Charset         =   255
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   0
         TabIndex        =   18
         Top             =   1080
         Width           =   2055
      End
      Begin VB.CommandButton Command4 
         Caption         =   "last color"
         BeginProperty Font 
            Name            =   "Terminal"
            Size            =   9
            Charset         =   255
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   0
         TabIndex        =   17
         Top             =   720
         Width           =   2055
      End
      Begin VB.Timer Timer2 
         Enabled         =   0   'False
         Interval        =   20
         Left            =   1920
         Top             =   1680
      End
      Begin VB.HScrollBar HScroll2 
         Height          =   255
         Index           =   0
         Left            =   0
         Max             =   10
         TabIndex        =   16
         Top             =   3840
         Value           =   1
         Width           =   2055
      End
      Begin VB.HScrollBar HScroll2 
         Height          =   255
         Index           =   1
         Left            =   0
         Max             =   10
         TabIndex        =   15
         Top             =   4200
         Value           =   1
         Width           =   2055
      End
      Begin VB.HScrollBar HScroll2 
         Height          =   255
         Index           =   2
         Left            =   0
         Max             =   10
         TabIndex        =   14
         Top             =   4560
         Value           =   1
         Width           =   2055
      End
      Begin VB.VScrollBar VScroll1 
         Height          =   2055
         Left            =   2160
         TabIndex        =   13
         Top             =   2760
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Fog"
         BeginProperty Font 
            Name            =   "Terminal"
            Size            =   9
            Charset         =   255
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   5040
         Width           =   2055
      End
      Begin VB.Label Label9 
         BackStyle       =   0  'Transparent
         Caption         =   "background:"
         BeginProperty Font 
            Name            =   "Terminal"
            Size            =   9
            Charset         =   255
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   615
         Left            =   0
         TabIndex        =   34
         Top             =   7200
         Width           =   3615
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Label8"
         BeginProperty Font 
            Name            =   "Terminal"
            Size            =   9
            Charset         =   255
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   780
         Left            =   0
         TabIndex        =   31
         Top             =   5280
         Width           =   3600
         WordWrap        =   -1  'True
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "RGB LIGHT"
         BeginProperty Font 
            Name            =   "Terminal"
            Size            =   9
            Charset         =   255
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   180
         Left            =   0
         TabIndex        =   25
         Top             =   1440
         Width           =   1455
         WordWrap        =   -1  'True
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "Terminal"
            Size            =   9
            Charset         =   255
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   180
         Left            =   0
         TabIndex        =   24
         Top             =   2280
         Width           =   4575
         WordWrap        =   -1  'True
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Label3"
         BeginProperty Font 
            Name            =   "Terminal"
            Size            =   9
            Charset         =   255
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   0
         TabIndex        =   23
         Top             =   1920
         Width           =   1695
         WordWrap        =   -1  'True
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Label4"
         BeginProperty Font 
            Name            =   "Terminal"
            Size            =   9
            Charset         =   255
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   180
         Left            =   2160
         TabIndex        =   22
         Top             =   0
         Visible         =   0   'False
         Width           =   720
      End
   End
   Begin VB.PictureBox Picture3 
      Appearance      =   0  'Flat
      BackColor       =   &H80000001&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   1215
      Left            =   8280
      ScaleHeight     =   81
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   449
      TabIndex        =   6
      Top             =   5520
      Visible         =   0   'False
      Width           =   6735
      Begin VB.PictureBox Picture5 
         Appearance      =   0  'Flat
         BackColor       =   &H00800000&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         ForeColor       =   &H80000008&
         Height          =   3015
         Left            =   0
         Picture         =   "Form100.frx":E617
         ScaleHeight     =   3015
         ScaleWidth      =   4815
         TabIndex        =   7
         Top             =   0
         Width           =   4815
         Begin VB.Label Label7 
            BackStyle       =   0  'Transparent
            Caption         =   "~hide, esc console, mousedown move this panel"
            BeginProperty Font 
               Name            =   "Terminal"
               Size            =   9
               Charset         =   255
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   615
            Left            =   0
            TabIndex        =   10
            Top             =   480
            Width           =   6495
         End
         Begin VB.Label Label5 
            BackStyle       =   0  'Transparent
            Caption         =   "Label5"
            BeginProperty Font 
               Name            =   "Terminal"
               Size            =   9
               Charset         =   255
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   0
            TabIndex        =   9
            Top             =   0
            Width           =   4455
         End
         Begin VB.Label Label6 
            BackStyle       =   0  'Transparent
            Caption         =   "Label6"
            BeginProperty Font 
               Name            =   "Terminal"
               Size            =   9
               Charset         =   255
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   735
            Left            =   0
            TabIndex        =   8
            Top             =   240
            Width           =   4455
         End
      End
   End
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      ForeColor       =   &H80000008&
      Height          =   6495
      Left            =   -1680
      ScaleHeight     =   433
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   561
      TabIndex        =   4
      Top             =   1200
      Width           =   8415
      Begin RMControl7.RMCanvas RMCanvas1 
         Height          =   6135
         Left            =   0
         TabIndex        =   5
         Top             =   480
         Width           =   10215
         _ExtentX        =   18018
         _ExtentY        =   10821
      End
   End
   Begin VB.PictureBox Picture4 
      Appearance      =   0  'Flat
      BackColor       =   &H80000001&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   6495
      Left            =   -2040
      ScaleHeight     =   433
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   441
      TabIndex        =   3
      Top             =   3000
      Width           =   6615
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   2
      Left            =   0
      Top             =   0
   End
   Begin VB.CommandButton Command2 
      Caption         =   "exit"
      BeginProperty Font 
         Name            =   "Terminal"
         Size            =   9
         Charset         =   255
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   480
      Width           =   1695
   End
   Begin MSComDlg.CommonDialog CDialog1 
      Left            =   14520
      Top             =   8160
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.PictureBox Picture1 
      AutoRedraw      =   -1  'True
      Height          =   615
      Left            =   12600
      ScaleHeight     =   37
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   157
      TabIndex        =   1
      Top             =   7440
      Width           =   2415
      Begin VB.Image Image1 
         Height          =   2895
         Left            =   0
         Top             =   0
         Width           =   1455
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "open picture"
      BeginProperty Font 
         Name            =   "Terminal"
         Size            =   9
         Charset         =   255
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      MaskColor       =   &H00808080&
      TabIndex        =   0
      Top             =   120
      Width           =   1695
   End
   Begin VB.Image Image2 
      Enabled         =   0   'False
      Height          =   3975
      Left            =   0
      Top             =   240
      Width           =   3375
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'variables declaration'variables declaration'variables declaration
'variables declaration'variables declaration'variables declaration
'variables declaration'variables declaration'variables declaration
Option Explicit

Dim picture3VECmice(1) As D3DVECTOR
Dim fogVALUE As D3DVECTOR
Dim fogVALUE2 As Integer
Dim KeyTIMERkeep As Integer
Dim screen1 As Boolean
Dim freFILE2 As Integer
Dim freFILE As Integer
Dim freFILE3 As Integer
Dim VAR1 As Integer
Dim VAR2 As Integer
Dim VAR1a As Integer
Dim VAR2a As Integer
Dim VAR1b As Integer
Dim VAR2b As Integer
Public MyR As Long
Public MyG As Long
Public MyB As Long
Dim ShiftMouse As Integer
'FONTS
Private Declare Function AddFontResourceEx Lib "gdi32.dll" _
Alias "AddFontResourceExA" (ByVal lpcstr As String, _
ByVal dword As Long, ByRef DESIGNVECTOR) As Long
Private Const FR_PRIVATE As Long = &H10
Dim Mesh_OBJCT(15) As Direct3DRMMeshBuilder3
Dim Frame_OBJCT(15) As Direct3DRMFrame3
Dim TEXTURE_forObject(15) As Direct3DRMTexture3
Dim COLORS_forObject(15) As D3DVECTOR
Dim VectorsOBJ(2, 15) As D3DVECTOR
Dim TEXTURE_bck As Direct3DRMTexture3
Dim COLOR_bck As D3DVECTOR
Dim CANVAlight As D3DVECTOR
Dim VectorsCAM(2) As D3DVECTOR
Dim PlayPressed As Boolean
Dim filenamepicture As String
Dim SCROLLVALUEx As Double
Dim SCROLLVALUEy As Double
Dim SCROLLVALUEz As Double
Dim SCROLL2VALUEx As Double
Dim SCROLL2VALUEy As Double
Dim SCROLL2VALUEz As Double

Dim KeypressedASC As Integer
Dim RotationEnabled As Integer
Dim MouseVEC As D3DVECTOR


Private Declare Sub keybd_event _
    Lib "user32" ( _
        ByVal bVk As Byte, _
        ByVal bScan As Byte, _
        ByVal dwFlags As Long, _
        ByVal dwExtraInfo As Long)
'variables declaration'variables declaration'variables declaration
'variables declaration'variables declaration'variables declaration
'variables declaration'variables declaration'variables declaration


Private Sub Check1_Click()


If Check1.Value = 1 Then






fogVALUE.X = HScroll3(0).Value
fogVALUE.Y = HScroll3(1).Value
fogVALUE.z = HScroll3(2).Value
RMCanvas1.SceneFrame.SetSceneFogColor RGB(fogVALUE.X, fogVALUE.Y, fogVALUE.z)
RMCanvas1.SceneFrame.SetSceneFogParams 100, 200, 10
RMCanvas1.SceneFrame.SetSceneFogMode D3DRMFOG_EXPONENTIAL
RMCanvas1.SceneFrame.SetSceneFogEnable D_TRUE


Label8.Caption = "fog:" + Chr$(13) + "R:" + Str(fogVALUE.z) + "G:" + Str(fogVALUE.Y) + "B:" + Str(fogVALUE.X) + " On"
End If

If Check1.Value = 0 Then
fogVALUE.X = HScroll3(0).Value
fogVALUE.Y = HScroll3(1).Value
fogVALUE.z = HScroll3(2).Value
RMCanvas1.SceneFrame.SetSceneFogColor RGB(fogVALUE.X, fogVALUE.Y, fogVALUE.z)
RMCanvas1.SceneFrame.SetSceneFogEnable D_FALSE
Label8.Caption = "fog:" + Chr$(13) + "R:" + Str(fogVALUE.z) + "G:" + Str(fogVALUE.Y) + "B:" + Str(fogVALUE.X) + " Off"


End If

Label8.Caption = Label8.Caption + Chr$(13) + "Q:" + Str(fogVALUE2)

End Sub

Private Sub Command1_Click()
On Error Resume Next
'open picture'open picture'open picture'open picture'open picture
'open picture'open picture'open picture'open picture'open picture
'open picture'open picture'open picture'open picture'open picture
  CDialog1.Filter = "BMP (*.bmp)|*.bmp|JPG (*.jpg)|*.jpg|GIF (*.gif)|*.gif|"
  CDialog1.ShowOpen
filenamepicture = CDialog1.FileTitle
If filenamepicture <> "" Then
Image1.Picture = LoadPicture(filenamepicture)
Picture1.PaintPicture Image1.Picture, 0, 0, 1024, 768, 0, 0, Image1.Width, Image1.Height
SavePicture Picture1.Image, App.Path + "\ProgramData\Object\TEMP.bmp"
Set TEXTURE_forObject(0) = RMCanvas1.D3DRM.LoadTexture(App.Path + "\ProgramData\Object\TEMP.bmp")
Mesh_OBJCT(0).SetTexture TEXTURE_forObject(0)
filenamepicture = ""
RMCanvas1.Update
End If
'open picture'open picture'open picture'open picture'open picture
'open picture'open picture'open picture'open picture'open picture
'open picture'open picture'open picture'open picture'open picture
'MsgBox "LMB\RMB\Shift rotates image", vbInformation, "tip"
End Sub

Private Sub Command2_Click()
'ending program'ending program'ending program'ending program
'ending program'ending program'ending program'ending program
'ending program'ending program'ending program'ending program
On Error Resume Next
Kill App.Path + "\ProgramData\Object\TEMP.bmp"
End
'ending program'ending program'ending program'ending program
'ending program'ending program'ending program'ending program
'ending program'ending program'ending program'ending program
End Sub

Private Sub Command3_Click()
'save light'save light'save light'save light'save light'save light
'save light'save light'save light'save light'save light'save light
'save light'save light'save light'save light'save light'save light
freFILE2 = FreeFile
Open App.Path + "\ProgramData\Object\programdata.dat" For Random As freFILE2
SCROLLVALUEx = 0.1 * HScroll1(0).Value
SCROLLVALUEy = 0.1 * HScroll1(1).Value
SCROLLVALUEz = 0.1 * HScroll1(2).Value
CANVAlight.X = SCROLLVALUEx: CANVAlight.Y = SCROLLVALUEy: CANVAlight.z = SCROLLVALUEz
RMCanvas1.AmbientLight.SetColorRGB CANVAlight.X, CANVAlight.Y, CANVAlight.z
RMCanvas1.Update
Put freFILE2, 1, HScroll1(0).Value
Put freFILE2, 2, HScroll1(1).Value
Put freFILE2, 3, HScroll1(2).Value
Put freFILE2, 4, HScroll2(0).Value
Put freFILE2, 5, HScroll2(1).Value
Put freFILE2, 6, HScroll2(2).Value

Put freFILE2, 7, VScroll1.Value
Put freFILE2, 8, VScroll2.Value
Put freFILE2, 9, HScroll3(2).Value
Put freFILE2, 10, HScroll3(1).Value
Put freFILE2, 11, HScroll3(0).Value

Put freFILE2, 12, HScroll4(0).Value
Put freFILE2, 13, HScroll4(1).Value
Put freFILE2, 14, HScroll4(2).Value

Close freFILE2
'save light'save light'save light'save light'save light'save light
'save light'save light'save light'save light'save light'save light
'save light'save light'save light'save light'save light'save light
End Sub




Private Sub LoadFromF()
'load'load'load'load'load'load'load'load'load'load'load'load'load
'load'load'load'load'load'load'load'load'load'load'load'load'load
'load'load'load'load'load'load'load'load'load'load'load'load'load
freFILE2 = FreeFile
Open App.Path + "\ProgramData\Object\programdata.dat" For Random As freFILE2
Get freFILE2, 1, VAR1a
Get freFILE2, 2, VAR2a
Get freFILE2, 3, VAR1b
Close freFILE2
HScroll1(0).Value = VAR1a
HScroll1(1).Value = VAR2a
HScroll1(2).Value = VAR1b
freFILE = FreeFile
Open App.Path + "\ProgramData\Object\programdata.dat" For Random As freFILE2
Get freFILE, 4, VAR1a
Get freFILE, 5, VAR2a
Get freFILE, 6, VAR1b
Close freFILE2
HScroll2(0).Value = VAR1a
HScroll2(1).Value = VAR2a
HScroll2(2).Value = VAR1b







SCROLL2VALUEx = 0.1 * HScroll2(0).Value
SCROLL2VALUEy = 0.1 * HScroll2(1).Value
SCROLL2VALUEz = 0.1 * HScroll2(2).Value
'canvas light seting'canvas light seting'canvas light seting
'canvas light seting'canvas light seting'canvas light seting
'canvas light seting'canvas light seting'canvas light seting
SCROLLVALUEx = 0.1 * HScroll1(0).Value
SCROLLVALUEy = 0.1 * HScroll1(1).Value
SCROLLVALUEz = 0.1 * HScroll1(2).Value
CANVAlight.X = SCROLLVALUEx: CANVAlight.Y = SCROLLVALUEy: CANVAlight.z = SCROLLVALUEz
RMCanvas1.AmbientLight.SetColorRGB CANVAlight.X, CANVAlight.Y, CANVAlight.z

Mesh_OBJCT(0).SetColorRGB SCROLL2VALUEx, SCROLL2VALUEy, SCROLL2VALUEz
Label2.Caption = "RGB PICTURE:" + Chr$(13) + "R:" + Str(SCROLL2VALUEx) + "G:" + Str(SCROLL2VALUEy) + "B:" + Str(SCROLL2VALUEz)


Label3.Caption = "Zoom:" + Str(VScroll1.Value)


freFILE3 = FreeFile
Open App.Path + "\ProgramData\Object\programdata.dat" For Random As freFILE2
'Put freFILE2, 7, VScroll1.Value
Get freFILE3, 8, VAR1
Get freFILE3, 9, VAR2
Get freFILE3, 10, VAR1a
Get freFILE3, 11, VAR2a



VScroll2.Value = VAR1
HScroll3(2).Value = VAR2
HScroll3(1).Value = VAR1a
HScroll3(0).Value = VAR2a


Label8.Caption = "fog:" + Chr$(13) + "R:" + Str(fogVALUE.z) + "G:" + Str(fogVALUE.Y) + "B:" + Str(fogVALUE.X) + " On"
Label8.Caption = Label8.Caption + Chr$(13) + "Q:" + Str(fogVALUE2)

Get freFILE3, 12, VAR1 'bckgrndRGB
Get freFILE3, 13, VAR2
Get freFILE3, 14, VAR1a
Close freFILE3

HScroll4(0).Value = VAR1
HScroll4(1).Value = VAR2
HScroll4(2).Value = VAR1a

COLOR_bck.X = HScroll4(0).Value * 0.1: COLOR_bck.Y = HScroll4(1).Value * 0.1: COLOR_bck.z = HScroll4(2).Value * 0.1
RMCanvas1.SceneFrame.SetSceneBackgroundRGB COLOR_bck.X, COLOR_bck.Y, COLOR_bck.z

Label9.Caption = "background:" + Chr$(13)
Label9.Caption = Label9.Caption + "R:" + Str(COLOR_bck.X)
Label9.Caption = Label9.Caption + "G:" + Str(COLOR_bck.Y)
Label9.Caption = Label9.Caption + "B:" + Str(COLOR_bck.z)



RMCanvas1.Update
'canvas light seting'canvas light seting'canvas light seting
'canvas light seting'canvas light seting'canvas light seting
'canvas light seting'canvas light seting'canvas light seting
'load'load'load'load'load'load'load'load'load'load'load'load'load
'load'load'load'load'load'load'load'load'load'load'load'load'load
'load'load'load'load'load'load'load'load'load'load'load'load'load
End Sub



Private Sub Command4_Click()
'load data from file'load data from file'load data from file
'load data from file'load data from file'load data from file
'load data from file'load data from file'load data from file
LoadFromF
'load data from file'load data from file'load data from file
'load data from file'load data from file'load data from file
'load data from file'load data from file'load data from file
End Sub

Private Sub Command5_Click()
'save picture
On Error Resume Next

 CDialog1.Filter = "BMP (*.bmp)|*.bmp|"
  CDialog1.ShowSave
filenamepicture = CDialog1.FileTitle
SavingPicture



      
      
      
      
End Sub

Private Sub Command6_Click()
'nColor = The color returned by the Color Dialog Box
'nColor = The color returned by the Color Dialog Box
  CDialog1.DialogTitle = "Fog Color"
  CDialog1.ShowColor
  GetRGB CDialog1.Color
  RMCanvas1.SceneFrame.SetSceneFogColor RGB(MyB, MyG, MyR)
  RMCanvas1.SceneFrame.SetSceneBackgroundRGB MyR, MyG, MyB
End Sub
Public Sub GetRGB(ByVal nColor As Long)
    'nColor = The color returned by the Color Dialog Box
    MyR = nColor And &HFF&
    MyG = (nColor And &HFF00&) \ &H100&
    MyB = (nColor And &HFF0000) \ &H10000
    'nColor = The color returned by the Color Dialog Box
    'nColor = The color returned by the Color Dialog Box
End Sub

Private Sub Command7_Click()
screen1 = True
Picture4.Left = 0: Picture4.Top = 0: Picture4.Width = Form1.ScaleWidth: Picture4.Height = Form1.ScaleHeight
Picture2.Left = 0: Picture2.Top = 0: Picture2.Width = Form1.ScaleWidth: Picture2.Height = Form1.ScaleHeight
RMCanvas1.Left = 0: RMCanvas1.Width = Picture2.Width
RMCanvas1.Top = 0: RMCanvas1.Height = Picture2.Height
'Picture3.Visible = True
Picture6.Visible = False
'picture




End Sub

Private Sub Form_Load()
'form load'form load'form load'form load'form load'form load
'form load'form load'form load'form load'form load'form load
'form load'form load'form load'form load'form load'form load














SavePicture Picture1.Image, App.Path + "\ProgramData\Object\TEMP.bmp"
Image2.Picture = LoadPicture(App.Path + "/programdata/texture/Larger.jpg")

Image1.Width = 1024: Image1.Height = 768


Shell "regsvr32 " + App.Path + "\ProgramData\Object\RMControl.ocx" + " /s"
With RMCanvas1
    .StartWindowed
    .Viewport.SetBack 5000
End With
RMCanvas1.CameraFrame.SetPosition Nothing, 0, 0, 0
Picture1.Top = -1999: Picture1.Width = 1024: Picture1.Height = 768
COLOR_bck.X = 0: COLOR_bck.Y = 0: COLOR_bck.z = 0.5
RMCanvas1.SceneFrame.SetSceneBackgroundRGB COLOR_bck.X, COLOR_bck.Y, CANVAlight.z
Set Mesh_OBJCT(0) = RMCanvas1.D3DRM.CreateMeshBuilder()
Mesh_OBJCT(0).LoadFromFile App.Path + "/programdata/object/background.x", 0, 0, Nothing, Nothing
Set Frame_OBJCT(0) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
Mesh_OBJCT(0).ScaleMesh 30, 30, 0.011
Frame_OBJCT(0).SetPosition Nothing, 0, 0, 111
Frame_OBJCT(0).AddVisual Mesh_OBJCT(0)









RMCanvas1.Update









Dim sArgs As String
    sArgs = Command$
filenamepicture = sArgs
'Text1.Text = filenamepicture
If filenamepicture <> "" And filenamepicture <> "*.*" Then
On Error Resume Next
'If Mid$(filenamepicture,' Len(filenamepicture) - 3, 3) = "jpg" Or Mid$(filenamepicture, Len(filenamepicture) - 3, 3) = "bmp" Or Mid$(filenamepicture, Len(filenamepicture) - 3, 3) = "gif" Then
Text1.Text = Mid$(filenamepicture, 2, Len(filenamepicture) - 2)
Image1.Picture = LoadPicture(Text1.Text)
Picture1.PaintPicture Image1.Picture, 0, 0, 1024, 768, 0, 0, Image1.Width, Image1.Height
SavePicture Picture1.Image, App.Path + "\ProgramData\Object\TEMP.bmp"
Set TEXTURE_forObject(0) = RMCanvas1.D3DRM.LoadTexture(App.Path + "\ProgramData\Object\TEMP.bmp")
Mesh_OBJCT(0).SetTexture TEXTURE_forObject(0)
RMCanvas1.Update
filenamepicture = ""





End If

'End If




'form load'form load'form load'form load'form load'form load
'form load'form load'form load'form load'form load'form load
'form load'form load'form load'form load'form load'form load
End Sub




Private Sub Form_Resize()
'form resize'form resize'form resize'form resize'form resize
'form resize'form resize'form resize'form resize'form resize
'form resize'form resize'form resize'form resize'form resize
Image2.Left = 0: Image2.Top = 0: Image2.Height = Form1.ScaleHeight: Image2.Width = Form1.ScaleWidth







Picture2.Left = 5: Picture2.Width = Form1.ScaleWidth - 200
Picture2.Top = 55: Picture2.Height = Form1.ScaleHeight - 60

Picture4.Left = 5: Picture4.Width = Form1.ScaleWidth - 200
Picture4.Top = 55: Picture4.Height = Form1.ScaleHeight - 60



RMCanvas1.Left = 0: RMCanvas1.Width = Picture2.Width
RMCanvas1.Top = 0: RMCanvas1.Height = Picture2.Height
LoadFromF
Label1.Caption = "RGB LIGHT:" + Chr$(13) + "R:" + Str(SCROLLVALUEx) + "G:" + Str(SCROLLVALUEy) + "B:" + Str(SCROLLVALUEz)
SetingFonts
'Check1.Top = Command3.Top + Command3.Height + 5: Check1.Left = Command4.Left + 5 + Command4.Width
'Command6.Top = Command3.Top + Command3.Height + 5: Command6.Left = 5 + Check1.Left + Check1.Width

Label5.Caption = "key " + Str(KeyTIMERkeep) + Str(KeypressedASC)
Picture6.Left = Picture2.Left + 5 + Picture2.Width: Picture6.Top = 0
Label8.Caption = "fog:" + Chr$(13) + "R:" + Str(fogVALUE.z) + "G:" + Str(fogVALUE.Y) + "B:" + Str(fogVALUE.X) + " Off"
Label8.Caption = Label8.Caption + Chr$(13) + "Q:" + Str(fogVALUE2)
Timer1.Enabled = True
'form resize'form resize'form resize'form resize'form resize
'form resize'form resize'form resize'form resize'form resize
'form resize'form resize'form resize'form resize'form resize
End Sub


Private Sub HScroll1_Change(Index As Integer)
'ambient light scroll'ambient light scroll'ambient light scroll
'ambient light scroll'ambient light scroll'ambient light scroll
'ambient light scroll'ambient light scroll'ambient light scroll
SCROLLVALUEx = 0.1 * HScroll1(0).Value
SCROLLVALUEy = 0.1 * HScroll1(1).Value
SCROLLVALUEz = 0.1 * HScroll1(2).Value
CANVAlight.X = SCROLLVALUEx: CANVAlight.Y = SCROLLVALUEy: CANVAlight.z = SCROLLVALUEz
RMCanvas1.AmbientLight.SetColorRGB CANVAlight.X, CANVAlight.Y, CANVAlight.z
RMCanvas1.Update
Label1.Caption = "RGB LIGHT:" + Chr$(13) + "R:" + Str(SCROLLVALUEx) + "G:" + Str(SCROLLVALUEy) + "B:" + Str(SCROLLVALUEz) 'ambient light scroll'ambient light scroll'ambient light scroll
'ambient light scroll'ambient light scroll'ambient light scroll
'ambient light scroll'ambient light scroll'ambient light scroll
End Sub

Private Sub HScroll1_Scroll(Index As Integer)
'ambient light scroll'ambient light scroll'ambient light scroll
'ambient light scroll'ambient light scroll'ambient light scroll
'ambient light scroll'ambient light scroll'ambient light scroll
SCROLLVALUEx = 0.1 * HScroll1(0).Value
SCROLLVALUEy = 0.1 * HScroll1(1).Value
SCROLLVALUEz = 0.1 * HScroll1(2).Value
CANVAlight.X = SCROLLVALUEx: CANVAlight.Y = SCROLLVALUEy: CANVAlight.z = SCROLLVALUEz
RMCanvas1.AmbientLight.SetColorRGB CANVAlight.X, CANVAlight.Y, CANVAlight.z
RMCanvas1.Update
Label1.Caption = "RGB LIGHT:" + Chr$(13) + "R:" + Str(SCROLLVALUEx) + "G:" + Str(SCROLLVALUEy) + "B:" + Str(SCROLLVALUEz) 'ambient light scroll'ambient light scroll'ambient light scroll
'ambient light scroll'ambient light scroll'ambient light scroll
'ambient light scroll'ambient light scroll'ambient light scroll


































End Sub

Private Sub HScroll2_Change(Index As Integer)
SCROLL2VALUEx = 0.1 * HScroll2(0).Value
SCROLL2VALUEy = 0.1 * HScroll2(1).Value
SCROLL2VALUEz = 0.1 * HScroll2(2).Value
Mesh_OBJCT(0).SetColorRGB SCROLL2VALUEx, SCROLL2VALUEy, SCROLL2VALUEz
Label2.Caption = "RGB PICTURE:" + Chr$(13) + "R:" + Str(SCROLL2VALUEx) + "G:" + Str(SCROLL2VALUEy) + "B:" + Str(SCROLL2VALUEz)
End Sub

Private Sub HScroll2_Scroll(Index As Integer)
SCROLL2VALUEx = 0.1 * HScroll2(0).Value
SCROLL2VALUEy = 0.1 * HScroll2(1).Value
SCROLL2VALUEz = 0.1 * HScroll2(2).Value
Mesh_OBJCT(0).SetColorRGB SCROLL2VALUEx, SCROLL2VALUEy, SCROLL2VALUEz
Label2.Caption = "RGB PICTURE:" + Chr$(13) + "R:" + Str(SCROLL2VALUEx) + "G:" + Str(SCROLL2VALUEy) + "B:" + Str(SCROLL2VALUEz)
End Sub

Private Sub HScroll3_Change(Index As Integer)



fogVALUE2 = VScroll2.Value \ 2
If Check1.Value = 1 Then






fogVALUE.X = HScroll3(0).Value
fogVALUE.Y = HScroll3(1).Value
fogVALUE.z = HScroll3(2).Value
RMCanvas1.SceneFrame.SetSceneFogColor RGB(fogVALUE.X, fogVALUE.Y, fogVALUE.z)
RMCanvas1.SceneFrame.SetSceneFogParams 100 - fogVALUE2, 200 + fogVALUE2, 10
RMCanvas1.SceneFrame.SetSceneFogMode D3DRMFOG_EXPONENTIAL
RMCanvas1.SceneFrame.SetSceneFogEnable D_TRUE


Label8.Caption = "fog:" + Chr$(13) + "R:" + Str(fogVALUE.z) + "G:" + Str(fogVALUE.Y) + "B:" + Str(fogVALUE.X) + " On"
Label8.Caption = Label8.Caption + Chr$(13) + "Q:" + Str(fogVALUE2)
End If

If Check1.Value = 0 Then
fogVALUE.X = HScroll3(0).Value
fogVALUE.Y = HScroll3(1).Value
fogVALUE.z = HScroll3(2).Value
RMCanvas1.SceneFrame.SetSceneFogColor RGB(fogVALUE.X, fogVALUE.Y, fogVALUE.z)
RMCanvas1.SceneFrame.SetSceneFogEnable D_FALSE
Label8.Caption = "fog:" + Chr$(13) + "R:" + Str(fogVALUE.z) + "G:" + Str(fogVALUE.Y) + "B:" + Str(fogVALUE.X) + " Off"
Label8.Caption = Label8.Caption + Chr$(13) + "Q:" + Str(fogVALUE2)

End If



End Sub

Private Sub HScroll3_Scroll(Index As Integer)


fogVALUE2 = VScroll2.Value \ 2
If Check1.Value = 1 Then






fogVALUE.X = HScroll3(0).Value
fogVALUE.Y = HScroll3(1).Value
fogVALUE.z = HScroll3(2).Value
RMCanvas1.SceneFrame.SetSceneFogColor RGB(fogVALUE.X, fogVALUE.Y, fogVALUE.z)
RMCanvas1.SceneFrame.SetSceneFogParams 100 - fogVALUE2, 200 + fogVALUE2, 10
RMCanvas1.SceneFrame.SetSceneFogMode D3DRMFOG_EXPONENTIAL
RMCanvas1.SceneFrame.SetSceneFogEnable D_TRUE


Label8.Caption = "fog:" + Chr$(13) + "R:" + Str(fogVALUE.z) + "G:" + Str(fogVALUE.Y) + "B:" + Str(fogVALUE.X) + " On"
Label8.Caption = Label8.Caption + Chr$(13) + "Q:" + Str(fogVALUE2)
End If

If Check1.Value = 0 Then
fogVALUE.X = HScroll3(0).Value
fogVALUE.Y = HScroll3(1).Value
fogVALUE.z = HScroll3(2).Value
RMCanvas1.SceneFrame.SetSceneFogColor RGB(fogVALUE.X, fogVALUE.Y, fogVALUE.z)
RMCanvas1.SceneFrame.SetSceneFogEnable D_FALSE
Label8.Caption = "fog:" + Chr$(13) + "R:" + Str(fogVALUE.z) + "G:" + Str(fogVALUE.Y) + "B:" + Str(fogVALUE.X) + " Off"
Label8.Caption = Label8.Caption + Chr$(13) + "Q:" + Str(fogVALUE2)

End If
End Sub

Private Sub HScroll4_Change(Index As Integer)




COLOR_bck.X = HScroll4(0).Value * 0.1: COLOR_bck.Y = HScroll4(1).Value * 0.1: COLOR_bck.z = HScroll4(2).Value * 0.1
RMCanvas1.SceneFrame.SetSceneBackgroundRGB COLOR_bck.X, COLOR_bck.Y, COLOR_bck.z

Label9.Caption = "background:" + Chr$(13)
Label9.Caption = Label9.Caption + "R:" + Str(COLOR_bck.X)
Label9.Caption = Label9.Caption + "G:" + Str(COLOR_bck.Y)
Label9.Caption = Label9.Caption + "B:" + Str(COLOR_bck.z)
End Sub

Private Sub HScroll4_Scroll(Index As Integer)





COLOR_bck.X = HScroll4(0).Value * 0.1: COLOR_bck.Y = HScroll4(1).Value * 0.1: COLOR_bck.z = HScroll4(2).Value * 0.1
RMCanvas1.SceneFrame.SetSceneBackgroundRGB COLOR_bck.X, COLOR_bck.Y, COLOR_bck.z
Label9.Caption = "background:" + Chr$(13)
Label9.Caption = Label9.Caption + "R:" + Str(COLOR_bck.X)
Label9.Caption = Label9.Caption + "G:" + Str(COLOR_bck.Y)
Label9.Caption = Label9.Caption + "B:" + Str(COLOR_bck.z)


End Sub

Private Sub picture3_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)

picture3VECmice(1).z = 1

picture3VECmice(0).X = X
picture3VECmice(0).Y = Y

Label6.Caption = Str(X) + Str(Y) + Str(Button) + Str(Shift)

End Sub

Private Sub picture3_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
Label6.Caption = Str(X) + Str(Y) + Str(Button) + Str(Shift)
If picture3VECmice(1).z <> 0 Then
picture3VECmice(1).X = X
picture3VECmice(1).Y = Y


If picture3VECmice(1).X > picture3VECmice(0).X Then Picture3.Left = Picture3.Left + 1
If picture3VECmice(1).X < picture3VECmice(0).X Then Picture3.Left = Picture3.Left - 1
If picture3VECmice(1).Y > picture3VECmice(0).Y Then Picture3.Top = Picture3.Top + 1
If picture3VECmice(1).Y < picture3VECmice(0).Y Then Picture3.Top = Picture3.Top - 1



End If



End Sub

Private Sub picture3_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
picture3VECmice(1).z = 0
Label6.Caption = Str(X) + Str(Y) + Str(Button) + Str(Shift)
End Sub

Private Sub Picture1_KeyPress(KeyAscii As Integer)




KeyTIMERkeep = 0


KeypressedASC = KeyAscii
End Sub

Private Sub Picture4_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
RotationEnabled = 1
MouseVEC.X = X: MouseVEC.Y = Y: MouseVEC.z = Button: ShiftMouse = Shift
Label6.Caption = Str(X) + Str(Y) + Str(Button) + Str(Shift)
End Sub

Private Sub Picture4_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
Label4.Caption = "x/y:" + Str(X) + "/" + Str(Y)
MouseVEC.X = X: MouseVEC.Y = Y
Label6.Caption = Str(X) + Str(Y) + Str(Button) + Str(Shift)
End Sub

Private Sub Picture4_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Frame_OBJCT(0).SetRotation Frame_OBJCT(0), 1, 1, 1, 0
RotationEnabled = 0
Label6.Caption = Str(X) + Str(Y) + Str(Button) + Str(Shift)
End Sub

Private Sub Timer1_Timer()
'canvas update\screen refresh'canvas update\screen refresh'canvas update\screen refresh
'canvas update\screen refresh'canvas update\screen refresh'canvas update\screen refresh
'canvas update\screen refresh'canvas update\screen refresh'canvas update\screen refresh



















If screen1 = True Then
Picture1.SetFocus
keyHANDLE
End If



WhatRotation
RMCanvas1.Update
'canvas update\screen refresh'canvas update\screen refresh'canvas update\screen refresh
'canvas update\screen refresh'canvas update\screen refresh'canvas update\screen refresh
'canvas update\screen refresh'canvas update\screen refresh'canvas update\screen refresh
End Sub


Private Sub SetingFonts()
'seting fonts'seting fonts'seting fonts'seting fonts'seting fonts
'seting fonts'seting fonts'seting fonts'seting fonts'seting fonts
'seting fonts'seting fonts'seting fonts'seting fonts'seting fonts
AddFontResourceEx App.Path + Trim("/ProgramData/Fonts/Celtic.ttf"), FR_PRIVATE, 0&
'Label1.Font = "Celtic"
'Label1.Font.Size = 12
Label1.Font.Italic = False
Label1.Font.Bold = False
Label1.Font.Underline = False
Label1.Font.Strikethrough = False
Label1.ForeColor = vbWhite
'Label2.Font = "Celtic"
'Label2.Font.Size = 12
Label2.Font.Italic = False
Label2.Font.Bold = False
Label2.Font.Underline = False
Label2.Font.Strikethrough = False
Label2.ForeColor = vbWhite
'Label3.Font = "Celtic"
'Label3.Font.Size = 12
Label3.Font.Italic = False
Label3.Font.Bold = False
Label3.Font.Underline = False
Label3.Font.Strikethrough = False
Label3.ForeColor = vbWhite
'Label4.Font = "Celtic"
'Label4.Font.Size = 12
Label4.Font.Italic = False
Label4.Font.Bold = False
Label4.Font.Underline = False
Label4.Font.Strikethrough = False
Label4.ForeColor = vbWhite
'
'Command1.Font = "GAEL"
'Command1.Font.Size = 6
Command1.Font.Italic = False
Command1.Font.Bold = False
Command1.Font.Underline = False
Command1.Font.Strikethrough = False
'Command2.Font = "GAEL"
'Command2.Font.Size = 6
Command2.Font.Italic = False
Command2.Font.Bold = False
Command2.Font.Underline = False
Command2.Font.Strikethrough = False
'Command3.Font = "GAEL"
'Command3.Font.Size = 6
Command3.Font.Italic = False
Command3.Font.Bold = False
Command3.Font.Underline = False
Command3.Font.Strikethrough = False
'Command4.Font = "GAEL"
'Command4.Font.Size = 6
Command4.Font.Italic = False
Command4.Font.Bold = False
Command4.Font.Underline = False
Command4.Font.Strikethrough = False
'Command5.Font = "GAEL"
'Command5.Font.Size = 6
Command5.Font.Italic = False
Command5.Font.Bold = False
Command5.Font.Underline = False
Command5.Font.Strikethrough = False
'seting fonts'seting fonts'seting fonts'seting fonts'seting fonts
'seting fonts'seting fonts'seting fonts'seting fonts'seting fonts
'seting fonts'seting fonts'seting fonts'seting fonts'seting fonts
End Sub
Private Sub SavingPicture()
'SavingPicture'SavingPicture'SavingPicture'SavingPicture'SavingPicture
'SavingPicture'SavingPicture'SavingPicture'SavingPicture'SavingPicture
'SavingPicture'SavingPicture'SavingPicture'SavingPicture'SavingPicture
VAR2b = 0
Timer2.Enabled = True
End Sub
Private Sub Timer2_Timer()
VAR2b = VAR2b + 1
If VAR2b = 2 Then
Picture2.Left = 0: Picture2.Top = 0: Picture2.Width = Form1.ScaleWidth: Picture2.Height = Form1.ScaleHeight
RMCanvas1.Left = 0: RMCanvas1.Width = Picture2.Width
RMCanvas1.Top = 0: RMCanvas1.Height = Picture2.Height
Picture6.Visible = False
End If
If VAR2b = 10 Then
Clipboard.Clear
    Call keybd_event(44, 2, 0, 0)
    DoEvents
    If Clipboard.GetFormat(vbCFBitmap) Then
        SavePicture Clipboard.GetData(vbCFBitmap), filenamepicture
      End If
      End If
If VAR2b = 18 Then
Picture2.Left = 5: Picture2.Width = Form1.ScaleWidth - 200
Picture2.Top = 55: Picture2.Height = Form1.ScaleHeight - 60
RMCanvas1.Left = 0: RMCanvas1.Width = Picture2.Width
RMCanvas1.Top = 0: RMCanvas1.Height = Picture2.Height

Picture6.Left = Picture2.Left + 5 + Picture2.Width: Picture6.Top = 0


Picture6.Visible = True
Timer2.Enabled = False
End If
'SavingPicture'SavingPicture'SavingPicture'SavingPicture'SavingPicture
'SavingPicture'SavingPicture'SavingPicture'SavingPicture'SavingPicture
'SavingPicture'SavingPicture'SavingPicture'SavingPicture'SavingPicture
End Sub



Private Sub VScroll1_Change()
VectorsOBJ(2, 15).z = VScroll1.Value \ 22 + 111
Frame_OBJCT(0).SetPosition Nothing, VectorsOBJ(2, 15).X, VectorsOBJ(2, 15).Y, VectorsOBJ(2, 15).z
Label3.Caption = "Zoom:" + Str(VScroll1.Value)

End Sub

Private Sub VScroll1_Scroll()
VectorsOBJ(2, 15).z = VScroll1.Value \ 22 + 111
Frame_OBJCT(0).SetPosition Nothing, VectorsOBJ(2, 15).X, VectorsOBJ(2, 15).Y, VectorsOBJ(2, 15).z
Label3.Caption = "Zoom:" + Str(VScroll1.Value)
End Sub



Private Sub WhatRotation()



If RotationEnabled = 1 Then




If ShiftMouse = 0 Then
If MouseVEC.z = 1 Then
If MouseVEC.Y < Picture2.Top + Picture2.Height \ 2 Then
Frame_OBJCT(0).SetRotation Nothing, 1, 0, 0, 0.02
End If
If MouseVEC.Y > Picture2.Top + Picture2.Height \ 2 Then
Frame_OBJCT(0).SetRotation Nothing, 1, 0, 0, -0.02
End If
End If
If MouseVEC.z = 2 Then
If MouseVEC.X < Picture2.Left + Picture2.Width \ 2 Then
Frame_OBJCT(0).SetRotation Nothing, 0, 1, 0, 0.02
End If
If MouseVEC.X > Picture2.Left + Picture2.Width \ 2 Then
Frame_OBJCT(0).SetRotation Nothing, 0, 1, 0, -0.02
End If
End If
End If

If ShiftMouse = 1 Then
If MouseVEC.z = 1 Then
If MouseVEC.Y < Picture2.Top + Picture2.Height \ 2 Then
Frame_OBJCT(0).SetRotation Frame_OBJCT(0), 1, 0, 0, 0.02
End If
If MouseVEC.Y > Picture2.Top + Picture2.Height \ 2 Then
Frame_OBJCT(0).SetRotation Frame_OBJCT(0), 1, 0, 0, -0.02
End If
End If
If MouseVEC.z = 2 Then
If MouseVEC.X < Picture2.Left + Picture2.Width \ 2 Then
Frame_OBJCT(0).SetRotation Frame_OBJCT(0), 0, 1, 0, 0.02
End If
If MouseVEC.X > Picture2.Left + Picture2.Width \ 2 Then
Frame_OBJCT(0).SetRotation Frame_OBJCT(0), 0, 1, 0, -0.02
End If
End If
End If
End If
End Sub









Private Sub keyHANDLE()






If KeypressedASC <> 0 Then
KeyTIMERkeep = KeyTIMERkeep + 1
Label5.Caption = "key " + Str(KeyTIMERkeep) + Str(KeypressedASC)
If KeyTIMERkeep > 37 Then
KeypressedASC = 0
Label5.Caption = "key " + Str(KeyTIMERkeep) + Str(KeypressedASC)
End If
End If


If KeypressedASC = 27 Then
KeypressedASC = 0
Picture4.Left = 5: Picture4.Width = Form1.ScaleWidth - 200
Picture4.Top = 55: Picture4.Height = Form1.ScaleHeight - 60
Label5.Caption = "key " + Str(KeyTIMERkeep) + Str(KeypressedASC)
Picture2.Left = 5: Picture2.Width = Form1.ScaleWidth - 200
Picture2.Top = 55: Picture2.Height = Form1.ScaleHeight - 60
RMCanvas1.Left = 0: RMCanvas1.Width = Picture2.Width
RMCanvas1.Top = 0: RMCanvas1.Height = Picture2.Height
Picture3.Visible = False
Picture6.Visible = True
screen1 = False
End If



If KeypressedASC = 96 Then
KeypressedASC = 0
If Picture3.Visible = False Then
Picture3.Visible = True
Else
Picture3.Visible = False
End If



End If











End Sub












Private Sub VScroll2_Change()

fogVALUE2 = VScroll2.Value \ 2
If Check1.Value = 1 Then






fogVALUE.X = HScroll3(0).Value
fogVALUE.Y = HScroll3(1).Value
fogVALUE.z = HScroll3(2).Value
RMCanvas1.SceneFrame.SetSceneFogColor RGB(fogVALUE.X, fogVALUE.Y, fogVALUE.z)
RMCanvas1.SceneFrame.SetSceneFogParams 100 - fogVALUE2, 200 + fogVALUE2, 10
RMCanvas1.SceneFrame.SetSceneFogMode D3DRMFOG_EXPONENTIAL
RMCanvas1.SceneFrame.SetSceneFogEnable D_TRUE


Label8.Caption = "fog:" + Chr$(13) + "R:" + Str(fogVALUE.z) + "G:" + Str(fogVALUE.Y) + "B:" + Str(fogVALUE.X) + " On"
End If

If Check1.Value = 0 Then
fogVALUE.X = HScroll3(0).Value
fogVALUE.Y = HScroll3(1).Value
fogVALUE.z = HScroll3(2).Value
RMCanvas1.SceneFrame.SetSceneFogColor RGB(fogVALUE.X, fogVALUE.Y, fogVALUE.z)
RMCanvas1.SceneFrame.SetSceneFogEnable D_FALSE
Label8.Caption = "fog:" + Chr$(13) + "R:" + Str(fogVALUE.z) + "G:" + Str(fogVALUE.Y) + "B:" + Str(fogVALUE.X) + " Off"


End If

















Label8.Caption = Label8.Caption + Chr$(13) + "Q:" + Str(fogVALUE2)





End Sub

Private Sub VScroll2_Scroll()
fogVALUE2 = VScroll2.Value \ 2
If Check1.Value = 1 Then






fogVALUE.X = HScroll3(0).Value
fogVALUE.Y = HScroll3(1).Value
fogVALUE.z = HScroll3(2).Value
RMCanvas1.SceneFrame.SetSceneFogColor RGB(fogVALUE.X, fogVALUE.Y, fogVALUE.z)
RMCanvas1.SceneFrame.SetSceneFogParams 100 - fogVALUE2, 200 + fogVALUE2, 10
RMCanvas1.SceneFrame.SetSceneFogMode D3DRMFOG_EXPONENTIAL
RMCanvas1.SceneFrame.SetSceneFogEnable D_TRUE


Label8.Caption = "fog:" + Chr$(13) + "R:" + Str(fogVALUE.z) + "G:" + Str(fogVALUE.Y) + "B:" + Str(fogVALUE.X) + " On"

End If

If Check1.Value = 0 Then
fogVALUE.X = HScroll3(0).Value
fogVALUE.Y = HScroll3(1).Value
fogVALUE.z = HScroll3(2).Value
RMCanvas1.SceneFrame.SetSceneFogColor RGB(fogVALUE.X, fogVALUE.Y, fogVALUE.z)
RMCanvas1.SceneFrame.SetSceneFogEnable D_FALSE
Label8.Caption = "fog:" + Chr$(13) + "R:" + Str(fogVALUE.z) + "G:" + Str(fogVALUE.Y) + "B:" + Str(fogVALUE.X) + " Off"


End If
Label8.Caption = Label8.Caption + Chr$(13) + "Q:" + Str(fogVALUE2)
End Sub
