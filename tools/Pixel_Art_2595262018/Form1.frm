VERSION 5.00
Begin VB.Form Form1 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pixel Drawer"
   ClientHeight    =   8820
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   15045
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   8820
   ScaleWidth      =   15045
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command3 
      Caption         =   "New"
      Height          =   375
      Left            =   11400
      TabIndex        =   3
      Top             =   8400
      Width           =   1095
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Save"
      Height          =   375
      Left            =   13800
      TabIndex        =   2
      Top             =   8400
      Width           =   1095
   End
   Begin VB.PictureBox Picture1 
      AutoRedraw      =   -1  'True
      Height          =   8175
      Left            =   120
      MousePointer    =   2  'Cross
      ScaleHeight     =   541
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   981
      TabIndex        =   1
      Top             =   120
      Width           =   14775
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Colour"
      Height          =   375
      Left            =   12600
      TabIndex        =   0
      Top             =   8400
      Width           =   1095
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim pressed As Boolean
Dim a
Dim b
Private Sub Command1_Click()
Colour_Picker.Show
End Sub

Private Sub Command2_Click()
SavePicture Picture1.Image, App.Path + "\drawing.bmp"
End Sub

Private Sub Command3_Click()
Picture1.Cls
End Sub

Private Sub Form_Load()
SelColDr = vbWhite
End Sub

Private Sub Picture1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
        Picture1.DrawWidth = 1
pressed = True
X = Int(X \ 20) * 20
Y = Int(Y \ 20) * 20
    For a = 0 To 20
    For b = 0 To 20
        
        Picture1.PSet (X + a, Y + b), SelColDr
Next
Next


End Sub

Private Sub Picture1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
        Picture1.DrawWidth = 1
If pressed = True Then
X = Int(X \ 20) * 20
Y = Int(Y \ 20) * 20
    For a = 0 To 20
    For b = 0 To 20
        
        Picture1.PSet (X + a, Y + b), SelColDr 'RGB(ColorPickedR, ColorPickedG, ColorPickedB)
Next
Next


End If
End Sub

Private Sub Picture1_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
pressed = False
End Sub
