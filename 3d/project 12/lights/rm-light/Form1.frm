VERSION 5.00
Object = "{08216199-47EA-11D3-9479-00AA006C473C}#2.1#0"; "rmcontrol.ocx"
Begin VB.Form Form1 
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   6195
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   11145
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   413
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   743
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.PictureBox Picture1 
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   4575
      Left            =   840
      ScaleHeight     =   305
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   505
      TabIndex        =   0
      Top             =   240
      Width           =   7575
      Begin RMControl7.RMCanvas RMCanvas1 
         Height          =   5415
         Left            =   360
         TabIndex        =   1
         Top             =   360
         Width           =   9735
         _ExtentX        =   17171
         _ExtentY        =   9551
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   10440
      Top             =   5280
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Declare Function SetCurrentDirectory Lib "kernel32" _
Alias "SetCurrentDirectoryA" (ByVal lpPathName As String) As Long
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Const SW_SHOWNORMAL = 1
Private Declare Function GetSystemDirectory Lib "kernel32" Alias "GetSystemDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Long) As Long
'rmcontrol functions
Dim Mesh_OBJecT(6) As Direct3DRMMeshBuilder3
Dim Frame_OBJecT(6)  As Direct3DRMFrame3
Dim TEXTURE_forObjeCt(6)  As Direct3DRMTexture3
Dim Vectors(6)  As D3DVECTOR
Dim TEXTURE_forBackground As Direct3DRMTexture3

Dim light As Direct3DRMLight
Dim light2 As Direct3DRMLight
Dim HOVERINGX As Double 'WAVES VARIABLES
Dim HOVERINGY As Double 'WAVES VARIABLES
Dim HOVERINGX2 As Double 'WAVES VARIABLES
Dim HOVERINGY2 As Double 'WAVES VARIABLES

Private Sub initObjects()

Set Frame_OBJecT(5) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
Set light = RMCanvas1.D3DRM.CreateLightRGB(D3DRMLIGHT_POINT, 0, 0, 1)
Frame_OBJecT(5).SetPosition Nothing, 0, 0, 23
Frame_OBJecT(5).AddLight light
Set Frame_OBJecT(6) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
Set light2 = RMCanvas1.D3DRM.CreateLightRGB(D3DRMLIGHT_POINT, 0, 1, 0)
Frame_OBJecT(6).SetPosition Nothing, 0, 0, 23
Frame_OBJecT(6).AddLight light2

Set Frame_OBJecT(4) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
Set Mesh_OBJecT(4) = RMCanvas1.D3DRM.CreateMeshBuilder()
Set TEXTURE_forObjeCt(4) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\moontexture.bmp")
Mesh_OBJecT(4).LoadFromFile App.Path & "\sphere.X", 0, 0, Nothing, Nothing
Mesh_OBJecT(4).ScaleMesh 2.7, 2.7, 2.7
Mesh_OBJecT(4).SetTexture TEXTURE_forObjeCt(4)
Frame_OBJecT(4).SetPosition Nothing, 5, 0, 33
'Frame_OBJCT(0).SetOrientation Nothing, 0.5, -0.2, -0.2, 0.28, 0.4, -0.4
Frame_OBJecT(4).SetRotation Nothing, 0, 1, 0, 0.005
Mesh_OBJecT(4).SetColorRGB 0.5, 0.5, 0.5 ' RGB
Frame_OBJecT(4).AddVisual Mesh_OBJecT(4)

Set Frame_OBJecT(3) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
Set Mesh_OBJecT(3) = RMCanvas1.D3DRM.CreateMeshBuilder()
Set TEXTURE_forObjeCt(3) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\earthtexture.bmp")
Mesh_OBJecT(3).LoadFromFile App.Path & "\sphere.X", 0, 0, Nothing, Nothing
Mesh_OBJecT(3).ScaleMesh 3.7, 3.7, 3.7
Mesh_OBJecT(3).SetTexture TEXTURE_forObjeCt(3)
Frame_OBJecT(3).SetPosition Nothing, -1, 0, 25
'Frame_OBJCT(0).SetOrientation Nothing, 0.5, -0.2, -0.2, 0.28, 0.4, -0.4
Frame_OBJecT(3).SetRotation Nothing, 0, 1, 0, 0.005
Mesh_OBJecT(3).SetColorRGB 0.5, 0.5, 0.5 ' RGB
Frame_OBJecT(3).AddVisual Mesh_OBJecT(3)



End Sub
Private Sub Form_Load()
' determine system directory and which system it is 32\64
' copy needed dll's (dirextx7) and ocx
' register files
'Dim sSave As String, Ret As Long
'sSave = Space(255)
'Ret = GetSystemDirectory(sSave, 255)
'sSave = Left$(sSave, Ret - 9)
'Dim OSBits
'OSBits = IIf(Len(Environ$("PROGRAMFILES(X86)")) > 0, 64, 32)
'If OSBits = 64 Then
'CopyFiles App.Path + "\dx7lib", sSave + "\syswow64"
'SetCurrentDirectory sSave + "\syswow64"
'Else
'CopyFiles App.Path + "\dx7lib", sSave + "\System32"
'SetCurrentDirectory sSave + "\System32"
'End If
'ShellExecute 0, "runas", "regsvr32", "RMControl.ocx /s", vbNullString, SW_SHOWNORMAL
'ShellExecute 0, "runas", "regsvr32", "dx7vb.dll /s", vbNullString, SW_SHOWNORMAL
End Sub
Public Function CopyFiles(ByRef strSource As String, ByRef strDestination As String)
'On Error Resume Next
'Dim EndFile As String
'Dim objfso
'Set objfso = CreateObject("Scripting.FileSystemObject")
'Dim strFile As String
'If Right$(strSource, 1) <> "\" Then strSource = strSource & "\"
'If Right$(strDestination, 1) <> "\" Then strDestination = strDestination & "\"
'strFile = Dir(strSource & "*.*")
'Do While Len(strFile)
'With objfso
'If Not .FolderExists(strDestination) Then .CreateFolder (strDestination)
'If Right$(strFile, 2) = "l_" Then
'EndFile = "l"
'Else
'EndFile = "x"
'End If
'.CopyFile strSource & strFile, Left$(strDestination & strFile, Len(strDestination & strFile) - 1) & EndFile
'End With
'strFile = Dir
'Loop
'Set objfso = Nothing
'Exit Function
End Function
Private Sub initRM()
With RMCanvas1
    .StartWindowed
    .SceneFrame.SetSceneBackgroundRGB 0, 0, 0.5   'background color rgb seen when no backgroud texture
    .Viewport.SetBack 5000                        'view depth
    .CameraFrame.SetPosition Nothing, 0, 0, 0     'camera position xyz
End With
RMCanvas1.AmbientLight.SetColorRGB 0.3, 0.3, 0.3
'RMCanvas1.CameraFrame.SetOrientation Nothing, Vectors(0).X, Vectors(0).Y, Vectors(0).z, Vectors(1).X, Vectors(1).Y, Vectors(1).z
Set TEXTURE_forBackground = RMCanvas1.D3DRM.LoadTexture(App.Path + "/background.bmp")
RMCanvas1.SceneFrame.SetSceneBackgroundImage TEXTURE_forBackground
'fog parameters
RMCanvas1.SceneFrame.SetSceneFogColor RGB(143, 43, 21)
RMCanvas1.SceneFrame.SetSceneFogParams 19, 45, 117
RMCanvas1.SceneFrame.SetSceneFogMode D3DRMFOG_EXPONENTIAL
RMCanvas1.SceneFrame.SetSceneFogEnable D_TRUE

End Sub

Private Sub Form_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
End
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

'Frame_OBJecT(5).AddLight light
End Sub

Private Sub Form_Resize()
Picture1.Left = 0: Picture1.Top = 0: Picture1.Width = Form1.ScaleWidth: Picture1.Height = Form1.ScaleHeight

RMCanvas1.Left = 0: RMCanvas1.Top = 0: RMCanvas1.Width = Form1.ScaleWidth: RMCanvas1.Height = Form1.ScaleHeight
' initialise rmcontrol
initRM
' initialise objects
initObjects
End Sub
Private Sub RMCanvas1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
End
End Sub

Private Sub Timer1_Timer()

HOVERINGX = HOVERINGX + 0.05
HOVERINGY = Sin(HOVERINGX) * 22
If HOVERINGX > 360 Then HOVERINGX = 0
HOVERINGX2 = HOVERINGX2 + 0.04
HOVERINGY2 = Sin(HOVERINGX2) * 22
If HOVERINGX2 > 360 Then HOVERINGX2 = 0
Frame_OBJecT(5).SetPosition Nothing, -HOVERINGY, HOVERINGY, 5
Frame_OBJecT(6).SetPosition Nothing, -HOVERINGY2, -HOVERINGY2, 25





'update view
RMCanvas1.Update
End Sub
