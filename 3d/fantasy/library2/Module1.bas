Attribute VB_Name = "Module1"
Option Explicit
'Public Declare Function RegisterDLL Lib "Regist10.dll" Alias "REGISTERDLL" (ByVal DllPath As String, bRegister As Boolean) As Boolean

Global ipbans As String
 
Declare Function SetWindowPos Lib "user32" (ByVal hWnd As Long, _
    ByVal hWndInsertAfter As Long, ByVal x As Long, ByVal y As Long, _
    ByVal cx As Long, ByVal cy As Long, ByVal uFlags As Long) As Long
    
Public Const HWND_TOPMOST = -1
Public Const HWND_NOTOPMOST = -2
 
Public Const SWP_NOSIZE = &H1
Public Const SWP_NOMOVE = &H2
Public Const SWP_NOACTIVATE = &H10
 
Public Sub SetTopmost(Form As Form, ByVal Topmost As Boolean)
    Dim hWndInsertAfter As Long
    
    If Topmost Then
        hWndInsertAfter = HWND_TOPMOST
    Else
        hWndInsertAfter = HWND_NOTOPMOST
    End If
    
    SetWindowPos Form.hWnd, hWndInsertAfter, 0, 0, 0, 0, _
        SWP_NOSIZE Or SWP_NOMOVE Or SWP_NOACTIVATE
End Sub



