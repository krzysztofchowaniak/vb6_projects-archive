VERSION 5.00
Begin VB.Form Form1 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "RESIZE PICTURE"
   ClientHeight    =   8310
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   11655
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   554
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   777
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command3 
      Caption         =   "REFRESH"
      Height          =   615
      Left            =   240
      TabIndex        =   9
      Top             =   7560
      Width           =   1695
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   3240
      ScaleHeight     =   33
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   57
      TabIndex        =   8
      Top             =   7680
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   5000
      Left            =   3600
      Top             =   600
   End
   Begin VB.TextBox Text2 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   5760
      TabIndex        =   5
      Text            =   "800"
      Top             =   7920
      Width           =   2055
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   5760
      TabIndex        =   4
      Text            =   "1800"
      Top             =   7560
      Width           =   2055
   End
   Begin VB.CommandButton Command2 
      Caption         =   "RESIZE"
      Height          =   615
      Left            =   7920
      TabIndex        =   3
      Top             =   7560
      Width           =   3495
   End
   Begin VB.CommandButton Command1 
      Appearance      =   0  'Flat
      Caption         =   "REFRESH"
      Height          =   375
      Left            =   4320
      TabIndex        =   2
      Top             =   240
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.FileListBox File1 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      ForeColor       =   &H00FF0000&
      Height          =   6855
      Index           =   1
      Left            =   5760
      TabIndex        =   1
      Top             =   600
      Width           =   5655
   End
   Begin VB.FileListBox File1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404040&
      ForeColor       =   &H0000FF00&
      Height          =   6855
      Index           =   0
      Left            =   240
      TabIndex        =   0
      Top             =   600
      Width           =   5055
   End
   Begin VB.Label Label3 
      Height          =   375
      Left            =   8160
      TabIndex        =   11
      Top             =   120
      Width           =   3255
   End
   Begin VB.Label Label2 
      Height          =   375
      Left            =   2640
      TabIndex        =   10
      Top             =   120
      Width           =   2655
   End
   Begin VB.Image Image1 
      Height          =   495
      Left            =   2040
      Top             =   7680
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "IMAGES OUT FOLDER (bmp)"
      Height          =   375
      Index           =   1
      Left            =   5760
      TabIndex        =   7
      Top             =   120
      Width           =   2295
   End
   Begin VB.Label Label1 
      Caption         =   "IMAGES IN FOLDER (bmp, jpg, gif)"
      Height          =   375
      Index           =   0
      Left            =   240
      TabIndex        =   6
      Top             =   120
      Width           =   2295
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command2_Click()
Dim Q
Dim NAMEpIC As String
On Error Resume Next
For Q = 0 To File1(0).ListCount

NAMEpIC = File1(0).List(Q)
If NAMEpIC <> "" Then



'Picture1.PaintPicture
Image1.Picture = LoadPicture(File1(0).Path + "\" + File1(0).List(Q))
Debug.Print "PICTURE LOADED: " + File1(0).Path + "\" + File1(0).List(Q)

'SAVING Picture
'SavePicture Image1.Picture, "PICTURE.bmp"
'Picture2.Print "CD:\"
'SavePicture Picture2.Image, (PATHFORFILES & "PICTURE.bmp")
Picture1.PaintPicture Image1.Picture, 0, 0, Picture1.Width, Picture1.Height, 0, 0, Image1.Width, Image1.Height


NAMEpIC = Replace(NAMEpIC, "JPG", "")
NAMEpIC = Replace(NAMEpIC, "BMP", "")
NAMEpIC = Replace(NAMEpIC, "GIF", "")
NAMEpIC = Replace(NAMEpIC, "jpg", "")
NAMEpIC = Replace(NAMEpIC, "bmp", "")
NAMEpIC = Replace(NAMEpIC, "gig", "")
NAMEpIC = NAMEpIC + "BMP"
SavePicture Picture1.Image, (File1(1).Path + "\" + NAMEpIC)




End If
Next

Command3_Click
End Sub

Private Sub Command3_Click()
File1(0).Refresh

File1(1).Refresh


Label2.Caption = Trim(Str(File1(0).ListCount)) + " IMAGE(S)"
Label3.Caption = Trim(Str(File1(1).ListCount)) + " IMAGE(S)"
End Sub

Private Sub Form_Load()
On Error Resume Next
File1(0).Path = App.Path + "\IMAGES IN"
File1(1).Path = App.Path + "\IMAGES OUT"
File1(0).Pattern = "*.BMP;*.JPG;*.GIF" '*.PNG;;*.TGA
File1(1).Pattern = "*.BMP;*.JPG;*.GIF"
Label2.Caption = Trim(Str(File1(0).ListCount)) + " IMAGE(S)"
Label3.Caption = Trim(Str(File1(1).ListCount)) + " IMAGE(S)"
Picture1.Width = Val(Text1.Text)
Picture1.Height = Val(Text2.Text)
End Sub

Private Sub Text1_Change()
On Error Resume Next
Picture1.Width = Val(Text1.Text)
Picture1.Height = Val(Text2.Text)
End Sub

Private Sub Text2_Change()
On Error Resume Next
Picture1.Width = Val(Text1.Text)
Picture1.Height = Val(Text2.Text)
End Sub

Private Sub Timer1_Timer()
File1(0).Refresh

File1(1).Refresh
Exit Sub



Dim LIST10ELEMENTS As Integer
Dim LIST11ELEMENTS As Integer
Dim I

LIST10ELEMENTS = Str((File1(0).ListCount))
LIST11ELEMENTS = Str((File1(1).ListCount))
On Error Resume Next
If LIST10ELEMENTS <> 0 Then
For I = 0 To LIST10ELEMENTS - 1
If File1(0).Selected(I) Then
Debug.Print File1(0).List(I)
Exit Sub
End If
'File1(0).Refresh
Next
Else

End If
File1(0).Refresh


If LIST11ELEMENTS <> 0 Then
File1(1).Refresh
Else
File1(1).Refresh
End If

Debug.Print "REFRESHED"

End Sub
