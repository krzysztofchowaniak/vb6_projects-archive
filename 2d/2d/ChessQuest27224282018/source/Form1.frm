VERSION 5.00
Begin VB.Form Form1 
   AutoRedraw      =   -1  'True
   BackColor       =   &H00808080&
   BorderStyle     =   0  'None
   Caption         =   "chessgame.exe"
   ClientHeight    =   10170
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   17880
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   678
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   1192
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.PictureBox pisoMask 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      HasDC           =   0   'False
      Height          =   10860
      Left            =   -6480
      Picture         =   "Form1.frx":406A
      ScaleHeight     =   720
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   500
      TabIndex        =   6
      Top             =   8520
      Visible         =   0   'False
      Width           =   7560
   End
   Begin VB.PictureBox piso 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      Height          =   10860
      Left            =   -6840
      Picture         =   "Form1.frx":10BB6C
      ScaleHeight     =   720
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   500
      TabIndex        =   5
      Top             =   5880
      Visible         =   0   'False
      Width           =   7560
   End
   Begin VB.PictureBox Picture4 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      ForeColor       =   &H80000008&
      Height          =   2415
      Left            =   11160
      ScaleHeight     =   161
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   425
      TabIndex        =   4
      Top             =   6000
      Width           =   6375
   End
   Begin VB.PictureBox Picture3 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   238
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   9975
      Left            =   12720
      ScaleHeight     =   665
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   1177
      TabIndex        =   3
      Top             =   7560
      Visible         =   0   'False
      Width           =   17655
   End
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      ForeColor       =   &H80000008&
      Height          =   9975
      Left            =   14760
      ScaleHeight     =   665
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   1177
      TabIndex        =   2
      Top             =   5280
      Width           =   17655
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      FillStyle       =   0  'Solid
      ForeColor       =   &H80000008&
      Height          =   2415
      Left            =   3000
      ScaleHeight     =   161
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   201
      TabIndex        =   0
      Top             =   360
      Width           =   3015
   End
   Begin VB.Timer Timer1 
      Interval        =   5
      Left            =   4200
      Top             =   4080
   End
   Begin VB.Image Image3 
      Height          =   1215
      Index           =   12
      Left            =   5520
      Top             =   4920
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Image Image3 
      Height          =   1215
      Index           =   11
      Left            =   4440
      Top             =   5280
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Image Image3 
      Height          =   1215
      Index           =   10
      Left            =   4920
      Top             =   5160
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Image Image3 
      Height          =   1215
      Index           =   9
      Left            =   5760
      Top             =   5040
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Image Image3 
      Height          =   1215
      Index           =   8
      Left            =   5400
      Top             =   5160
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Image Image3 
      Height          =   1215
      Index           =   7
      Left            =   4920
      Top             =   5040
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Image Image3 
      Height          =   1215
      Index           =   6
      Left            =   5160
      Top             =   5160
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Image Image3 
      Height          =   1215
      Index           =   5
      Left            =   4440
      Top             =   5160
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Image Image5 
      Height          =   1095
      Left            =   240
      Stretch         =   -1  'True
      Top             =   3840
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Image Image4 
      Height          =   12000
      Left            =   2400
      Top             =   7200
      Visible         =   0   'False
      Width           =   18000
   End
   Begin VB.Image Image3 
      Height          =   1215
      Index           =   4
      Left            =   3480
      Top             =   6000
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Image Image3 
      Height          =   1215
      Index           =   3
      Left            =   2760
      Top             =   5760
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Image Image3 
      Height          =   1215
      Index           =   2
      Left            =   2040
      Top             =   5520
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Image Image3 
      Height          =   1215
      Index           =   1
      Left            =   1200
      Top             =   5280
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Image Image3 
      Height          =   1215
      Index           =   0
      Left            =   360
      Top             =   5040
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Image Image2 
      Height          =   12000
      Left            =   4200
      Top             =   8400
      Visible         =   0   'False
      Width           =   18000
   End
   Begin VB.Image Image1 
      Height          =   9390
      Left            =   -6480
      Picture         =   "Form1.frx":21366E
      Top             =   8520
      Visible         =   0   'False
      Width           =   9390
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Enabled         =   0   'False
      Height          =   2055
      Left            =   9960
      TabIndex        =   1
      Top             =   4320
      Width           =   2535
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'boardActive

Option Explicit
Dim FieldSymbol As String
Dim SquareLineDrawed As Boolean
Dim WantSelect As Boolean
Dim WhosMove(8) As String
Dim PercentSaved
Dim PercentLoaded
Dim bootGame
Dim freFILE
Dim Colour As String
Dim Music(3) As Boolean
Dim retVal As Long

'Will hold the path of the audio to open.
Dim filename As String

Private Declare Function mciSendString Lib "winmm.dll" Alias "mciSendStringA" (ByVal lpstrCommand As String, ByVal lpstrReturnString As String, ByVal uReturnLength As Long, ByVal hwndCallback As Long) As Long



Dim HostName As String
Dim GuestName As String
Dim MAPAxChk
Dim MAPAyChk
Dim Pola3(8) As String
Dim Pola2(8, 7, 7) As String '!
Dim Boardselected As Integer
Dim menuON As Boolean
Dim RND5b '0-main,1,2,3,4,5,6,7,8,9
Dim ChessBoard(7, 7, 8)
Dim consoleOff As Boolean
Dim IsboardActive As Boolean
Dim hillsPICTURE
Dim boardActive As Boolean
Dim WHICHMAPMASTER
Dim cASTLEmASTER(8) As Integer
'Dim cASTLEmASTER(8) As Integer
Private Type RECT
        Left As Long
        Top As Long
        Right As Long
        Bottom As Long
End Type
 
Private Declare Function DrawText Lib "user32" Alias "DrawTextA" (ByVal hdc As Long, ByVal lpStr As String, ByVal nCount As Long, lpRect As RECT, ByVal wFormat As Long) As Long
Private Const DT_WORDBREAK = &H10
 
 
'usage
Dim rct As RECT



Dim whichCastle
Dim whichWall
Dim whichMap
Dim oldWall
Dim Oldmap
Dim rectangleColor As ColorConstants
Dim DEBUGSHOW As Boolean
Dim Pola(7, 7) As String '!
Dim Turn As String
'Dim Pola(7, 7) As String!
'Dim Pola(7, 7) As String!
'Dim Pola(7, 7) As String!
'Dim Pola(7, 7) As String!
'Dim Pola(7, 7) As String!
'Dim Pola(7, 7) As String!
'Dim Pola(7, 7) As String!
Dim TableAbsoluteX
Dim TableAbsoluteY
Dim tableXzero As Double
Dim tableYzero As Double
Dim CursorPointScaleX As Double
Dim CursorPointScaleY As Double
Private Declare Function AddFontResourceEx Lib "gdi32.dll" _
    Alias "AddFontResourceExA" (ByVal lpcstr As String, _
    ByVal dword As Long, ByRef DESIGNVECTOR) As Long
Private Const FR_PRIVATE As Long = &H10

Private Declare Function BitBlt Lib "gdi32" (ByVal hDestDC As Long, ByVal x As Long, ByVal y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, ByVal dwRop As Long) As Long
Dim a, herox, pisox

'MAKEGREY
Private Declare Function GetColorAdjustment Lib "gdi32" (ByVal hdc As Long, lpca As COLORADJUSTMENT) As Long
Private Declare Function SetColorAdjustment Lib "gdi32" (ByVal hdc As Long, lpca As COLORADJUSTMENT) As Long
Private Declare Function SetStretchBltMode Lib "gdi32" (ByVal hdc As Long, ByVal nStretchMode As Long) As Long
Private Declare Function StretchBlt Lib "gdi32" (ByVal hdc As Long, ByVal x As Long, ByVal y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, ByVal nSrcWidth As Long, ByVal nSrcHeight As Long, ByVal dwRop As Long) As Long

Private Type COLORADJUSTMENT
    caSize As Integer
    caFlags As Integer
    caIlluminantIndex As Integer
    caRedGamma As Integer
    caGreenGamma As Integer
    caBlueGamma As Integer
    caReferenceBlack As Integer
    caReferenceWhite As Integer
    caContrast As Integer
    caBrightness As Integer
    caColorfulness As Integer
    caRedGreenTint As Integer
End Type

Private Const HALFTONE = 4
'MAKEGREY






Dim bacgrnr
Dim bacgrnr2
Dim MousePressed

Private Type POINTAPI
    x As Long
    y As Long
End Type
Private Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
Private Declare Function ScreenToClient Lib "user32" (ByVal hWnd As Long, _
    lpPoint As POINTAPI) As Long









Dim SpriteDiv As Integer
Dim ScreenH As Integer
Dim WalkLVL As Integer
Dim Jump As Boolean
Dim JumpD As Boolean
Dim jumpDir As Boolean
Dim JumpHight As Integer

Dim animCounter As Integer
Dim animFrame As Integer
Dim MinerX
Dim MinerLeft As Boolean
Const AC_SRC_ALPHA = &H1

Dim HAlpha As Long
Dim HAlpha2 As Long
Dim HAlpha3 As Long
Dim HAlpha4 As Long
Dim HAlpha5 As Long
Dim HAlpha6 As Long

Dim HAlphaB As Long
Dim HAlpha2B As Long
Dim HAlpha3B As Long
Dim HAlpha4B As Long
Dim HAlpha5B As Long
Dim HAlpha6B As Long

Dim HAlphaC As Long
Dim HAlpha2C As Long
Dim HAlpha3C As Long
Dim HAlpha4C As Long
Dim HAlpha5C As Long
Dim HAlpha6C As Long
Dim HAlpha7C As Long
Dim HAlpha8C As Long
Dim HAlpha9C As Long

Dim HAlphaTGA(200) As Long

Dim HBack As Long
Dim HBackBmOld As Long
Dim LBF As Long



''''''''''key input''''''''''''''key input''''''''''''''key input''''''''''''''key input''''
''''''''''key input''''''''''''''key input''''''''''''''key input''''''''''''''key input''''
''''''''''key input''''''''''''''key input''''''''''''''key input''''''''''''''key input''''
Dim keypressed(255) As Boolean
''''''''''key input''''''''''''''key input''''''''''''''key input''''''''''''''key input''''
''''''''''key input''''''''''''''key input''''''''''''''key input''''''''''''''key input''''
''''''''''key input''''''''''''''key input''''''''''''''key input''''''''''''''key input''''
'///////////////////////////////////////////////////////////////////////////////////////////
'gamepad input''''''gamepad input''''''gamepad input''''''gamepad input'''''
'gamepad input''''''gamepad input''''''gamepad input''''''gamepad input'''''
'gamepad input''''''gamepad input''''''gamepad input''''''gamepad input'''''
Private Const JOY_RETURNBUTTONS As Long = &H80&
Private Const JOY_RETURNCENTERED As Long = &H400&
Private Const JOY_RETURNPOV As Long = &H40&
Private Const JOY_RETURNPOVCTS As Long = &H200&
Private Const JOY_RETURNR As Long = &H8&
Private Const JOY_RETURNRAWDATA As Long = &H100&
Private Const JOY_RETURNU As Long = &H10
Private Const JOY_RETURNV As Long = &H20
Private Const JOY_RETURNX As Long = &H1&
Private Const JOY_RETURNY As Long = &H2&
Private Const JOY_RETURNZ As Long = &H4&
Private Const JOY_RETURNALL As Long = (JOY_RETURNX Or JOY_RETURNY Or JOY_RETURNZ Or JOY_RETURNR Or JOY_RETURNU Or JOY_RETURNV Or JOY_RETURNPOV Or JOY_RETURNBUTTONS)
Private Type JOYINFOEX
    dwSize As Long ' size of structure
    dwFlags As Long ' flags to dicate what to return
    dwXpos As Long ' x position
    dwYpos As Long ' y position
    dwZpos As Long ' z position
    dwRpos As Long ' rudder/4th axis position
    dwUpos As Long ' 5th axis position
    dwVpos As Long ' 6th axis position
    dwButtons As Long ' button states
    dwButtonNumber As Long ' current button number pressed
    dwPOV As Long ' point of view state
    dwReserved1 As Long ' reserved for communication between winmm driver
    dwReserved2 As Long ' reserved for future expansion
End Type
Private Declare Function joyGetPosEx Lib "winmm.dll" (ByVal uJoyID As Long, ByRef pji As JOYINFOEX) As Long
Dim JI As JOYINFOEX
Const JNum As Long = 0
'Set this to the number of the joystick that
'you want to read (a value between 0 and 15).
'The first joystick plugged in is number 0.
'The API for reading joysticks supports up to
'16 simultaniously plugged in joysticks.
'Change this Const to a Dim if you want to set
'it at runtime.
'gamepad input''''''gamepad input''''''gamepad input''''''gamepad input'''''
'gamepad input''''''gamepad input''''''gamepad input''''''gamepad input'''''
'gamepad input''''''gamepad input''''''gamepad input''''''gamepad input'''''
Private Declare Function SetWindowPos Lib "user32" (ByVal hWnd As Long, ByVal hWndInsertAfter As Long, ByVal x As Long, ByVal y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long
' Get mouse X coordinates in pixels
'
' If a window handle is passed, the result is relative to the client area
' of that window, otherwise the result is relative to the screen
Private Declare Function GetAsyncKeyState Lib "user32" _
        (ByVal vKey As Long) As Integer
Private Const VK_LBUTTON = &H1
Private Const VK_RBUTTON = &H2
Function MouseX(Optional ByVal hWnd As Long) As Long
    Dim lpPoint As POINTAPI
    GetCursorPos lpPoint
    If hWnd Then ScreenToClient hWnd, lpPoint
    MouseX = lpPoint.x
End Function

' Get mouse Y coordinates in pixels
'
' If a window handle is passed, the result is relative to the client area
' of that window, otherwise the result is relative to the screen

Function MouseY(Optional ByVal hWnd As Long) As Long
    Dim lpPoint As POINTAPI
    GetCursorPos lpPoint
    If hWnd Then ScreenToClient hWnd, lpPoint
    MouseY = lpPoint.y
End Function
' Set/unset a form as AlwaysOnTop
Public Sub SetAlwaysOnTop(ByVal hWnd As Long, Optional ByVal AlwaysOnTop As Boolean = True)
    Const SWP_NOSIZE = &H1
    Const SWP_NOMOVE = &H2
    Const SWP_SHOWWINDOW = &H40
    Const HWND_NOTOPMOST = -2
    Const HWND_TOPMOST = -1
    
    If AlwaysOnTop Then
        SetWindowPos hWnd, HWND_TOPMOST, 0&, 0&, 0&, 0&, SWP_NOMOVE Or SWP_NOSIZE Or SWP_SHOWWINDOW
    Else
        SetWindowPos hWnd, HWND_NOTOPMOST, 0&, 0&, 0&, 0&, SWP_NOMOVE Or SWP_NOSIZE Or SWP_SHOWWINDOW
    End If
End Sub
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
 retVal = mciSendString("stop songNumber4", vbNullString, 0, 0)
  retVal = mciSendString("stop songNumber5", vbNullString, 0, 0)
   retVal = mciSendString("stop songNumber6", vbNullString, 0, 0)
    retVal = mciSendString("stop songNumber7", vbNullString, 0, 0)

If Music(3) = True Then


End If

If Music(2) = True Then


End If
If Music(1) = True Then


End If
If Music(0) = True Then
 mciSendString "setaudio songNumber4 volume to " & 1200, vbNullString, 0, 0
 retVal = mciSendString("play songNumber2 from 1", vbNullString, 0, 0)

End If







'keypress'keypress'keypress'keypress'keypress
keypressed(KeyCode) = True
'keypress'keypress'keypress'keypress'keypress
'exit'exit'exit'exit'exit'exit'exit
'If KeyCode = 27 Then endProcedure
'exit'exit'exit'exit'exit'exit'exit
'show console'''show console'''show console''
If KeyCode = 192 Then


If consoleOff = True Then
consoleOff = False
Else
consoleOff = True
End If
If Picture2.Visible = True Then
Picture2.Visible = False
Else
Picture2.Visible = True
End If


End If
'show console'''show console'''show console''
End Sub
Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
'keypress-off'keypress-off'keypress-off
keypressed(KeyCode) = False
'keypress-off'keypress-off'keypress-off
End Sub

Private Sub Form_Load()
Dim o
For o = 0 To 8
WhosMove(o) = "White"
Next o



Colour = "White"
   Dim Hv

        filename = Chr(34) & App.Path + "\sounds\page.mp3" & Chr(34)
        retVal = mciSendString("open " & filename & " type mpegvideo alias songNumber1", vbNullString, 0, 0)
         Hv = 500
  mciSendString "setaudio songNumber1 volume to " & Hv, vbNullString, 0, 0

        filename = Chr(34) & App.Path + "\sounds\switch2.mp3" & Chr(34)
        retVal = mciSendString("open " & filename & " type mpegvideo alias songNumber2", vbNullString, 0, 0)
         Hv = 800
  mciSendString "setaudio songNumber2 volume to " & Hv, vbNullString, 0, 0


        filename = Chr(34) & App.Path + "\sounds\chess1.mp3" & Chr(34)
        retVal = mciSendString("open " & filename & " type mpegvideo alias songNumber3", vbNullString, 0, 0)
          Hv = 500
  mciSendString "setaudio songNumber3 volume to " & Hv, vbNullString, 0, 0




        filename = Chr(34) & App.Path + "\sounds\magical_theme.mp3" & Chr(34)
        retVal = mciSendString("open " & filename & " type mpegvideo alias songNumber4", vbNullString, 0, 0)
         Hv = 500
  mciSendString "setaudio songNumber4 volume to " & Hv, vbNullString, 0, 0

        filename = Chr(34) & App.Path + "\sounds\birds.mp3" & Chr(34)
        retVal = mciSendString("open " & filename & " type mpegvideo alias songNumber5", vbNullString, 0, 0)
         Hv = 800
  mciSendString "setaudio songNumber5 volume to " & Hv, vbNullString, 0, 0


        filename = Chr(34) & App.Path + "\sounds\crickets.mp3" & Chr(34)
       retVal = mciSendString("open " & filename & " type mpegvideo alias songNumber6", vbNullString, 0, 0)
          Hv = 500
  mciSendString "setaudio songNumber6 volume to " & Hv, vbNullString, 0, 0

        filename = Chr(34) & App.Path + "\sounds\wind.mp3" & Chr(34)
       retVal = mciSendString("open " & filename & " type mpegvideo alias songNumber7", vbNullString, 0, 0)
          Hv = 500
  mciSendString "setaudio songNumber7 volume to " & Hv, vbNullString, 0, 0




 retVal = mciSendString("stop songNumber4", vbNullString, 0, 0)
  retVal = mciSendString("stop songNumber5", vbNullString, 0, 0)
   retVal = mciSendString("stop songNumber6", vbNullString, 0, 0)
    retVal = mciSendString("stop songNumber7", vbNullString, 0, 0)

If Music(3) = True Then


End If

If Music(2) = True Then


End If
If Music(1) = True Then


End If
If Music(0) = True Then
' mciSendString "setaudio songNumber4 volume to " & 1200, vbNullString, 0, 0
' retVal = mciSendString("play songNumber2 from 1", vbNullString, 0, 0)

End If






'Boardselected = True
Pola3(0) = "Open"
Pola3(1) = "Open"
Pola3(2) = "Open"
Pola3(3) = "Closed"
Pola3(4) = "Closed"
Pola3(5) = "Closed"
Pola3(6) = "Closed"
Pola3(7) = "Closed"
Pola3(8) = "Closed"
cASTLEmASTER(0) = 1
'hidden board
menuON = True
IsboardActive = False 'ok

cASTLEmASTER(1) = 2
cASTLEmASTER(2) = 3
cASTLEmASTER(3) = 4
cASTLEmASTER(4) = 5
cASTLEmASTER(5) = 6
cASTLEmASTER(6) = 7
cASTLEmASTER(7) = 8
cASTLEmASTER(8) = 9
'BoardEnabled(8) = False

WHICHMAPMASTER = 0
boardSET 1
boardSET 0
AddFontResourceEx App.Path + "\pieces\vinque.ttf", FR_PRIVATE, 0&
Picture3.Font = "Vinque-Regular"
Picture3.Font.Size = 17
Picture3.Font.Italic = False
Picture3.Font.Bold = True
Picture3.Font.Underline = False
Picture3.Font.Strikethrough = False
Picture3.ForeColor = vbWhite
'Picture2.Caption = "TEXT"
Picture4.Font = "Vinque-Regular"
Picture4.Font.Size = 17
Picture4.Font.Italic = False
Picture4.Font.Bold = True
Picture4.Font.Underline = False
Picture4.Font.Strikethrough = False
Picture4.ForeColor = vbRed

Picture2.Font = "Vinque-Regular"
Picture2.Font.Size = 20
Picture2.Font.Italic = False
Picture2.Font.Bold = True
Picture2.Font.Underline = False
Picture2.Font.Strikethrough = False
Picture2.ForeColor = vbRed
Dim rnd5
rnd5 = 1
whichMap = 1
rnd5 = whichMap



retVal = mciSendString("stop songNumber4", vbNullString, 0, 0)
retVal = mciSendString("stop songNumber5", vbNullString, 0, 0)
retVal = mciSendString("stop songNumber6", vbNullString, 0, 0)
retVal = mciSendString("stop songNumber7", vbNullString, 0, 0)



retVal = mciSendString("play songNumber4 from 1 repeat", vbNullString, 0, 0)
piso.Picture = LoadPicture(App.Path + "\backgrounds\map4.bmp")





If menuON = False Then
If rnd5 = 0 Then
piso.Picture = LoadPicture(App.Path + "\backgrounds\map.bmp")
ElseIf rnd5 = 1 Then
piso.Picture = LoadPicture(App.Path + "\backgrounds\map0.bmp")
ElseIf rnd5 = 2 Then
piso.Picture = LoadPicture(App.Path + "\backgrounds\map2.bmp")
Else

End If
Else


End If

Image3(0).Picture = LoadPicture(App.Path + "\backgrounds\0.jpg")
Image3(1).Picture = LoadPicture(App.Path + "\backgrounds\1.jpg")
Image3(2).Picture = LoadPicture(App.Path + "\backgrounds\2.jpg")
Image3(3).Picture = LoadPicture(App.Path + "\backgrounds\3.jpg")
Image3(4).Picture = LoadPicture(App.Path + "\backgrounds\4.jpg")
Image3(8).Picture = LoadPicture(App.Path + "\backgrounds\5.jpg")
Image3(9).Picture = LoadPicture(App.Path + "\backgrounds\6.jpg")
Image3(10).Picture = LoadPicture(App.Path + "\backgrounds\7.jpg")
Image3(11).Picture = LoadPicture(App.Path + "\backgrounds\8.jpg")

Image3(12).Picture = LoadPicture(App.Path + "\backgrounds\main.jpg")

Image3(5).Picture = LoadPicture(App.Path + "\backgrounds\fondo_de_pantalla_relajante_HD.jpg")
Image3(6).Picture = LoadPicture(App.Path + "\backgrounds\Hills-3.jpg")
Image3(7).Picture = LoadPicture(App.Path + "\backgrounds\The-Shropshire-Hills-Late-Autumn-by-Jordan-Mansfield-1024x687.jpg")


'Image5.Picture = LoadPicture(App.Path + "\backgrounds\tablemap.bmp")


'Image2.Picture = Image3(1).Picture
Randomize
bacgrnr = Int(Rnd * 5)
bacgrnr2 = Int(Rnd * 5)
whichCastle = 0
whichWall = 0 '0-4
bacgrnr = whichWall
Image2.Picture = Image3(bacgrnr).Picture
Image4.Picture = Image3(bacgrnr2).Picture
    Me.Move 0, 0, Screen.Width, Screen.Height
    SetAlwaysOnTop Me.hWnd

oldWall = bacgrnr
Oldmap = rnd5



'gamepad'gamepad'gamepad'gamepad
JI.dwSize = Len(JI)
JI.dwFlags = JOY_RETURNALL
'gamepad'gamepad'gamepad'gamepad
On Error Resume Next

Dim res As Long
Dim Bf As BLENDFUNCTION
    'Dim TablePictures(1000)
    HAlpha = LOADTGA(App.Path & "\pieces\1a.tga", True) 'Make Alpha bitmap
    HAlpha2 = LOADTGA(App.Path & "\pieces\2a.tga", True) 'Make Alpha bitmap
    HAlpha3 = LOADTGA(App.Path & "\pieces\3a.tga", True) 'Make Alpha bitmap
    HAlpha4 = LOADTGA(App.Path & "\pieces\4a.tga", True) 'Make Alpha bitmap
    HAlpha5 = LOADTGA(App.Path & "\pieces\5a.tga", True) 'Make Alpha bitmap
    HAlpha6 = LOADTGA(App.Path & "\pieces\6a.tga", True) 'Make Alpha bitmap
    
    HAlphaB = LOADTGA(App.Path & "\pieces\1b.tga", True) 'Make Alpha bitmap
    HAlpha2B = LOADTGA(App.Path & "\pieces\2b.tga", True) 'Make Alpha bitmap
    HAlpha3B = LOADTGA(App.Path & "\pieces\3b.tga", True) 'Make Alpha bitmap
    HAlpha4B = LOADTGA(App.Path & "\pieces\4b.tga", True) 'Make Alpha bitmap
    HAlpha5B = LOADTGA(App.Path & "\pieces\5b.tga", True) 'Make Alpha bitmap
    HAlpha6B = LOADTGA(App.Path & "\pieces\6b.tga", True) 'Make Alpha bitmap
   ' Dim TGAcount
 HAlphaC = LOADTGA(App.Path & "\pieces\king0.tga", True) 'Make Alpha bitmap
HAlpha2C = LOADTGA(App.Path & "\pieces\king1.tga", True) 'Make Alpha bitmap
HAlpha3C = LOADTGA(App.Path & "\pieces\king2.tga", True) 'Make Alpha bitmap
 HAlpha4C = LOADTGA(App.Path & "\pieces\king3.tga", True) 'Make Alpha bitmap
 HAlpha5C = LOADTGA(App.Path & "\pieces\king4.tga", True) 'Make Alpha bitmap
HAlpha6C = LOADTGA(App.Path & "\pieces\king5.tga", True) 'Make Alpha bitmap
HAlpha7C = LOADTGA(App.Path & "\pieces\king6.tga", True) 'Make Alpha bitmap
HAlpha8C = LOADTGA(App.Path & "\pieces\king7.tga", True) 'Make Alpha bitmap
 HAlpha9C = LOADTGA(App.Path & "\pieces\king8.tga", True) 'Make Alpha bitmap
   ' For TGAcount = 0 To 200
    
   ' HAlphaTGA(TGAcount) = LOADTGA(App.Path & "\sprites\mm1l.tga", True) 'Make Alpha bitmap
   ' Next
    

    
    
    'Create blend function
    Bf.BlendOp = 0
    Bf.BlendFlags = 0
    Bf.SourceConstantAlpha = 255
    Bf.AlphaFormat = AC_SRC_ALPHA
    'Must be transefered as a long value so we copy the BLENDFUNCTION stuct
    'directly into a long variable
    CopyMemory LBF, Bf, 4











End Sub



Private Sub Form_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
WantSelect = False
MousePressed = 0
End Sub

Private Sub Form_Resize()
'main screen-picbox resize
Picture2.Left = 0: Picture2.Width = 1400
Picture2.Top = 0: Picture2.Height = 800 'ScaleHeight
Dim res As Long
    HBack = CreateCompatibleDC(Picture2.hdc) 'Make Back Buffer and Back buffer Bitmap
    res = CreateCompatibleBitmap(Picture2.hdc, Picture2.ScaleWidth, Picture2.ScaleHeight)

Picture4.Left = 0: Picture4.Width = ScaleWidth
Picture4.Top = 0: Picture4.Height = ScaleHeight



Picture3.Left = 0: Picture3.Width = 1400 ' ScaleWidth
Picture3.Top = 0: Picture3.Height = 800 ' ScaleHeight
ScreenH = Picture3.ScaleHeight
'Dim res As Long
SpriteDiv = 7
WalkLVL = 200
    res = CreateCompatibleBitmap(Picture2.hdc, Picture2.ScaleWidth, Picture2.ScaleHeight)
    res = SelectObject(HBack, res)
    res = DeleteObject(res)
'main screen-picbox resize
'Image1.Height = ScaleHeight
'Image1.Width = ScaleHeight
Image2.Picture = Image3(5).Picture
RND5b = 1

'tables
'LoadGame 0




End Sub

Private Sub Form_Unload(Cancel As Integer)
SaveGame
  retVal = mciSendString("close songNumber1", vbNullString, 0, 0)
  retVal = mciSendString("close songNumber2", vbNullString, 0, 0)
  retVal = mciSendString("close songNumber3", vbNullString, 0, 0)
  
retVal = mciSendString("close songNumber4", vbNullString, 0, 0)
retVal = mciSendString("close songNumber5", vbNullString, 0, 0)
retVal = mciSendString("close songNumber6", vbNullString, 0, 0)
retVal = mciSendString("close songNumber7", vbNullString, 0, 0)
End
End Sub

Private Sub Timer1_Timer()







SquareLineDrawed = False
Dim Drawed As String
On Error Resume Next

'HostName = ""
'GuestName = ""

If Boardselected = 1 Then
HostName = "Player 1"
GuestName = ""
End If
'End If
If Boardselected = 2 Then
HostName = "Player 2"
GuestName = "Player 1"
End If
If Boardselected = 3 Then
HostName = "Player 3"
GuestName = "Player 1"
End If
If Boardselected = 4 Then
HostName = "Player 4"
GuestName = "Player 1"
End If
'End If
If Boardselected = 5 Then
HostName = "Player 5"
GuestName = "Player 1"
End If
If Boardselected = 6 Then
HostName = "Player 6"
GuestName = "Player 1"
End If

If Boardselected = 7 Then
HostName = "Player 7"
GuestName = "Player 1"
End If
'End If
If Boardselected = 8 Then
HostName = "Player 8"
GuestName = "Player 1"
End If
If Boardselected = 9 Then
HostName = "Player 9"
GuestName = "Player 1"
End If













If Boardselected < 1 Then


HostName = ""
GuestName = ""


Else
'------------------------------------------------------------------------------'

'If Boardselected = 1 Then






End If




CursorPointScaleX = (MouseX * 1400) \ ScaleWidth
CursorPointScaleY = (MouseY * 800) \ ScaleHeight
DoEvents
Dim Xszach
Dim Yszach
Picture3.Cls
Picture3.ScaleMode = 3
't�o
If menuON = True Then Image2.Picture = Image3(12).Picture
'zamek1


Picture3.PaintPicture Image2.Picture, 0, 0, Picture2.ScaleWidth, Picture2.ScaleHeight, 0, 0, Image2.Width, Image2.Height


'zamek2
'Picture3.PaintPicture Image4.Picture, Picture2.ScaleWidth \ 2, 0, Picture2.ScaleWidth \ 2, Picture2.ScaleHeight, 0, 0, Image4.Width, Image4.Height
'bwx Picture3
'RND5b

Picture3.ScaleMode = 3
'szachownica
If IsboardActive = True And menuON = False Then
Picture3.PaintPicture Image1.Picture, Picture3.ScaleWidth / 3 - Image1.Width / 2, Picture3.ScaleHeight / 2 - Image1.Height / 2, Image1.Width, Image1.Height, 0, 0, Image1.Width, Image1.Height
End If
'bwx Picture3




'mapa
'Picture3.PaintPicture Image5.Picture, ScaleWidth \ 2 + ScaleWidth \ 4, ScaleHeight \ 2 - ScaleHeight \ 4, Image5.Width * 7.5, Image5.Height * 12, 0, 0, ScaleWidth, ScaleHeight


DEBUGSHOW = consoleOff
'DEBUGSHOW = True
If DEBUGSHOW Then
Picture3.ForeColor = vbRed

Picture3.Print "screenX " + Str(MouseX)
Picture3.Print "screenY " + Str(MouseY)
'Picture3.Print Str(MousePressed)
If GetAsyncKeyState(VK_LBUTTON) Then
        Picture3.Print "Left Click"
    ElseIf GetAsyncKeyState(VK_RBUTTON) Then
        Picture3.Print "Right Click"
    Else
        Picture3.Print ""
    End If
Picture3.Print "selected False"
Picture3.Print "background " + Str(bacgrnr)
Picture3.Print "background2 " + Str(bacgrnr2)
Picture3.Print "scaleX " + Str(CursorPointScaleX)
Picture3.Print "scaleY " + Str(CursorPointScaleY)
'Dim TableAbsoluteX
'Dim TableAbsoluteY
Picture3.Print "TableAbsoluteX " + Str(TableAbsoluteX)
Picture3.Print "TableAbsolutey " + Str(TableAbsoluteY)
Picture3.Print "whichMap " + Str(whichMap)
Picture3.Print "whichWall " + Str(whichWall)
Picture3.Print "whichCastle " + Str(whichCastle)
Picture3.Print "Boardselected " + Str(Boardselected)
Picture3.Print "PercentLoaded" + Str(PercentLoaded)

Dim o
For o = 0 To 8
WhosMove(o) = "White"
Picture3.Print "Board Move" + Str(o) + " " + WhosMove(o)
Next o

'pictur3.Print 'Str(Music(0)) + " music0"
'pictur3.Print 'Str(Music(1)) + " music1"
'pictur3.Print 'Str(Music(2)) + " music2"
'pictur3.Print 'Str(Music(3)) + " music3"
End If
 

'boardSET Boardselected - 1


Dim res
DoEvents


If (((keypressed(39) And Not keypressed(37)) Or (keypressed(37) And Not keypressed(39))) And Jump = False) Or jumpDir Then
animCounter = animCounter + 1
If animCounter >= 4 Then animCounter = 0
If (keypressed(39) And Not jumpDir) Or (jumpDir And Not MinerLeft) Then
MinerX = MinerX + 5
MinerLeft = False
End If
If (keypressed(37) And Not jumpDir) Or (jumpDir And MinerLeft) Then
MinerX = MinerX - 5
MinerLeft = True
End If
End If

If keypressed(38) And Jump = False Then
Jump = True
JumpD = False
If ((keypressed(39) And Not keypressed(37)) Or (keypressed(37) And Not keypressed(39))) Then jumpDir = True
End If
If Jump = True Then
If JumpD = False Then
JumpHight = JumpHight - 5
If -JumpHight >= (ScreenH \ (SpriteDiv + 7)) Then
JumpD = True
End If
Else
JumpHight = JumpHight + 5
If JumpHight >= 0 Then
JumpHight = 0
Jump = False
JumpD = False
jumpDir = False
End If
End If
End If

If animCounter < 1 Then animFrame = 1
If animCounter >= 1 And animCounter < 2 Then animFrame = 2
If animCounter >= 2 And animCounter < 3 Then animFrame = 3
If animCounter >= 3 Then animFrame = 2


Picture3.DrawWidth = 4
'fields
tableXzero = (-63 * 3) + (Int((CursorPointScaleX - 25) \ 63) * 63) + ((Picture3.ScaleWidth \ 3 - Image1.Width \ 2) + 61)
tableYzero = (-63 * 2) + (Int((CursorPointScaleY - 25) \ 63) * 63) + ((Picture3.ScaleHeight \ 2 - Image1.Height \ 2) + 61)
'Picture2
TableAbsoluteX = (tableXzero - 214) \ 63 '(Abs(CursorPointScaleX - ((Picture3.ScaleWidth \ 3 - Image1.Width \ 2) + 61))) \ 63
TableAbsoluteY = (tableYzero - 148) \ 63 '(Abs(CursorPointScaleY - ((Picture3.ScaleHeight \ 2 - Image1.Height \ 2) + 61))) \ 63


If TableAbsoluteX < 0 Then TableAbsoluteX = 0
If TableAbsoluteY < 0 Then TableAbsoluteY = 0
If TableAbsoluteX > 7 Then TableAbsoluteX = 7
If TableAbsoluteY > 7 Then TableAbsoluteY = 7
On Error Resume Next
If Pola2(Boardselected, TableAbsoluteX, TableAbsoluteY) <> "" Then
'Picture3.ForeColor = vbYellow


If Right(Pola2(Boardselected, TableAbsoluteX, TableAbsoluteY), 1) = "B" Then
rectangleColor = RGB(0, 148, 255)
Else
rectangleColor = vbYellow 'RGB(255, 106, 0)
End If


Else
rectangleColor = vbYellow
End If



If tableXzero >= ((Picture3.ScaleWidth \ 3 - Image1.Width \ 2) + 61) Then
If tableYzero >= ((Picture3.ScaleHeight \ 2 - Image1.Height \ 2) + 61) Then
If tableXzero <= ((Picture3.ScaleWidth \ 3 - Image1.Width \ 2) + 61) + 63 * 7 Then
If tableYzero <= ((Picture3.ScaleHeight \ 2 - Image1.Height \ 2) + 61) + 63 * 7 Then
If Pola2(Boardselected, TableAbsoluteX, TableAbsoluteY) <> "" Then

If IsboardActive And menuON = False Then


If Pola3(Boardselected - 1) <> "Closed" Then
'may select
Picture3.Line (tableXzero, tableYzero)-(tableXzero + 63, tableYzero + 63), rectangleColor, B 'bf fill
SquareLineDrawed = True









End If


End If

End If
'Picture3.ForeColor = vbYellow
End If
End If
End If
End If




res = BitBlt(HBack, 0, 0, Picture3.ScaleWidth, Picture3.ScaleHeight, Picture3.hdc, 0, 0, vbSrcCopy)




If IsboardActive = True And menuON = False Then

Dim pieceFieldX
Dim pieceFieldY
For pieceFieldX = 0 To 7
For pieceFieldY = 0 To 7




'kingson map1-9
'kingson map
'which mar

'res = AlphaBlend(HBack, 190 + (pieceFieldX * 63), 90 + (pieceFieldY * 63), ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha6, 0, 0, 128, 128, LBF)
'res = AlphaBlend(HBack, 190 + (pieceFieldX * 63), 90 + (pieceFieldY * 63), ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha6, 0, 0, 128, 128, LBF)
'res = AlphaBlend(HBack, 190 + (pieceFieldX * 63), 90 + (pieceFieldY * 63), ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha6, 0, 0, 128, 128, LBF)
'res = AlphaBlend(HBack, 190 + (pieceFieldX * 63), 90 + (pieceFieldY * 63), ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha6, 0, 0, 128, 128, LBF)
'res = AlphaBlend(HBack, 190 + (pieceFieldX * 63), 90 + (pieceFieldY * 63), ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha6, 0, 0, 128, 128, LBF)
'res = AlphaBlend(HBack, 190 + (pieceFieldX * 63), 90 + (pieceFieldY * 63), ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha6, 0, 0, 128, 128, LBF)
'res = AlphaBlend(HBack, 190 + (pieceFieldX * 63), 90 + (pieceFieldY * 63), ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha6, 0, 0, 128, 128, LBF)
'res = AlphaBlend(HBack, 190 + (pieceFieldX * 63), 90 + (pieceFieldY * 63), ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha6, 0, 0, 128, 128, LBF)
'res = AlphaBlend(HBack, 190 + (pieceFieldX * 63), 90 + (pieceFieldY * 63), ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha6, 0, 0, 128, 128, LBF)
'kingson map
'kingson map
'Xszach = ((Picture3.ScaleWidth / 3 - Image1.Width / 2) * 1400 / ScaleWidth) + Picture3.Width / 60
'Yszach = ((Picture3.ScaleHeight / 2 - Image1.Height / 2) * 800 / ScaleHeight)

Xszach = -22 + (-63 * 3) + (Int((250 - 25) \ 63) * 63) + ((Picture3.ScaleWidth \ 3 - Image1.Width \ 2) + 61)
Yszach = -60 + (-63 * 2) + (Int((180 - 25) \ 63) * 63) + ((Picture3.ScaleHeight \ 2 - Image1.Height \ 2) + 61)

'TableAbsoluteX = (tableXzero - 214) \ 63 '(Abs(CursorPointScaleX - ((Picture3.ScaleWidth \ 3 - Image1.Width \ 2) + 61))) \ 63
'TableAbsoluteY = (tableYzero - 148) \ 63 '(Abs(CursorPointScaleY - ((Picture3.ScaleHeight \ 2 - Image1.Height \ 2) + 61))) \ 63
'CursorPointScaleX = (MouseX * 1400) \ ScaleWidth
'CursorPointScaleY = (MouseY * 800) \ ScaleHeight
On Error Resume Next
'pieces
'pieces
If Pola2(Boardselected - 1, pieceFieldX, pieceFieldY) = "HAlpha6" Then
res = AlphaBlend(HBack, Xszach + (pieceFieldX * 63), Yszach + (pieceFieldY * 63), ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha6, 0, 0, 128, 128, LBF)
End If
If Pola2(Boardselected - 1, pieceFieldX, pieceFieldY) = "HAlpha6B" Then
res = AlphaBlend(HBack, Xszach + (pieceFieldX * 63), Yszach + (pieceFieldY * 63), ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha6B, 0, 0, 128, 128, LBF)
End If
If Pola2(Boardselected - 1, pieceFieldX, pieceFieldY) = "HAlpha5" Then
res = AlphaBlend(HBack, Xszach + (pieceFieldX * 63), Yszach + (pieceFieldY * 63), ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha5, 0, 0, 128, 128, LBF)
End If
If Pola2(Boardselected - 1, pieceFieldX, pieceFieldY) = "HAlpha5B" Then
res = AlphaBlend(HBack, Xszach + (pieceFieldX * 63), Yszach + (pieceFieldY * 63), ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha5B, 0, 0, 128, 128, LBF)
End If
If Pola2(Boardselected - 1, pieceFieldX, pieceFieldY) = "HAlpha4" Then
res = AlphaBlend(HBack, Xszach + (pieceFieldX * 63), Yszach + (pieceFieldY * 63), ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha4, 0, 0, 128, 128, LBF)
End If
If Pola2(Boardselected - 1, pieceFieldX, pieceFieldY) = "HAlpha4B" Then
res = AlphaBlend(HBack, Xszach + (pieceFieldX * 63), Yszach + (pieceFieldY * 63), ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha4B, 0, 0, 128, 128, LBF)
End If
If Pola2(Boardselected - 1, pieceFieldX, pieceFieldY) = "HAlpha3" Then
res = AlphaBlend(HBack, Xszach + (pieceFieldX * 63), Yszach + (pieceFieldY * 63), ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha3, 0, 0, 128, 128, LBF)
End If
If Pola2(Boardselected - 1, pieceFieldX, pieceFieldY) = "HAlpha3B" Then
res = AlphaBlend(HBack, Xszach + (pieceFieldX * 63), Yszach + (pieceFieldY * 63), ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha3B, 0, 0, 128, 128, LBF)
End If
If Pola2(Boardselected - 1, pieceFieldX, pieceFieldY) = "HAlpha2" Then
res = AlphaBlend(HBack, Xszach + (pieceFieldX * 63), Yszach + (pieceFieldY * 63), ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha2, 0, 0, 128, 128, LBF)
End If
If Pola2(Boardselected - 1, pieceFieldX, pieceFieldY) = "HAlpha2B" Then
res = AlphaBlend(HBack, Xszach + (pieceFieldX * 63), Yszach + (pieceFieldY * 63), ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha2B, 0, 0, 128, 128, LBF)
End If
If Pola2(Boardselected - 1, pieceFieldX, pieceFieldY) = "HAlphaB" Then
res = AlphaBlend(HBack, Xszach + (pieceFieldX * 63), Yszach + (pieceFieldY * 63), ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlphaB, 0, 0, 128, 128, LBF)
End If
If Pola2(Boardselected - 1, pieceFieldX, pieceFieldY) = "HAlpha" Then
res = AlphaBlend(HBack, Xszach + (pieceFieldX * 63), Yszach + (pieceFieldY * 63), ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha, 0, 0, 128, 128, LBF)
End If
'res = AlphaBlend(HBack, 164 + MinerX, WalkLVL + JumpHight, ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha, 0, 0, 128, 128, LBF)
'res = AlphaBlend(HBack, 164 + MinerX, WalkLVL + JumpHight, ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha, 0, 0, 128, 128, LBF)
'res = AlphaBlend(HBack, 164 + MinerX, WalkLVL + JumpHight, ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha, 0, 0, 128, 128, LBF)
'res = AlphaBlend(HBack, 164 + MinerX, WalkLVL + JumpHight, ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha, 0, 0, 128, 128, LBF)
'res = AlphaBlend(HBack, 164 + MinerX, WalkLVL + JumpHight, ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha, 0, 0, 128, 128, LBF)
'res = AlphaBlend(HBack, 164 + MinerX, WalkLVL + JumpHight, ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha, 0, 0, 128, 128, LBF)
'res = AlphaBlend(HBack, 164 + MinerX, WalkLVL + JumpHight, ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha, 0, 0, 128, 128, LBF)
'res = AlphaBlend(HBack, 164 + MinerX, WalkLVL + JumpHight, ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha, 0, 0, 128, 128, LBF)
'res = AlphaBlend(HBack, 164 + MinerX, WalkLVL + JumpHight, ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha, 0, 0, 128, 128, LBF)
'res = AlphaBlend(HBack, 164 + MinerX, WalkLVL + JumpHight, ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha, 0, 0, 128, 128, LBF)
'res = AlphaBlend(HBack, 164 + MinerX, WalkLVL + JumpHight, ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha, 0, 0, 128, 128, LBF)
'pieces
'pieces
Next
Next
End If
'rzut hback na pcture2
res = BitBlt(Picture2.hdc, 0, 0, Picture2.ScaleWidth, Picture2.ScaleHeight, HBack, 0, 0, vbSrcCopy)
'Picture2.ScaleMode = 2
If boardActive = False Then
'bwx Picture2
End If

'Dim i
'i=
'For i = 0 To 8
'If whichCastle <> -1 And BoardEnabled(whichCastle) = False Then bwx Picture2
'map1open






With rct
    .Left = (Picture2.ScaleWidth \ 2 - Picture2.ScaleWidth \ 2) + 30
    .Right = Picture2.ScaleWidth
    .Top = (Picture2.ScaleWidth \ 35) '+ 34
    .Bottom = Picture2.ScaleHeight
End With

'If Boardselected = 1 Then
 'Picture2.ForeColor = vbBlue
' Else
 ' Picture2.ForeColor = RGB(250, 250, 100)

 'End If
 
If Boardselected <> 0 Then


If Boardselected = 1 Then

'If Colour = "Black" Then
'Picture2.ForeColor = RGB(250, 250, 100)
' Else
' Picture2.ForeColor = vbBlue
' End If


If Colour = "White" Then
Picture2.ForeColor = RGB(250, 250, 100)
Else
Picture2.ForeColor = RGB(0, 148, 255)
End If

If Colour = "White" Then
DrawText Picture2.hdc, "                  Host - " + HostName, -1, rct, DT_WORDBREAK
Else
DrawText Picture2.hdc, "                                                      Host - " + HostName, -1, rct, DT_WORDBREAK
End If


End If
If Boardselected <> 1 Then
'DrawText Picture2.hdc, "                  Host - " + HostName, -1, rct, DT_WORDBREAK
'If Colour = "White" Then

' Picture2.ForeColor = vbBlue
' Else
'Picture2.ForeColor = RGB(250, 250, 100)
' End If

If Colour = "Black" Then
Picture2.ForeColor = RGB(250, 250, 100)
Else
Picture2.ForeColor = RGB(0, 148, 255)
End If

If Colour = "White" Then
DrawText Picture2.hdc, "                                                      Host - " + HostName, -1, rct, DT_WORDBREAK
Else
DrawText Picture2.hdc, "                  Host - " + HostName, -1, rct, DT_WORDBREAK
End If
End If

End If
With rct
    .Left = (Picture2.ScaleWidth \ 2 - Picture2.ScaleWidth \ 2) + 30
    .Right = Picture2.ScaleWidth
    .Top = (Picture2.ScaleWidth \ 35) '+ 34
    .Bottom = Picture2.ScaleHeight
End With


'If Colour = "White" Then
'If Boardselected = 1 Then

' Picture2.ForeColor = vbBlue
 'Else
 '  Picture2.ForeColor = RGB(250, 250, 100)
 'End If
 'Else
' If Boardselected = 1 Then
' Picture2.ForeColor = vbBlue
' Else
 '  Picture2.ForeColor = RGB(250, 250, 100)
' End If
 'End If
 
 
 
If Boardselected <> 0 Then
If Boardselected = 1 Then
If Colour = "White" Then

 Picture2.ForeColor = RGB(0, 148, 255)
 Else
Picture2.ForeColor = RGB(250, 250, 100)
  
 End If
 
 
If Colour = "White" Then
DrawText Picture2.hdc, "                                                      Guest - " + GuestName, -1, rct, DT_WORDBREAK
 Else
DrawText Picture2.hdc, "                  Guest - " + GuestName, -1, rct, DT_WORDBREAK
End If
 



End If






If Boardselected <> 1 Then
'DrawText Picture2.hdc, "                                                      Guest - " + GuestName, -1, rct, DT_WORDBREAK

If Colour = "White" Then
Picture2.ForeColor = RGB(250, 250, 100)
Else
Picture2.ForeColor = RGB(0, 148, 255)
End If



If Colour = "White" Then
DrawText Picture2.hdc, "                  Guest - " + GuestName, -1, rct, DT_WORDBREAK
Else
DrawText Picture2.hdc, "                                                      Guest - " + GuestName, -1, rct, DT_WORDBREAK

End If



End If

End If
'-----------------------------------







If Boardselected <> 0 Then
If Pola3(Boardselected - 1) = "Closed" Then bwx Picture2
End If
'Next
'map
BitBlt Picture2.hdc, Picture2.ScaleWidth \ 2 + Picture2.ScaleWidth \ 10, Picture2.ScaleWidth \ 35, piso.Width, piso.Height, Form1.pisoMask.hdc, 0, 0, vbSrcAnd
BitBlt Picture2.hdc, Picture2.ScaleWidth \ 2 + Picture2.ScaleWidth \ 10, Picture2.ScaleWidth \ 35, piso.Width, piso.Height, Form1.piso.hdc, 0, 0, vbSrcPaint
'Picture2'Picture2'Picture2'Picture2'Picture2
'Picture2'Picture2'Picture2'Picture2'Picture2



Dim MAPAx
Dim MAPAy
MAPAx = Picture2.ScaleWidth \ 2 + Picture2.ScaleWidth \ 10
MAPAy = Picture2.ScaleWidth \ 35
'SHOW
'If whichMap = 0 Then
'End If
Picture2.DrawWidth = 4.5
whichCastle = -1



If Boardselected <> 0 Then

If Colour = "Black" Then
Picture2.ForeColor = RGB(250, 250, 100)
Else

Picture2.ForeColor = vbBlue
End If

If Boardselected = 1 Then
If Colour = "Black" Then
'Picture2.ForeColor = vbYellow
'Else
Picture2.ForeColor = vbBlue

Else
Picture2.ForeColor = vbYellow
End If


End If

If Boardselected = 1 Then Picture2.Circle (MAPAxChk + 130, MAPAyChk + 530), 50
If Boardselected = 2 Then Picture2.Circle (MAPAxChk + 240, MAPAyChk + 195), 50
If Boardselected = 3 Then Picture2.Circle (MAPAxChk + 391, MAPAyChk + 505), 50
If Boardselected = 7 Then Picture2.Circle (MAPAxChk + 78, MAPAyChk + 385), 50
If Boardselected = 8 Then Picture2.Circle (MAPAxChk + 390, MAPAyChk + 195), 50
If Boardselected = 9 Then Picture2.Circle (MAPAxChk + 385, MAPAyChk + 515), 50
If Boardselected = 4 Then Picture2.Circle (MAPAxChk + 265, MAPAyChk + 525), 50
If Boardselected = 5 Then Picture2.Circle (MAPAxChk + 130, MAPAyChk + 205), 50
If Boardselected = 6 Then Picture2.Circle (MAPAxChk + 370, MAPAyChk + 200), 50
 

'MAPAxChk = MAPAx
'MAPAyChk = MAPAy
End If


If whichMap = 1 And menuON = False Then 'MAP1-KINGS
'IF SCALEX=MAPAx + 130
'Debug.Print "sTART_KCVBT 1.0.0.0"
'...
'...
'...
'--------
'--------
'--------
'--------
'--------
'--------
'--------
'--------


If CursorPointScaleX > MAPAx + 130 - 30 Then
If CursorPointScaleY > MAPAy + 530 - 30 Then
If CursorPointScaleX < MAPAx + 130 + 60 Then
If CursorPointScaleY < MAPAy + 530 + 60 Then
whichCastle = 0

If cASTLEmASTER(whichCastle) = 1 Then Picture2.ForeColor = vbYellow
If Colour = "White" Then
Picture2.ForeColor = vbYellow
Else
Picture2.ForeColor = vbBlue
End If


Picture2.Circle (MAPAx + 130, MAPAy + 530), 50
MAPAxChk = MAPAx
MAPAyChk = MAPAy

End If
End If
End If
End If

If CursorPointScaleX > MAPAx + 240 - 30 Then
If CursorPointScaleY > MAPAy + 195 - 30 Then
If CursorPointScaleX < MAPAx + 240 + 60 Then
If CursorPointScaleY < MAPAy + 195 + 60 Then
whichCastle = 1
Picture2.ForeColor = vbBlue
If cASTLEmASTER(whichCastle) = 1 Then Picture2.ForeColor = vbYellow
If Colour = "White" Then
Picture2.ForeColor = vbBlue
Else
Picture2.ForeColor = vbYellow
End If





Picture2.Circle (MAPAx + 240, MAPAy + 195), 50
MAPAxChk = MAPAx
MAPAyChk = MAPAy
End If
End If
End If
End If

If CursorPointScaleX > MAPAx + 391 - 30 Then
If CursorPointScaleY > MAPAy + 505 - 30 Then
If CursorPointScaleX < MAPAx + 391 + 60 Then
If CursorPointScaleY < MAPAy + 505 + 60 Then
whichCastle = 2
Picture2.ForeColor = vbBlue
If cASTLEmASTER(whichCastle) = 1 Then Picture2.ForeColor = vbYellow

If Colour = "White" Then
Picture2.ForeColor = vbBlue
Else
Picture2.ForeColor = vbYellow
End If


Picture2.Circle (MAPAx + 391, MAPAy + 505), 50
MAPAxChk = MAPAx
MAPAyChk = MAPAy
End If
End If
End If
End If



res = AlphaBlend(Picture2.hdc, MAPAx + 50, MAPAy + 400, ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlphaC, 0, 0, 128, 128, LBF)
res = AlphaBlend(Picture2.hdc, MAPAx + 180, MAPAy + 220, ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha2C, 0, 0, 128, 128, LBF)
res = AlphaBlend(Picture2.hdc, MAPAx + 320, MAPAy + 370, ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha3C, 0, 0, 128, 128, LBF)



End If
If whichMap = 2 And menuON = False Then  'MAP3-KINGS



'Picture2.ForeColor = vbBlue
If CursorPointScaleX > MAPAx + 78 - 30 Then
If CursorPointScaleY > MAPAy + 385 - 30 Then
If CursorPointScaleX < MAPAx + 78 + 60 Then
If CursorPointScaleY < MAPAy + 385 + 60 Then
whichCastle = 6
Picture2.ForeColor = vbBlue
If cASTLEmASTER(whichCastle) = 1 Then Picture2.ForeColor = vbYellow

If Colour = "White" Then
Picture2.ForeColor = vbBlue
Else
Picture2.ForeColor = vbYellow
End If


Picture2.Circle (MAPAx + 78, MAPAy + 385), 50
MAPAxChk = MAPAx
MAPAyChk = MAPAy
End If
End If
End If
End If
If CursorPointScaleX > MAPAx + 390 - 30 Then
If CursorPointScaleY > MAPAy + 195 - 30 Then
If CursorPointScaleX < MAPAx + 390 + 60 Then
If CursorPointScaleY < MAPAy + 195 + 60 Then
whichCastle = 7
Picture2.ForeColor = vbBlue
If cASTLEmASTER(whichCastle) = 1 Then Picture2.ForeColor = vbYellow

If Colour = "White" Then
Picture2.ForeColor = vbBlue
Else
Picture2.ForeColor = vbYellow
End If


Picture2.Circle (MAPAx + 390, MAPAy + 195), 50
MAPAxChk = MAPAx
MAPAyChk = MAPAy
End If
End If
End If
End If
If CursorPointScaleX > MAPAx + 385 - 30 Then
If CursorPointScaleY > MAPAy + 515 - 30 Then
If CursorPointScaleX < MAPAx + 385 + 60 Then
If CursorPointScaleY < MAPAy + 515 + 60 Then
whichCastle = 8
Picture2.ForeColor = vbBlue
If cASTLEmASTER(whichCastle) = 1 Then Picture2.ForeColor = vbYellow



If Colour = "White" Then
Picture2.ForeColor = vbBlue
Else
Picture2.ForeColor = vbYellow
End If
Picture2.Circle (MAPAx + 385, MAPAy + 515), 50
MAPAxChk = MAPAx
MAPAyChk = MAPAy
End If
End If
End If
End If






res = AlphaBlend(Picture2.hdc, MAPAx + 100, MAPAy + 350, ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha4C, 0, 0, 128, 128, LBF)
res = AlphaBlend(Picture2.hdc, MAPAx + 270, MAPAy + 180, ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha5C, 0, 0, 128, 128, LBF)
res = AlphaBlend(Picture2.hdc, MAPAx + 320, MAPAy + 370, ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha6C, 0, 0, 128, 128, LBF)
End If
If whichMap = 0 And menuON = False Then 'MAP2-KINGS


If CursorPointScaleX > MAPAx + 265 - 20 Then
If CursorPointScaleY > MAPAy + 525 - 20 Then
If CursorPointScaleX < MAPAx + 265 + 40 Then
If CursorPointScaleY < MAPAy + 525 + 40 Then
whichCastle = 3
Picture2.ForeColor = vbBlue
If cASTLEmASTER(whichCastle) = 1 Then Picture2.ForeColor = vbYellow


If Colour = "White" Then
Picture2.ForeColor = vbBlue
Else
Picture2.ForeColor = vbYellow
End If
Picture2.Circle (MAPAx + 265, MAPAy + 525), 50
MAPAxChk = MAPAx
MAPAyChk = MAPAy
End If
End If
End If
End If
If CursorPointScaleX > MAPAx + 130 - 20 Then
If CursorPointScaleY > MAPAy + 205 - 20 Then
If CursorPointScaleX < MAPAx + 130 + 40 Then
If CursorPointScaleY < MAPAy + 205 + 40 Then
whichCastle = 4
Picture2.ForeColor = vbBlue
If cASTLEmASTER(whichCastle) = 1 Then Picture2.ForeColor = vbYellow
If Colour = "White" Then
Picture2.ForeColor = vbBlue
Else
Picture2.ForeColor = vbYellow
End If

Picture2.Circle (MAPAx + 130, MAPAy + 205), 50
MAPAxChk = MAPAx
MAPAyChk = MAPAy
End If
End If
End If
End If
If CursorPointScaleX > MAPAx + 370 - 20 Then
If CursorPointScaleY > MAPAy + 200 - 20 Then
If CursorPointScaleX < MAPAx + 370 + 40 Then
If CursorPointScaleY < MAPAy + 200 + 40 Then
whichCastle = 5
Picture2.ForeColor = vbBlue
If cASTLEmASTER(whichCastle) = 1 Then Picture2.ForeColor = vbYellow
If Colour = "White" Then
Picture2.ForeColor = vbBlue
Else
Picture2.ForeColor = vbYellow
End If
Picture2.Circle (MAPAx + 370, MAPAy + 200), 50
MAPAxChk = MAPAx
MAPAyChk = MAPAy
End If
End If
End If
End If




res = AlphaBlend(Picture2.hdc, MAPAx + 150, MAPAy + 400, ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha7C, 0, 0, 128, 128, LBF)
res = AlphaBlend(Picture2.hdc, MAPAx + 120, MAPAy + 220, ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha8C, 0, 0, 128, 128, LBF)
res = AlphaBlend(Picture2.hdc, MAPAx + 320, MAPAy + 210, ScreenH \ SpriteDiv, ScreenH \ SpriteDiv, HAlpha9C, 0, 0, 128, 128, LBF)

End If




With rct
    .Left = (Picture2.ScaleWidth \ 2 + Picture2.ScaleWidth \ 10) + 30
    .Right = Picture2.ScaleWidth
    .Top = (Picture2.ScaleWidth \ 35) + 34
    .Bottom = Picture2.ScaleHeight
End With
 Picture2.ForeColor = RGB(75, 0, 0)
 
Dim RelativeMapNr


If whichMap = 1 Then RelativeMapNr = 1
If whichMap = 0 Then RelativeMapNr = 2
If whichMap = 2 Then RelativeMapNr = 3


If menuON = False Then

If Boardselected <> 0 Then

Drawed = "Map " + Str(RelativeMapNr) + " Board " + Str(Boardselected) + " " + Pola3(Boardselected - 1) + Chr(13) + "" + "" + Chr(13) + "" + Chr(13) + "" + Chr(13) + _
Chr(13) + Chr(13) + "" + "" + Chr(13) + "" + Chr(13) + "" + Chr(13) + _
Chr(13) + Chr(13) + "" + "" + Chr(13) + "" + Chr(13) + "" + Chr(13) + _
Chr(13) + Chr(13) + "" + "" + Chr(13) + "" + Chr(13) + Chr(13)

If Boardselected = 1 And Pola3(Boardselected - 1) = "Open" Then
Drawed = Drawed + "                                         Invite" + Chr(13) + _
"Back"
End If
If Boardselected = 1 And Pola3(Boardselected - 1) = "Started" Then
Drawed = Drawed + "                                    Surrender" + Chr(13) + _
"Back"
End If

If Boardselected <> 1 And Pola3(Boardselected - 1) = "Open" Then
Drawed = Drawed + "                                         Siege" + Chr(13) + _
"Back"
End If
If Boardselected <> 1 And Pola3(Boardselected - 1) = "Started" Then
Drawed = Drawed + "                                    Surrender" + Chr(13) + _
"Back"
End If

If Boardselected <> 0 And Pola3(Boardselected - 1) = "Closed" Then
Drawed = Drawed + "                                  " + Chr(13) + _
"Back"
End If




DrawText Picture2.hdc, Drawed, -1, rct, DT_WORDBREAK

Else
Drawed = "Map " + Str(RelativeMapNr) + Chr(13) + "" + "" + Chr(13) + "" + Chr(13) + "" + Chr(13) + _
Chr(13) + Chr(13) + "" + "" + Chr(13) + "" + Chr(13) + "" + Chr(13) + _
Chr(13) + Chr(13) + "" + "" + Chr(13) + "" + Chr(13) + "" + Chr(13) + _
Chr(13) + Chr(13) + "" + "" + Chr(13) + "" + Chr(13) + Chr(13) + "" + Chr(13) + _
"Back"

DrawText Picture2.hdc, Drawed, -1, rct, DT_WORDBREAK
End If

Else


Dim ktmapa
If whichMap = 0 Then ktmapa = 2
If whichMap = 1 Then ktmapa = 1
If whichMap = 2 Then ktmapa = 3
If whichMap = 3 Then ktmapa = 2
'ktmapa = whichMap
 
Drawed = "Chess Quest" + Chr(13) + "" + "" + Chr(13) + "" + Chr(13) + "" + Chr(13) + _
 "See maps" + Chr(13) + "Map selected " + Str(ktmapa) + Chr(13) _
  + "" + Chr(13) _
  + "Player 1 Colour " + Colour + Chr(13) _
 + "" + Chr(13) _
 + "Board 1  - " + Pola3(0) + Chr(13) _
 + "Board 2 - " + Pola3(1) + Chr(13) _
 + "Board 3 - " + Pola3(2) + Chr(13) _
 + "Board 4 - " + Pola3(3) + Chr(13) _
 + "Board 5 - " + Pola3(4) + Chr(13) _
 + "Board 6 - " + Pola3(5) + Chr(13) _
 + "Board 7 - " + Pola3(6) + Chr(13) _
 + "Board 8 - " + Pola3(7) + Chr(13) _
 + "Board 9 - " + Pola3(8) + Chr(13) _
 + Chr(13) _
            + "                                           Exit" + Chr(13) _
+ "Save        Load "
If PercentLoaded = 100 Then PercentLoaded = 0
If PercentLoaded <> 0 Then Drawed = Drawed + Str(Int(PercentLoaded)) + "%"





DrawText Picture2.hdc, Drawed, -1, rct, DT_WORDBREAK



End If

With rct
    .Left = 0 '(Picture2.ScaleWidth \ 2 + Picture2.ScaleWidth \ 10) + 30
    .Right = Picture2.ScaleWidth
    .Top = 0 '(Picture2.ScaleWidth \ 35) + 34
    .Bottom = Picture2.ScaleHeight
End With
 Picture2.ForeColor = RGB(175, 220, 175)
Drawed = ""
If SquareLineDrawed And WantSelect Then

'FieldSymbol =""

If TableAbsoluteY = 0 Then FieldSymbol = "a"
If TableAbsoluteY = 1 Then FieldSymbol = "b"
If TableAbsoluteY = 2 Then FieldSymbol = "c"
If TableAbsoluteY = 3 Then FieldSymbol = "d"
If TableAbsoluteY = 4 Then FieldSymbol = "e"
If TableAbsoluteY = 5 Then FieldSymbol = "f"
If TableAbsoluteY = 6 Then FieldSymbol = "g"
If TableAbsoluteY = 7 Then FieldSymbol = "h"


If TableAbsoluteX = 0 Then FieldSymbol = FieldSymbol + "1"
If TableAbsoluteX = 1 Then FieldSymbol = FieldSymbol + "2"
If TableAbsoluteX = 2 Then FieldSymbol = FieldSymbol + "3"
If TableAbsoluteX = 3 Then FieldSymbol = FieldSymbol + "4"
If TableAbsoluteX = 4 Then FieldSymbol = FieldSymbol + "5"
If TableAbsoluteX = 5 Then FieldSymbol = FieldSymbol + "6"
If TableAbsoluteX = 6 Then FieldSymbol = FieldSymbol + "7"
If TableAbsoluteX = 7 Then FieldSymbol = FieldSymbol + "8"



Drawed = Drawed + _
"Field selected XY: " + FieldSymbol
Else
FieldSymbol = ""
End If


DrawText Picture2.hdc, Drawed, -1, rct, DT_WORDBREAK







'If Boardselected = 0 Then
'HostName = ""
'GuestName = ""
'End If
'End If
'If Boardselected = 1 Then
'HostName = "Player 1"
'GuestName = ""
'End If
'End If
'If Boardselected = 2 Then
'HostName = ""
'GuestName = ""
'End If
'End If
'If Boardselected = 3 Then
'HostName = ""
'GuestName = ""
'End If
'End If
'If Boardselected = 4 Then
'HostName = ""
'GuestName = ""
'End If
'End If
'If Boardselected = 5 Then
'HostName = ""
'GuestName = ""
'End If
'End If
'If Boardselected = 6 Then
'HostName = ""
'GuestName = ""
'End If
'End If
'If Boardselected = 7 Then
'HostName = ""
'GuestName = ""
'End If
'End If
'If Boardselected = 8 Then
'HostName = ""
'G 'uestName = ""
'E 'nd If
'E 'nd If
'If Boardselected = 9 Then
'H 'ostName = ""
'G 'uestName = ""
'E'nd If

'-----------------------------------






If RelativeMapNr = 1 Then
boardActive = True
Else
boardActive = False
End If
Picture2.DrawWidth = 5
Picture2.ForeColor = vbGreen
'Picture2.PSet (CursorPointScaleX, CursorPointScaleY)






'shown'shown'shown'shown'shown'shown'shown'shown
'shown'shown'shown'shown'shown'shown'shown'shown
'shown'shown'shown'shown'shown'shown'shown'shown
'shown'shown'shown'shown'shown'shown'shown'shown
'shown'shown'shown'shown'shown'shown'shown'shown
'shown'shown'shown'shown'shown'shown'shown'shown
'shown'shown'shown'shown'shown'shown'shown'shown
'shown'shown'shown'shown'shown'shown'shown'shown
'shown'shown'shown'shown'shown'shown'shown'shown'WITH SCALE

Picture4.PaintPicture Picture2.Image, 0, 0, ScaleWidth, ScaleHeight, 0, 0, Picture2.ScaleWidth, Picture2.ScaleHeight

'shown'shown'shown'shown'shown'shown'shown'shown
'shown'shown'shown'shown'shown'shown'shown'shown
'shown'shown'shown'shown'shown'shown'shown'shown
'shown'shown'shown'shown'shown'shown'shown'shown
'shown'shown'shown'shown'shown'shown'shown'shown
'shown'shown'shown'shown'shown'shown'shown'shown
'shown'shown'shown'shown'shown'shown'shown'shown
'shown'shown'shown'shown'shown'shown'shown'shown
'shown'shown'shown'shown'shown'shown'shown'shown


'check keypressed on console'check keypressed on console
'check keypressed on console'check keypressed on console
Dim loopkey
Label1.Caption = "keys pressed (form focus):"
For loopkey = 0 To 255
If keypressed(loopkey) = True Then
Label1.Caption = Label1.Caption + Str(loopkey)
End If
Next loopkey
'check keypressed on console'check keypressed on console
'check keypressed on console'check keypressed on console
'gamepd check values'gamepd check values'gamepd check values'gamepd check values'gamepd check values
'gamepd check values'gamepd check values'gamepd check values'gamepd check values'gamepd check values
'gamepd check values'gamepd check values'gamepd check values'gamepd check values'gamepd check values
'gamepd check values'gamepd check values'gamepd check values'gamepd check values'gamepd check values
Cls
If joyGetPosEx(JNum, JI) <> 0 Then
    Print "Joystick #"; CStr(JNum); " is not plugged in, or is not working."
Else
    With JI
        Print "X = "; CStr(.dwXpos)
        Print "Y = "; CStr(.dwYpos)
        Print "Z = "; CStr(.dwZpos)
        Print "R = "; CStr(.dwRpos)
        Print "U = "; CStr(.dwUpos)
        Print "V = "; CStr(.dwVpos)
        If .dwPOV < &HFFFF& Then
        Print "PovAngle = "; CStr(.dwPOV / 100)
        If .dwPOV / 100 = 270 Or .dwPOV / 100 = 315 Or .dwPOV / 100 = 225 Then
        Else
        End If
        If .dwPOV / 100 = 90 Or .dwPOV / 100 = 45 Or .dwPOV / 100 = 135 Then
        Else
        End If
        If .dwPOV / 100 = 0 Or .dwPOV / 100 = 315 Or .dwPOV / 100 = 45 Then
        Else
        End If
        If .dwPOV / 100 = 180 Or .dwPOV / 100 = 135 Or .dwPOV / 100 = 225 Then
        Else
        End If
        Else: Print "PovCentered"
        End If
        Print "ButtonsPressedCount = "; CStr(.dwButtonNumber)
        Print "ButtonBinaryFlags = "; CStr(.dwButtons)
        If .dwButtons = 1 Or .dwButtons = 5 Or .dwButtons = 3 Or .dwButtons = 7 Or .dwButtons = 9 Or .dwButtons = 11 Or .dwButtons = 13 Or .dwButtons = 15 Then
        Else
        End If
        If .dwButtons = 8 Or .dwButtons = 12 Or .dwButtons = 14 Or .dwButtons = 9 Or .dwButtons = 10 Or .dwButtons = 11 Or .dwButtons = 13 Or .dwButtons = 15 Then
        Else
        End If
       If .dwButtons = 2 Or .dwButtons = 6 Or .dwButtons = 7 Or .dwButtons = 3 Or .dwButtons = 10 Or .dwButtons = 11 Or .dwButtons = 14 Or .dwButtons = 15 Then
       Else
       End If
       If .dwButtons = 4 Or .dwButtons = 5 Or .dwButtons = 7 Or .dwButtons = 6 Or .dwButtons = 12 Or .dwButtons = 13 Or .dwButtons = 14 Or .dwButtons = 15 Then
       Else
       End If
        Picture1.Cls
        
        
        
        Picture1.Circle (.dwXpos / &HFFFF& * (Picture1.Width - 1), .dwYpos / &HFFFF& * (Picture1.Height - 1)), 2
    
    
    End With
End If
'gamepd check values'gamepd check values'gamepd check values'gamepd check values'gamepd check values
'gamepd check values'gamepd check values'gamepd check values'gamepd check values'gamepd check values
'gamepd check values'gamepd check values'gamepd check values'gamepd check values'gamepd check values
'gamepd check values'gamepd check values'gamepd check values'gamepd check values'gamepd check values
End Sub
Public Sub endProcedure()
'exit program'exit program'exit program'exit program
End
'exit program'exit program'exit program'exit program
End Sub


Public Sub bwx(PICTURETOGREY)


    Dim ca As COLORADJUSTMENT
    With PICTURETOGREY
       '' .AutoRedraw = True
        '.ScaleMode = vbPixels
        SetStretchBltMode .hdc, HALFTONE
        GetColorAdjustment .hdc, ca
        ca.caContrast = 0
        ca.caRedGreenTint = 0
        ca.caBrightness = 0
        ca.caIlluminantIndex = 0
        ca.caColorfulness = -100 'No colors!
        ca.caReferenceBlack = 1000
        ca.caReferenceWhite = 9000
        ca.caBrightness = 5
        SetColorAdjustment .hdc, ca
        StretchBlt .hdc, 0, 0, .ScaleWidth, .ScaleHeight, .hdc, 0, 0, .ScaleWidth, .ScaleHeight, vbSrcCopy
       '' .Refresh
        .ScaleMode = 3
    End With
End Sub
Private Sub boardSET(q)



















Dim a1
Dim b1
Dim c1
'Select Case q
'Case 0
'Pola(7, 7)
'left
On Error GoTo enderr
For c1 = 0 To 8
For a1 = 0 To 7
For b1 = 0 To 7
Next b1
Next a1
Next c1

For c1 = 0 To 8

a1 = 1 'druga kolumna
b1 = 0: Pola2(c1, a1, b1) = "HAlpha6"
b1 = 1: Pola2(c1, a1, b1) = "HAlpha6"
b1 = 2: Pola2(c1, a1, b1) = "HAlpha6"
b1 = 3: Pola2(c1, a1, b1) = "HAlpha6"
b1 = 4: Pola2(c1, a1, b1) = "HAlpha6"
b1 = 5: Pola2(c1, a1, b1) = "HAlpha6"
b1 = 6: Pola2(c1, a1, b1) = "HAlpha6"
b1 = 7: Pola2(c1, a1, b1) = "HAlpha6"
a1 = 0 'pierwsza kolumna
b1 = 0: Pola2(c1, a1, b1) = "HAlpha5"
b1 = 1: Pola2(c1, a1, b1) = "HAlpha4"
b1 = 2: Pola2(c1, a1, b1) = "HAlpha3"
b1 = 3: Pola2(c1, a1, b1) = "HAlpha"
b1 = 4: Pola2(c1, a1, b1) = "HAlpha2"
b1 = 5: Pola2(c1, a1, b1) = "HAlpha3"
b1 = 6: Pola2(c1, a1, b1) = "HAlpha4"
b1 = 7: Pola2(c1, a1, b1) = "HAlpha5"
'right
a1 = 6 'druga kolumna
b1 = 0: Pola2(c1, a1, b1) = "HAlpha6B"
b1 = 1: Pola2(c1, a1, b1) = "HAlpha6B"
b1 = 2: Pola2(c1, a1, b1) = "HAlpha6B"
b1 = 3: Pola2(c1, a1, b1) = "HAlpha6B"
b1 = 4: Pola2(c1, a1, b1) = "HAlpha6B"
b1 = 5: Pola2(c1, a1, b1) = "HAlpha6B"
b1 = 6: Pola2(c1, a1, b1) = "HAlpha6B"
b1 = 7: Pola2(c1, a1, b1) = "HAlpha6B"
a1 = 7 'pierwsza kolumna
b1 = 0: Pola2(c1, a1, b1) = "HAlpha5B"
b1 = 1: Pola2(c1, a1, b1) = "HAlpha4B"
b1 = 2: Pola2(c1, a1, b1) = "HAlpha3B"
b1 = 3: Pola2(c1, a1, b1) = "HAlphaB"
b1 = 4: Pola2(c1, a1, b1) = "HAlpha2B"
b1 = 5: Pola2(c1, a1, b1) = "HAlpha3B"
b1 = 6: Pola2(c1, a1, b1) = "HAlpha4B"
b1 = 7: Pola2(c1, a1, b1) = "HAlpha5B"
For a1 = 2 To 5
b1 = 0: Pola2(c1, a1, b1) = ""
b1 = 1: Pola2(c1, a1, b1) = ""
b1 = 2: Pola2(c1, a1, b1) = ""
b1 = 3: Pola2(c1, a1, b1) = ""
b1 = 4: Pola2(c1, a1, b1) = ""
b1 = 5: Pola2(c1, a1, b1) = ""
b1 = 6: Pola2(c1, a1, b1) = ""
b1 = 7: Pola2(c1, a1, b1) = ""
Next
'Case 1
Next
Exit Sub
enderr:
MsgBox ""
End
End Sub

Private Sub CastleWall(a)
If a = 0 Then Image2.Picture = Image3(0).Picture
If a = 1 Then Image2.Picture = Image3(1).Picture
If a = 2 Then Image2.Picture = Image3(2).Picture
If a = 3 Then Image2.Picture = Image3(3).Picture
If a = 4 Then Image2.Picture = Image3(4).Picture
If a = 5 Then Image2.Picture = Image3(8).Picture
If a = 6 Then Image2.Picture = Image3(9).Picture
If a = 7 Then Image2.Picture = Image3(10).Picture
If a = 8 Then Image2.Picture = Image3(11).Picture

End Sub
Private Sub Form_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
WantSelect = True

'If SquareLineDrawed Then MsgBox "Can't move piece"

'If Pola3(Boardselected - 1) <> "Closed" Then
PercentLoaded = 0
On Error Resume Next















Dim RelativeMapNr
'------------------------------------------------------------------------------'
If menuON = True Then
If CursorPointScaleX >= 860 And CursorPointScaleX <= 1560 Then
If CursorPointScaleY >= 290 And CursorPointScaleY <= 340 Then
If Colour = "White" Then
Colour = "Black"
Else
Colour = "White"
End If
 mciSendString "setaudio songNumber2 volume to " & 1200, vbNullString, 0, 0
 retVal = mciSendString("play songNumber2 from 1", vbNullString, 0, 0)
Exit Sub
End If
End If
End If
'If menuON = True And CursorPointScaleX >= 860 And CursorPointScaleX <= 1000 And CursorPointScaleY >= 405 And CursorPointScaleY <= 505 Then
'LoadGame (1)
'If Color = "White" Then
'Color = "Black"
'Else
'Color = "White"
'End If
' mciSendString "setaudio songNumber2 volume to " & 1200, vbNullString, 0, 0
' retVal = mciSendString("play songNumber2 from 1", vbNullString, 0, 0)
' Exit Sub
' End If
 

If menuON = True And CursorPointScaleX >= 1007 And CursorPointScaleX <= 1089 And CursorPointScaleY >= 705 Then
LoadGame (1)

 mciSendString "setaudio songNumber2 volume to " & 1200, vbNullString, 0, 0
 retVal = mciSendString("play songNumber2 from 1", vbNullString, 0, 0)
 Exit Sub
 
End If
If menuON = True And CursorPointScaleX >= 860 And CursorPointScaleX <= 960 And CursorPointScaleY >= 705 Then
SaveGame

 mciSendString "setaudio songNumber2 volume to " & 1200, vbNullString, 0, 0
 retVal = mciSendString("play songNumber2 from 1", vbNullString, 0, 0)
 Exit Sub
End If




If menuON = False And CursorPointScaleX >= 1200 And CursorPointScaleY >= 680 Then
If Pola3(Boardselected - 1) = "Open" Then
Pola3(Boardselected - 1) = "Started":

 mciSendString "setaudio songNumber2 volume to " & 1200, vbNullString, 0, 0
 retVal = mciSendString("play songNumber2 from 1", vbNullString, 0, 0)
HostName = ""
GuestName = ""
If Pola3(Boardselected - 1) = "Started" Then

If Boardselected = 1 Then
HostName = "Player 1"
End If
'End If
If Boardselected = 2 Then
HostName = "Player 2"
GuestName = "Player 1"
End If
If Boardselected = 3 Then
HostName = "Player 3"
GuestName = "Player 1"
End If
If Boardselected = 4 Then
HostName = "Player 4"
GuestName = "Player 1"
End If
'End If
If Boardselected = 5 Then
HostName = "Player 5"
GuestName = "Player 1"
End If
If Boardselected = 6 Then
HostName = "Player 6"
GuestName = "Player 1"
End If

If Boardselected = 7 Then
HostName = "Player 7"
GuestName = "Player 1"
End If
'End If
If Boardselected = 8 Then
HostName = "Player 8"
GuestName = "Player 1"
End If
If Boardselected = 9 Then
HostName = "Player 9"
GuestName = "Player 1"
End If






End If






Exit Sub

End If






End If

If menuON = False And CursorPointScaleX >= 1200 And CursorPointScaleY >= 680 Then
If Pola3(Boardselected - 1) = "Started" Then
Pola3(Boardselected - 1) = "Open":
 mciSendString "setaudio songNumber2 volume to " & 1200, vbNullString, 0, 0
 retVal = mciSendString("play songNumber2 from 1", vbNullString, 0, 0)


'------------------------------------------------------------------------------'






Exit Sub
End If
End If




If menuON = True And CursorPointScaleX >= 1200 And CursorPointScaleY >= 680 Then
 mciSendString "setaudio songNumber2 volume to " & 1200, vbNullString, 0, 0
 retVal = mciSendString("play songNumber2 from 1", vbNullString, 0, 0)
 
 
'retVal = mciSendString("stop songNumber4", vbNullString, 0, 0)
'retVal = mciSendString("stop songNumber5", vbNullString, 0, 0)
'retVal = mciSendString("stop songNumber6", vbNullString, 0, 0)
'retVal = mciSendString("stop songNumber7", vbNullString, 0, 0)
'retVal = mciSendString("stop songNumber1", vbNullString, 0, 0)
'retVal = mciSendString("stop songNumber2", vbNullString, 0, 0)
'retVal = mciSendString("stop songNumber3", vbNullString, 0, 0)
'retVal = mciSendString("stop songNumber8", vbNullString, 0, 0)
 
 
' If Music(3) = False And RelativeMapNr = 0 Then

' retVal = mciSendString("stop songNumber4", vbNullString, 0, 0)
'  retVal = mciSendString("stop songNumber5", vbNullString, 0, 0)
'   retVal = mciSendString("stop songNumber6", vbNullString, 0, 0)
'    retVal = mciSendString("stop songNumber7", vbNullString, 0, 0)
' mciSendString "setaudio songNumber4 volume to " & 1200, vbNullString, 0, 0
' retVal = mciSendString("play songNumber4 from 1", vbNullString, 0, 0)
' Music(1) = False
' Music(0) = False
' Music(2) = False'
 'Music(3) = False
'Music(3) = True
'End If

'If Music(2) = False And RelativeMapNr = 3 Then
' retVal = mciSendString("stop songNumber4", vbNullString, 0, 0)
'  retVal = mciSendString("stop songNumber5", vbNullString, 0, 0)
'   retVal = mciSendString("stop songNumber6", vbNullString, 0, 0)
'    retVal = mciSendString("stop songNumber7", vbNullString, 0, 0)
' mciSendString "setaudio songNumber4 volume to " & 1200, vbNullString, 0, 0
' retVal = mciSendString("play songNumber5 from 1", vbNullString, 0, 0)
' Music(0) = False
' Music(1) = False
' Music(2) = True
' Music(3) = False''

'End If
'If Music(1) = False And RelativeMapNr = 2 Then
' retVal = mciSendString("stop songNumber4", vbNullString, 0, 0)
'  retVal = mciSendString("stop songNumber5", vbNullString, 0, 0)
'   retVal = mciSendString("stop songNumber6", vbNullString, 0, 0)
'    retVal = mciSendString("stop songNumber7", vbNullString, 0, 0)
' mciSendString "setaudio songNumber4 volume to " & 1200, vbNullString, 0, 0
' retVal = mciSendString("play songNumber6 from 1", vbNullString, 0, 0)
' Music(1) = True
' Music(0) = False
' Music(2) = False
' Music(3) = False
'End If
'If Music(0) = False And RelativeMapNr = 1 Then
' retVal = mciSendString("stop songNumber4", vbNullString, 0, 0)
'  retVal = mciSendString("stop songNumber5", vbNullString, 0, 0)
'   retVal = mciSendString("stop songNumber6", vbNullString, 0, 0)
'    retVal = mciSendString("stop songNumber7", vbNullString, 0, 0)
' mciSendString "setaudio songNumber4 volume to " & 1200, vbNullString, 0, 0
' retVal = mciSendString("play songNumber7 from 1", vbNullString, 0, 0)
' Music(0) = True
' Music(1) = False
' Music(2) = False
' Music(3) = False
'End If

 
 
 
 
 
 
 
 
 
 

retVal = mciSendString("pause songNumber1", vbNullString, 0, 0)
retVal = mciSendString("pause songNumber2", vbNullString, 0, 0)
retVal = mciSendString("pause songNumber3", vbNullString, 0, 0)
retVal = mciSendString("close songNumber4", vbNullString, 0, 0)
  retVal = mciSendString("close songNumber5", vbNullString, 0, 0)
retVal = mciSendString("close songNumber6", vbNullString, 0, 0)
retVal = mciSendString("close songNumber7", vbNullString, 0, 0)

retVal = mciSendString("play songNumber3 from 1", vbNullString, 0, 0)

'SaveGame


End


End If
'End If





If CursorPointScaleX >= 863 Then


'Image3(0).Picture = LoadPicture(App.Path + "\backgrounds\VC_CaliforniaCastles_Hero_CastelloDiAmorosa_Supplied_CastleWideFallFull_1280x640_0.jpg")
'Image3(1).Picture = LoadPicture(App.Path + "\backgrounds\Scotand-Edinburgh_Castle-thinkstockphotos.com-iStockphoto.jpg")
'Image3(2).Picture = LoadPicture(App.Path + "\backgrounds\Netherlands_Castles_Landscape_design_Castle_De_539232_1280x854.jpg")
'Image3(3).Picture = LoadPicture(App.Path + "\backgrounds\170801123333-spis-castle-full-169.jpg")
'Image3(4).Picture = LoadPicture(App.Path + "\backgrounds\1200px-Burg_Hohenzollern_ak.jpg")
repeatsearch:
Randomize
'bacgrnr = Int(Rnd * 5)


'whichWall = whichWall + 1
'If whichWall > 4 Then whichWall = 0


'bacgrnr = whichWall
'Image2.Picture = Image3(bacgrnr).Picture


CastleWall (whichCastle)



'bacgrnr2 = Int(Rnd * 5)
'whichWall = bacgrnr
'Image2.Picture = Image3(bacgrnr).Picture
'Image4.Picture = Image3(bacgrnr2).Picture
Dim rnd5



If whichCastle <> -1 Then
'rnd5 = 1
'whichMap = 1
IsboardActive = True

Else







If menuON = True And CursorPointScaleX >= 863 And CursorPointScaleY >= 200 And CursorPointScaleY <= 270 Then
menuON = False

'retVal = mciSendString("stop songNumber4", vbNullString, 0, 0)
'retVal = mciSendString("stop songNumber5", vbNullString, 0, 0)
'retVal = mciSendString("stop songNumber6", vbNullString, 0, 0)
'retVal = mciSendString("stop songNumber7", vbNullString, 0, 0)





'If whichMap = 1 Then RelativeMapNr = 1
'If whichMap = 0 Then RelativeMapNr = 2
'If whichMap = 2 Then RelativeMapNr = 3

'If RelativeMapNr = 1 Then
'retVal = mciSendString("play songNumber5 from 1 repeat", vbNullString, 0, 0)
'End If
'If RelativeMapNr = 2 Then
'retVal = mciSendString("play songNumber7 from 1 repeat", vbNullString, 0, 0)
'End If
'If RelativeMapNr = 3 Then
'retVal = mciSendString("play songNumber6 from 1 repeat", vbNullString, 0, 0)
'End If



 mciSendString "setaudio songNumber1 volume to " & 1000, vbNullString, 0, 0
 retVal = mciSendString("play songNumber1 from 1", vbNullString, 0, 0)
End If
If menuON = False And CursorPointScaleX >= 863 And CursorPointScaleX <= 970 And CursorPointScaleY >= 720 Then
menuON = True


'If whichMap = 1 Then RelativeMapNr = 1
'If whichMap = 0 Then RelativeMapNr = 2
'If whichMap = 2 Then RelativeMapNr = 3

'If RelativeMapNr = 1 Then
'retVal = mciSendString("play songNumber4 from 1 repeat", vbNullString, 0, 0)
'End If
'If RelativeMapNr = 2 Then
'retVal = mciSendString("play songNumber4 from 1 repeat", vbNullString, 0, 0)
'End If
'If RelativeMapNr = 3 Then
'retVal = mciSendString("play songNumber5 from 1 repeat", vbNullString, 0, 0)
'End If









 mciSendString "setaudio songNumber2 volume to " & 600, vbNullString, 0, 0
 retVal = mciSendString("play songNumber2 from 1", vbNullString, 0, 0)
End If

If CursorPointScaleX >= 863 And CursorPointScaleY <= 120 And menuON = False Then



'If CursorPointScaleX >= 863 Then
hillsPICTURE = hillsPICTURE + 1
'End If
'retVal = mciSendString("stop songNumber4", vbNullString, 0, 0)
'retVal = mciSendString("stop songNumber5", vbNullString, 0, 0)
'retVal = mciSendString("stop songNumber6", vbNullString, 0, 0)
'retVal = mciSendString("stop songNumber7", vbNullString, 0, 0)





'If whichMap = 1 Then RelativeMapNr = 1
'If whichMap = 0 Then RelativeMapNr = 2
'If whichMap = 2 Then RelativeMapNr = 3

'If RelativeMapNr = 1 Then
'retVal = mciSendString("play songNumber5 from 1 repeat", vbNullString, 0, 0)
'End If
'If RelativeMapNr = 2 Then
'retVal = mciSendString("play songNumber7 from 1 repeat", vbNullString, 0, 0)
'End If
'If RelativeMapNr = 3 Then
'retVal = mciSendString("play songNumber6 from 1 repeat", vbNullString, 0, 0)
'End If



If hillsPICTURE > 2 Then hillsPICTURE = 0


If menuON = True Then


Else






If whichMap = 2 Then
whichMap = 1
IsboardActive = False
'retVal = mciSendString("play songNumber5 from 1 repeat", vbNullString, 0, 0)
Else
If whichMap = 0 Then
whichMap = 2:
IsboardActive = False
'retVal = mciSendString("play songNumber7 from 1 repeat", vbNullString, 0, 0)
End If
If whichMap = 1 Then
whichMap = 0:
IsboardActive = False
'retVal = mciSendString("play songNumber6 from 1 repeat", vbNullString, 0, 0)
End If
End If
End If



End If



If menuON = False Then
Image2.Picture = Image3(5 + hillsPICTURE).Picture


Else

End If


IsboardActive = False
End If

rnd5 = whichMap
'rnd5 = RND5b

If menuON = False Then





If rnd5 = 0 Then
piso.Picture = LoadPicture(App.Path + "\backgrounds\map.bmp")
ElseIf rnd5 = 1 Then
piso.Picture = LoadPicture(App.Path + "\backgrounds\map0.bmp")
ElseIf rnd5 = 2 Then
piso.Picture = LoadPicture(App.Path + "\backgrounds\map2.bmp")
Else






End If
Else
piso.Picture = LoadPicture(App.Path + "\backgrounds\map4.bmp")
End If




'empty'piso.Picture = LoadPicture(App.Path + "\backgrounds\map4.bmp")

'If bacgrnr = oldWall Or rnd5 = Oldmap Then GoTo repeatsearch
oldWall = bacgrnr
Oldmap = rnd5
MousePressed = Button
Boardselected = 0
If whichCastle <> -1 Then
Boardselected = whichCastle + 1
Else
If menuON = False Then

'If CursorPointScaleX > 1200 Then
 mciSendString "setaudio songNumber1 volume to " & 1200, vbNullString, 0, 0
 retVal = mciSendString("play songNumber1 from 1", vbNullString, 0, 0)
 'End If
 
 End If
End If


End If




If Boardselected <> 0 Then
If CursorPointScaleX > 855 Then
 mciSendString "setaudio songNumber2 volume to " & 1200, vbNullString, 0, 0
 retVal = mciSendString("play songNumber2 from 1", vbNullString, 0, 0)
End If
Else
' mciSendString "setaudio songNumber1 volume to " & 1200, vbNullString, 0, 0
' retVal = mciSendString("play songNumber1 from 1", vbNullString, 0, 0)

End If

' retVal = mciSendString("stop songNumber4", vbNullString, 0, 0)
' retVal = mciSendString("stop songNumber5", vbNullString, 0, 0)
' retVal = mciSendString("stop songNumber6", vbNullString, 0, 0)
' retVal = mciSendString("stop songNumber7", vbNullString, 0, 0)
 
 
'If whichMap = 0 Then
'   retVal = mciSendString("play songNumber4", vbNullString, 0, 0)
'End If
'If menuON = True Then
'retVal = mciSendString("play songNumber4", vbNullString, 0, 0)

'End If
 
'If whichMap = 1 Then
'   retVal = mciSendString("play songNumber5", vbNullString, 0, 0)
'End If
'If whichMap = -1 Then

'retVal = mciSendString("pause songNumber3", vbNullString, 0, 0)
'retVal = mciSendString("pause songNumber2", vbNullString, 0, 0)
'retVal = mciSendString("pause songNumber1", vbNullString, 0, 0)
'End If
'If whichMap = 0 Then

'   retVal = mciSendString("play songNumber6", vbNullString, 0, 0)
'End If
'If whichMap = 2 Then

'   retVal = mciSendString("play songNumber7", vbNullString, 0, 0)
'End If
If CursorPointScaleX >= 863 Then

'If CursorPointScaleY < 300 Or CursorPointScaleY > 800 Or menuON = False Then
retVal = mciSendString("pause songNumber4", vbNullString, 0, 0)
retVal = mciSendString("pause songNumber5", vbNullString, 0, 0)
retVal = mciSendString("pause songNumber6", vbNullString, 0, 0)
retVal = mciSendString("pause songNumber7", vbNullString, 0, 0)
'End If

If menuON = False Then
If whichMap = 1 Then
retVal = mciSendString("play songNumber5 from 1 repeat", vbNullString, 0, 0)
End If
If whichMap = 2 Then
retVal = mciSendString("play songNumber7 from 1 repeat", vbNullString, 0, 0)
End If
If whichMap = 0 Then
retVal = mciSendString("play songNumber6 from 1 repeat", vbNullString, 0, 0)
End If
Else
'If menuON = True Then
If menuON = True Then 'And CursorPointScaleX < 860
'If CursorPointScaleY < 300 Or CursorPointScaleY > 700 Then
retVal = mciSendString("play songNumber4 from 1 repeat", vbNullString, 0, 0)
'End If

End If
'End If
End If
End If
End Sub
Private Sub LoadGame(boot)
On Error Resume Next
Dim ReadValue As Variant
Dim ReadValue2 As String
Dim ReadValue3 As Boolean
'boardSET (0)
If boot = 1 Then









freFILE = FreeFile 'default
Open App.Path + "\pieces\tables1.DAT" For Random As freFILE
Get freFILE, 1, ReadValue
whichMap = ReadValue
Get freFILE, 2, ReadValue
If whichMap = 0 Then hillsPICTURE = 1
If whichMap = 1 Then hillsPICTURE = 0
If whichMap = 2 Then hillsPICTURE = 2 '= ReadValue
Get freFILE, 3, ReadValue2
Colour = ReadValue2

'Get freFILE, 4, ReadValue
'Get freFILE, 5, ReadValue
'Get freFILE, 6, ReadValue
'Colour = ReadValue2
Get freFILE, 4, ReadValue
Pola3(0) = ReadValue
Get freFILE, 5, ReadValue
Pola3(1) = ReadValue
Get freFILE, 6, ReadValue
Pola3(2) = ReadValue
Get freFILE, 7, ReadValue
Pola3(3) = ReadValue
Get freFILE, 8, ReadValue
Pola3(4) = ReadValue
Get freFILE, 9, ReadValue
Pola3(5) = ReadValue
Get freFILE, 10, ReadValue
Pola3(6) = ReadValue
Get freFILE, 11, ReadValue
Pola3(7) = ReadValue
Get freFILE, 12, ReadValue
Pola3(8) = ReadValue





Dim c2, a2, b2 ', counters
'On Error Resume Next
Dim Counter877
Counter877 = 1
For c2 = 0 To 8
For a2 = 0 To 7
For b2 = 0 To 7
DoEvents
PercentLoaded = (c2 * 13) ' / 8 * (a2 + 1) '+a2+b2
If PercentLoaded > 100 Then PercentLoaded = 100
Debug.Print "PercentLoaded" + Str(PercentLoaded)

'Put freFILE, 8 + 4 + Counter, Trim(Pola2(c1, a1, b1))
'counters = counters + 1
Debug.Print Str(c2) '" '; board; " + Str(c2 + 1); "; counters; " + Str(counters)"
Get freFILE, 12 + Counter877, ReadValue
Counter877 = Counter877 + 1
'Pola2(c2, a2, b2) = ReadValue
Debug.Print "ReadValue " + Trim(ReadValue) '"Pola2(c2, a2, b2)" + Pola2(c2, a2, b2)
Next b2
Next a2
Next c2
'\Dim Counter
'Dim a1
'Dim b1
'Dim c1
'Counter = 1
'For c1 = 0 To 8
'For a1 = 0 To 7
'For b1 = 0 To 7
'Get freFILE, 12 + Counter, ReadValue
'Trim(Pola2(c1, a1, b1)) = ReadValue
'Counter = Counter + 1
'Next
'Next
'Next






Close freFILE



Else
'load saved
'freFILE = FreeFile 'default
'Open App.Path + "\pieces\tables2.DAT" For Random As freFILE
'Get freFILE, 1, ReadValue
'If Val(Trim(ReadValue)) = 1 Then whichMap = 1
'If Val(Trim(ReadValue)) = 0 Then whichMap = 2
'If Val(Trim(ReadValue)) = 2 Then whichMap = 0
'Get freFILE, 2, ReadValue
'hillsPICTURE = Val(ReadValue)
'If whichMap = 0 Then hillsPICTURE = 1
'If whichMap = 1 Then hillsPICTURE = 0
'If whichMap = 2 Then hillsPICTURE = 2 '= ReadValue



'G'et freFILE, 3, ReadValue
'If ReadValue = 0 Then Colour = "White"
'If ReadValue = 1 Then Colour = "Black"





'Close freFILE


'boardSET (1)

End If


End Sub
Private Sub SaveGame()
On Error Resume Next
Dim SaveValue As String
Dim Counter5
Dim a1
Dim b1
Dim c1
freFILE = FreeFile
Open App.Path + "\pieces\tables1.DAT" For Random As freFILE
Put freFILE, 1, whichMap
Put freFILE, 2, hillsPICTURE
'If Colour = "White" Then
SaveValue = Colour
Put freFILE, 3, SaveValue
'If Colour = "Black" Then Put freFILE, 3, 1
'Put freFILE, 4, SaveValue
'Put freFILE, 5, SaveValue
'Put freFILE, 6, SaveValue

Dim i
For i = 0 To 8
Put freFILE, i + 4, Trim(Pola3(i))
Next i

Dim c2, a2, b2, counters877b
On Error Resume Next
counters877b = 13
For c2 = 0 To 8
For a2 = 0 To 7
For b2 = 0 To 7
PercentLoaded = 0
PercentSaved = c2 * 13
If PercentSaved > 100 Then PercentSaved = 100
'Put freFILE, 8 + 4 + Counter, Trim(Pola2(c1, a1, b1))
counters877b = counters877b + 1
Debug.Print "counters877b " + Str(counters877b) '; " counters " + Str(counters)
Debug.Print "Pola2(c2, a2, b2)" + Pola2(c2, a2, b2)
Put freFILE, counters877b, Pola2(c2, a2, b2)
Next b2
Next a2
Next c2


Close freFILE
End Sub
