Attribute VB_Name = "Module1"
Public Type TGAHeader
    IDLength As Byte
    ColorMapType As Byte
    ImageType As Byte
    CMapStart As Integer
    CMapLength As Integer
    CMapDepth As Byte
    XOffset As Integer
    YOffset As Integer
    Width As Integer
    Height As Integer
    PixelDepth As Byte
    ImageDescriptor As Byte
End Type

Public Type BITMAPINFOHEADER
    biSize As Long
    biWidth As Long
    biHeight As Long
    biPlanes As Integer
    biBitCount As Integer
    biCompression As Long
    biSizeImage As Long
    biXPelsPerMeter As Long
    biYPelsPerMeter As Long
    biClrUsed As Long
    biClrImportant As Long
End Type

Public Type RGBQUAD
    rgbBlue As Byte
    rgbGreen As Byte
    rgbRed As Byte
    rgbReserved As Byte
End Type

Public Type BITMAPINFO
    bmiHeader As BITMAPINFOHEADER
    bmiColors As RGBQUAD
End Type

Public Type BLENDFUNCTION
    BlendOp As Byte
    BlendFlags As Byte
    SourceConstantAlpha As Byte
    AlphaFormat As Byte
End Type

Public Const DIB_RGB_COLORS = 0

Public Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)
Public Declare Function CreateCompatibleBitmap Lib "gdi32" (ByVal hdc As Long, ByVal nWidth As Long, ByVal nHeight As Long) As Long
Public Declare Function DeleteDC Lib "gdi32" (ByVal hdc As Long) As Long
Public Declare Function CreateSolidBrush Lib "gdi32" (ByVal crColor As Long) As Long
Public Declare Function AlphaBlend Lib "msimg32.dll" (ByVal hdc As Long, ByVal lInt As Long, ByVal lInt As Long, ByVal lInt As Long, ByVal lInt As Long, ByVal hdc As Long, ByVal lInt As Long, ByVal lInt As Long, ByVal lInt As Long, ByVal lInt As Long, ByVal BLENDFUNCT As Long) As Long
Public Declare Function Ellipse Lib "gdi32" (ByVal hdc As Long, ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long) As Long
Public Declare Function BitBlt Lib "gdi32" (ByVal hDestDC As Long, ByVal x As Long, ByVal y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, ByVal dwRop As Long) As Long

Public Declare Function CreateCompatibleDC Lib "gdi32" (ByVal hdc As Long) As Long
Public Declare Function CreateDIBSection Lib "gdi32" (ByVal hdc As Long, pBitmapInfo As BITMAPINFO, ByVal un As Long, lplpVoid As Long, ByVal handle As Long, ByVal dw As Long) As Long
Public Declare Function SelectObject Lib "gdi32" (ByVal hdc As Long, ByVal hObject As Long) As Long
Public Declare Function DeleteObject Lib "gdi32" (ByVal hObject As Long) As Long
Public Declare Function SetDIBits Lib "gdi32" (ByVal hdc As Long, ByVal hBitmap As Long, ByVal nStartScan As Long, ByVal nNumScans As Long, lpBits As Any, lpBI As BITMAPINFO, ByVal wUsage As Long) As Long
Public Declare Function GetCurrentObject Lib "gdi32" (ByVal hdc As Long, ByVal uObjectType As Long) As Long
Public Declare Function GetDIBits Lib "gdi32" (ByVal aHDC As Long, ByVal hBitmap As Long, ByVal nStartScan As Long, ByVal nNumScans As Long, lpBits As Any, lpBI As BITMAPINFO, ByVal wUsage As Long) As Long

'***** Loads a Alpha Tga file in to a Device *****
Public Function LOADTGA(File As String, PMult As Boolean) As Long
Dim i, res, hRDC, hRBM, Fail As Long
Dim TGAHdr As TGAHeader
Dim BMInfo As BITMAPINFO
Dim Bits() As RGBQUAD
    Open File For Binary Access Read As 1 'Open Binary file
        Get #1, , TGAHdr 'Get TGA header from file
        'We only want to load a 32 bit unindexed alpha tga file
        If Not (TGAHdr.IDLength = 0) Then Fail = 1 'Fail if file doesn't exist
        If Not (TGAHdr.ImageType = 2) Then Fail = 1 'if file is indexed
        If Not (TGAHdr.PixelDepth = 32) Then Fail = 1 'if not 32 bit pixels
        If Not (TGAHdr.ColorMapType = 0) Then Fail = 1 'if has pallet
        If Not (TGAHdr.ImageDescriptor = 8) Then Fail = 1 'if origin is not upper left
      If Fail = 0 Then 'Get Unindexed Bits, 32 bytes each
          ReDim Bits(0 To TGAHdr.Width * TGAHdr.Height - 1) 'Resize bit array, simple W*H
          For i = 0 To TGAHdr.Width * TGAHdr.Height - 1
              Get #1, , Bits(i)
          Next i
      End If
    Close
    If Fail = 0 Then 'Continue with operation
    'Alpha rgb pixels must have the same intensity as their alpha value.
    'If the TGA rgb pixels are not already set to the right intensity
    'we set them here. To acheive various effects try not using this.
      If PMult = True Then
          For i = 0 To TGAHdr.Width * TGAHdr.Height - 1
              multpixel Bits(i)
         Next i
      End If
        BMInfo.bmiHeader.biSize = 40 'Set the standard 32 bitmap header
        BMInfo.bmiHeader.biWidth = TGAHdr.Width
        BMInfo.bmiHeader.biHeight = TGAHdr.Height
        BMInfo.bmiHeader.biPlanes = 1
        BMInfo.bmiHeader.biBitCount = 32
        hRDC = CreateCompatibleDC(0) 'Create dc to store the alpha bitmap
        hRBM = CreateDIBSection(hRDC, BMInfo, DIB_RGB_COLORS, 0, 0, 0) 'Create the DIB
        'This is where we set the DIB bits using an array of RGBQUAD structures
        'This can be used to set any alpha bits needed
        res = SetDIBits(hRDC, hRBM, 0, BMInfo.bmiHeader.biHeight, Bits(0), BMInfo, DIB_RGB_COLORS)
        res = SelectObject(hRDC, hRBM) 'Select the bitmap into the DC
        res = DeleteObject(res) 'delete the original 1*1 bitmap
        LOADTGA = hRDC 'Return the DC handle
    End If
End Function

'***** Makes a Alpha bitmap device from 2 nonalpha devices *****
Public Function MAKEALPHAFromDC(hRGB As Long, hA As Long, W As Long, H As Long, PMult As Boolean) As Long
Dim i, j, res, hRDC, hRBM, BMrgb, BMa As Long
Dim BMInfo As BITMAPINFO
Dim Bits() As RGBQUAD
Dim BitsA() As RGBQUAD
    BMInfo.bmiHeader.biSize = 40 'Create the alpha format header
    BMInfo.bmiHeader.biWidth = W
    BMInfo.bmiHeader.biHeight = H
    BMInfo.bmiHeader.biPlanes = 1
    BMInfo.bmiHeader.biBitCount = 32
    BMrgb = GetCurrentObject(hRGB, 7) 'Get the bitmap from the RGB device
    BMa = GetCurrentObject(hA, 7) 'Get the bitmap from the Alpha device
    ReDim Bits(0 To W * H - 1) 'Resize both bit buffers
    ReDim BitsA(0 To W * H - 1)
    res = GetDIBits(hRGB, BMrgb, 0, H, Bits(0), BMInfo, DIB_RGB_COLORS) 'Get the bits
    res = GetDIBits(hA, BMa, 0, H, BitsA(0), BMInfo, DIB_RGB_COLORS) 'Get the bits
    'Set the alpha value equal to the red component of the Alpha device
    'Since the Bitmap should be gray scale the RGB components will be the same
    For i = 0 To W * H - 1
        If BitsA(i).rgbRed Then
        Bits(i).rgbReserved = BitsA(i).rgbRed
        End If
    Next i
    If PMult = True Then 'If pixels are not already set
        For i = 0 To W * H - 1
            multpixel Bits(i)
        Next i
    End If
    hRDC = CreateCompatibleDC(0) 'Create the alpha surface, same as above
    hRBM = CreateDIBSection(hRDC, BMInfo, DIB_RGB_COLORS, 0, 0, 0)
    res = SetDIBits(hRDC, hRBM, 0, BMInfo.bmiHeader.biHeight, Bits(0), BMInfo, DIB_RGB_COLORS)
    res = SelectObject(hRDC, hRBM)
    res = DeleteObject(res) 'Delete original bitmap
    MAKEALPHAFromDC = hRDC 'Return Alpha Device handle
End Function
Private Sub multpixel(P As RGBQUAD) 'Sets the intensity of the RGB value to the A value
Dim Rbuf, Gbuf, Bbuf, Abuf As Single
    Rbuf = P.rgbRed: Gbuf = P.rgbGreen: Bbuf = P.rgbBlue: Abuf = P.rgbReserved
    P.rgbRed = CByte(Rbuf * Abuf / 255)
    P.rgbGreen = CByte(Gbuf * Abuf / 255)
    P.rgbBlue = CByte(Bbuf * Abuf / 255)
End Sub
