VERSION 5.00
Begin VB.Form Form3 
   BackColor       =   &H00808080&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "LONGEVITY STATS"
   ClientHeight    =   8295
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   15855
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   8295
   ScaleWidth      =   15855
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command2 
      Caption         =   "Reset"
      Height          =   315
      Left            =   4320
      TabIndex        =   17
      Top             =   6720
      Width           =   1455
   End
   Begin VB.ListBox List5 
      BackColor       =   &H00C0C0C0&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   8220
      Left            =   1080
      Style           =   1  'Checkbox
      TabIndex        =   15
      Top             =   0
      Width           =   3015
   End
   Begin VB.ListBox List4 
      BackColor       =   &H00E0E0E0&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   8220
      Left            =   0
      Style           =   1  'Checkbox
      TabIndex        =   14
      Top             =   0
      Width           =   1095
   End
   Begin VB.ListBox List3 
      BackColor       =   &H00E0E0E0&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   238
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6060
      Left            =   6840
      TabIndex        =   10
      Top             =   120
      Width           =   1095
   End
   Begin VB.ListBox List2 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   238
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000003&
      Height          =   6060
      Left            =   8040
      TabIndex        =   8
      Top             =   120
      Width           =   1935
   End
   Begin VB.ListBox List1 
      BackColor       =   &H00C0C000&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   238
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6060
      Left            =   4320
      TabIndex        =   7
      Top             =   120
      Width           =   2415
   End
   Begin VB.TextBox Text2 
      Enabled         =   0   'False
      Height          =   1335
      Left            =   7440
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   4
      Top             =   6360
      Visible         =   0   'False
      Width           =   6735
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Next"
      Enabled         =   0   'False
      Height          =   255
      Left            =   11640
      TabIndex        =   2
      Top             =   5880
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Run"
      Height          =   255
      Left            =   4320
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   6360
      Width           =   1455
   End
   Begin VB.Timer Timer1 
      Interval        =   2000
      Left            =   12240
      Top             =   1680
   End
   Begin VB.TextBox Text1 
      Height          =   855
      Left            =   7080
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Text            =   "Form3.frx":0000
      Top             =   4200
      Visible         =   0   'False
      Width           =   8175
   End
   Begin VB.Label Label8 
      Caption         =   "Label8"
      Height          =   495
      Left            =   6120
      TabIndex        =   16
      Top             =   1200
      Visible         =   0   'False
      Width           =   3135
   End
   Begin VB.Image Image1 
      Height          =   495
      Left            =   7320
      Top             =   3120
      Width           =   1215
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "number"
      Height          =   495
      Left            =   4200
      TabIndex        =   13
      Top             =   2040
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "word"
      Height          =   495
      Left            =   1560
      TabIndex        =   12
      Top             =   2040
      Width           =   2415
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   "position on list"
      Height          =   495
      Left            =   0
      TabIndex        =   11
      Top             =   2040
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "App.Path + ""\SITES.txt"""
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   10080
      TabIndex        =   9
      Top             =   1080
      Width           =   5775
   End
   Begin VB.Label Label3 
      Caption         =   "probe 21"
      Height          =   495
      Left            =   10080
      TabIndex        =   6
      Top             =   7200
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.Label Label2 
      BackColor       =   &H00000080&
      BackStyle       =   0  'Transparent
      Caption         =   "Label2"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   238
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   10080
      TabIndex        =   5
      Top             =   720
      Width           =   5775
   End
   Begin VB.Label Label1 
      BackColor       =   &H00000080&
      BackStyle       =   0  'Transparent
      Caption         =   "website adress..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   238
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   735
      Left            =   10080
      TabIndex        =   3
      Top             =   120
      Width           =   11295
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "Form3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Declare Function URLDownloadToFile Lib "urlmon" Alias "URLDownloadToFileA" (ByVal pCaller As Long, ByVal szURL As String, ByVal szFileName As String, ByVal dwReserved As Long, ByVal lpfnCB As Long) As Long
Private Declare Function InternetOpen Lib "wininet" Alias "InternetOpenA" (ByVal sAgent As String, ByVal lAccessType As Long, ByVal sProxyName As String, ByVal sProxyBypass As String, ByVal lFlags As Long) As Long
Private Declare Function InternetCloseHandle Lib "wininet" (ByVal hInet As Long) As Integer
Const FreFile = 1
Dim URLline As Long
Dim Titles() As String
Dim ProbeStart
Dim ProbeTLIST(20000) As String
Dim probeCount(20000) As Long
Dim probeB(20000) As Boolean
Dim probeB2(40) As Long
Dim Fromlist As Long
Dim h2
Dim STEPnr As Long
Dim STASstep(20) As Boolean

Private Sub ReadLines()

On Error Resume Next
Dim MyLine As String
Open App.Path + "\sites.txt" For Input As #FreFile
    Do While Not EOF(1)
    If Text1.Text <> "" Then Text1.Text = Text1.Text + vbCrLf
        Line Input #FreFile, MyLine
        Text1.Text = Text1.Text + MyLine
    Loop
Close #FreFile
End Sub

Private Sub Check1_Click()
DoEvents
On Error Resume Next
If Check1.Value = 1 Then
Timer1.Enabled = True
Command1.Enabled = False
Else
Timer1.Enabled = False
Command1.Enabled = True
End If
End Sub

Private Sub Command1_Click()
DoEvents
On Error Resume Next
Timer1.Enabled = False


DoEvents
Randomize ProbeStart
Label1.Caption = Titles(URLline) + vbCrLf + Str(URLline)

On Error Resume Next
Kill App.Path + "\filename.txt"
InternetGetFile Titles(URLline), App.Path + "\filename.txt"


Dim intFile As Integer
Dim strData As String
intFile = FreeFile
Open App.Path + "\filename.txt" For Input As intFile
strData = Input(LOF(intFile), intFile)
'put strData in a textbox, a listbox, or any other control that will contain text values
Close intFile
'If URLline <> 17 And URLline <> 0 And URLline <> 39 Then
strData = HtmlToText(strData)

Text2.Text = strData




URLline = URLline + 1
If URLline > UBound(Titles) Then URLline = 0
ocurencesSTR


End Sub

Private Sub Command2_Click()
Dim d
For d = 0 To 20000
probeB(d) = True
Next



End Sub

Private Sub Form_Load()
Me.Show
DoEvents
On Error Resume Next






Text1.Text = ""
Label2.Caption = ""
ReadLines
  Debug.Print HtmlToText("<html><body>Some Body-Text</body</html>")
  
  Debug.Print HtmlToText("<div>div-text with umlauts &auml;&ouml;&uuml;</div>")
  
StatTitlesLetters






End Sub
Function InternetGetFile(sURLFileName As String, sSaveToFile As String, Optional bOverwriteExisting As Boolean = False) As Boolean
   DoEvents
On Error Resume Next
    Dim lRet As Long
    Const S_OK As Long = 0, E_OUTOFMEMORY = &H8007000E
    Const INTERNET_OPEN_TYPE_PRECONFIG = 0, INTERNET_FLAG_EXISTING_CONNECT = &H20000000
    Const INTERNET_OPEN_TYPE_DIRECT = 1, INTERNET_OPEN_TYPE_PROXY = 3
    Const INTERNET_FLAG_RELOAD = &H80000000
    
    On Error Resume Next
    'Create an internet connection
    lRet = InternetOpen("", INTERNET_OPEN_TYPE_DIRECT, vbNullString, vbNullString, 0)
    
    If bOverwriteExisting Then
        If Len(Dir$(sSaveToFile)) Then
            VBA.Kill sSaveToFile
        End If
    End If
    'Check file doesn't already exist
    If Len(Dir$(sSaveToFile)) = 0 Then
        'Download file
        lRet = URLDownloadToFile(0&, sURLFileName, sSaveToFile, 0&, 0)
        If Len(Dir$(sSaveToFile)) Then
            'File successfully downloaded
            InternetGetFile = True
        Else
            'Failed to download file
            If lRet = E_OUTOFMEMORY Then
                Debug.Print "The buffer length is invalid or there was insufficient memory to complete the operation."
            Else
                'Debug.Assert False
                Debug.Print "Error occurred " & lRet & " (this is probably a proxy server error)."
            End If
            InternetGetFile = False
        End If
    End If
    On Error GoTo 0
    
End Function



Private Function HtmlToText(HTMLContent As String) As String
DoEvents
On Error Resume Next
  With CreateObject("HtmlFile") 'the ProgID for the MS-HTML-DocObject is just: "HtmlFile"
    .Write HTMLContent
    HtmlToText = .body.innertext
  End With
End Function
Private Sub StatTitlesLetters()
Dim c_a As Long
Dim c_b As Long
Dim c_c As Long
Dim c_d As Long
Dim c_e As Long
Dim c_f As Long
Dim c_g As Long
Dim c_h As Long







Titles() = Split(Trim(Text1.Text), vbCrLf)
Dim i
For i = 0 To UBound(Titles)
Dim q
For q = 1 To Len(Titles(i))
If LCase(Mid(Titles(i), q, 1)) = "a" Then c_a = c_a + 1
If LCase(Mid(Titles(i), q, 1)) = "b" Then c_b = c_b + 1
If LCase(Mid(Titles(i), q, 1)) = "c" Then c_c = c_c + 1
If LCase(Mid(Titles(i), q, 1)) = "d" Then c_d = c_d + 1
If LCase(Mid(Titles(i), q, 1)) = "e" Then c_e = c_e + 1
If LCase(Mid(Titles(i), q, 1)) = "f" Then c_f = c_f + 1
If LCase(Mid(Titles(i), q, 1)) = "g" Then c_g = c_g + 1
If LCase(Mid(Titles(i), q, 1)) = "h" Then c_h = c_h + 1




'add letter and count

Next
Next
Text1.Text = Trim(Text1.Text) + vbCrLf + "a" + Str(c_a)
Text1.Text = Trim(Text1.Text) + vbCrLf + "b" + Str(c_b)
Text1.Text = Trim(Text1.Text) + vbCrLf + "c" + Str(c_c)
Text1.Text = Trim(Text1.Text) + vbCrLf + "d" + Str(c_d)
Text1.Text = Trim(Text1.Text) + vbCrLf + "e" + Str(c_e)
Text1.Text = Trim(Text1.Text) + vbCrLf + "f" + Str(c_f)
Text1.Text = Trim(Text1.Text) + vbCrLf + "g" + Str(c_g)
Text1.Text = Trim(Text1.Text) + vbCrLf + "h" + Str(c_h)

End Sub

Private Sub List5_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
'Dim h22
'For h22 = 1 To 29

'If List5.Selected(h22) = True Then
'probeB2(h22) = h
'List5.Selected(h22) = False
'probeB(probeB2(h22)) = False
'End If
'Next
    Dim i As Long

    For i = 0 To List5.ListCount - 1    'loop through the items in the ListBox
        If List5.Selected(i) = False Then
        List4.Selected(i) = False
        ' if the item is selected(checked)
        probeB(probeB2(i)) = False
        Else
        probeB(probeB2(i)) = True
        
          '  MsgBox List5.List(i)        ' display the item
        End If
    Next
End Sub

Private Sub Timer1_Timer()


DoEvents
On Error Resume Next


If STASstep(2) = True And STASstep(3) = False Then
Dim intFile As Integer
Dim strData As String
intFile = FreeFile
Open App.Path + "\filename.txt" For Input As intFile
strData = Input(LOF(intFile), intFile)
'put strData in a textbox, a listbox, or any other control that will contain text values
Close intFile
STASstep(3) = True
End If
If STASstep(1) = True And STASstep(2) = False Then
InternetGetFile Titles(URLline), App.Path + "\filename.txt"
STASstep(2) = True
End If
If STASstep(0) = True And STASstep(1) = False Then
On Error Resume Next
Kill App.Path + "\filename.txt"
STASstep(1) = True
End If
If STASstep(0) = False Then
Randomize ProbeStart
Label1.Caption = Titles(URLline) + vbCrLf + Str(URLline)
STASstep(0) = True
End If




If STASstep(3) = True And STASstep(4) = False Then
'If URLline <> 17 And URLline <> 0 And URLline <> 39 Then
strData = HtmlToText(strData)
STASstep(4) = True
End If










If STASstep(4) = True And STASstep(5) = False Then
Text2.Text = strData
STASstep(5) = True
End If


If STASstep(5) = True And STASstep(6) = False Then
URLline = URLline + 1
If URLline > UBound(Titles) Then URLline = 0
ocurencesSTR
STASstep(6) = True
End If

If STASstep(6) = True Then
Dim r

For r = 0 To 6
STASstep(r) = False
Next
End If


End Sub
Private Sub ocurencesSTR()
DoEvents
On Error Resume Next
Dim words() As String
Dim ProbeT(20) As String


'Next

List1.Clear
List2.Clear
List3.Clear
List4.Clear
List5.Clear
'List4.Clear
'For h2 = 0 To 30
'List4.AddItem 0
'List5.AddItem "XXXXX"

words() = Split(Trim(Text2.Text), " ")
Label2.Caption = "/" & Str(UBound(words)) '+ " total words on 1 website"


ProbeStart = (Rnd * (UBound(words) - 20))
Dim h
Dim o



Dim k
k = -1
Dim pozycja As Boolean
For o = 0 To 20
ProbeT(o) = words(ProbeStart + o)
ProbeT(o) = LCase(ProbeT(o))
'Text3.Text = Text3.Text + ProbeT(o) + vbCrLf
List1.AddItem ProbeT(o) '[X[]>
 pozycja = False
For h = 0 To 20000
DoEvents


If (ProbeTLIST(h) = ProbeT(o) Or ProbeTLIST(h) = "") And pozycja = False Then

If ProbeTLIST(h) = ProbeT(o) Then
probeCount(h) = probeCount(h) + 1

End If
If ProbeTLIST(h) = "" Then
ProbeTLIST(h) = ProbeT(o)
probeB(h) = True



probeCount(h) = 1
End If

k = h
pozycja = True
End If



Next h






'////////////////////////////////////////////////
List2.AddItem k '<[]X]
If pozycja <> True Then List3.AddItem 0
If pozycja = True Then List3.AddItem probeCount(k)





Next




'Dim managed3 As Boolean


For h = 0 To 20000
For h2 = 0 To 30
DoEvents
If List5.ListCount - 1 > 30 Then
If List4.ListCount - 1 > 30 Then List4.RemoveItem (List4.ListCount - 1)
List5.RemoveItem (List5.ListCount - 1)
End If
If probeCount(h) > List4.List(h2) And ProbeTLIST(h) <> List5.List(h2) Then


If probeB(h) = True Then
List5.AddItem ProbeTLIST(h), h2 '":(", h2 'ProbeTLIST(h), h2
List4.AddItem probeCount(h), h2
List5.Selected(h2) = True
List4.Selected(h2) = True
List5.ListIndex = -1
List4.ListIndex = -1

probeB2(h2) = h


End If
'If List5.ListCount > 20 Then
'List4.RemoveItem (List4.ListCount - 1)
'List5.RemoveItem (List5.ListCount - 1)
'managed3 = True
'End If
'If List4.ListCount > 10 Then

'If List4.List(0) = 0 Or List4.List(0) = "0" Then
'List4.RemoveItem (0)
'List5.RemoveItem (0)
'List4.RemoveItem (List4.ListCount)
'List4.Refresh
'End If

'End If






Exit For
'Else


'If ProbeTLIST(h) <> List5.List(h2) And probeCount(h) < List4.List(h2) Then
'List4.AddItem probeCount(h), h2 + 1
'List5.AddItem ProbeTLIST(h), h2 + 1
'End If
End If



Next h2




Next h



STEPnr = STEPnr + 1
Label2 = Str(STEPnr) + Label2
Label8.Caption = List5.ListCount

If Form3.Visible Then Form3.Show
End Sub
