VERSION 5.00
Begin VB.Form Form2 
   BackColor       =   &H00404040&
   BorderStyle     =   0  'None
   Caption         =   "Form2"
   ClientHeight    =   8865
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   15735
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   591
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   1049
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   7455
      Left            =   120
      ScaleHeight     =   497
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   929
      TabIndex        =   0
      Top             =   120
      Width           =   13935
   End
End
Attribute VB_Name = "Form2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function GetColorAdjustment Lib "gdi32" (ByVal hdc As Long, lpca As COLORADJUSTMENT) As Long
Private Declare Function SetColorAdjustment Lib "gdi32" (ByVal hdc As Long, lpca As COLORADJUSTMENT) As Long
Private Declare Function SetStretchBltMode Lib "gdi32" (ByVal hdc As Long, ByVal nStretchMode As Long) As Long
Private Declare Function StretchBlt Lib "gdi32" (ByVal hdc As Long, ByVal x As Long, ByVal y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, ByVal nSrcWidth As Long, ByVal nSrcHeight As Long, ByVal dwRop As Long) As Long

Private Type COLORADJUSTMENT
    caSize As Integer
    caFlags As Integer
    caIlluminantIndex As Integer
    caRedGamma As Integer
    caGreenGamma As Integer
    caBlueGamma As Integer
    caReferenceBlack As Integer
    caReferenceWhite As Integer
    caContrast As Integer
    caBrightness As Integer
    caColorfulness As Integer
    caRedGreenTint As Integer
End Type

Private Const HALFTONE = 4
Public Sub bwx()


    Dim ca As COLORADJUSTMENT
    With Picture1
        .AutoRedraw = True
        .ScaleMode = vbPixels
        SetStretchBltMode .hdc, HALFTONE
        GetColorAdjustment .hdc, ca
        ca.caContrast = 0
        ca.caRedGreenTint = 0
        ca.caBrightness = 0
        ca.caIlluminantIndex = 0
        ca.caColorfulness = -100 'No colors!
        ca.caReferenceBlack = 1000
        ca.caReferenceWhite = 9000
        ca.caBrightness = 5
        SetColorAdjustment .hdc, ca
        StretchBlt .hdc, 0, 0, .ScaleWidth, .ScaleHeight, .hdc, 0, 0, .ScaleWidth, .ScaleHeight, vbSrcCopy
        .Refresh
    End With
End Sub



Private Sub Form_GotFocus()
bwx
End Sub

Private Sub Form_Load()
Form2.Visible = False
End Sub

Private Sub Form_Resize()
Picture1.top = 0: Picture1.left = 0
Picture1.Width = Form2.ScaleWidth
Picture1.Height = Form2.ScaleHeight


End Sub
