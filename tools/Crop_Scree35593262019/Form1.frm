VERSION 5.00
Begin VB.Form Form1 
   AutoRedraw      =   -1  'True
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Crop"
   ClientHeight    =   10830
   ClientLeft      =   375
   ClientTop       =   1710
   ClientWidth     =   22185
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10830
   ScaleWidth      =   22185
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton Command3 
      Caption         =   "hide"
      Height          =   375
      Left            =   4200
      TabIndex        =   6
      Top             =   10320
      Width           =   1815
   End
   Begin VB.CommandButton Command2 
      Caption         =   "save"
      Height          =   375
      Left            =   2160
      TabIndex        =   5
      Top             =   10320
      Width           =   1935
   End
   Begin VB.CommandButton Command1 
      Caption         =   "end"
      Height          =   375
      Left            =   120
      TabIndex        =   4
      Top             =   10320
      Width           =   1935
   End
   Begin VB.PictureBox Picture3 
      AutoRedraw      =   -1  'True
      Height          =   1335
      Left            =   12600
      ScaleHeight     =   1275
      ScaleWidth      =   3075
      TabIndex        =   3
      Top             =   5400
      Visible         =   0   'False
      Width           =   3135
   End
   Begin VB.Timer Timer1 
      Interval        =   200
      Left            =   11640
      Top             =   3240
   End
   Begin VB.PictureBox Picture2 
      AutoRedraw      =   -1  'True
      Height          =   735
      Left            =   11160
      ScaleHeight     =   675
      ScaleWidth      =   1275
      TabIndex        =   1
      Top             =   120
      Width           =   1335
   End
   Begin VB.PictureBox Picture1 
      AutoRedraw      =   -1  'True
      Height          =   10095
      Left            =   120
      ScaleHeight     =   10035
      ScaleWidth      =   21795
      TabIndex        =   0
      Top             =   120
      Width           =   21855
   End
   Begin VB.Image Image1 
      Height          =   1215
      Left            =   11880
      Top             =   5160
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Label Label1 
      Caption         =   "Select region on the main image by pressing mouse down and dragging it"
      Height          =   255
      Left            =   600
      TabIndex        =   2
      Top             =   7680
      Width           =   5175
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Declare Function GetAsyncKeyState Lib "user32" (ByVal vKey As Long) As Integer
Private Type TRect
    Left As Single
    Top As Single
    Right As Single
    Bottom As Single
End Type
Private mbDragging As Boolean
Private mRect As TRect
Private Declare Sub keybd_event _
    Lib "user32" ( _
        ByVal bVk As Byte, _
        ByVal bScan As Byte, _
        ByVal dwFlags As Long, _
        ByVal dwExtraInfo As Long)

Public Sub CropImage(pctSource As PictureBox, _
                     pctDest As PictureBox, _
                     X As Single, Y As Single, _
                     h As Single, w As Single)
'================================================
On Error Resume Next

    With pctDest
        .Width = w
        .Height = h
        .AutoRedraw = True
        .AutoSize = True
        Set .Picture = Nothing
        .PaintPicture pctSource, _
                      0, 0, _
                      w, h, _
                      X, Y, _
                      .ScaleWidth, .ScaleHeight
        .Refresh
    End With

End Sub

Private Sub Command1_Click()
End
End Sub

Private Sub Command2_Click()
SavePicture Picture2.Image, App.Path + "\IMAGE.bmp"
End Sub

Private Sub Command3_Click()
Command2.Enabled = False
Me.Hide
End Sub

Private Sub Form_Load()

    Dim rc As Long

    If App.PrevInstance Then
        rc = MsgBox("Application is already running", vbCritical, App.Title)
        End

    End If




Label1.Visible = False
Me.Hide

Picture2.Left = Picture1.Left
Picture2.Top = Picture1.Top
Picture2.Visible = False


End Sub

Private Sub Picture1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    mbDragging = True
    Picture1.DrawMode = vbInvert
    With mRect
        .Left = X
        .Top = Y
        .Right = X
        .Bottom = Y
    End With
End Sub

Private Sub Picture1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Not mbDragging Then Exit Sub
    With mRect
        ' erase box
        Picture1.Line (.Left, .Top)-(.Right, .Bottom), , B
        .Right = X
        .Bottom = Y
        ' draw box
        Picture1.Line (.Left, .Top)-(X, Y), , B
    End With
End Sub

Private Sub Picture1_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim iLeft%, iTop%

    mbDragging = False
    ' erase box by redrawing
    Picture1.Line (mRect.Left, mRect.Top)-(mRect.Right, mRect.Bottom), , B
    Picture1.DrawMode = vbCopyPen
    
    iLeft = IIf(X < mRect.Left, X, mRect.Left)
    iTop = IIf(Y < mRect.Top, Y, mRect.Top)
    
    CropImage Picture1, Picture2, _
              Abs(iLeft), Abs(iTop), _
              Abs(mRect.Bottom - mRect.Top), Abs(mRect.Right - mRect.Left)
              
              
              
Picture1.Visible = False
Picture2.Visible = True
Command2.Enabled = True
End Sub

Private Sub Timer1_Timer()


'Set the timer interval to 400

If (GetAsyncKeyState(44)) Then


'  Clipboard.Clear
'   Call keybd_event(44, 2, 0, 0)
    DoEvents



Command2.Enabled = False

Picture1.Visible = True
Picture2.Visible = False


Me.Show





'MsgBox "Print Screen pressed"

    If Not Clipboard.GetFormat(vbCFBitmap) Then Exit Sub
On Error Resume Next
Image1.Picture = Clipboard.GetData(vbCFBitmap)


    Picture1.AutoRedraw = True
    Picture1.PaintPicture Image1.Picture, _
        Picture1.ScaleLeft, Picture1.ScaleTop, _
            Picture1.ScaleWidth, Picture1.ScaleHeight, _
        0, 0, _
            Image1.Width, Image1.Height
    Picture1.Picture = Picture1.Image

'Picture1.PaintPicture Image1.Picture, 0, 0, Picture1.ScaleWidth, Picture1.ScaleHeight, 0, 0, Picture1.Width, Picture1.Height


End If



End Sub
