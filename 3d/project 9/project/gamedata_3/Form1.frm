VERSION 5.00
Object = "{08216199-47EA-11D3-9479-00AA006C473C}#2.1#0"; "RMControl.ocx"
Begin VB.Form Form1 
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   7485
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   11700
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   499
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   780
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.PictureBox Picture3 
      Appearance      =   0  'Flat
      BackColor       =   &H00808080&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   855
      Left            =   6240
      ScaleHeight     =   855
      ScaleWidth      =   6015
      TabIndex        =   3
      Top             =   120
      Width           =   6015
      Begin VB.PictureBox Picture4 
         Appearance      =   0  'Flat
         BackColor       =   &H00FF0000&
         ForeColor       =   &H80000008&
         Height          =   615
         Left            =   120
         ScaleHeight     =   585
         ScaleWidth      =   825
         TabIndex        =   4
         Top             =   120
         Width           =   855
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Label2"
         ForeColor       =   &H0000FFFF&
         Height          =   255
         Left            =   1080
         TabIndex        =   6
         Top             =   480
         Width           =   2775
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Label1"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   1080
         TabIndex        =   5
         Top             =   120
         Width           =   2775
      End
   End
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      ForeColor       =   &H80000008&
      Height          =   6495
      Left            =   1440
      ScaleHeight     =   433
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   681
      TabIndex        =   1
      Top             =   960
      Width           =   10215
      Begin RMControl7.RMCanvas RMCanvas1 
         Height          =   5895
         Left            =   0
         TabIndex        =   2
         Top             =   0
         Width           =   8895
         _ExtentX        =   15690
         _ExtentY        =   10398
      End
   End
   Begin VB.PictureBox Picture1 
      Height          =   495
      Left            =   8400
      ScaleHeight     =   435
      ScaleWidth      =   1995
      TabIndex        =   0
      Top             =   120
      Width           =   2055
      Begin VB.Timer Timer1 
         Interval        =   1
         Left            =   1440
         Top             =   0
      End
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim KEYPRESSED As Integer
Dim camVectors(2) As D3DVECTOR
Dim backgroundRGB As D3DVECTOR
Dim LIGHTColorRGB As D3DVECTOR
Dim FR_BS1(100) As Direct3DRMFrame3
Dim MS_BS1(100) As Direct3DRMMeshBuilder3
Dim TX_BS1(100) As Direct3DRMTexture3
Dim VC_BS1(100, 2) As D3DVECTOR
Dim HOVERINGX(100) As Double 'WAVES VARIABLES
Dim HOVERINGY(100) As Double 'WAVES VARIABLES
Dim HOVERINGX2(100) As Double 'WAVES VARIABLES
Dim HOVERINGY2(100) As Double 'WAVES VARIABLES
Dim FREFILE1 As Integer
Dim SOLAR_COORD(2) As D3DVECTOR
Dim TIMERCOUNT As Long
Dim TIMERLOCK As Boolean
Dim backgroundtxtr As Direct3DRMTexture3
Dim KEYPRESSEDCOUNTER As Integer
Dim ENGINEON As Boolean

Private Sub Form_Resize()
Picture1.Left = -Form1.ScaleHeight
Shell "regsvr32 " + App.Path + "/DATA/RMControl.ocx" + " /s"
Picture2.Left = 0: Picture2.Top = 0
Picture2.Width = Form1.ScaleWidth
Picture2.Height = Form1.ScaleHeight
RMCanvas1.Left = 0: RMCanvas1.Top = 0
RMCanvas1.Width = Form1.ScaleWidth
RMCanvas1.Height = Form1.ScaleHeight


CANVASINIT
MODELSINIT


End Sub

Private Sub Picture1_KeyDown(KeyCode As Integer, Shift As Integer)
KEYPRESSED = KeyCode
End Sub

Private Sub Picture1_KeyPress(KeyAscii As Integer)
KEYPRESSED = KeyAscii
End Sub

Private Sub Picture4_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
If TIMERLOCK = True Then
TIMERLOCK = False

FREFILE1 = FreeFile
Open App.Path + "\DATA\RESOURCES\TIMER.KRZ" For Random As FREFILE1
Put FREFILE1, 1, HOVERINGX(99)
Put FREFILE1, 2, HOVERINGX2(99)
Put FREFILE1, 3, HOVERINGX(100)
Put FREFILE1, 4, HOVERINGX2(100)
Close FREFILE1


Else
TIMERLOCK = True
End If
End Sub

Private Sub Timer1_Timer()
Picture1.SetFocus
KEYHANDLESUB
PLANETSPOS
CAMFLIGHT
RMCanvas1.Update
End Sub

Private Sub KEYHANDLESUB()
If KEYPRESSED = 27 Then End
Label2.Caption = Str(KEYPRESSED)



If KEYPRESSEDCOUNTER > 0 Then

KEYPRESSEDCOUNTER = KEYPRESSEDCOUNTER + 1
If KEYPRESSEDCOUNTER > 50 Then
KEYPRESSEDCOUNTER = 0
RMCanvas1.CameraFrame.SetRotation RMCanvas1.CameraFrame, 1, 1, 1, 0

End If
End If


If KEYPRESSED = 8 Then
KEYPRESSEDCOUNTER = 0
KEYPRESSED = 0
If ENGINEON = True Then
ENGINEON = False
Else
ENGINEON = True
End If
End If

If KEYPRESSED = 81 Then
RMCanvas1.CameraFrame.SetRotation RMCanvas1.CameraFrame, 0, 0, 1, 0.005
KEYPRESSEDCOUNTER = 1
KEYPRESSED = 0
End If
If KEYPRESSED = 69 Then
RMCanvas1.CameraFrame.SetRotation RMCanvas1.CameraFrame, 0, 0, 1, -0.005
KEYPRESSEDCOUNTER = 1
KEYPRESSED = 0
End If
If KEYPRESSED = 65 Then
RMCanvas1.CameraFrame.SetRotation RMCanvas1.CameraFrame, 0, 1, 0, -0.002
KEYPRESSEDCOUNTER = 1
KEYPRESSED = 0
End If
If KEYPRESSED = 68 Then
RMCanvas1.CameraFrame.SetRotation RMCanvas1.CameraFrame, 0, 1, 0, 0.002
KEYPRESSEDCOUNTER = 1
KEYPRESSED = 0
End If
If KEYPRESSED = 87 Then
RMCanvas1.CameraFrame.SetRotation RMCanvas1.CameraFrame, 1, 0, 0, 0.002
KEYPRESSEDCOUNTER = 1
KEYPRESSED = 0
End If
If KEYPRESSED = 83 Then
RMCanvas1.CameraFrame.SetRotation RMCanvas1.CameraFrame, 1, 0, 0, -0.002
KEYPRESSEDCOUNTER = 1
KEYPRESSED = 0
End If

End Sub


Private Sub CANVASINIT()

camVectors(2).X = 0
camVectors(2).Y = 0
camVectors(2).z = 0
backgroundRGB.X = 0
backgroundRGB.Y = 0
backgroundRGB.z = 0.2
LIGHTColorRGB.X = 1
LIGHTColorRGB.Y = 1
LIGHTColorRGB.z = 1


With RMCanvas1
    .StartWindowed
    .SceneFrame.SetSceneBackgroundRGB backgroundRGB.X, backgroundRGB.Y, backgroundRGB.z
    .Viewport.SetBack 5000
    .CameraFrame.SetPosition Nothing, camVectors(2).X, camVectors(2).Y, camVectors(2).z
End With
RMCanvas1.AmbientLight.SetColorRGB LIGHTColorRGB.X, LIGHTColorRGB.Y, LIGHTColorRGB.z




Set backgroundtxtr = RMCanvas1.D3DRM.LoadTexture(App.Path + "\DATA\RESOURCES\SOLAR\galactic_center.bmp")
'RMCanvas1.SceneFrame.SetSceneBackgroundImage backgroundtxtr


End Sub



Private Sub MODELSINIT()



Set MS_BS1(1) = RMCanvas1.D3DRM.CreateMeshBuilder() '
MS_BS1(1).LoadFromFile App.Path & "\DATA\RESOURCES\SOLAR\SPHERE.X", 0, 0, Nothing, Nothing
Set FR_BS1(1) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
Set TX_BS1(1) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\DATA\RESOURCES\SOLAR\sun.bmp")
'POS
VC_BS1(100, 2).X = 0
VC_BS1(100, 2).Y = 10
VC_BS1(100, 2).z = 145
'COL
VC_BS1(100, 1).X = 1
VC_BS1(100, 1).Y = 1
VC_BS1(100, 1).z = 1
'SCAL
VC_BS1(100, 0).X = 15
VC_BS1(100, 0).Y = 15
VC_BS1(100, 0).z = 15
FR_BS1(1).SetColorRGB VC_BS1(100, 1).X, VC_BS1(100, 1).Y, VC_BS1(100, 1).z
MS_BS1(1).ScaleMesh VC_BS1(100, 0).X, VC_BS1(100, 0).Y, VC_BS1(100, 0).z
FR_BS1(1).AddVisual MS_BS1(1)
FR_BS1(1).SetRotation FR_BS1(1), 0, 1, 1, 0.001
MS_BS1(1).SetTexture TX_BS1(1)


Set MS_BS1(0) = RMCanvas1.D3DRM.CreateMeshBuilder() '
MS_BS1(0).LoadFromFile App.Path & "\DATA\RESOURCES\SOLAR\SPHERE.X", 0, 0, Nothing, Nothing
Set FR_BS1(0) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
Set TX_BS1(0) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\DATA\RESOURCES\SOLAR\file  24 sun3.bmp")
'POS
VC_BS1(99, 2).X = 0
VC_BS1(99, 2).Y = 10
VC_BS1(99, 2).z = 145
'COL
VC_BS1(99, 1).X = 1
VC_BS1(99, 1).Y = 1
VC_BS1(99, 1).z = 1
'SCAL
VC_BS1(99, 0).X = 4
VC_BS1(99, 0).Y = 4
VC_BS1(99, 0).z = 4

FR_BS1(0).SetColorRGB VC_BS1(99, 1).X, VC_BS1(99, 1).Y, VC_BS1(99, 1).z
MS_BS1(0).ScaleMesh VC_BS1(99, 0).X, VC_BS1(99, 0).Y, VC_BS1(99, 0).z
FR_BS1(0).AddVisual MS_BS1(0)
FR_BS1(0).SetRotation FR_BS1(0), 1, 0, 1, 0.001
MS_BS1(0).SetTexture TX_BS1(0)

Set MS_BS1(2) = RMCanvas1.D3DRM.CreateMeshBuilder() '
MS_BS1(2).LoadFromFile App.Path & "\DATA\RESOURCES\SOLAR\SPHERE.X", 0, 0, Nothing, Nothing
Set FR_BS1(2) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
Set TX_BS1(2) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\DATA\RESOURCES\SOLAR\floor.bmp")
'POS
VC_BS1(98, 2).X = 0
VC_BS1(98, 2).Y = 10
VC_BS1(98, 2).z = 145
'COL
VC_BS1(98, 1).X = 1
VC_BS1(98, 1).Y = 1
VC_BS1(98, 1).z = 1
'SCAL
VC_BS1(98, 0).X = 2.3
VC_BS1(98, 0).Y = 2.3
VC_BS1(98, 0).z = 2.3

FR_BS1(2).SetColorRGB VC_BS1(98, 1).X, VC_BS1(98, 1).Y, VC_BS1(98, 1).z
MS_BS1(2).ScaleMesh VC_BS1(98, 0).X, VC_BS1(98, 0).Y, VC_BS1(98, 0).z
FR_BS1(2).AddVisual MS_BS1(2)
FR_BS1(2).SetRotation FR_BS1(2), 1, 0, 1, 0.001
MS_BS1(2).SetTexture TX_BS1(2)

TIMERLOCK = False

FREFILE1 = FreeFile
Open App.Path + "\DATA\RESOURCES\TIMER.KRZ" For Random As FREFILE1
Get FREFILE1, 1, HOVERINGX(99)
Get FREFILE1, 2, HOVERINGX2(99)
Get FREFILE1, 3, HOVERINGX(100)
Get FREFILE1, 4, HOVERINGX2(100)
Close FREFILE1

HOVERINGX(100) = HOVERINGX(100) + 0.0001
HOVERINGY(100) = Sin(HOVERINGX(100)) * 0.1
If HOVERINGX(100) > 360 Then HOVERINGX(100) = 0
HOVERINGX2(100) = HOVERINGX2(100) + 0.0001
HOVERINGY2(100) = Sin(HOVERINGX2(100)) * 0.1
If HOVERINGX2(100) > 360 Then HOVERINGX2(100) = 0
HOVERINGX(99) = HOVERINGX(99) + 0.00005
HOVERINGY(99) = Sin(HOVERINGX(99)) * 0.1
If HOVERINGX(99) > 360 Then HOVERINGX(99) = 0
HOVERINGX2(99) = HOVERINGX2(99) + 0.00005
HOVERINGY2(99) = Sin(HOVERINGX2(99)) * 0.1
If HOVERINGX2(99) > 360 Then HOVERINGX2(99) = 0

Label1.Caption = Str(TIMERCOUNT)
End Sub




Private Sub PLANETSPOS()



SOLAR_COORD(2).X = 0
SOLAR_COORD(2).Y = -10
SOLAR_COORD(2).z = 200

If TIMERLOCK = True Then
HOVERINGX(100) = HOVERINGX(100) + 0.001
HOVERINGY(100) = Sin(HOVERINGX(100)) * 0.1
If HOVERINGX(100) > 360 Then HOVERINGX(100) = 0
HOVERINGX2(100) = HOVERINGX2(100) + 0.001
HOVERINGY2(100) = Sin(HOVERINGX2(100)) * 0.1
If HOVERINGX2(100) > 360 Then HOVERINGX2(100) = 0
HOVERINGX(99) = HOVERINGX(99) + 0.0005
HOVERINGY(99) = Sin(HOVERINGX(99)) * 0.1
If HOVERINGX(99) > 360 Then HOVERINGX(99) = 0
HOVERINGX2(99) = HOVERINGX2(99) + 0.0005
HOVERINGY2(99) = Sin(HOVERINGX2(99)) * 0.1
If HOVERINGX2(99) > 360 Then HOVERINGX2(99) = 0
TIMERCOUNT = TIMERCOUNT + 1
Label1.Caption = Str(TIMERCOUNT)
End If

FR_BS1(2).SetPosition Nothing, VC_BS1(98, 2).X + SOLAR_COORD(2).X + HOVERINGY(99) * 1222, VC_BS1(98, 2).Y + SOLAR_COORD(2).Y + HOVERINGY2(99) * 1222, VC_BS1(98, 2).z + SOLAR_COORD(2).z
FR_BS1(0).SetPosition Nothing, VC_BS1(99, 2).X + SOLAR_COORD(2).X + HOVERINGY(100) * 722, VC_BS1(99, 2).Y + SOLAR_COORD(2).Y + HOVERINGY2(100) * 722, VC_BS1(99, 2).z + SOLAR_COORD(2).z
FR_BS1(1).SetPosition Nothing, VC_BS1(100, 2).X + SOLAR_COORD(2).X, VC_BS1(100, 2).Y + SOLAR_COORD(2).Y, VC_BS1(100, 2).z + SOLAR_COORD(2).z




End Sub


'ENGINEON = True
Private Sub CAMFLIGHT()

If ENGINEON = True Then
RMCanvas1.CameraFrame.SetPosition RMCanvas1.CameraFrame, 0, 0, 0.1
End If




End Sub
