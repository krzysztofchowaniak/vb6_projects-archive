VERSION 5.00
Begin VB.Form Form1 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Banknotes Memory Puzzle"
   ClientHeight    =   9300
   ClientLeft      =   150
   ClientTop       =   495
   ClientWidth     =   14715
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   15
      Charset         =   238
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   PaletteMode     =   2  'Custom
   Picture         =   "Form1.frx":406A
   ScaleHeight     =   9300
   ScaleWidth      =   14715
   StartUpPosition =   1  'CenterOwner
   Begin VB.Timer Timer5 
      Enabled         =   0   'False
      Interval        =   100
      Left            =   10200
      Top             =   3120
   End
   Begin VB.Timer Timer4 
      Enabled         =   0   'False
      Interval        =   2000
      Left            =   4920
      Top             =   240
   End
   Begin VB.Timer Timer3 
      Interval        =   10
      Left            =   3600
      Top             =   360
   End
   Begin VB.Timer Timer2 
      Enabled         =   0   'False
      Left            =   2520
      Tag             =   "SortSilence"
      Top             =   360
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   2500
      Left            =   1680
      Tag             =   "End Clock"
      Top             =   240
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   0
      Left            =   0
      Picture         =   "Form1.frx":7549
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   31
      Top             =   840
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   1
      Left            =   1560
      Picture         =   "Form1.frx":94BA
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   30
      Top             =   960
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   2
      Left            =   3360
      OLEDropMode     =   2  'Automatic
      Picture         =   "Form1.frx":B42B
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   29
      Top             =   1440
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   3
      Left            =   1320
      Picture         =   "Form1.frx":D39C
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   28
      Top             =   2160
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   4
      Left            =   3000
      Picture         =   "Form1.frx":F30D
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   27
      Top             =   2040
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   5
      Left            =   0
      Picture         =   "Form1.frx":1127E
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   26
      Top             =   2160
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   6
      Left            =   3120
      Picture         =   "Form1.frx":131EF
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   25
      Top             =   4200
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   7
      Left            =   1680
      Picture         =   "Form1.frx":1F01F
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   24
      Top             =   4200
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   8
      Left            =   0
      Picture         =   "Form1.frx":2AE4F
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   23
      Top             =   4200
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   9
      Left            =   4560
      Picture         =   "Form1.frx":36C7F
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   22
      Top             =   4440
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   10
      Left            =   6240
      Picture         =   "Form1.frx":42AAF
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   21
      Top             =   2160
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   11
      Left            =   4800
      Picture         =   "Form1.frx":4E8DF
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   20
      Top             =   960
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   12
      Left            =   6240
      Picture         =   "Form1.frx":5A70F
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   19
      Top             =   4320
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   13
      Left            =   4560
      Picture         =   "Form1.frx":6653F
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   18
      Top             =   2280
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   14
      Left            =   6600
      Picture         =   "Form1.frx":684B0
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   17
      Top             =   240
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   15
      Left            =   8880
      Picture         =   "Form1.frx":742E0
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   16
      Top             =   240
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   16
      Left            =   8520
      Picture         =   "Form1.frx":80110
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   15
      Top             =   4320
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   17
      Left            =   8520
      Picture         =   "Form1.frx":8BF40
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   14
      Top             =   2160
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   18
      Left            =   5880
      Picture         =   "Form1.frx":97D70
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   13
      Top             =   7680
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   19
      Left            =   4680
      Picture         =   "Form1.frx":A3BA0
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   12
      Top             =   7920
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   20
      Left            =   120
      Picture         =   "Form1.frx":AF9D0
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   11
      Top             =   6600
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   21
      Left            =   1800
      Picture         =   "Form1.frx":BB800
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   10
      Top             =   6600
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   22
      Left            =   3240
      Picture         =   "Form1.frx":C7630
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   9
      Top             =   6600
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   23
      Left            =   8640
      Picture         =   "Form1.frx":D3460
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   8
      Top             =   6720
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   24
      Left            =   11160
      Picture         =   "Form1.frx":DF290
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   7
      Top             =   360
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   25
      Left            =   10800
      Picture         =   "Form1.frx":EB0C0
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   6
      Top             =   4440
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   26
      Left            =   10800
      Picture         =   "Form1.frx":F6EF0
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   5
      Top             =   2280
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   27
      Left            =   10920
      Picture         =   "Form1.frx":102D20
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   4
      Top             =   6840
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   28
      Left            =   12240
      Picture         =   "Form1.frx":10EB50
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   3
      Top             =   240
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   29
      Left            =   12600
      Picture         =   "Form1.frx":11A980
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   2
      Top             =   4320
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   30
      Left            =   12600
      Picture         =   "Form1.frx":1267B0
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   1
      Top             =   2160
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Index           =   31
      Left            =   12720
      Picture         =   "Form1.frx":1325E0
      ScaleHeight     =   1785
      ScaleWidth      =   1185
      TabIndex        =   0
      Top             =   6720
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Segoe UI Black"
         Size            =   24
         Charset         =   0
         Weight          =   900
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FFFF&
      Height          =   8895
      Left            =   480
      TabIndex        =   34
      Top             =   720
      Visible         =   0   'False
      Width           =   2895
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   " 0"
      BeginProperty Font 
         Name            =   "Segoe UI Black"
         Size            =   24
         Charset         =   0
         Weight          =   900
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   8535
      Left            =   3120
      TabIndex        =   33
      Top             =   720
      Visible         =   0   'False
      Width           =   5775
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Segoe UI Black"
         Size            =   24
         Charset         =   0
         Weight          =   900
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FFFF&
      Height          =   615
      Left            =   2640
      TabIndex        =   32
      Top             =   6720
      Visible         =   0   'False
      Width           =   9255
   End
   Begin VB.Image Image3 
      Height          =   4215
      Left            =   2400
      Stretch         =   -1  'True
      Top             =   3000
      Visible         =   0   'False
      Width           =   6615
   End
   Begin VB.Image Image2 
      Height          =   1815
      Left            =   13320
      Picture         =   "Form1.frx":13E410
      Stretch         =   -1  'True
      Top             =   240
      Width           =   1215
   End
   Begin VB.Image Image1 
      Height          =   10095
      Left            =   0
      Picture         =   "Form1.frx":1412CF
      Stretch         =   -1  'True
      Top             =   0
      Width           =   15135
   End
   Begin VB.Menu Menu 
      Caption         =   "Play"
      Index           =   0
      Begin VB.Menu New2 
         Caption         =   "New"
         Enabled         =   0   'False
      End
      Begin VB.Menu Simple 
         Caption         =   "Run Simple"
      End
      Begin VB.Menu Medium 
         Caption         =   "Run Medium"
      End
      Begin VB.Menu Big 
         Caption         =   "Run Big"
      End
      Begin VB.Menu sort1 
         Caption         =   "Pictures 1"
         Checked         =   -1  'True
         Visible         =   0   'False
      End
      Begin VB.Menu sort2 
         Caption         =   "Pictures 2"
         Visible         =   0   'False
      End
      Begin VB.Menu Default_Pictures 
         Caption         =   "Reset"
      End
      Begin VB.Menu Exit 
         Caption         =   "Exit"
      End
   End
   Begin VB.Menu Help 
      Caption         =   "Help"
      Index           =   1
      Begin VB.Menu Info 
         Caption         =   "About"
      End
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'--------------------------------------------------------------------
'--------------------------------------------------------------------
'--------------------------------------------------------------------
        Option Explicit
'--------------------------------------------------------------------
'--------------------------------------------------------------------
Dim RND_DECK As Integer

Dim SortDeck As Integer
Dim startCard

Dim places(2) As String
Dim TimeTotal As Long
Dim CardTurned(31) As Boolean
Dim firstCard As Integer
Dim secondCard As Integer
Dim NextPair As Boolean
'--------------------------------------------------------------------
Dim PicSet(31) As Integer
Dim PathNr
Dim TimC As Long
Dim FileName As String
Dim Dificulty As Integer
Dim TimerP As Long
Private Declare Function mciSendString Lib "winmm.dll" Alias "mciSendStringA" (ByVal lpstrCommand As String, ByVal lpstrReturnString As String, ByVal uReturnLength As Long, ByVal hwndCallback As Long) As Long
Dim retVal As Long
Dim PicturesPath As String
'-------------------------------------
Dim PicturesTable(31) As Integer
'-------------------------------------

Private Sub Big_Click()
Randomize TimC
startCard = 0


Label3.Visible = False
Label1.Visible = False
Label2.Visible = False
Image3.Visible = False
Dim Lb
Dificulty = 32
TimerP = 0
StartNewBoward
Medium.Checked = False
Simple.Checked = False
Big.Checked = True


If Simple.Checked = True Then
For Lb = 0 To 31
Picture1(Lb).Visible = False
Next
End If
If Medium.Checked = True Then
For Lb = 0 To 31
Picture1(Lb).Visible = False
Next
End If
If Big.Checked = True Then
For Lb = 0 To 31
Picture1(Lb).Visible = False
Next
End If

New2.Enabled = True
ambience
Image2.Visible = True
End Sub

Private Sub Default_Pictures_Click()
places(0) = "0"
places(1) = "0"
places(2) = "0"
Label2.Visible = False
Label3.Visible = False
Label1.Visible = False
Image3.Visible = False
Default_Pictures.Checked = False
'User_Pictures.Checked = False
PicturesPath = App.Path & "\Default Pictures\"
'PicturesPath = App.Path & "\User Pictures\"
'PicturesPath = App.Path & "\Backup Pictures\"



ambience

Dim Lb



For Lb = 0 To 31
Picture1(Lb).Visible = False
Next

'Default_Pictures.Enabled
Image2.Visible = True

Simple.Checked = False
Medium.Checked = False
Big.Checked = False
New2.Enabled = False








Label2.Visible = True
Label3.Visible = True

'If TimeTotal <= Val(places(2)) And Val(places(2)) <> 0 Then
'places(0) = places(1)
'places(1) = places(2)
places(2) = Str(0)
'Else
'If Val(places(2)) = 0 Then places(2) = Str(TimeTotal)

'End If




'If Val(places(2)) <> 0 Then
Label2.Caption = places(2) '+ "0" + Chr(13)
'Else
'plus = True
'End If














End Sub

Private Sub Form_Unload(Cancel As Integer)
'retVal = mciSendString("close songNumber2", vbNullString, 0, 0)
'retVal = mciSendString("close songNumber1", vbNullString, 0, 0)
'retVal = mciSendString("close songNumber2", vbNullString, 0, 0)
retVal = mciSendString("close songNumber3", vbNullString, 0, 0)
'retVal = mciSendString("close songNumber1", vbNullString, 0, 0)
End Sub
Private Sub sounds()

FileName = Chr(34) & App.Path + "\sound\sort1.mp3" & Chr(34)
retVal = mciSendString("close songNumber1", vbNullString, 0, 0)
retVal = mciSendString("open " & FileName & " type mpegvideo alias songNumber1", vbNullString, 0, 0)
mciSendString "setaudio songNumber1 volume to " & 500, vbNullString, 0, 0
retVal = mciSendString("play songNumber1", vbNullString, 0, 0)
End Sub
Private Sub ambience()



FileName = Chr(34) & App.Path + "\sound\take1.mp3" & Chr(34)
retVal = mciSendString("close songNumber2", vbNullString, 0, 0)
retVal = mciSendString("open " & FileName & " type mpegvideo alias songNumber2", vbNullString, 0, 0)
mciSendString "setaudio songNumber2 volume to " & 520, vbNullString, 0, 0
retVal = mciSendString("play songNumber2", vbNullString, 0, 0)




End Sub

Private Sub ambience0()



FileName = Chr(34) & App.Path + "\sound\321220tavern-1.wav" & Chr(34)
retVal = mciSendString("close songNumber3", vbNullString, 0, 0)
retVal = mciSendString("open " & FileName & " type mpegvideo alias songNumber3", vbNullString, 0, 0)
mciSendString "setaudio songNumber3 volume to " & 20, vbNullString, 0, 0
retVal = mciSendString("play songNumber3 repeat", vbNullString, 0, 0)




End Sub

Private Sub Exit_Click()

ambience
Timer1.Enabled = True
End Sub

Private Sub Form_Load()
startCard = Int(Rnd * 15)
sort1.Checked = True
sort2.Checked = False
places(0) = "0"
places(1) = "0"
places(2) = "0"
Label3.Caption = "Top Score:" '"1st" + Chr(13) + "2nd" + Chr(13) + "3rd"


Debug.Print "*************************************************"
Debug.Print "Program Started"

Debug.Print "-------------------------------------------------"
Debug.Print "--------card-----picture-------------------------"

'--------------------------------------------------------------------
'--------------------------------------------------------------------
'''Me.Width = 1260 + Picture1(0).Width * 3
'''Me.Height = 1700 + Picture1(0).Height * 2
'small

retVal = mciSendString("close songNumber1", vbNullString, 0, 0)
retVal = mciSendString("close songNumber2", vbNullString, 0, 0)
retVal = mciSendString("close songNumber3", vbNullString, 0, 0)
retVal = mciSendString("close songNumber4", vbNullString, 0, 0)




SortBig


PicturesPath = App.Path & "\Default Pictures\"
'PicturesPath = App.Path & "\User Pictures\"
'PicturesPath = App.Path & "\Backup Pictures\"
Dim z
For z = 0 To 31
PicSet(z) = 0
PicturesTable(z) = 0
Next

'AssignImage
Dificulty = 6
TimerP = 0

Dim Lb


For Lb = 0 To 31
Picture1(Lb).Visible = False
Picture1(Lb).Picture = LoadPicture(PicturesPath + "card.jpg")
CardTurned(Lb) = False
Next

ambience0

'--------------------------------------------------------------------
'--------------------------------------------------------------------
'--------------------------------------------------------------------

Label2.Visible = True
Label3.Visible = True

If TimeTotal <= Val(places(2)) And Val(places(2)) <> 0 Then
places(0) = places(1)
places(1) = places(2)
places(2) = Str(TimeTotal)
Else
If Val(places(2)) = 0 Then places(2) = Str(TimeTotal)

End If




If Val(places(2)) <> 0 Then
Label2.Caption = Label2.Caption + places(2) + Chr(13)
Else
'plus = True
End If
Randomize TimC

End Sub

Private Sub Image2_Click()






Randomize TimC
RND_DECK = Int(Rnd * 2)
If sort1.Checked = True Then SortDeck = 0
If sort2.Checked = True Then SortDeck = 1
SortDeck = RND_DECK
Debug.Print Str(SortDeck) + "=SortDeck"







Randomize TimC
TimeTotal = 0
Timer5.Enabled = True
Dim loopO
For loopO = 0 To 31
Picture1(loopO).Enabled = True
Next


If New2.Enabled = True Then


Label2.Visible = False
Label3.Visible = False


firstCard = 0: secondCard = 0
Dim z
For z = 0 To 31
PicSet(z) = 0
PicturesTable(z) = 0
Next
If Dificulty = 6 Then Chose3Images
If Dificulty = 14 Then Chose7Images
If Dificulty = 32 Then Chose16Images

'AssignImage




Dim Lb


For Lb = 0 To 31
'Picture1(Lb).Visible = False
'Picture1(Lb).Picture = LoadPicture(App.Path + "\Default pictures\card.jpg")
Picture1(Lb).Picture = LoadPicture(PicturesPath + "card.jpg")
CardTurned(Lb) = False
Next
sounds
If Simple.Checked = True Then
SortSmall
Timer2.Interval = 250
Timer2.Enabled = True

For Lb = 0 To 5
Picture1(Lb).Visible = True
Next

End If
If Medium.Checked = True Then
SortMedium
Timer2.Interval = 410
Timer2.Enabled = True


For Lb = 0 To 13
Picture1(Lb).Visible = True
Next
End If
If Big.Checked = True Then

SortBig
Timer2.Interval = 1500
Timer2.Enabled = True


For Lb = 0 To 31
Picture1(Lb).Visible = True
Next
End If

Simple.Checked = False
Medium.Checked = False
Big.Checked = False

New2.Enabled = False
Image2.Visible = False

End If

End Sub

Private Sub Index_Click()
ambience
End Sub

Private Sub Info_Click()
'ambience
MsgBox "Memory Puzzle by krzysztofchowaniak@aol.fr", vbDefaultButton1

End Sub

Private Sub Medium_Click()

Randomize TimC
startCard = Int(Rnd * 10)


Label3.Visible = False
Label1.Visible = False
Label2.Visible = False
Image3.Visible = False
Dim Lb
Dificulty = 14
TimerP = 0
StartNewBoward
Medium.Checked = True
Simple.Checked = False
Big.Checked = False


If Simple.Checked = True Then
For Lb = 0 To 31
Picture1(Lb).Visible = False
Next
End If
If Medium.Checked = True Then
For Lb = 0 To 31
Picture1(Lb).Visible = False
Next
End If
If Big.Checked = True Then
For Lb = 0 To 31
Picture1(Lb).Visible = False
Next
End If
New2.Enabled = True
ambience
Image2.Visible = True
End Sub



Private Sub New2_Click()

Label2.Visible = False
Label3.Visible = False

Randomize TimC
RND_DECK = Int(Rnd * 2)





If sort1.Checked = True Then SortDeck = 0
If sort2.Checked = True Then SortDeck = 1
SortDeck = RND_DECK
Debug.Print Str(SortDeck) + "=SortDeck"



Label3.Visible = False
Label1.Visible = False
Label2.Visible = False
TimeTotal = 0
Timer5.Enabled = True
Randomize TimC
Image3.Visible = False
firstCard = 0: secondCard = 0
Dim loopO
For loopO = 0 To 31
Picture1(loopO).Enabled = True
Next
Dim z
For z = 0 To 31
PicSet(z) = 0
PicturesTable(z) = 0
Next
'AssignImage

If Dificulty = 6 Then Chose3Images
If Dificulty = 14 Then Chose7Images
If Dificulty = 32 Then Chose16Images






Dim Lb


For Lb = 0 To 31
'Picture1(Lb).Visible = False
'Picture1(Lb).Picture = LoadPicture(App.Path + "\Default pictures\card.jpg")
Picture1(Lb).Picture = LoadPicture(PicturesPath + "card.jpg")
CardTurned(Lb) = False
Next
sounds
If Simple.Checked = True Then
SortSmall
Timer2.Interval = 250
Timer2.Enabled = True

For Lb = 0 To 5
Picture1(Lb).Visible = True
Next

End If
If Medium.Checked = True Then
SortMedium
Timer2.Interval = 410
Timer2.Enabled = True


For Lb = 0 To 13
Picture1(Lb).Visible = True
Next
End If
If Big.Checked = True Then

SortBig
Timer2.Interval = 1500
Timer2.Enabled = True


For Lb = 0 To 31
Picture1(Lb).Visible = True
Next
End If

Simple.Checked = False
Medium.Checked = False
Big.Checked = False

New2.Enabled = False
Image2.Visible = False
End Sub

Private Sub Picture1_Click(Index As Integer)
Picture1(Index).Enabled = False
CardTurned(Index) = True
ambience
Debug.Print "picture nr:" + Str(PicturesTable(Index))
'Dim allImg
'allImg = Int(Rnd * 22) + 1







'SortDeck
If SortDeck = 0 Then

If Index < 16 Then
Picture1(Index).Picture = LoadPicture(App.Path + "\Default Pictures\" + Trim(Str((PicturesTable(Index))) + ".jpg"))
Else
Picture1(Index).Picture = LoadPicture(App.Path + "\Default Pictures\" + Trim(Str((PicturesTable(Index))) + ".jpg"))
'PicturesTable (LopB)
End If

ElseIf SortDeck = 1 Then



If Index < 16 Then
Picture1(Index).Picture = LoadPicture(App.Path + "\user Pictures\" + Trim(Str((PicturesTable(Index))) + ".jpg"))
Else
Picture1(Index).Picture = LoadPicture(App.Path + "\user Pictures\" + Trim(Str((PicturesTable(Index))) + ".jpg"))
'PicturesTable (LopB)
End If


Else
If Index < 16 Then
Picture1(Index).Picture = LoadPicture(App.Path + "\User Pictures\" + Trim(Str((PicturesTable(Index))) + ".jpg"))
Else
Picture1(Index).Picture = LoadPicture(App.Path + "\User Pictures\" + Trim(Str((PicturesTable(Index))) + ".jpg"))
'PicturesTable (LopB)
End If
End If
'If firstCard <> 0 And secondCard = 0 Then

'If firstCard = PicturesTable(Index) Then
'NextPair = True
'Debug.Print "secondcard" + Str(PicturesTable(Index))
'End If

If firstCard <> 0 And secondCard = 0 Then

If firstCard = PicturesTable(Index) Then
secondCard = firstCard
Debug.Print "card found!" + Str(firstCard)
firstCard = 0: secondCard = 0
Winer_test
Exit Sub
Else
'reset'reset'reset'reset'reset
Debug.Print "cards hide timer"
Timer4.Enabled = True

Dim loopO
For loopO = 0 To 31
Picture1(loopO).Enabled = False
Next



firstCard = 0: secondCard = 0
Winer_test
Exit Sub
'reset'reset'reset'reset'reset
End If



End If

'End If
If firstCard = 0 And secondCard = 0 Then
firstCard = PicturesTable(Index)
Debug.Print "firstcard" + Str(firstCard)
End If




'Dim firstCard As Integer
'Dim secondCard As Integer
Winer_test

End Sub


Private Sub Simple_Click()


Randomize TimC
startCard = Int(Rnd * 14)





Label3.Visible = False
Label1.Visible = False
Label2.Visible = False
Image3.Visible = False
Dim Lb
Dificulty = 6
TimerP = 0
StartNewBoward
Medium.Checked = False
Simple.Checked = True
Big.Checked = False


If Simple.Checked = True Then
For Lb = 0 To 31
Picture1(Lb).Visible = False
Next
End If
If Medium.Checked = True Then
For Lb = 0 To 31
Picture1(Lb).Visible = False
Next
End If
If Big.Checked = True Then
For Lb = 0 To 31
Picture1(Lb).Visible = False
Next
End If


New2.Enabled = True
ambience
If Simple.Checked = True Then
For Lb = 0 To 31
Picture1(Lb).Visible = False
Next
End If
If Medium.Checked = True Then
For Lb = 0 To 31
Picture1(Lb).Visible = False
Next
End If
If Big.Checked = True Then
For Lb = 0 To 31
Picture1(Lb).Visible = False
Next
End If
Image2.Visible = True
End Sub
Private Sub StartNewBoward()

End Sub

Private Sub sort1_Click()
ambience
sort1.Checked = True
sort2.Checked = False
End Sub

Private Sub sort2_Click()
ambience
sort2.Checked = True
sort1.Checked = False
End Sub

Private Sub Timer1_Timer()

retVal = mciSendString("close songNumber1", vbNullString, 0, 0)
retVal = mciSendString("close songNumber2", vbNullString, 0, 0)
retVal = mciSendString("close songNumber3", vbNullString, 0, 0)
retVal = mciSendString("close songNumber4", vbNullString, 0, 0)

End
End Sub

Private Sub Timer2_Timer()
retVal = mciSendString("close songNumber1", vbNullString, 0, 0)
Timer2.Enabled = False
End Sub

'Private Sub User_Pictures_Click()
'Default_Pictures.Checked = False
'User_Pictures.Checked = True
'PisturesPath = App.Path & "\User Pictures\"
'ambience






'Dim Lb



'F'or Lb = 0 To 31
'Picture1(Lb).Visible = False
'Next



'Image2.Visible = True




'End Sub
Private Sub SortSmall()
Dim Lc

For Lc = 0 To 5

If Lc < 3 Then
Picture1(Lc).Top = 2430
Else
Picture1(Lc).Top = 4430
End If

If Lc = 0 Then Picture1(0).Left = 4400
If Lc = 1 Then Picture1(1).Left = 4400 + 2400
If Lc = 2 Then Picture1(2).Left = 4400 + 4800
If Lc = 3 Then Picture1(3).Left = 4400
If Lc = 4 Then Picture1(4).Left = 4400 + 2400
If Lc = 5 Then Picture1(5).Left = 4400 + 4800
Next
End Sub
Private Sub SortMedium()

Dim Lc

For Lc = 0 To 14
If Lc < 5 Then
Picture1(Lc).Top = 2160 - 750
Picture1(Lc).Left = 2100 + (2400 * Lc)
ElseIf Lc < 10 Then
Picture1(Lc).Top = 4350 - 750
Picture1(Lc).Left = 2100 + (2400 * (Lc - 5))
Else
Picture1(Lc).Top = 4330 + 2230 - 750
Picture1(Lc).Left = 2100 + (2400 * (Lc - 10))
End If




'    If Lc = 0 Then Picture1(0).Left = 4400
'If Lc = 1 Then Picture1(1).Left = 4400 + 2400
'If Lc = 2 Then Picture1(2).Left = 4400 + 4800
'If Lc = 3 Then Picture1(3).Left = 4400
'If Lc = 4 Then Picture1(4).Left = 4400 + 2400
'If Lc = 5 Then Picture1(5).Left = 4400 + 4800
Next

End Sub

Private Sub SortBig()

Dim Lc

For Lc = 0 To 31


If Lc < 8 Then
Picture1(Lc).Top = 2160 - 2000
Picture1(Lc).Left = 13500 + (1800 * (Lc - 8)) + 1350
ElseIf Lc < 16 Then
Picture1(Lc).Top = 3160 + 800 - 2000 + 500
Picture1(Lc).Left = 13500 + (1800 * (Lc - 16)) + 1350
ElseIf Lc < 24 Then
Picture1(Lc).Top = 4160 + 1600 - 2000 + 1000
Picture1(Lc).Left = 13500 + (1800 * (Lc - 28 + 4)) + 1350
Else
Picture1(Lc).Top = 5160 + 2400 - 2000 + 1500
Picture1(Lc).Left = 13500 + (1800 * (Lc - 32)) + 1350
End If




'    If Lc = 0 Then Picture1(0).Left = 4400
'If Lc = 1 Then Picture1(1).Left = 4400 + 2400
'If Lc = 2 Then Picture1(2).Left = 4400 + 4800
'If Lc = 3 Then Picture1(3).Left = 4400
'If Lc = 4 Then Picture1(4).Left = 4400 + 2400
'If Lc = 5 Then Picture1(5).Left = 4400 + 4800











'If Lc < 3 Then
'Picture1(Lc).Top = 2430
'Else
'Picture1(Lc).Top = 4430
'End If



Next

End Sub
Private Sub AssignImage()
Dim RND1 As Double
Dim LoopVar As Long

If Dificulty = 6 Then
For LoopVar = 1 To 32

'RND1

Next

End If

If Dificulty = 14 Then
For LoopVar = 1 To 32

'RND1

Next
End If

If Dificulty = 32 Then
For LoopVar = 1 To 32
'RND1

Next
End If


End Sub
'--------------------------------------------------------------------
'--------------------------------------------------------------------
'--------------------------------------------------------------------
Private Sub Chose3Images()
'Dim PicturesTable(31) As Integer
Dim LopB
Dim AllDone As Boolean
AllDone = True
For LopB = 0 To 5
If PicturesTable(LopB) = 0 Then AllDone = False
'od 0 do 31
Next
If AllDone = False Then
Dim LopC
Do While LopC < 6
If PicturesTable(LopC) = 0 Then
'**********************************************
'wybierz nr obrazka do wstawienia
Dim rndH As Double
Dim loopP
AGAIN:
rndH = Int(Rnd * 3) + 1 + startCard


For loopP = 0 To 31
If PicturesTable(loopP) = rndH Then
PicSet(rndH) = PicSet(rndH) + 1

If PicSet(rndH) > 2 Then GoTo AGAIN
End If
Next


Debug.Print Str(LopC) & Str(rndH)
'czy juz byla taka karta rndH
'Dim x
'For x = 0 To 5


'If PicturesTable(x) = rndH Then

'End If

'Next


'if <>
PicturesTable(LopC) = rndH
PicSet(rndH) = 1

'If LopC = 5 Then
'PicturesTable(LopC) = 2
'PicSet(2) = 1
'End If
'Dim Firstchoice
'Randomize TimC
'Firstchoice = (Rnd * 16) + 1




'If PicSet(Firstchoice) = 0 Then
'PicturesTable(LopC) = Firstchoice
'Dim SecondPic As Boolean
'losowe miejsce
'Do While SecondPic = False


'PicturesTable(LopC + 1) = Firstchoice
'SecondPic = True

'Loop

'PicSet(Firstchoice) = 1
End If


'wybiezr obrazek 1-16 (obrazek nie wybrany)-'sprawdz czy wybrany 0 nie 1 tak PicSet(15)
'wybierz duplikat i umiesc
'PicturesTable (31)




LopC = LopC + 1
Loop
End If

End Sub
Private Sub Chose7Images()
'Dim PicturesTable(31) As Integer
Dim LopB
Dim AllDone As Boolean
AllDone = True
For LopB = 0 To 13
If PicturesTable(LopB) = 0 Then AllDone = False
'od 0 do 31
Next
If AllDone = False Then
Dim LopC
Do While LopC < 14
If PicturesTable(LopC) = 0 Then

'**********************************************
'wybierz nr obrazka do wstawienia
Dim rndH As Double



Dim loopP
AGAIN:
rndH = Int(Rnd * 7) + 1 + startCard


For loopP = 0 To 13
If PicturesTable(loopP) = rndH Then
PicSet(rndH) = PicSet(rndH) + 1

If PicSet(rndH) > 2 Then GoTo AGAIN
End If
Next






Debug.Print Str(LopC) & Str(rndH)
'czy juz byla taka karta rndH




PicturesTable(LopC) = rndH
PicSet(rndH) = 1

'If LopC = 13 Then
'PicturesTable(LopC) = 2
'PicSet(2) = 1
'End If



'wybiezr obrazek 1-16 (obrazek nie wybrany)-'sprawdz czy wybrany 0 nie 1 tak PicSet(15)
'wybierz duplikat i umiesc
'PicturesTable (31)
End If



LopC = LopC + 1
Loop
End If
End Sub
Private Sub Chose16Images()
'Dim PicturesTable(31) As Integer
Dim LopB
Dim AllDone As Boolean
AllDone = True
For LopB = 0 To 31
If PicturesTable(LopB) = 0 Then AllDone = False
'od 0 do 31
Next
If AllDone = False Then
Dim LopC
Do While LopC < 32
If PicturesTable(LopC) = 0 Then


'**********************************************
'wybierz nr obrazka do wstawienia
Dim rndH As Double


Dim loopP
AGAIN:
rndH = Int(Rnd * 16) + 1


For loopP = 0 To 31
If PicturesTable(loopP) = rndH Then
PicSet(rndH) = PicSet(rndH) + 1

If PicSet(rndH) > 2 Then GoTo AGAIN
End If
Next





Debug.Print Str(LopC) & Str(rndH)
'czy juz byla taka karta rndH



PicturesTable(LopC) = rndH
PicSet(rndH) = 1

'If LopC = 31 Then
'PicturesTable(LopC) = 2
'PicSet(2) = 1
'End If
'wybiezr obrazek 1-16 (obrazek nie wybrany)-'sprawdz czy wybrany 0 nie 1 tak PicSet(15)
'wybierz duplikat i umiesc
'PicturesTable (31)



End If
LopC = LopC + 1
Loop
End If
End Sub

'--------------------------------------------------------------------
'--------------------------------------------------------------------
'--------------------------------------------------------------------
'--------------------------------------------------------------------















Private Sub Timer3_Timer()
TimC = TimC + 1
If TimC > 20000 Then TimC = 0
End Sub

Private Sub Timer4_Timer()
Dim Lb


For Lb = 0 To 31
'Picture1(Lb).Visible = False
Picture1(Lb).Picture = LoadPicture(PicturesPath + "card.jpg")


CardTurned(Lb) = False



Next
Timer4.Enabled = False


Dim loopO
For loopO = 0 To 31
Picture1(loopO).Enabled = True
Next












End Sub
Private Sub Winer_test()
Dim loopT
Dim Notwiner
'Notwiner = False

For loopT = 0 To Dificulty - 1
If CardTurned(loopT) = False Then Notwiner = True
Next

If Notwiner = False Then



Dim Lb
For Lb = 0 To 31
Picture1(Lb).Visible = False
Next
Debug.Print "winer!" '+ Str(Notwiner)
Image3.Left = 4000: Image3.Top = 1700
Timer5.Enabled = False
Image3.Visible = True
Label2.Caption = ""
Label3.Visible = True

Label1.Caption = "Time -" + Str(TimeTotal)

If TimeTotal <= Val(places(2)) And Val(places(2)) <> 0 Then
places(0) = places(1)
places(1) = places(2)
places(2) = Str(TimeTotal)
Else
If Val(places(2)) = 0 Then places(2) = Str(TimeTotal)

End If




If Val(places(2)) <> 0 Then
Label2.Caption = Label2.Caption + places(2) + Chr(13)
Else
'plus = True
End If

'If Val(places(1)) <> 0 Then
'Label2.Caption = Label2.Caption + places(1) + Chr(13)
'Else
'If plus = True Then
'plus2 = True
'Else
'plus = True
'End If
'End If
'If Val(places(0)) <> 0 Then
'Label2.Caption = Trim(Label2.Caption + places(0))

'Else
'If plus = True Then
'plus2 = True
'Else
'plus = True
'End If
'End If
'If plus = True Then Label2.Caption = Label2.Caption + Chr(13) + "0"
'If plus2 = True Then Label2.Caption = Label2.Caption + Chr(13) + "0"










Label1.Visible = True
Label2.Visible = True
FileName = Chr(34) & App.Path + "\sound\coin.wav" & Chr(34)
retVal = mciSendString("close songNumber4", vbNullString, 0, 0)
retVal = mciSendString("open " & FileName & " type mpegvideo alias songNumber4", vbNullString, 0, 0)
mciSendString "setaudio songNumber4 volume to " & 520, vbNullString, 0, 0
retVal = mciSendString("play songNumber4", vbNullString, 0, 0)









End If

End Sub

Private Sub Timer5_Timer()
On Error Resume Next
'time over
If TimeTotal < 2147483647 Then TimeTotal = TimeTotal + 1
End Sub
