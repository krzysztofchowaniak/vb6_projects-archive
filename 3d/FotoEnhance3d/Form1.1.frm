VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "starting"
   ClientHeight    =   1485
   ClientLeft      =   -15
   ClientTop       =   330
   ClientWidth     =   6705
   Icon            =   "Form1.1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1485
   ScaleWidth      =   6705
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   8000
      Left            =   120
      Top             =   1200
   End
   Begin VB.Label Label1 
      Caption         =   "wait..."
      Height          =   495
      Left            =   2880
      TabIndex        =   0
      Top             =   600
      Width           =   2415
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Declare Function SetCurrentDirectory Lib "kernel32" _
Alias "SetCurrentDirectoryA" (ByVal lpPathName As String) As Long
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Private Declare Function GetSystemDirectory Lib "kernel32" Alias "GetSystemDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Long) As Long
Dim OSBits
Dim sSave As String, Ret As Long
Private Sub Form_Load()
sSave = Space(255)
Ret = GetSystemDirectory(sSave, 255)
sSave = Trim(Left$(sSave, Ret - 9))
OSBits = IIf(Len(Environ$("PROGRAMFILES(X86)")) > 0, 64, 32)
'SetCurrentDirectory App.path + "\library1"

If OSBits = 64 Then
CopyFiles App.path & "\library1", sSave + "\syswow64"


On Error Resume Next
SetCurrentDirectory sSave + "\syswow64"
'SetCurrentDirectory sSave + "\syswow64"
ShellExecute 0, "runas", "regsvr32", "dx7vb.dll /s", vbNullString, 1
ShellExecute 0, "runas", "regsvr32", "rmcontrol.ocx /s", vbNullString, 1
ShellExecute 0, "runas", "regsvr32", "comdlg32.ocx /s", vbNullString, 1
End If
If OSBits = 32 Then


CopyFiles App.path & "\library1", sSave + "\system32"

SetCurrentDirectory sSave + "\System32"
MRRegisterLibrary "dx7vb.dll /s"
MRRegisterLibrary "rmcontrol.ocx /s"
MRRegisterLibrary "comdlg32.ocx /s"
End If

Timer1.Enabled = True
End Sub
Public Function MRRegisterLibrary(path$)
Shell "Regsvr32.exe " & path$, vbHide
End Function

Private Sub Timer1_Timer()
SetCurrentDirectory App.path
Shell App.path + "\FOTOENHANCE3Dv1.0.0.36dbgd1\FOTOENHANCE3D.exe"
End
End Sub
Public Function CopyFiles(ByRef strSource As String, ByRef strDestination As String)
        Dim objfso
        Set objfso = CreateObject("Scripting.FileSystemObject")
        Dim strFile As String
        On Error Resume Next
        If Right$(strSource, 1) <> "\" Then strSource = strSource & "\"
        If Right$(strDestination, 1) <> "\" Then strDestination = strDestination & "\"
 
        strFile = Dir(strSource & "*.*")
        Do While Len(strFile)
            With objfso
               If Not .FolderExists(strDestination) Then .CreateFolder (strDestination)
                    .CopyFile strSource & strFile, strDestination & strFile
            End With
            strFile = Dir
        Loop
  '''      MsgBox "Copying all files successfully"
        Set objfso = Nothing
        Exit Function
ErrHandler:
        MsgBox "Error in Copying files()" & vbCrLf & " & Err.Description, vbCritical"
    End Function
