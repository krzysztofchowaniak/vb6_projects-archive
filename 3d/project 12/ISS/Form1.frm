VERSION 5.00
Object = "{6BF52A50-394A-11D3-B153-00C04F79FAA6}#1.0#0"; "wmp.dll"
Object = "{08216199-47EA-11D3-9479-00AA006C473C}#2.1#0"; "RMControl.ocx"
Begin VB.Form Form1 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BackColor       =   &H80000005&
   BorderStyle     =   0  'None
   ClientHeight    =   8895
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   14520
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   MouseIcon       =   "Form1.frx":0000
   MousePointer    =   99  'Custom
   ScaleHeight     =   593
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   968
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.CommandButton Command1 
      Height          =   495
      Index           =   5
      Left            =   120
      TabIndex        =   13
      Top             =   1920
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.CommandButton Command1 
      Height          =   495
      Index           =   4
      Left            =   120
      TabIndex        =   12
      Top             =   2520
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.CommandButton Command1 
      Height          =   495
      Index           =   3
      Left            =   120
      TabIndex        =   11
      Top             =   3120
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.CommandButton Command1 
      Height          =   495
      Index           =   2
      Left            =   120
      TabIndex        =   10
      Top             =   1320
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.CommandButton Command1 
      Height          =   495
      Index           =   1
      Left            =   120
      TabIndex        =   9
      Top             =   720
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.CommandButton Command1 
      Height          =   495
      Index           =   0
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.PictureBox Picture3 
      Appearance      =   0  'Flat
      BackColor       =   &H00400000&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      ForeColor       =   &H80000008&
      Height          =   1455
      Left            =   120
      Picture         =   "Form1.frx":030A
      ScaleHeight     =   1455
      ScaleWidth      =   4455
      TabIndex        =   4
      Top             =   120
      Visible         =   0   'False
      Width           =   4455
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   12
            Charset         =   238
            Weight          =   900
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Left            =   120
         TabIndex        =   5
         Top             =   120
         Width           =   4215
      End
   End
   Begin VB.PictureBox Picture4 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   5055
      Left            =   2640
      ScaleHeight     =   337
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   729
      TabIndex        =   3
      Top             =   1680
      Width           =   10935
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         ForeColor       =   &H80000008&
         Height          =   5775
         Left            =   720
         MousePointer    =   99  'Custom
         ScaleHeight     =   5775
         ScaleWidth      =   10335
         TabIndex        =   6
         Top             =   120
         Width           =   10335
         Begin RMControl7.RMCanvas RMCanvas1 
            Height          =   3015
            Left            =   1680
            TabIndex        =   7
            Top             =   840
            Width           =   4215
            _ExtentX        =   7435
            _ExtentY        =   5318
         End
      End
   End
   Begin VB.PictureBox Picture2 
      Height          =   735
      Left            =   120
      ScaleHeight     =   675
      ScaleWidth      =   1155
      TabIndex        =   0
      Top             =   8040
      Width           =   1215
      Begin VB.Timer Timer1 
         Interval        =   1
         Left            =   0
         Top             =   120
      End
   End
   Begin WMPLibCtl.WindowsMediaPlayer WindowsMediaPlayer1 
      Height          =   615
      Index           =   1
      Left            =   1560
      TabIndex        =   2
      Top             =   7800
      Visible         =   0   'False
      Width           =   2415
      URL             =   ""
      rate            =   1
      balance         =   0
      currentPosition =   0
      defaultFrame    =   ""
      playCount       =   1
      autoStart       =   -1  'True
      currentMarker   =   0
      invokeURLs      =   -1  'True
      baseURL         =   ""
      volume          =   50
      mute            =   0   'False
      uiMode          =   "full"
      stretchToFit    =   0   'False
      windowlessVideo =   0   'False
      enabled         =   -1  'True
      enableContextMenu=   -1  'True
      fullScreen      =   0   'False
      SAMIStyle       =   ""
      SAMILang        =   ""
      SAMIFilename    =   ""
      captioningID    =   ""
      enableErrorDialogs=   0   'False
      _cx             =   4260
      _cy             =   1085
   End
   Begin WMPLibCtl.WindowsMediaPlayer WindowsMediaPlayer1 
      Height          =   615
      Index           =   0
      Left            =   1560
      TabIndex        =   1
      Top             =   7200
      Visible         =   0   'False
      Width           =   2415
      URL             =   ""
      rate            =   1
      balance         =   0
      currentPosition =   0
      defaultFrame    =   ""
      playCount       =   1
      autoStart       =   -1  'True
      currentMarker   =   0
      invokeURLs      =   -1  'True
      baseURL         =   ""
      volume          =   50
      mute            =   0   'False
      uiMode          =   "full"
      stretchToFit    =   0   'False
      windowlessVideo =   0   'False
      enabled         =   -1  'True
      enableContextMenu=   -1  'True
      fullScreen      =   0   'False
      SAMIStyle       =   ""
      SAMILang        =   ""
      SAMIFilename    =   ""
      captioningID    =   ""
      enableErrorDialogs=   0   'False
      _cx             =   4260
      _cy             =   1085
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim TEXTURE_forBACKGROUND As Direct3DRMTexture3
Dim Mesh_OBJCT(15) As Direct3DRMMeshBuilder3
Dim Frame_OBJCT(15) As Direct3DRMFrame3
Dim TEXTURE_forObject(15) As Direct3DRMTexture3
Dim PLANETdir1 As D3DVECTOR
Dim PLANETdir2 As D3DVECTOR
Dim freFILE As Long
Dim mouseBUTTON As D3DVECTOR
Dim RNDmodel As String
Dim rndvariable As Long
Dim STATIONdir1 As D3DVECTOR
Dim STATIONdir2 As D3DVECTOR

Private Sub Form_Load()
Shell "regsvr32 " + App.Path + "/RMControl.ocx" + " /s"

WindowsMediaPlayer1(0).Left = -1000
WindowsMediaPlayer1(1).Left = -1000
Picture2.Left = -1000



rmcanvasinitial
musicinitial
End Sub
Private Sub musicinitial()

WindowsMediaPlayer1(1).URL = App.Path + "\Lista.wpl"
WindowsMediaPlayer1(1).settings.setMode "loop", True '
WindowsMediaPlayer1(1).settings.volume = 30
WindowsMediaPlayer1(1).Controls.stop
WindowsMediaPlayer1(1).Controls.play



End Sub
Private Sub Form_Resize()

Picture4.Left = 0
Picture4.Top = 0
Picture4.Width = Form1.Width
Picture4.Height = Form1.Height
Picture1.Left = 0
Picture1.Top = 0
Picture1.Width = Form1.Width
Picture1.Height = Form1.Height

RMCanvas1.Left = 0
RMCanvas1.Top = 0
RMCanvas1.Width = Form1.Width
RMCanvas1.Height = Form1.Height
End Sub

Private Sub Picture4_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

mouseBUTTON.X = X
mouseBUTTON.Y = Y
mouseBUTTON.z = Button

mouseoperations


End Sub


Private Sub mouseoperations()

Label1.Caption = Str(mouseBUTTON.X) + " " + Str(mouseBUTTON.Y) + " " + Str(mouseBUTTON.z)

End Sub

Private Sub Picture4_KeyPress(KeyAscii As Integer)
If KeyAscii = 27 Then Ending
End Sub

Private Sub Ending()


Frame_OBJCT(1).GetOrientation Nothing, STATIONdir1, STATIONdir2
Frame_OBJCT(0).GetOrientation Nothing, PLANETdir1, PLANETdir2
freFILE = FreeFile
Open App.Path + "\FILEdat.DAT" For Random As freFILE
Put freFILE, 1, PLANETdir1.X
Put freFILE, 2, PLANETdir1.Y
Put freFILE, 3, PLANETdir1.z
Put freFILE, 4, PLANETdir2.X
Put freFILE, 5, PLANETdir2.Y
Put freFILE, 6, PLANETdir2.z

Put freFILE, 7, STATIONdir1.X
Put freFILE, 8, STATIONdir1.Y
Put freFILE, 9, STATIONdir1.z
Put freFILE, 10, STATIONdir2.X
Put freFILE, 11, STATIONdir2.Y
Put freFILE, 12, STATIONdir2.z








Close freFILE

'Put freFILE, 1, VectorsOBJ(2, 5).X
End

End Sub
Private Sub cursorcoord()



End Sub

Private Sub Timer1_Timer()
Picture4.SetFocus
cursorcoord
RMCanvas1.Update



End Sub
Private Sub rmcanvasinitial()


With RMCanvas1
    .StartWindowed
    .SceneFrame.SetSceneBackgroundRGB 0, 0, 0.1
    .Viewport.SetBack 4000
    .CameraFrame.SetPosition Nothing, 0, 0, 0
End With
Set TEXTURE_forBACKGROUND = RMCanvas1.D3DRM.LoadTexture(App.Path + "/spacetime1.bmp")
RMCanvas1.SceneFrame.SetSceneBackgroundImage TEXTURE_forBACKGROUND
planetinitial


RMCanvas1.AmbientLight.SetColorRGB 0.5, 0.5, 0.5

End Sub
Private Sub planetinitial()





rndvariable = Rnd * 1000


'If rndvariable < 500 Then


Set Frame_OBJCT(2) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
Set Mesh_OBJCT(2) = RMCanvas1.D3DRM.CreateMeshBuilder()

Set TEXTURE_forObject(2) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\jupiter.bmp")

Mesh_OBJCT(2).LoadFromFile App.Path & "\Orbiter.x", 0, 0, Nothing, Nothing
Mesh_OBJCT(2).ScaleMesh 0.02, 0.02, 0.02

'Mesh_OBJCT(2).SetTexture TEXTURE_forObject(2)

Frame_OBJCT(2).SetPosition Nothing, 18, 15, 83
Frame_OBJCT(2).SetOrientation Nothing, 0.5, -0.2, -0.2, 0.28, 0.4, -0.4
Frame_OBJCT(2).SetRotation Nothing, 0, 1, 0, 0.0004
Mesh_OBJCT(2).SetColorRGB 0.4, 0.4, 0.4 ' RGB

Set Frame_OBJCT(0) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
Set Mesh_OBJCT(0) = RMCanvas1.D3DRM.CreateMeshBuilder()

Set TEXTURE_forObject(0) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\earthtexture.bmp")

Mesh_OBJCT(0).LoadFromFile App.Path & "\atr.x", 0, 0, Nothing, Nothing
Mesh_OBJCT(0).ScaleMesh 0.4, 0.4, 0.4

'Mesh_OBJCT(0).SetTexture TEXTURE_forObject(0)

Frame_OBJCT(0).SetPosition Nothing, 0, 0, 13
'Frame_OBJCT(0).SetOrientation Nothing, 0.5, -0.2, -0.2, 0.28, 0.4, -0.4
Frame_OBJCT(0).SetRotation Nothing, 0, 1, 0, 0.0003
Mesh_OBJCT(0).SetColorRGB 1, 1, 1 ' RGB


'Else
Set Frame_OBJCT(1) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
Set Mesh_OBJCT(1) = RMCanvas1.D3DRM.CreateMeshBuilder()

'Set TEXTURE_forObject(0) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\earthtexture.bmp")

Mesh_OBJCT(1).LoadFromFile App.Path & "\station.X", 0, 0, Nothing, Nothing
Mesh_OBJCT(1).ScaleMesh 1.1, 1.1, 1.1

Mesh_OBJCT(1).SetTexture TEXTURE_forObject(1)


Frame_OBJCT(1).SetPosition Nothing, -1, 0.03, 4
'Frame_OBJCT(0).SetOrientation Nothing, 0.5, -0.2, -0.2, 0.28, 0.4, -0.4
Frame_OBJCT(1).SetRotation Nothing, 0.1, 1, 0.5, 0.0003
Mesh_OBJCT(1).SetColorRGB 0.7, 0.7, 0.7 ' RGB

'End If









'Frame_OBJCT(0).GetOrientation Nothing, PLANETdir1, PLANETdir2
freFILE = FreeFile
Open App.Path + "\FILEdat.DAT" For Random As freFILE
Get freFILE, 1, PLANETdir1.X
Get freFILE, 2, PLANETdir1.Y
Get freFILE, 3, PLANETdir1.z
Get freFILE, 4, PLANETdir2.X
Get freFILE, 5, PLANETdir2.Y
Get freFILE, 6, PLANETdir2.z


Get freFILE, 7, STATIONdir1.X
Get freFILE, 8, STATIONdir1.Y
Get freFILE, 9, STATIONdir1.z
Get freFILE, 10, STATIONdir2.X
Get freFILE, 11, STATIONdir2.Y
Get freFILE, 12, STATIONdir2.z


Close freFILE

Frame_OBJCT(0).SetOrientation Nothing, PLANETdir1.X, PLANETdir1.Y, PLANETdir1.z, PLANETdir2.X, PLANETdir2.Y, PLANETdir2.z
Frame_OBJCT(1).SetOrientation Nothing, STATIONdir1.X, STATIONdir1.Y, STATIONdir1.z, STATIONdir2.X, STATIONdir2.Y, STATIONdir2.z


Frame_OBJCT(0).AddVisual Mesh_OBJCT(0)
Frame_OBJCT(1).AddVisual Mesh_OBJCT(1)
Frame_OBJCT(2).AddVisual Mesh_OBJCT(2)


End Sub
