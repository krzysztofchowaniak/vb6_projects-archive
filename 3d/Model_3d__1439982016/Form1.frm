VERSION 5.00
Object = "{08216199-47EA-11D3-9479-00AA006C473C}#2.1#0"; "RMControl.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form Form1 
   BorderStyle     =   0  'None
   Caption         =   "Xviewer"
   ClientHeight    =   7680
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   11880
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   512
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   792
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   5055
      Left            =   0
      ScaleHeight     =   337
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   529
      TabIndex        =   20
      Top             =   0
      Width           =   7935
      Begin RMControl7.RMCanvas RMCanvas1 
         Height          =   5055
         Left            =   120
         TabIndex        =   21
         Top             =   240
         Width           =   7935
         _ExtentX        =   13996
         _ExtentY        =   8916
      End
   End
   Begin VB.CommandButton Command4 
      Caption         =   "reset"
      Height          =   375
      Left            =   9240
      TabIndex        =   19
      Top             =   960
      Width           =   855
   End
   Begin VB.CheckBox Check2 
      Caption         =   "rev"
      Height          =   375
      Left            =   11280
      Style           =   1  'Graphical
      TabIndex        =   18
      Top             =   2520
      Width           =   495
   End
   Begin VB.CheckBox Check1 
      Caption         =   "rotate z"
      Height          =   375
      Index           =   2
      Left            =   10320
      Style           =   1  'Graphical
      TabIndex        =   17
      Top             =   2520
      Width           =   855
   End
   Begin VB.CheckBox Check1 
      Caption         =   "rotate y"
      Height          =   375
      Index           =   1
      Left            =   9360
      Style           =   1  'Graphical
      TabIndex        =   16
      Top             =   2520
      Width           =   855
   End
   Begin VB.CheckBox Check1 
      Caption         =   "rotate x"
      Height          =   375
      Index           =   0
      Left            =   8400
      Style           =   1  'Graphical
      TabIndex        =   15
      Top             =   2520
      Width           =   855
   End
   Begin VB.CommandButton Command3 
      Caption         =   "fullscreen\esc"
      Height          =   375
      Left            =   10200
      TabIndex        =   11
      Top             =   3240
      Width           =   1695
   End
   Begin VB.TextBox Text7 
      Height          =   285
      Left            =   10200
      TabIndex        =   8
      Top             =   2160
      Width           =   1575
   End
   Begin VB.TextBox Text6 
      Height          =   285
      Left            =   10200
      TabIndex        =   7
      Top             =   1800
      Width           =   1575
   End
   Begin VB.TextBox Text5 
      Height          =   285
      Left            =   10200
      TabIndex        =   6
      Top             =   1440
      Width           =   1575
   End
   Begin VB.TextBox Text4 
      Height          =   285
      Left            =   8400
      TabIndex        =   5
      Top             =   2160
      Width           =   1695
   End
   Begin VB.TextBox Text3 
      Height          =   285
      Left            =   8400
      TabIndex        =   4
      Top             =   1800
      Width           =   1695
   End
   Begin VB.TextBox Text2 
      Height          =   285
      Left            =   8400
      TabIndex        =   3
      Top             =   1440
      Width           =   1695
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   8400
      TabIndex        =   2
      Top             =   480
      Width           =   1695
   End
   Begin VB.CommandButton Command2 
      Caption         =   "end"
      Height          =   375
      Left            =   8400
      TabIndex        =   1
      Top             =   4800
      Width           =   1695
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   2280
      Top             =   5520
   End
   Begin VB.CommandButton Command1 
      Caption         =   "open model .x"
      Height          =   375
      Left            =   8400
      TabIndex        =   0
      Top             =   3240
      Width           =   1695
   End
   Begin MSComDlg.CommonDialog CDialog1 
      Left            =   1680
      Top             =   5520
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label Label5 
      Caption         =   "0 images"
      Height          =   495
      Left            =   10200
      TabIndex        =   14
      Top             =   4200
      Width           =   3495
   End
   Begin VB.Label Label4 
      Caption         =   """r"" for render picture when fullscreen -"
      Height          =   375
      Left            =   10200
      TabIndex        =   13
      Top             =   3720
      Width           =   1695
   End
   Begin VB.Label Label3 
      Caption         =   "Label3"
      Height          =   375
      Left            =   3840
      TabIndex        =   12
      Top             =   6000
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label2 
      Caption         =   "orientation"
      Height          =   255
      Left            =   8400
      TabIndex        =   10
      Top             =   960
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "distance"
      Height          =   255
      Left            =   8400
      TabIndex        =   9
      Top             =   120
      Width           =   1455
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim filenamepicture As String
Dim Mesh_OBJCT As Direct3DRMMeshBuilder3
Dim Frame_OBJCT As Direct3DRMFrame3
Dim v1 As String
Dim v2 As Long
Dim rotatDir As Integer

Dim frefile As Long
Dim fulscren As Boolean
Dim renderednumer As Long
Private Declare Sub keybd_event _
    Lib "user32" ( _
        ByVal bVk As Byte, _
        ByVal bScan As Byte, _
        ByVal dwFlags As Long, _
        ByVal dwExtraInfo As Long)
Dim mdlOrntDIR As D3DVECTOR
Dim mdlOrntUP As D3DVECTOR









Private Sub Command1_Click()



CDialog1.Filter = "X (*.x)"
CDialog1.ShowOpen
filenamepicture = CDialog1.FileTitle
If filenamepicture <> "" Then
'Set TEXTURE_forObject(0) = RMCanvas1.D3DRM.LoadTexture(App.Path + "\TEMP.bmp")
'Mesh_OBJCT(0).SetTexture TEXTURE_forObject(0)

Mesh_OBJCT.Empty
On Error Resume Next
Mesh_OBJCT.LoadFromFile filenamepicture, 0, 0, Nothing, Nothing
Mesh_OBJCT.ScaleMesh 1, 1, 1
Frame_OBJCT.SetPosition Nothing, 0, 0, Text1.Text
Frame_OBJCT.AddVisual Mesh_OBJCT






filenamepicture = ""

End If













End Sub

Private Sub Command2_Click()



'Frame_OBJCT.GetOrientation Nothing, mdlOrntDIR, mdlOrntUP
'Get frefile, 2, v1
'Text2.Text = Str(mdlOrntDIR.x)
'Get frefile, 3, v1
'Text3.Text = Str(mdlOrntDIR.y)
'Get frefile, 4, v1
'Text4.Text = Str(mdlOrntDIR.z)
'Get frefile, 5, v1
'Text5.Text = Str(mdlOrntUP.x)
'Get frefile, 6, v1
'Text6.Text = Str(mdlOrntUP.y)
'Get frefile, 7, v1
'Text7.Text = Str(mdlOrntUP.z)







frefile = FreeFile
Open App.Path + "\FILE.DAT" For Random As frefile

Frame_OBJCT.GetOrientation Nothing, mdlOrntDIR, mdlOrntUP
Put frefile, 1, Text1.Text
Put frefile, 2, mdlOrntDIR.x
Put frefile, 3, mdlOrntDIR.y
Put frefile, 4, mdlOrntDIR.z
Put frefile, 5, mdlOrntUP.x
Put frefile, 6, mdlOrntUP.y
Put frefile, 7, mdlOrntUP.z




Close frefile


End
End Sub

Private Sub Command3_Click()

If fulscren = False Then
fulscren = True



Picture1.Left = 0: Picture1.Top = 0: Picture1.Width = Form1.ScaleWidth: Picture1.Height = Form1.ScaleHeight
RMCanvas1.Left = 0: RMCanvas1.Top = 0: RMCanvas1.Width = Form1.ScaleWidth: RMCanvas1.Height = Form1.ScaleHeight


Form1.SetFocus
End If
End Sub



Private Sub Command4_Click()



Text2.Text = 0
Text3.Text = 0
Text4.Text = 1
Text5.Text = 0
Text6.Text = 1
Text7.Text = 0






End Sub

Private Sub Form_Load()


Picture1.Left = 20: Picture1.Width = 520
Picture1.Top = 20: Picture1.Height = 320
RMCanvas1.Left = 0: RMCanvas1.Top = 0:
RMCanvas1.Width = 520: RMCanvas1.Height = 320



With RMCanvas1
    .StartWindowed
    .SceneFrame.SetSceneBackgroundRGB 0, 0.2, 0.4
    .Viewport.SetBack 4000
    .CameraFrame.SetPosition Nothing, 0, 0, 0
End With
'RMCanvas1.AmbientLight.SetColorRGB 0.3, 0.3, 0.3


Set Frame_OBJCT = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
Set Mesh_OBJCT = RMCanvas1.D3DRM.CreateMeshBuilder()



frefile = FreeFile
Open App.Path + "\FILE.DAT" For Random As frefile
Get frefile, 1, v1
Text1.Text = v1


Get frefile, 2, mdlOrntDIR.x
Text2.Text = mdlOrntDIR.x
Get frefile, 3, mdlOrntDIR.y
Text3.Text = mdlOrntDIR.y
Get frefile, 4, mdlOrntDIR.z
Text4.Text = mdlOrntDIR.z
Get frefile, 5, mdlOrntUP.x
Text5.Text = mdlOrntUP.x
Get frefile, 6, mdlOrntUP.y
Text6.Text = mdlOrntUP.y
Get frefile, 7, mdlOrntUP.z
Text7.Text = mdlOrntUP.z



Close frefile

Frame_OBJCT.SetOrientation Nothing, mdlOrntDIR.x, mdlOrntDIR.y, mdlOrntDIR.z, mdlOrntUP.x, mdlOrntUP.y, mdlOrntUP.z



End Sub

Private Sub Form_LostFocus()
Form1.SetFocus
End Sub

Private Sub Picture1_KeyPress(KeyAscii As Integer)
Label3.Caption = Str(KeyAscii)
If KeyAscii = 27 Then
If fulscren = True Then
fulscren = False
Picture1.Left = 20: Picture1.Width = 520
Picture1.Top = 20: Picture1.Height = 320
RMCanvas1.Left = 0: RMCanvas1.Top = 0:
RMCanvas1.Width = 520: RMCanvas1.Height = 320
End If
End If
If KeyAscii = 114 Then
If fulscren = True Then

Clipboard.Clear
    Call keybd_event(44, 2, 0, 0)
    DoEvents
    If Clipboard.GetFormat(vbCFBitmap) Then
    SavePicture Clipboard.GetData(vbCFBitmap), Trim(App.Path) + Trim("\images\IMAGE_") + Trim(Str(renderednumer)) + Trim("_") + Trim(".bmp")
    End If
Label5.Caption = Trim(Str(renderednumer + 1)) + " images"
renderednumer = renderednumer + 1

End If
End If




End Sub

Private Sub Text1_Change()
On Error Resume Next

Frame_OBJCT.SetPosition Nothing, 0, 0, Text1.Text


End Sub

Private Sub Text2_Change()
On Error Resume Next
If Text2.Enabled = True Then Frame_OBJCT.SetOrientation Nothing, Val(Text2.Text), Val(Text3.Text), Val(Text4.Text), Val(Text5.Text), Val(Text6.Text), Val(Text7.Text)
End Sub

Private Sub Text3_Change()
On Error Resume Next
If Text3.Enabled = True Then Frame_OBJCT.SetOrientation Nothing, Val(Text2.Text), Val(Text3.Text), Val(Text4.Text), Val(Text5.Text), Val(Text6.Text), Val(Text7.Text)
End Sub

Private Sub Text4_Change()
On Error Resume Next
If Text4.Enabled = True Then Frame_OBJCT.SetOrientation Nothing, Val(Text2.Text), Val(Text3.Text), Val(Text4.Text), Val(Text5.Text), Val(Text6.Text), Val(Text7.Text)
End Sub

Private Sub Text5_Change()
On Error Resume Next
If Text5.Enabled = True Then Frame_OBJCT.SetOrientation Nothing, Val(Text2.Text), Val(Text3.Text), Val(Text4.Text), Val(Text5.Text), Val(Text6.Text), Val(Text7.Text)

End Sub

Private Sub Text6_Change()
On Error Resume Next
If Text6.Enabled = True Then Frame_OBJCT.SetOrientation Nothing, Val(Text2.Text), Val(Text3.Text), Val(Text4.Text), Val(Text5.Text), Val(Text6.Text), Val(Text7.Text)

End Sub

Private Sub Text7_Change()
On Error Resume Next

If Text7.Enabled = True Then Frame_OBJCT.SetOrientation Nothing, Val(Text2.Text), Val(Text3.Text), Val(Text4.Text), Val(Text5.Text), Val(Text6.Text), Val(Text7.Text)

End Sub

Private Sub Timer1_Timer()
If fulscren = True Then Picture1.SetFocus


If Check1(0).Value = 0 And Check1(1).Value = 0 And Check1(2).Value = 0 Then
Frame_OBJCT.SetRotation Frame_OBJCT, Check1(0).Value, Check1(1).Value, Check1(2).Value, 0
Text2.Enabled = True
Text3.Enabled = True
Text4.Enabled = True
Text5.Enabled = True
Text6.Enabled = True
Text7.Enabled = True

Else
If Check2.Value = 1 Then
Frame_OBJCT.SetRotation Frame_OBJCT, Check1(0).Value, Check1(1).Value, Check1(2).Value, 0.01
Else
Frame_OBJCT.SetRotation Frame_OBJCT, Check1(0).Value, Check1(1).Value, Check1(2).Value, -0.01
End If

Text2.Enabled = False
Text3.Enabled = False
Text4.Enabled = False
Text5.Enabled = False
Text6.Enabled = False
Text7.Enabled = False




Frame_OBJCT.GetOrientation Nothing, mdlOrntDIR, mdlOrntUP
Text2.Text = mdlOrntDIR.x
Text3.Text = mdlOrntDIR.y
Text4.Text = mdlOrntDIR.z
Text5.Text = mdlOrntUP.x
Text6.Text = mdlOrntUP.y
Text7.Text = mdlOrntUP.z

End If










RMCanvas1.Update























End Sub
