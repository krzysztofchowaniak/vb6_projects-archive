VERSION 5.00
Object = "{08216199-47EA-11D3-9479-00AA006C473C}#2.1#0"; "rmcontrol.ocx"
Begin VB.Form Form1 
   BackColor       =   &H00404000&
   BorderStyle     =   0  'None
   Caption         =   "KnOfRe3D"
   ClientHeight    =   10395
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   17520
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   HasDC           =   0   'False
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   MouseIcon       =   "Form1.frx":1B3EA
   MousePointer    =   99  'Custom
   Moveable        =   0   'False
   NegotiateMenus  =   0   'False
   ScaleHeight     =   693
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   1168
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.PictureBox Picture3 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   8055
      Left            =   14640
      MouseIcon       =   "Form1.frx":1BA14
      MousePointer    =   99  'Custom
      ScaleHeight     =   8055
      ScaleWidth      =   2655
      TabIndex        =   8
      Top             =   1320
      Width           =   2655
      Begin VB.CommandButton Command1 
         Caption         =   "Exit"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   4560
         Width           =   2175
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Continue"
         Enabled         =   0   'False
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   4200
         Width           =   2175
      End
      Begin VB.CommandButton Command3 
         Caption         =   "New Game"
         Enabled         =   0   'False
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   3840
         Width           =   2175
      End
   End
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      BackColor       =   &H00008000&
      Enabled         =   0   'False
      ForeColor       =   &H80000008&
      Height          =   4935
      Left            =   480
      ScaleHeight     =   4905
      ScaleWidth      =   10785
      TabIndex        =   2
      Top             =   1080
      Visible         =   0   'False
      Width           =   10815
      Begin VB.PictureBox Picture4 
         Appearance      =   0  'Flat
         AutoRedraw      =   -1  'True
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   2415
         Left            =   2400
         ScaleHeight     =   159
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   159
         TabIndex        =   14
         Top             =   240
         Width           =   2415
      End
      Begin VB.TextBox Text1 
         Height          =   9255
         Left            =   11160
         TabIndex        =   6
         Text            =   "Text1"
         Top             =   600
         Width           =   10575
      End
      Begin VB.Label Label6 
         Caption         =   "Label6"
         Height          =   1455
         Left            =   4920
         TabIndex        =   13
         Top             =   2400
         Width           =   5655
      End
      Begin VB.Label Label5 
         Caption         =   "SAVE OFF"
         Height          =   375
         Left            =   120
         TabIndex        =   9
         Top             =   4440
         Width           =   1215
      End
      Begin VB.Label Label4 
         Caption         =   "Label4"
         Height          =   375
         Left            =   4920
         TabIndex        =   7
         Top             =   1920
         Width           =   5655
      End
      Begin VB.Label Label3 
         Caption         =   "keys: w,a,s,d,q,e,r,f,t,g,shift"
         Height          =   375
         Left            =   4920
         TabIndex        =   5
         Top             =   1440
         Width           =   5655
      End
      Begin VB.Label Label2 
         Caption         =   "Label2"
         Height          =   495
         Left            =   4920
         TabIndex        =   4
         Top             =   840
         Width           =   5655
      End
      Begin VB.Label Label1 
         Caption         =   "Label1"
         Height          =   495
         Left            =   4920
         TabIndex        =   3
         Top             =   240
         Width           =   5655
         WordWrap        =   -1  'True
      End
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   1815
      Left            =   240
      MouseIcon       =   "Form1.frx":1EC5E
      MousePointer    =   99  'Custom
      ScaleHeight     =   121
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   153
      TabIndex        =   0
      Top             =   240
      Width           =   2295
      Begin RMControl7.RMCanvas RMCanvas1 
         Height          =   1215
         Left            =   0
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   240
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   2143
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   5
      Left            =   2640
      Top             =   120
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


''''''''''''''''''''''''''''''''''''''''''''''''''''map

Dim HrFrapsCounter2 As Long
Dim HrFrapsCounter As Integer
Dim Hr As Integer
'HERO_MESH
'gamepad input''''''gamepad input''''''gamepad input''''''gamepad input'''''
'gamepad input''''''gamepad input''''''gamepad input''''''gamepad input'''''
Private Const JOY_RETURNBUTTONS As Long = &H80&
Private Const JOY_RETURNCENTERED As Long = &H400&
Private Const JOY_RETURNPOV As Long = &H40&
Private Const JOY_RETURNPOVCTS As Long = &H200&
Private Const JOY_RETURNR As Long = &H8&
Private Const JOY_RETURNRAWDATA As Long = &H100&
Private Const JOY_RETURNU As Long = &H10
Private Const JOY_RETURNV As Long = &H20
Private Const JOY_RETURNX As Long = &H1&
Private Const JOY_RETURNY As Long = &H2&
Private Const JOY_RETURNZ As Long = &H4&
Private Const JOY_RETURNALL As Long = (JOY_RETURNX Or JOY_RETURNY Or JOY_RETURNZ Or JOY_RETURNR Or JOY_RETURNU Or JOY_RETURNV Or JOY_RETURNPOV Or JOY_RETURNBUTTONS)
Private Type JOYINFOEX
    dwSize As Long ' size of structure
    dwFlags As Long ' flags to dicate what to return
    dwXpos As Long ' x position
    dwYpos As Long ' y position
    dwZpos As Long ' z position
    dwRpos As Long ' rudder/4th axis position
    dwUpos As Long ' 5th axis position
    dwVpos As Long ' 6th axis position
    dwButtons As Long ' button states
    dwButtonNumber As Long ' current button number pressed
    dwPOV As Long ' point of view state
    dwReserved1 As Long ' reserved for communication between winmm driver
    dwReserved2 As Long ' reserved for future expansion
End Type
Private Declare Function joyGetPosEx Lib "winmm.dll" (ByVal uJoyID As Long, ByRef pji As JOYINFOEX) As Long
Dim JI As JOYINFOEX
Const JNum As Long = 0
'Set this to the number of the joystick that
'you want to read (a value between 0 and 15).
'The first joystick plugged in is number 0.
'The API for reading joysticks supports up to
'16 simultaniously plugged in joysticks.
'Change this Const to a Dim if you want to set
'it at runtime.
'gamepad input''''''gamepad input''''''gamepad input''''''gamepad input'''''




'daytimelight
Dim SunVector As D3DVECTOR
Dim SunRGBVector As D3DVECTOR
Dim Godzina As Double
Dim NaWschod As Boolean
Dim NaPolnoc As Boolean
'daytimelight
Dim XcursorMice As Double
Dim ZcursorMice As Double
'swampAnim
'Dim swampAnim1 As Integer
'Dim swampAnim2 As Boolean
'Dim swampAnim3 As Integer
'swampAnim
Dim MOUSElock As Boolean
'MAKEGREY
Private Declare Function GetColorAdjustment Lib "gdi32" (ByVal hdc As Long, lpca As COLORADJUSTMENT) As Long
Private Declare Function SetColorAdjustment Lib "gdi32" (ByVal hdc As Long, lpca As COLORADJUSTMENT) As Long
Private Declare Function SetStretchBltMode Lib "gdi32" (ByVal hdc As Long, ByVal nStretchMode As Long) As Long
Private Declare Function StretchBlt Lib "gdi32" (ByVal hdc As Long, ByVal x As Long, ByVal y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, ByVal nSrcWidth As Long, ByVal nSrcHeight As Long, ByVal dwRop As Long) As Long

Private Type COLORADJUSTMENT
    caSize As Integer
    caFlags As Integer
    caIlluminantIndex As Integer
    caRedGamma As Integer
    caGreenGamma As Integer
    caBlueGamma As Integer
    caReferenceBlack As Integer
    caReferenceWhite As Integer
    caContrast As Integer
    caBrightness As Integer
    caColorfulness As Integer
    caRedGreenTint As Integer
End Type

Private Const HALFTONE = 4
'MAKEGREY
'printscreen
Private Declare Sub keybd_event Lib "user32" ( _
               ByVal bVk As Byte, _
               ByVal bScan As Byte, _
               ByVal dwFlags As Long, _
               ByVal dwExtraInfo As Long)

'printscreen
Dim XAbsolute As Long
Dim ZAbsolute As Long
Dim CameraPosAdX As Long
Dim CameraPosAdZ As Long
Dim BlockedNewLine As Boolean
'sound
Private Declare Function SetCurrentDirectory Lib "kernel32" _
Alias "SetCurrentDirectoryA" (ByVal lpPathName As String) As Long
Dim retVal As Long
Dim filename As String
Private Declare Function mciSendString Lib "winmm.dll" Alias "mciSendStringA" (ByVal lpstrCommand As String, ByVal lpstrReturnString As String, ByVal uReturnLength As Long, ByVal hwndCallback As Long) As Long
'sound
Dim PrevX As Long, PrevZ As Long
Dim PrevNoChange As Boolean
Dim bigmap(500, 500) As Integer
Dim monsters(500, 500, 5) As Integer
Dim MapLoaded As Boolean
Dim MonstersLoaded As Boolean
Dim HOVERINGX As Double 'WAVES VARIABLES
Dim HOVERINGY As Double 'WAVES VARIABLES

Dim freFile As Integer
'cursor limit'cursor limit'cursor limit
Private Type RECT2
Left As Integer
Top As Integer '
Right As Integer
Bottom As Integer
End Type
Private Type POINT
x As Long
y As Long
End Type
Private Declare Sub ClipCursor Lib "user32" (lpRect As Any)
Private Declare Sub GetClientRect Lib "user32" (ByVal hWnd As Long, lpRect As RECT2)
Private Declare Sub ClientToScreen Lib "user32" (ByVal hWnd As Long, lpPoint As POINT)
Private Declare Sub OffsetRect Lib "user32" (lpRect As RECT2, ByVal x As Long, ByVal y As Long)
'cursor limit'cursor limit'cursor limit
Dim light As Direct3DRMLight
Public di As DirectInput
Public mdevice As DirectInputDevice
Dim mstate As DIMOUSESTATE
Public dx7 As New DirectX7
Dim handlemice(3) As Long
Dim capsON As Boolean
Dim keypressed(255) As Boolean
'rmcontrol functions

Dim Vectors(100) As D3DVECTOR
Dim TEXTURE_forBackground(20) As Direct3DRMTexture3
Dim lightON As Boolean
'---------------------------------
Dim MNTvec(1) As D3DVECTOR
''''''''''''''''''''''''''''''''''''''''''''''''''''map
'Dim Mesh_crs As Direct3DRMMeshBuilder3
'Dim Frame_crs As Direct3DRMFrame3
'''''''''''''''''''''''''''''''''''''''''''''''''''''
'Dim Mesh_crs As Direct3DRMMeshBuilder3
'Dim Frame_crs As Direct3DRMFrame3
'''''''''''''''''''''''''''''''''''''''''''''''''''''
Dim Mesh_hr(10, 2) As Direct3DRMMeshBuilder3
Dim Frame_hr(10) As Direct3DRMFrame3
Dim Vectors_hr(10, 2) As D3DVECTOR

Dim Mesh_brd(1) As Direct3DRMMeshBuilder3
Dim Frame_brd As Direct3DRMFrame3
Dim brdWNG As Integer
Dim brdWNG2 As Integer
Dim brdWNG3 As Integer
Dim brdWNG4 As Boolean
Dim brdWNG5 As Integer

Dim Mesh_frm As Direct3DRMMeshBuilder3
Dim Frame_frm As Direct3DRMFrame3
Dim Xfrm As Long, Zfrm As Long
Dim prevXfrm As Long, prevZfrm As Long

Dim Mesh_surface(10, 10) As Direct3DRMMeshBuilder3
Dim Frame_surface(10, 10) As Direct3DRMFrame3
Dim TEXTURE_surface(18) As Direct3DRMTexture3

Dim Mesh_Asset(10, 10) As Direct3DRMMeshBuilder3
Dim Frame_Asset(10, 10) As Direct3DRMFrame3

Dim Mesh_Mnst(10, 10, 1) As Direct3DRMMeshBuilder3
Dim Frame_Mnst(10, 10, 1) As Direct3DRMFrame3

Dim randommnst As Integer
Dim randommap As Integer
'Dim XLeftBotom(1) As Integer
Dim XReplaceSrfc(12) As Long
Dim ZReplaceSrfc(12) As Long
Dim Zdist As Long
Dim Xdist As Long
Dim actualX As Long
Dim actualZ As Long
'Dim BReplaceSrfc As Boolean
''''''''''''''''''''''''''''''''''''''''''''''''''''map
Dim Xmap As Long, Zmap As Long
Dim xi As Long, zi As Long
Dim svpos As Long
Dim loopsvy As Long
Dim loopsvx As Long

Private Sub GetScreenshot(Optional ByVal hWnd As Long = 0)

    Clipboard.Clear
    Call keybd_event(44, 2, 0, 0)
    DoEvents
    'If Clipboard.GetFormat(vbCFBitmap) Then

Picture3.PaintPicture Clipboard.GetData(vbCFBitmap), 0, 0, Me.Width, Me.Height, Form1.Left, Form1.Top, Me.Width, Me.Height

'End If
End Sub

Private Sub loadMAP()
'Text1.Text = ""
svpos = 0
freFile = FreeFile
Open App.Path & "\programdata2\mapFILE.DAT" For Random As freFile
For loopsvy = 0 To 500
For loopsvx = 0 To 500
Get freFile, svpos + 1, bigmap(loopsvx, 500 - loopsvy)
'Text1.Text = Text1.Text + Str(bigmap(loopsvx, 500 - loopsvy))
svpos = svpos + 1
Next loopsvx
'Text1.Text = Text1.Text + vbCrLf
Next loopsvy
Close freFile
MapLoaded = True
End Sub

Private Sub LoadTextures()
'TEXTURE_surface(10, 10) As Direct3DRMTexture3

Set TEXTURE_surface(0) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\0.bmp")

Set TEXTURE_surface(4) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\4.bmp")
Set TEXTURE_surface(18) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\18.bmp")

Set TEXTURE_surface(5) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\5.bmp")
Set TEXTURE_surface(6) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\6.bmp")
Set TEXTURE_surface(7) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\7.bmp")
Set TEXTURE_surface(8) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\8.bmp")
Set TEXTURE_surface(9) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\9.bmp")
Set TEXTURE_surface(10) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\10.bmp")
Set TEXTURE_surface(11) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\11.bmp")
Set TEXTURE_surface(12) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\12.bmp")
Set TEXTURE_surface(13) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\13.bmp")
Set TEXTURE_surface(14) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\14.bmp")
Set TEXTURE_surface(15) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\15.bmp")
Set TEXTURE_surface(16) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\16.bmp")
Set TEXTURE_surface(17) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\17.bmp")


End Sub
Private Sub SETmeshes()
'swampAnim1 = 225
On Error Resume Next


Set Frame_hr(10) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
Set Mesh_hr(10, 2) = RMCanvas1.D3DRM.CreateMeshBuilder()
Set Mesh_hr(10, 1) = RMCanvas1.D3DRM.CreateMeshBuilder()
Set Mesh_hr(10, 0) = RMCanvas1.D3DRM.CreateMeshBuilder()
'RMCanvas1.CameraFrame.GetPosition Nothing, Vectors(93)

Mesh_hr(10, 0).ScaleMesh 1, 1, 1
Mesh_hr(10, 1).ScaleMesh 1, 1, 1
Mesh_hr(10, 2).ScaleMesh 1, 1, 1
Frame_hr(10).SetPosition RMCanvas1.CameraFrame, 0, 0, 21



Set Frame_frm = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
Set Mesh_frm = RMCanvas1.D3DRM.CreateMeshBuilder()

For xi = 0 To 10
For zi = 0 To 10

Set Frame_surface((xi), (zi)) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
Set Mesh_surface((xi), (zi)) = RMCanvas1.D3DRM.CreateMeshBuilder()
Set Frame_Asset((xi), (zi)) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
Set Mesh_Asset((xi), (zi)) = RMCanvas1.D3DRM.CreateMeshBuilder()
Set Frame_Mnst(xi, zi, 1) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
Set Mesh_Mnst(xi, zi, 1) = RMCanvas1.D3DRM.CreateMeshBuilder()

Frame_Asset((xi), (zi)).SetOrientation Nothing, MNTvec(0).x, MNTvec(0).y, MNTvec(0).z, MNTvec(1).x, MNTvec(1).y, MNTvec(1).z


Next
Next

Set Frame_brd = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
Set Mesh_brd(1) = RMCanvas1.D3DRM.CreateMeshBuilder()
Set Mesh_brd(0) = RMCanvas1.D3DRM.CreateMeshBuilder()
Mesh_brd(0).LoadFromFile App.Path & "\programdata1\b1.x", 0, 0, Nothing, Nothing
Mesh_brd(1).LoadFromFile App.Path & "\programdata1\b2.x", 0, 0, Nothing, Nothing
Mesh_brd(0).ScaleMesh 1, 1, 1
Mesh_brd(1).ScaleMesh 1, 1, 1
Mesh_brd(0).SetColorRGB 0.4, 0.1, 0.1
Mesh_brd(1).SetColorRGB 0.4, 0.1, 0.1
Frame_brd.SetRotation Nothing, 0, 1, 0, 0.01
If brdWNG = 0 Then
Frame_brd.AddVisual Mesh_brd(1)
Else
Frame_brd.AddVisual Mesh_brd(0)
End If
 Frame_brd.SetName "Birdy1"
 brdWNG4 = True
 
'Set Frame_crs = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
'Set Mesh_crs = RMCanvas1.D3DRM.CreateMeshBuilder()
'Mesh_crs.LoadFromFile App.Path & "\programdata1\crsr3d.x", 0, 0, Nothing, Nothing
'Mesh_crs.ScaleMesh 0.1, 0.1, 0.1
'Mesh_crs.SetColorRGB 0.1, 0.1, 0.4
'Frame_crs.AddVisual Mesh_crs
End Sub


Private Sub initObjects()
On Error Resume Next

'XReplaceSrfc(0) = 10
'ZReplaceSrfc(0) = 10

'XReplaceSrfc(12) = 10
'ZReplaceSrfc(12) = 10

'''Set light = RMCanvas1.D3DRM.CreateLightRGB(D3DRMLIGHT_POINT, 0, 0, 11)
'light.SetPenumbra 0.5


'XLeftBotom(0) = 0
'XLeftBotom(1) = 0

For xi = 0 To 10
For zi = 0 To 10

'Frame_Asset(xi, zi).DeleteLight light
Frame_surface((xi), (zi)).DeleteVisual Mesh_surface((xi), (zi))
Frame_Asset(xi, zi).DeleteVisual Mesh_Asset(xi, zi)
Mesh_surface(xi, zi).Empty
Mesh_Asset((xi), (zi)).Empty
Mesh_Mnst((xi), (zi), 0).Empty
Mesh_Mnst((xi), (zi), 1).Empty
Next
Next
Frame_frm.DeleteVisual Mesh_frm
Mesh_frm.Empty
If brdWNG = 1 Then
Frame_brd.DeleteVisual Mesh_brd(0)
Else
Frame_brd.DeleteVisual Mesh_brd(1)
End If
Mesh_brd(0).Empty
Mesh_brd(1).Empty
'empty
If Hr = 0 Then
Frame_hr(10).DeleteVisual Mesh_hr(10, 0)
End If
If Hr = 1 Then
Frame_hr(10).DeleteVisual Mesh_hr(10, 1)
End If
If Hr = 2 Then
Frame_hr(10).DeleteVisual Mesh_hr(10, 2)
End If

Mesh_hr(10, 0).Empty
Mesh_hr(10, 1).Empty
Mesh_hr(10, 2).Empty

'Set Frame_hr(10) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
'Set Mesh_hr(10, 2) = RMCanvas1.D3DRM.CreateMeshBuilder()
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
SETmeshes

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'CameraPosAdX = 0
'CameraPosAdZ = 0



Mesh_hr(10, 0).LoadFromFile App.Path & "\programdata1\front_centered1.x", 0, 0, Nothing, Nothing
Mesh_hr(10, 1).LoadFromFile App.Path & "\programdata1\front_centered2.x", 0, 0, Nothing, Nothing
Mesh_hr(10, 2).LoadFromFile App.Path & "\programdata1\front_centered3.x", 0, 0, Nothing, Nothing



If Hr = 0 Then
Frame_hr(10).AddVisual Mesh_hr(10, 0)
End If
If Hr = 1 Then
Frame_hr(10).AddVisual Mesh_hr(10, 1)
End If
If Hr = 2 Then
Frame_hr(10).AddVisual Mesh_hr(10, 2)
End If



Mesh_frm.LoadFromFile App.Path & "\programdata1\frm.x", 0, 0, Nothing, Nothing
Mesh_frm.ScaleMesh 2.44, 0.1, 3.62 '2.44, 0.0001, 3.62
XAbsolute = -(500 - Xmap)
ZAbsolute = -(500 - Zmap)
'Frame_frm.AddLight light
Frame_frm.SetPosition Nothing, (XAbsolute + Xfrm) * 10, -1.7, (ZAbsolute + Zfrm) * 10





Frame_frm.AddVisual Mesh_frm
getFrameColor
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


For xi = 1 To 11
For zi = 1 To 11

Mesh_surface(XReplaceSrfc(xi), ZReplaceSrfc(zi)).LoadFromFile App.Path & "\programdata1\surface0.x", 0, 0, Nothing, Nothing
Mesh_surface(XReplaceSrfc(xi), ZReplaceSrfc(zi)).ScaleMesh 2.44, 0.0001, 3.62 '2.44, 0.0001, 3.62front_centered1.xsurface0.x

randommap = bigmap(actualX + Xmap + xi - 1, actualZ + Zmap + zi - 1)
randommnst = monsters(actualX + Xmap + xi - 1, actualZ + Zmap + zi - 1, 5)
If randommnst <> 0 Then 'raft
monsterShow randommnst, XReplaceSrfc(xi), ZReplaceSrfc(zi), ((actualX + xi - 1) * 10) + 0, ((actualZ + zi - 1) * 10) + 0
End If

If (randommap < 5 Or randommap > 17) And randommap <> 0 Then

If randommap = 1 Then
Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).LoadFromFile App.Path & "\programdata1\tree13ds2.x", 0, 0, Nothing, Nothing
Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).ScaleMesh 2, 2, 2
Frame_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetPosition Nothing, (actualX + xi - 1) * 10, 0.4, (actualZ + zi - 1) * 10
Frame_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).AddVisual Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi))
'Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).addshadow

'Mesh_Asset(xi, zi).SetColorRGB 0.1, 0.4, 0.1
'Frame_Asset(xi, zi).AddLight light
End If
If randommap = 2 Then
Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).LoadFromFile App.Path & "\programdata1\tr33ds.x", 0, 0, Nothing, Nothing
Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).ScaleMesh 2.5, 2.5, 2.5
Frame_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetPosition Nothing, (actualX + xi - 1) * 10, 1, (actualZ + zi - 1) * 10
Frame_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).AddVisual Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi))
End If
If randommap = 3 Then
Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).LoadFromFile App.Path & "\programdata1\mount3ds.x", 0, 0, Nothing, Nothing
Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).ScaleMesh 2, 3.5, 3
Frame_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetPosition Nothing, (actualX + xi - 1) * 10, 1, (actualZ + zi - 1) * 10
'''''Frame_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetOrientation Nothing, MNTvec(0).x, MNTvec(0).y, MNTvec(0).z, MNTvec(1).x, MNTvec(1).y, MNTvec(1).z
'RMCanvas1.CameraFrame.SetOrientation Nothing, Vectors(95).x, Vectors(95).y, Vectors(95).z, Vectors(94).x, Vectors(94).y, Vectors(94).z
Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetColorRGB 0.5, 0.5, 0.4
Frame_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).AddVisual Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi))

End If
If randommap = 4 Or randommap = 18 Then
Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).LoadFromFile App.Path & "\programdata1\swamp3.x", 0, 0, Nothing, Nothing
Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).ScaleMesh 1, 1, 1
Frame_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetPosition Nothing, (actualX + xi - 1) * 10, -1.5, (actualZ + zi - 1) * 10
Frame_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).AddVisual Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi))
End If
If randommap = 21 Or randommap = 211 Then
Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).LoadFromFile App.Path & "\programdata1\castle3ds.x", 0, 0, Nothing, Nothing
Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).ScaleMesh 3, 4, 4.5
Frame_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetPosition Nothing, (actualX + xi - 1) * 10, 1, (actualZ + zi - 1) * 10
Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetColorRGB 0.4, 0.4, 0.4 '
Frame_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).AddVisual Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi))
'Frame_Asset(xi, zi).AddLight light
Animatebrd (actualX + xi - 1) * 10, (actualZ + zi - 1) * 10, 2
End If
If randommap = 22 Or randommap = 221 Then

Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).LoadFromFile App.Path & "\programdata1\houses3ds.x", 0, 0, Nothing, Nothing
Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).ScaleMesh 1.5, 1.5, 1.5
Frame_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetPosition Nothing, (actualX + xi - 1) * 10, -0.5, (actualZ + zi - 1) * 10
Frame_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).AddVisual Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi))


End If

End If

If (randommap < 4 Or randommap > 18) And randommap <> 0 Then randommap = 0
'And randommap <> 3

'Set TEXTURE_surface(0, 0) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\" + Trim(Str(randommap)) + ".bmp")
Mesh_surface(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetTexture TEXTURE_surface(randommap)
'Mesh_surface(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetColorRGB 0.8, 0.8, 1 ' RGB'only when textured
Frame_surface(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetPosition Nothing, (actualX + xi - 1) * 10, -1.7, (actualZ + zi - 1) * 10
Frame_surface(XReplaceSrfc(xi), ZReplaceSrfc(zi)).AddVisual Mesh_surface(XReplaceSrfc(xi), ZReplaceSrfc(zi))






Next
Next


'LIGHT
'Set Frame_OBJecT(8) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
'Set light = RMCanvas1.D3DRM.CreateLightRGB(D3DRMLIGHT_POINT, 0, 0, 10)

'Frame_OBJecT(8).SetPosition Nothing, 15, 5, 42
'Frame_OBJecT(8).AddLight light
'lightON = True
'surface
'Set Frame_OBJecT(0) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
'Set Mesh_OBJecT(0) = RMCanvas1.D3DRM.CreateMeshBuilder()
'Mesh_OBJecT(0).LoadFromFile App.Path & "\programdata1\surface0.x", 0, 0, Nothing, Nothing
'Mesh_OBJecT(0).ScaleMesh 45, 0.1, 45
'Frame_OBJecT(0).SetPosition Nothing, 0, -1.7, 13
'Set TEXTURE_forObject(0) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\surface0.bmp")
'Mesh_OBJecT(0).SetTexture TEXTURE_forObject(0)
'Frame_OBJecT(0).AddVisual Mesh_OBJecT(0)
'Mesh_OBJecT(0).SetColorRGB 1, 1, 1 ' RGB'only when textured

'castle1
'Set Frame_OBJecT(2) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
'Set Mesh_OBJecT(2) = RMCanvas1.D3DRM.CreateMeshBuilder()
'Set TEXTURE_forObject = RMCanvas1.D3DRM.LoadTexture(App.Path & "\earthtexture.bmp")
'Mesh_OBJecT(2).LoadFromFile App.Path & "\programdata1\castle3ds.x", 0, 0, Nothing, Nothing
'Mesh_OBJecT(2).ScaleMesh 4.7, 4.7, 4.7
'Mesh_OBJecT.SetTexture TEXTURE_forObject
'Frame_OBJecT(2).SetPosition Nothing, 15, 1, 43
'Frame_OBJCT(0).SetOrientation Nothing, 0.5, -0.2, -0.2, 0.28, 0.4, -0.4
'Frame_OBJecT(2).SetRotation Nothing, 0, 1, 0, 0.003
'''''''''''''Mesh_OBJecT.SetColorRGB 0.5, 0.5, 0.5 ' RGB'only when textured
'Frame_OBJecT(2).AddVisual Mesh_OBJecT(2)

'mountain1
'Set Frame_OBJecT(3) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
'Set Mesh_OBJecT(3) = RMCanvas1.D3DRM.CreateMeshBuilder()
'Set TEXTURE_forObject = RMCanvas1.D3DRM.LoadTexture(App.Path & "\earthtexture.bmp")
'Mesh_OBJecT(3).LoadFromFile App.Path & "\programdata1\mount3ds.x", 0, 0, Nothing, Nothing
'Mesh_OBJecT(3).ScaleMesh 8.7, 8.7, 8.7
'Mesh_OBJecT.SetTexture TEXTURE_forObject
'Frame_OBJecT(3).SetPosition Nothing, 5, 2, 63
'Frame_OBJCT(0).SetOrientation Nothing, 0.5, -0.2, -0.2, 0.28, 0.4, -0.4
'Frame_OBJecT(2).SetRotation Nothing, 0, 1, 0, 0.003
'''''''''''''Mesh_OBJecT.SetColorRGB 0.5, 0.5, 0.5 ' RGB'only when textured
'Frame_OBJecT(3).AddVisual Mesh_OBJecT(3)

'houses1
'Set Frame_OBJecT(4) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
'Set Mesh_OBJecT(4) = RMCanvas1.D3DRM.CreateMeshBuilder()
'Set TEXTURE_forObject = RMCanvas1.D3DRM.LoadTexture(App.Path & "\earthtexture.bmp")
'Mesh_OBJecT(4).LoadFromFile App.Path & "\programdata1\houses3ds.x", 0, 0, Nothing, Nothing
'Mesh_OBJecT(4).ScaleMesh 4.7, 4.7, 4.7
'Mesh_OBJecT.SetTexture TEXTURE_forObject
'Frame_OBJecT(4).SetPosition Nothing, -10, 0.5, 33
'Frame_OBJCT(0).SetOrientation Nothing, 0.5, -0.2, -0.2, 0.28, 0.4, -0.4
'Frame_OBJecT(2).SetRotation Nothing, 0, 1, 0, 0.003
'''''''''''''Mesh_OBJecT.SetColorRGB 0.5, 0.5, 0.5 ' RGB'only when textured
'Frame_OBJecT(4).AddVisual Mesh_OBJecT(4)

'tree1
'Set Frame_OBJecT(5) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
'Set Mesh_OBJecT(5) = RMCanvas1.D3DRM.CreateMeshBuilder()
'Set TEXTURE_forObject = RMCanvas1.D3DRM.LoadTexture(App.Path & "\earthtexture.bmp")
'Mesh_OBJecT(5).LoadFromFile App.Path & "\programdata1\tree13ds.x", 0, 0, Nothing, Nothing
'Mesh_OBJecT(5).ScaleMesh 2.7, 2.7, 2.7
'Mesh_OBJecT.SetTexture TEXTURE_forObject
'Frame_OBJecT(5).SetPosition Nothing, 8, 0.5, 33
'Frame_OBJCT(0).SetOrientation Nothing, 0.5, -0.2, -0.2, 0.28, 0.4, -0.4
'Frame_OBJecT(2).SetRotation Nothing, 0, 1, 0, 0.003
'''''''''''''Mesh_OBJecT.SetColorRGB 0.5, 0.5, 0.5 ' RGB'only when textured
'Frame_OBJecT(5).AddVisual Mesh_OBJecT(5)

'tree3
'Set Frame_OBJecT(6) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
'Set Mesh_OBJecT(6) = RMCanvas1.D3DRM.CreateMeshBuilder()
'Set TEXTURE_forObject = RMCanvas1.D3DRM.LoadTexture(App.Path & "\earthtexture.bmp")
'Mesh_OBJecT(6).LoadFromFile App.Path & "\programdata1\tr33ds.x", 0, 0, Nothing, Nothing
'Mesh_OBJecT(6).ScaleMesh 2.7, 2.7, 2.7
'Mesh_OBJecT.SetTexture TEXTURE_forObject
'Frame_OBJecT(6).SetPosition Nothing, 13, 0.5, 23
'Frame_OBJCT(0).SetOrientation Nothing, 0.5, -0.2, -0.2, 0.28, 0.4, -0.4
'Frame_OBJecT(2).SetRotation Nothing, 0, 1, 0, 0.003
'''''''''''''Mesh_OBJecT.SetColorRGB 0.5, 0.5, 0.5 ' RGB'only when textured
'Frame_OBJecT(6).AddVisual Mesh_OBJecT(6)


'swamp
'Set Frame_OBJecT(7) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
'Set Mesh_OBJecT(7) = RMCanvas1.D3DRM.CreateMeshBuilder()
'Set TEXTURE_forObject = RMCanvas1.D3DRM.LoadTexture(App.Path & "\earthtexture.bmp")
'Mesh_OBJecT(7).LoadFromFile App.Path & "\programdata1\swamp3.x", 0, 0, Nothing, Nothing
'Mesh_OBJecT(7).ScaleMesh 1.2, 1.2, 1.2
'Mesh_OBJecT.SetTexture TEXTURE_forObject
'Frame_OBJecT(7).SetPosition Nothing, -5, -0.55, 13
'Frame_OBJCT(0).SetOrientation Nothing, 0.5, -0.2, -0.2, 0.28, 0.4, -0.4
'Frame_OBJecT(2).SetRotation Nothing, 0, 1, 0, 0.003
'''''''''''''Mesh_OBJecT.SetColorRGB 0.5, 0.5, 0.5 ' RGB'only when textured
'Frame_OBJecT(7).AddVisual Mesh_OBJecT(7)


'hero1
'Set Frame_OBJecT(1) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
'Set Mesh_OBJecT(1) = RMCanvas1.D3DRM.CreateMeshBuilder()
'Set TEXTURE_forObject = RMCanvas1.D3DRM.LoadTexture(App.Path & "\earthtexture.bmp")
'Mesh_OBJecT(1).LoadFromFile App.Path & "\programdata1\hero2a3ds.x", 0, 0, Nothing, Nothing
'Mesh_OBJecT(1).ScaleMesh 1.7, 1.7, 1.7
'Mesh_OBJecT.SetTexture TEXTURE_forObject
'Frame_OBJecT(1).SetPosition Nothing, 0, -0.4, 13
'Frame_OBJCT(0).SetOrientation Nothing, 0.5, -0.2, -0.2, 0.28, 0.4, -0.4
'Frame_OBJecT(1).SetRotation Nothing, 0, 1, 0, 0.003
'''''''''''''Mesh_OBJecT.SetColorRGB 0.5, 0.5, 0.5 ' RGB'only when textured
'Frame_OBJecT(1).AddVisual Mesh_OBJecT(1)


End Sub



Private Sub Command1_Click()
retVal = mciSendString("close songNumber2", vbNullString, 0, 0)
retVal = mciSendString("close songNumber3", vbNullString, 0, 0)
retVal = mciSendString("close songNumber4", vbNullString, 0, 0)
retVal = mciSendString("close songNumber5", vbNullString, 0, 0)
retVal = mciSendString("close songNumber6", vbNullString, 0, 0)
End
End Sub

Private Sub Command2_Click()
On Error Resume Next
play2
play3
play4
play5
play6
Picture3.Visible = False
MOUSElock = False
Timer1.Enabled = True
'Frame_brd.SetPosition Nothing, Vectors(80).x, Vectors(80).y, Vectors(80).z
Frame_brd.SetOrientation Nothing, Vectors(89).x, Vectors(89).y, Vectors(89).z, Vectors(87).x, Vectors(87).y, Vectors(87).z
End Sub

Private Sub Command3_Click()
On Error Resume Next
play2
play3
play4
play5
play6
Picture3.Visible = False
MOUSElock = False
Timer1.Enabled = True

'Frame_brd.SetPosition Nothing, Vectors(80).x, Vectors(80).y, Vectors(80).z
Frame_brd.SetOrientation Nothing, Vectors(89).x, Vectors(89).y, Vectors(89).z, Vectors(87).x, Vectors(87).y, Vectors(87).z

End Sub

Private Sub FrameLeft()
If Xfrm < 500 Then
BlockedNewLine = True
Xfrm = Xfrm + 1
'Frame_frm.SetPosition Nothing, (XAbsolute + Xfrm) * 10, -1.7, (ZAbsolute + Zfrm) * 10
'Get freFile, 58, Vectors(90).x 'camera abolute right-top
'Get freFile, 59, Vectors(90).z 'camera abolute right-top
'CameraPosAdX = (500 - Xfrm) * 10
'CameraPosAdZ = (500 - Zfrm) * 10
'RMCanvas1.CameraFrame.SetPosition Nothing, Vectors(90).x - CameraPosAdX, Vectors(96).y, Vectors(90).z - CameraPosAdZ
RebuildMap
End If
End Sub
Private Sub FrameRight()
If Xfrm > 0 Then
BlockedNewLine = True
Xfrm = Xfrm - 1
'Frame_frm.SetPosition Nothing, (XAbsolute + Xfrm) * 10, -1.7, (ZAbsolute + Zfrm) * 10
'Get freFile, 58, Vectors(90).x 'camera abolute right-top
'Get freFile, 59, Vectors(90).z 'camera abolute right-top
'CameraPosAdX = (500 - Xfrm) * 10
'CameraPosAdZ = (500 - Zfrm) * 10
'RMCanvas1.CameraFrame.SetPosition Nothing, Vectors(90).x - CameraPosAdX, Vectors(96).y, Vectors(90).z - CameraPosAdZ
RebuildMap
End If

End Sub
Private Sub FrameUp()
If Zfrm < 500 Then
BlockedNewLine = True
Zfrm = Zfrm + 1
'Frame_frm.SetPosition Nothing, (XAbsolute + Xfrm) * 10, -1.7, (ZAbsolute + Zfrm) * 10
'Get freFile, 58, Vectors(90).x 'camera abolute right-top
'Get freFile, 59, Vectors(90).z 'camera abolute right-top
'CameraPosAdX = (500 - Xfrm) * 10
'C'ameraPosAdZ = (500 - Zfrm) * 10
'RMCanvas1.CameraFrame.SetPosition Nothing, Vectors(90).x - CameraPosAdX, Vectors(96).y, Vectors(90).z - CameraPosAdZ
RebuildMap
End If
End Sub
Private Sub FrameDown()
If Zfrm > 0 Then
BlockedNewLine = True
Zfrm = Zfrm - 1
'Frame_frm.SetPosition Nothing, (XAbsolute + Xfrm) * 10, -1.7, (ZAbsolute + Zfrm) * 10
'Get freFile, 58, Vectors(90).x 'camera abolute right-top
'Get freFile, 59, Vectors(90).z 'camera abolute right-top
'CameraPosAdX = (500 - Xfrm) * 10
'CameraPosAdZ = (500 - Zfrm) * 10
'RMCanvas1.CameraFrame.SetPosition Nothing, Vectors(90).x - CameraPosAdX, Vectors(96).y, Vectors(90).z - CameraPosAdZ
RebuildMap
End If
End Sub
Private Sub Form_Load()
SetTopmost Me, True
If App.PrevInstance = True Then
    'MsgBox "Already running...."
    Unload Me
    Exit Sub
End If
'FormP.Show
JI.dwSize = Len(JI)
JI.dwFlags = JOY_RETURNALL
End Sub

Private Sub initRM()
With RMCanvas1
    .StartWindowed
    .SceneFrame.SetSceneBackgroundRGB 0, 0, 0.5   'background color rgb seen when no backgroud texture
    .Viewport.SetBack 5000                        'view depth
    '.CameraFrame.SetPosition Nothing, 0, 0, 0     'camera position xyz
End With

'RMCanvas1.CameraFrame.SetOrientation Nothing, Vectors(0).X, Vectors(0).Y, Vectors(0).z, Vectors(1).X, Vectors(1).Y, Vectors(1).z
Set TEXTURE_forBackground(0) = RMCanvas1.D3DRM.LoadTexture(App.Path + "\programdata2\background.bmp")
RMCanvas1.SceneFrame.SetSceneBackgroundImage TEXTURE_forBackground(0)
SetFog
RMCanvas1.Device.SetTextureQuality D3DRMTEXTURE_LINEAR

SunVector.x = 0:
SunVector.y = 20:
SunVector.z = -15
    
DayTimeLight

    




End Sub

Private Sub Form_Resize()
On Error Resume Next
Command3.Left = Me.Width \ 2 - Command3.Width \ 2
Command2.Left = Me.Width \ 2 - Command2.Width \ 2
Command1.Left = Me.Width \ 2 - Command1.Width \ 2


''''''''''''''''''''''''''''''''''''''
Picture3.Left = 0: Picture3.Top = 0: Picture3.Width = Form1.ScaleWidth: Picture3.Height = Form1.ScaleHeight
Picture1.Left = 0: Picture1.Top = 0: Picture1.Width = Form1.ScaleWidth: Picture1.Height = Form1.ScaleHeight
RMCanvas1.Left = 0: RMCanvas1.Top = 0: RMCanvas1.Width = Form1.ScaleWidth: RMCanvas1.Height = Form1.ScaleHeight

Picture3.Picture = LoadPicture((App.Path & "\PROGRAMDATA2\PICTURE.bmp"))

capsON = False
Form1.MousePointer = 0
Zdist = 0
Xdist = 50
Xmap = 250
Zmap = 250
Xfrm = 500
Zfrm = 500
For xi = 1 To 11
XReplaceSrfc(xi) = xi - 1
ZReplaceSrfc(xi) = xi - 1
Next
''''''''''''''''''''''''''''''''''''''
PrevNoChange = False
' initialise rmcontrol
initRM
' initialise objects
SETmeshes
''''''''''''''''''''''''''''''''''''''
LoadTextures
loadCAM
loadMAP
monster_load
'''''''''''''''''''''/////////////////

initObjects

'start camera pos
'RMCanvas1.CameraFrame.GetPosition Nothing, Vectors(99)
'RMCanvas1.CameraFrame.GetOrientation Nothing, Vectors(98), Vectors(97)
End Sub

Private Sub HandleKeys2()

If keypressed(40) Then
keypressed(40) = False
''FrameDown
End If
If keypressed(38) Then
keypressed(38) = False
''FrameUp
End If
If keypressed(37) Then
keypressed(37) = False
''FrameRight
End If
If keypressed(39) Then
keypressed(39) = False
''FrameLeft
End If


End Sub




Private Sub HandleKeys()




If keypressed(17) Then 'ctrl
'prevXfrm = Xfrm
'prevZfrm = Zfrm
Xfrm = Int(Rnd * 501)
Zfrm = Int(Rnd * 501)
'CameraPosAdX = (500 - Xfrm) * 10
'CameraPosAdZ = (500 - Zfrm) * 10

'Frame_frm.SetPosition Nothing, (XAbsolute + Xfrm) * 10, -1.7, (ZAbsolute + Zfrm) * 10
'RMCanvas1.CameraFrame.SetPosition Nothing, Vectors(90).x - CameraPosAdX, Vectors(96).y, Vectors(90).z - CameraPosAdZ

BlockedNewLine = True
RebuildMap

keypressed(17) = False
End If



If keypressed(16) Then 'shift

'Frame_brd.DeleteVisual Mesh_brd(0)
'Frame_brd.DeleteVisual Mesh_brd(1)
handlemice(3) = 1
loadCAM
initObjects
Else
handlemice(3) = 0
'start camera pos
'Zdist = 0
'Xdist = 50
'Xmap = 250
'Zmap = 250
'actualX = 0: actualZ = 0
'RMCanvas1.CameraFrame.SetPosition Nothing, Vectors(99).X, Vectors(99).Y, Vectors(99).z
'RMCanvas1.CameraFrame.SetOrientation Nothing, Vectors(98).X, Vectors(98).Y, Vectors(98).z, Vectors(97).X, Vectors(97).Y, Vectors(97).z
'keypressed(16) = False
End If






If keypressed(82) Then
RMCanvas1.CameraFrame.SetPosition RMCanvas1.CameraFrame, 0, 0.2, 0
End If
If keypressed(70) Then
RMCanvas1.CameraFrame.SetPosition RMCanvas1.CameraFrame, 0, -0.2, 0
End If
If keypressed(68) Then
RMCanvas1.CameraFrame.SetPosition RMCanvas1.CameraFrame, 0.2, 0, 0
End If
If keypressed(65) Then
RMCanvas1.CameraFrame.SetPosition RMCanvas1.CameraFrame, -0.2, 0, 0
End If
If keypressed(87) Then
RMCanvas1.CameraFrame.SetPosition RMCanvas1.CameraFrame, 0, 0, 0.2
End If
If keypressed(83) Then
RMCanvas1.CameraFrame.SetPosition RMCanvas1.CameraFrame, 0, 0, -0.2
End If
If keypressed(192) Then
If Picture2.Visible = False Then
Picture2.Visible = True
Else
Picture2.Visible = False
End If
keypressed(192) = False
End If
If keypressed(69) Then 'Or (handlemice(1) > Form1.Width \ 2 And capsON)
RMCanvas1.CameraFrame.SetRotation RMCanvas1.CameraFrame, 0, 1, 0, 0.07
Else
If keypressed(81) Then 'Or (handlemice(1) < Form1.Width \ 2 And capsON)
RMCanvas1.CameraFrame.SetRotation RMCanvas1.CameraFrame, 0, 1, 0, -0.07
Else
If keypressed(84) Then
RMCanvas1.CameraFrame.SetRotation RMCanvas1.CameraFrame, 1, 0, 0, 0.03
Else
If keypressed(71) Then
RMCanvas1.CameraFrame.SetRotation RMCanvas1.CameraFrame, 1, 0, 0, -0.03
Else
RMCanvas1.CameraFrame.SetRotation RMCanvas1.CameraFrame, 1, 1, 0, 0
End If
End If
End If
End If

End Sub
Private Sub HandleMouse()
'If keypressed(20) Then
'If capsON = False Then
'capsON = True
'Form1.MousePointer = 99
'Else
'capsON = False
'Form1.MousePointer = 0
'End If
'keypressed(20) = False
'End If

If capsON = True Then
Set di = dx7.DirectInputCreate()
Set mdevice = di.CreateDevice("GUID_SysMouse")
mdevice.SetCommonDataFormat DIFORMAT_MOUSE
mdevice.SetCooperativeLevel Form1.hWnd, DISCL_EXCLUSIVE Or DISCL_FOREGROUND
mdevice.Acquire
mdevice.GetDeviceStateMouse mstate
mdevice.Unacquire

HandleMice5

Else
RMCanvas1.CameraFrame.GetPosition Nothing, Vectors(100)
If handlemice(1) < 10 And Vectors(100).x > -2495 Then
Vectors(100).x = Vectors(100).x - 0.9
RMCanvas1.CameraFrame.SetPosition Nothing, Vectors(100).x, Vectors(100).y, Vectors(100).z
End If
If handlemice(1) > Me.ScaleWidth - 10 And Vectors(100).x < 2495 Then
Vectors(100).x = Vectors(100).x + 0.9
RMCanvas1.CameraFrame.SetPosition Nothing, Vectors(100).x, Vectors(100).y, Vectors(100).z
End If
If handlemice(2) < 10 And Vectors(100).z < 2455 Then
Vectors(100).z = Vectors(100).z + 0.9
RMCanvas1.CameraFrame.SetPosition Nothing, Vectors(100).x, Vectors(100).y, Vectors(100).z
End If
If handlemice(2) > Me.ScaleHeight - 10 And Vectors(100).z > -2521.42 Then
Vectors(100).z = Vectors(100).z - 0.9
RMCanvas1.CameraFrame.SetPosition Nothing, Vectors(100).x, Vectors(100).y, Vectors(100).z
End If
End If

Label2.Caption = "Mouseview(CapsLock)=" + Str(capsON) + "\" + Str(handlemice(1)) + "\" + Str(handlemice(2)) + "\" + Str(handlemice(0)) + "\" + Str(handlemice(3))
'If handlemice(0) = 1 Then HandleMice5
'x > -2495 x < 2495 z < 2455 z > -2521.42

End Sub




Private Sub Picture1_KeyDown(KeyCode As Integer, Shift As Integer)
keypressed(KeyCode) = True
''''''''''''''''''''''''''''''~BLOCK CAMERA!
If keypressed(192) Then
If Picture2.Visible = False Then
Picture2.Visible = True
Else
Picture2.Visible = False
End If
keypressed(192) = False
End If
''''''''''''''''''''''''''''~~BLOCK CAMERA
End Sub

Private Sub Picture1_KeyPress(KeyAscii As Integer)
If KeyAscii = 27 Then
MOUSElock = True


Timer1.Enabled = False
'
brdWNG5 = 0
Frame_brd.SetRotation Nothing, 1, 1, 1, 0
Frame_brd.GetOrientation Nothing, Vectors(89), Vectors(87) 'Vectors(80)
Frame_brd.GetPosition Nothing, Vectors(80)
brdWNG4 = False
'
GetScreenshot
Picture3.Visible = True


retVal = mciSendString("PAUSE songNumber2", vbNullString, 0, 0)
retVal = mciSendString("PAUSE songNumber3", vbNullString, 0, 0)
retVal = mciSendString("PAUSE songNumber4", vbNullString, 0, 0)
retVal = mciSendString("PAUSE songNumber5", vbNullString, 0, 0)
retVal = mciSendString("PAUSE songNumber6", vbNullString, 0, 0)
'CLOSE
bwx Picture3
SavePicture Picture3.Image, (App.Path & "\PROGRAMDATA2\PICTURE.bmp")

If Picture2.Visible = False Then saveCAM
'End If
'End



End If
End Sub

Private Sub Picture1_KeyUp(KeyCode As Integer, Shift As Integer)
keypressed(KeyCode) = False
End Sub

Private Sub Picture1_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

























'If handlemice(0) = 1 Then HandleMice5
End Sub

Private Sub Picture1_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
'handlemice(0) = Button
handlemice(1) = x
handlemice(2) = y
'handlemice(3) = Shift
End Sub

Private Sub RMCanvas1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
 handlemice(0) = Button
handlemice(1) = x
handlemice(2) = y
handlemice(3) = Shift
 Dim mb As Direct3DRMFrame3
 Dim strName As String
 Dim strName2 As String
 
    Set mb = RMCanvas1.PickTopFrame(CLng(x), CLng(y))
    If mb Is Nothing Then
    strName = "nic"
    Else
    
    strName2 = mb.GetName
    mb.GetPosition Nothing, Vectors(70)
    strName = Str(Vectors(70).x) + " \ " + Str(Vectors(70).z) ' mb.GetName()
    
    
    If strName2 <> "Birdy1" Then
play1
Frame_frm.SetPosition Nothing, Vectors(70).x, -1.7, Vectors(70).z
Xfrm = Int((Vectors(70).x \ 10) + (500 - Xmap))
Zfrm = Int((Vectors(70).z \ 10) + (500 - Zmap))

getFrameColor


    End If
    
    End If
 'If there is nothing exit
   ' If strName = "" Then
   '     Text1.Visible = False
   '     Exit Sub
   ' End If
'Confirm whether it is a point name
   
   ' If InStr(strName, "points") <> 0 Then Exit Sub
'Acquire the 7th character

    'p = Val(Mid$(strName, 7))
   ' Text1.Visible = True
'Get data from the array and use

  ' With m_Points(p)
        Label6.Caption = strName + "\" + strName2
End Sub

Private Sub RMCanvas1_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
 handlemice(0) = Button
handlemice(1) = x
handlemice(2) = y
handlemice(3) = Shift
 Dim mb As Direct3DRMFrame3
 Dim strName As String
 Dim strName2 As String
 
    Set mb = RMCanvas1.PickTopFrame(CLng(x), CLng(y))
    If mb Is Nothing Then
    strName = "nic"
    Else
    
    strName2 = mb.GetName
    mb.GetPosition Nothing, Vectors(70)
    strName = Str(Vectors(70).x) + " \ " + Str(Vectors(70).z) ' mb.GetName()
    End If
 'If there is nothing exit
   ' If strName = "" Then
   '     Text1.Visible = False
   '     Exit Sub
   ' End If
'Confirm whether it is a point name
   
   ' If InStr(strName, "points") <> 0 Then Exit Sub
'Acquire the 7th character

    'p = Val(Mid$(strName, 7))
   ' Text1.Visible = True
'Get data from the array and use

  ' With m_Points(p)
        Label6.Caption = strName + "\" + strName2
End Sub

Private Sub Timer1_Timer()
pad
If Picture3.Visible = False Then Picture1.SetFocus

If MOUSElock = False And Picture3.Visible = False And brdWNG4 = False And brdWNG5 < 2 Then
brdWNG5 = brdWNG5 + 1
If brdWNG5 = 2 Then
Frame_brd.SetRotation Nothing, 0, 1, 0, 0.01
'Frame_brd.GetRotation
brdWNG4 = True
brdWNG5 = 0
End If
End If

Dim loopkey
Label1.Caption = "keys pressed (form focus):"
For loopkey = 0 To 255
If keypressed(loopkey) = True Then
Label1.Caption = Label1.Caption + Str(loopkey)
End If
Next loopkey
RMCanvas1.CameraFrame.GetPosition Nothing, Vectors(93)








Label4.Caption = "CAM|  " + Str(Vectors(93).x) + "  |  " + Str(Vectors(93).z) + "  |"
Label4.Caption = Label4.Caption + vbCrLf + Str(Xdist) + Str(Zdist)
Label3.Caption = "ACTUALX " + Str(actualX) + "  ACTUALZ " + Str(actualZ) + "  XFRM " + Str(Xfrm) + "  ZFRM " + Str(Zfrm)
If BlockedNewLine = False Then SetNewLine

If MOUSElock = False And Picture3.Visible = False Then AnimateRaftsWaves

'AnimateSwamp
If Picture2.Visible = True Then HandleKeys
If Picture3.Visible = False And MOUSElock <> True Then
HandleKeys2
HandleMouse
DayTimeLight

End If
RMCanvas1.Update
environmentsound




'hold cursor in form
Dim client As RECT2
Dim upperleft As POINT
GetClientRect Form1.hWnd, client
upperleft.x = client.Left
upperleft.y = client.Top
ClientToScreen Form1.hWnd, upperleft
OffsetRect client, upperleft.x, upperleft.y
ClipCursor client
'hold cursor in form


If Picture3.Visible = True Then
If PrevNoChange = True And MapLoaded And MonstersLoaded Then
Command3.Enabled = True
Command2.Enabled = True
End If
End If
End Sub
Private Sub Form_Unload(Cancel As Integer)
'Releases the cursor limits
ClipCursor ByVal 0&











End Sub
Private Sub saveCAM()

RMCanvas1.CameraFrame.GetPosition Nothing, Vectors(96)
RMCanvas1.CameraFrame.GetOrientation Nothing, Vectors(95), Vectors(94)

freFile = FreeFile
Open App.Path + "\camera.DAT" For Random As freFile
'camera start pos
'Put freFile, 1, Vectors(99).x
'Put freFile, 2, Vectors(99).y
'Put freFile, 3, Vectors(99).z
'Put freFile, 4, Vectors(98).x
'Put freFile, 5, Vectors(98).y
'Put freFile, 6, Vectors(98).z
'Put freFile, 7, Vectors(97).x
'Put freFile, 8, Vectors(97).y
'Put freFile, 9, Vectors(97).z
'camera end pos
Put freFile, 10, Vectors(96).x
Put freFile, 11, Vectors(96).y
Put freFile, 12, Vectors(96).z
Put freFile, 13, Vectors(95).x
Put freFile, 14, Vectors(95).y
Put freFile, 15, Vectors(95).z
Put freFile, 16, Vectors(94).x
Put freFile, 17, Vectors(94).y
Put freFile, 18, Vectors(94).z
'
Put freFile, 25, actualX
Put freFile, 26, actualZ
Put freFile, 27, Xdist
Put freFile, 28, Zdist
Dim I
For I = 0 To 12
Put freFile, 29 + I, XReplaceSrfc(I)
Next
For I = 0 To 12
Put freFile, 29 + 14 + I, ZReplaceSrfc(I)
Next
Put freFile, 29 + 14 + 13, Xmap
Put freFile, 29 + 14 + 14, Zmap
'''
'''Put freFile, 58, Vectors(96).x
'''Put freFile, 59, Vectors(96).z
Put freFile, 60, Xfrm
Put freFile, 61, Zfrm
'Vectors(89), Vectors(87)'Vectors(80)
Put freFile, 62, Vectors(89).x
Put freFile, 63, Vectors(89).y
Put freFile, 64, Vectors(89).z
Put freFile, 65, Vectors(87).x
Put freFile, 66, Vectors(87).y
Put freFile, 67, Vectors(87).z
Put freFile, 68, Vectors(80).x
Put freFile, 69, Vectors(80).y
Put freFile, 70, Vectors(80).z
Put freFile, 71, brdWNG


Close freFile


End Sub
Private Sub loadCAM()


On Error Resume Next
freFile = FreeFile
Open App.Path + "\camera.DAT" For Random As freFile
'camera start pos
Get freFile, 1, Vectors(99).x
Get freFile, 2, Vectors(99).y
Get freFile, 3, Vectors(99).z
Get freFile, 4, Vectors(98).x
Get freFile, 5, Vectors(98).y
Get freFile, 6, Vectors(98).z
Get freFile, 7, Vectors(97).x
Get freFile, 8, Vectors(97).y
Get freFile, 9, Vectors(97).z
'camera end pos
Get freFile, 10, Vectors(96).x
Get freFile, 11, Vectors(96).y
Get freFile, 12, Vectors(96).z
Get freFile, 13, Vectors(95).x
Get freFile, 14, Vectors(95).y
Get freFile, 15, Vectors(95).z
Get freFile, 16, Vectors(94).x
Get freFile, 17, Vectors(94).y
Get freFile, 18, Vectors(94).z
'
Get freFile, 19, MNTvec(0).x
Get freFile, 20, MNTvec(0).y
Get freFile, 21, MNTvec(0).z
Get freFile, 22, MNTvec(1).x
Get freFile, 23, MNTvec(1).y
Get freFile, 24, MNTvec(1).z
'
Get freFile, 25, actualX
Get freFile, 26, actualZ
Get freFile, 27, Xdist
Get freFile, 28, Zdist
Dim I
For I = 0 To 12
Get freFile, 29 + I, XReplaceSrfc(I)
Next
For I = 0 To 12
Get freFile, 29 + 14 + I, ZReplaceSrfc(I)
Next
Get freFile, 29 + 14 + 13, Xmap
Get freFile, 29 + 14 + 14, Zmap
'''
Get freFile, 58, Vectors(90).x 'camera abolute right-top
Get freFile, 59, Vectors(90).z 'camera abolute right-top
'''
Get freFile, 60, Xfrm
Get freFile, 61, Zfrm
''
Get freFile, 62, Vectors(89).x
Get freFile, 63, Vectors(89).y
Get freFile, 64, Vectors(89).z
Get freFile, 65, Vectors(87).x
Get freFile, 66, Vectors(87).y
Get freFile, 67, Vectors(87).z
Get freFile, 68, Vectors(80).x
Get freFile, 69, Vectors(80).y
Get freFile, 70, Vectors(80).z
Get freFile, 71, brdWNG
Close freFile



RMCanvas1.CameraFrame.SetPosition Nothing, Vectors(96).x, Vectors(96).y, Vectors(96).z
RMCanvas1.CameraFrame.SetOrientation Nothing, Vectors(95).x, Vectors(95).y, Vectors(95).z, Vectors(94).x, Vectors(94).y, Vectors(94).z







End Sub
Private Sub SetNewLine()
If PrevNoChange = False And Picture3.Visible = True Then
PrevX = actualX
PrevZ = actualZ
End If




'Dim randommap As Integer
'''''''''''''''''''''''''''''''''''''MAP-UP''''''''''''''''''
If Vectors(93).z > Zdist + 5 And Zmap + actualZ < 490 Then
actualZ = actualZ + 1
zi = ZReplaceSrfc(1)



For xi = 1 To 11
On Error Resume Next
Frame_surface(XReplaceSrfc(xi), zi).DeleteVisual Mesh_surface(XReplaceSrfc(xi), zi)
Frame_Asset(XReplaceSrfc(xi), zi).DeleteVisual Mesh_Asset(XReplaceSrfc(xi), zi)
Frame_Mnst(XReplaceSrfc(xi), zi, 1).DeleteVisual Mesh_Mnst(XReplaceSrfc(xi), zi, 1)
Mesh_Asset(XReplaceSrfc(xi), zi).Empty
Mesh_Mnst(XReplaceSrfc(xi), zi, 0).Empty
Mesh_Mnst(XReplaceSrfc(xi), zi, 1).Empty


randommap = bigmap(Xmap + xi - 1 + actualX, Zmap + actualZ + 10)
NewAsset randommap, XReplaceSrfc(xi), zi, (actualX + xi - 1) * 10, (actualZ + 10) * 10
randommnst = monsters(actualX + Xmap + xi - 1, actualZ + Zmap + 10, 5)
If randommnst <> 0 Then 'raft
monsterShow randommnst, XReplaceSrfc(xi), (zi), ((actualX + xi - 1) * 10) + 0, ((actualZ + 10) * 10) + 0
End If


If (randommap < 4 Or randommap > 18) And randommap <> 0 Then randommap = 0
'.Set TEXTURE_surface(0, 0) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\" + Trim(Str(randommap)) + ".bmp")
Mesh_surface(XReplaceSrfc(xi), zi).SetTexture TEXTURE_surface(randommap)
Frame_surface(XReplaceSrfc(xi), zi).SetPosition Nothing, (actualX + xi - 1) * 10, -1.7, (actualZ + 10) * 10
Frame_surface(XReplaceSrfc(xi), zi).AddVisual Mesh_surface(XReplaceSrfc(xi), zi)
'''''''''''''''
Next
Zdist = Zdist + 10
For zi = 0 To 10
ZReplaceSrfc(zi) = ZReplaceSrfc(zi + 1)
Next
ZReplaceSrfc(11) = ZReplaceSrfc(0)
End If
'''''''''''''''''''''''''''''''''''''MAP-UP''''''''''''''''''
'''''''''''''''''''''''''''''''''''''MAP-DOWN''''''''''''''''
If Vectors(93).z < Zdist - 5 And Zmap + actualZ > 0 Then
actualZ = actualZ - 1
zi = ZReplaceSrfc(11)


For xi = 1 To 11
On Error Resume Next
Frame_surface(XReplaceSrfc(xi), zi).DeleteVisual Mesh_surface(XReplaceSrfc(xi), zi)
Frame_Asset(XReplaceSrfc(xi), zi).DeleteVisual Mesh_Asset(XReplaceSrfc(xi), zi)
Frame_Mnst(XReplaceSrfc(xi), zi, 1).DeleteVisual Mesh_Mnst(XReplaceSrfc(xi), zi, 1)
Mesh_Asset(XReplaceSrfc(xi), zi).Empty
Mesh_Mnst(XReplaceSrfc(xi), zi, 0).Empty
Mesh_Mnst(XReplaceSrfc(xi), zi, 1).Empty



randommap = bigmap(Xmap + xi - 1 + actualX, Zmap + actualZ)
NewAsset randommap, XReplaceSrfc(xi), zi, (actualX + xi - 1) * 10, (actualZ) * 10
randommnst = monsters(actualX + Xmap + xi - 1, actualZ + Zmap, 5)
If randommnst <> 0 Then 'raft
monsterShow randommnst, XReplaceSrfc(xi), (zi), ((actualX + xi - 1) * 10) + 0, ((actualZ) * 10) + 0
End If




If (randommap < 4 Or randommap > 18) And randommap <> 0 Then randommap = 0
'Set TEXTURE_surface(0, 0) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\" + Trim(Str(randommap)) + ".bmp")
Mesh_surface(XReplaceSrfc(xi), zi).SetTexture TEXTURE_surface(randommap)
Frame_surface(XReplaceSrfc(xi), zi).SetPosition Nothing, (actualX + xi - 1) * 10, -1.7, (actualZ) * 10
Frame_surface(XReplaceSrfc(xi), zi).AddVisual Mesh_surface(XReplaceSrfc(xi), zi)
'''''''''''''''
Next
Zdist = Zdist - 10
For zi = 12 To 1 Step -1
ZReplaceSrfc(zi) = ZReplaceSrfc(zi - 1)
Next
ZReplaceSrfc(1) = ZReplaceSrfc(12)
End If
'''''''''''''''''''''''''''''''''''''MAP-DOWN''''''''''''''''
'''''''''''''''''''''''''''''''''''''MAP-LEFT''''''''''''''''
If Vectors(93).x < Xdist - 5 And Xmap + actualX > 0 Then
actualX = actualX - 1
xi = XReplaceSrfc(11)



For zi = 1 To 11
On Error Resume Next
Frame_surface((xi), ZReplaceSrfc(zi)).DeleteVisual Mesh_surface((xi), ZReplaceSrfc(zi))
Frame_Asset((xi), ZReplaceSrfc(zi)).DeleteVisual Mesh_Asset((xi), ZReplaceSrfc(zi))
Frame_Mnst((xi), ZReplaceSrfc(zi), 1).DeleteVisual Mesh_Mnst((xi), ZReplaceSrfc(zi), 1)
Mesh_Asset((xi), ZReplaceSrfc(zi)).Empty
Mesh_Mnst((xi), ZReplaceSrfc(zi), 0).Empty
Mesh_Mnst((xi), ZReplaceSrfc(zi), 1).Empty
'''''''''''''''





randommap = bigmap(Xmap + actualX, Zmap + zi - 1 + actualZ)
NewAsset randommap, (xi), ZReplaceSrfc(zi), (actualX) * 10, (actualZ + zi - 1) * 10
randommnst = monsters(actualX + Xmap, actualZ + Zmap + zi - 1, 5)
If randommnst <> 0 Then 'raft
monsterShow randommnst, (xi), ZReplaceSrfc(zi), ((actualX) * 10) + 0, ((actualZ + zi - 1) * 10) + 0
End If



If (randommap < 4 Or randommap > 18) And randommap <> 0 Then randommap = 0
'Set TEXTURE_surface(0, 0) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\" + Trim(Str(randommap)) + ".bmp")
Mesh_surface((xi), ZReplaceSrfc(zi)).SetTexture TEXTURE_surface(randommap)
Frame_surface((xi), ZReplaceSrfc(zi)).SetPosition Nothing, (actualX) * 10, -1.7, (actualZ + zi - 1) * 10
Frame_surface((xi), ZReplaceSrfc(zi)).AddVisual Mesh_surface((xi), ZReplaceSrfc(zi))
'''''''''''''''
Next
Xdist = Xdist - 10
For xi = 12 To 1 Step -1
XReplaceSrfc(xi) = XReplaceSrfc(xi - 1)
Next
XReplaceSrfc(1) = XReplaceSrfc(12)
End If
'''''''''''''''''''''''''''''''''''''MAP-LEFT''''''''''''''''
'''''''''''''''''''''''''''''''''''''MAP-right''''''''''''''''
If Vectors(93).x > Xdist + 5 And Xmap + actualX < 490 Then
actualX = actualX + 1
xi = XReplaceSrfc(1)

For zi = 1 To 11
On Error Resume Next
Frame_surface((xi), ZReplaceSrfc(zi)).DeleteVisual Mesh_surface((xi), ZReplaceSrfc(zi))
Frame_Asset((xi), ZReplaceSrfc(zi)).DeleteVisual Mesh_Asset((xi), ZReplaceSrfc(zi))
Frame_Mnst((xi), ZReplaceSrfc(zi), 1).DeleteVisual Mesh_Mnst((xi), ZReplaceSrfc(zi), 1)
Mesh_Asset((xi), ZReplaceSrfc(zi)).Empty
Mesh_Mnst((xi), ZReplaceSrfc(zi), 0).Empty
Mesh_Mnst((xi), ZReplaceSrfc(zi), 1).Empty
'''''''''''''''




randommap = bigmap(Xmap + actualX + 10, Zmap + zi - 1 + actualZ)
NewAsset randommap, (xi), ZReplaceSrfc(zi), (actualX + 10) * 10, (actualZ + zi - 1) * 10
randommnst = monsters(actualX + Xmap + 10, actualZ + Zmap + zi - 1, 5)
If randommnst <> 0 Then 'raft
monsterShow randommnst, (xi), ZReplaceSrfc(zi), ((actualX + 10) * 10) + 0, ((actualZ + zi - 1) * 10) + 0
End If


If (randommap < 4 Or randommap > 18) And randommap <> 0 Then randommap = 0
'Set TEXTURE_surface(0, 0) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\" + Trim(Str(randommap)) + ".bmp")
Mesh_surface((xi), ZReplaceSrfc(zi)).SetTexture TEXTURE_surface(randommap)
Frame_surface((xi), ZReplaceSrfc(zi)).SetPosition Nothing, (actualX + 10) * 10, -1.7, (actualZ + zi - 1) * 10
Frame_surface((xi), ZReplaceSrfc(zi)).AddVisual Mesh_surface((xi), ZReplaceSrfc(zi))
'''''''''''''''
Next
Xdist = Xdist + 10
For xi = 0 To 10
XReplaceSrfc(xi) = XReplaceSrfc(xi + 1)
Next
XReplaceSrfc(11) = XReplaceSrfc(0)
End If
'''''''''''''''''''''''''''''''''''''MAP-right''''''''''''''''


If PrevNoChange = False And Picture3.Visible = True Then
If actualX = PrevX And PrevZ = actualZ Then PrevNoChange = True
End If


End Sub
Private Sub NewAsset(randommap, xi, zi, posx, posz)


If (randommap < 5 Or randommap > 17) And randommap <> 0 Then

If randommap = 1 Then
Mesh_Asset(xi, zi).LoadFromFile App.Path & "\programdata1\tree13ds2.x", 0, 0, Nothing, Nothing
Mesh_Asset(xi, zi).ScaleMesh 2, 2, 2
Frame_Asset(xi, zi).SetPosition Nothing, posx, 0.4, posz
Frame_Asset(xi, zi).AddVisual Mesh_Asset(xi, zi)
'Mesh_Asset(xi, zi).SetColorRGB 0.1, 0.4, 0.1
'Frame_Asset(xi, zi).AddLight light
End If
If randommap = 2 Then
Mesh_Asset(xi, zi).LoadFromFile App.Path & "\programdata1\tr33ds.x", 0, 0, Nothing, Nothing
Mesh_Asset(xi, zi).ScaleMesh 2.5, 2.5, 2.5
Frame_Asset(xi, zi).SetPosition Nothing, posx, 1, posz
Frame_Asset(xi, zi).AddVisual Mesh_Asset(xi, zi)
End If
If randommap = 3 Then
Mesh_Asset(xi, zi).LoadFromFile App.Path & "\programdata1\mount3ds.x", 0, 0, Nothing, Nothing
Mesh_Asset(xi, zi).ScaleMesh 2, 3.5, 3
Frame_Asset(xi, zi).SetPosition Nothing, posx, 1, posz
'''''''Frame_Asset(xi, zi).SetOrientation Nothing, MNTvec(0).x, MNTvec(0).y, MNTvec(0).z, MNTvec(1).x, MNTvec(1).y, MNTvec(1).z
'
Mesh_Asset(xi, zi).SetColorRGB 0.5, 0.5, 0.4
Frame_Asset(xi, zi).AddVisual Mesh_Asset(xi, zi)

End If
If randommap = 4 Or randommap = 18 Then
Mesh_Asset(xi, zi).LoadFromFile App.Path & "\programdata1\swamp3.x", 0, 0, Nothing, Nothing
Mesh_Asset(xi, zi).ScaleMesh 1, 1, 1
Frame_Asset(xi, zi).SetPosition Nothing, posx, -1.5, posz
Frame_Asset(xi, zi).AddVisual Mesh_Asset(xi, zi)
End If
If randommap = 21 Or randommap = 211 Then
Mesh_Asset(xi, zi).LoadFromFile App.Path & "\programdata1\castle3ds.x", 0, 0, Nothing, Nothing
Mesh_Asset(xi, zi).ScaleMesh 3, 4, 4.5
Frame_Asset(xi, zi).SetPosition Nothing, posx, 1, posz
Mesh_Asset(xi, zi).SetColorRGB 0.4, 0.4, 0.4 '
Frame_Asset(xi, zi).AddVisual Mesh_Asset(xi, zi)
'Frame_Asset(xi, zi).AddLight light
Animatebrd posx, posz, 0
End If
If randommap = 22 Or randommap = 221 Then

Mesh_Asset(xi, zi).LoadFromFile App.Path & "\programdata1\houses3ds.x", 0, 0, Nothing, Nothing
Mesh_Asset(xi, zi).ScaleMesh 1.5, 1.5, 1.5
Frame_Asset(xi, zi).SetPosition Nothing, posx, -0.5, posz
Frame_Asset(xi, zi).AddVisual Mesh_Asset(xi, zi)


End If

End If




End Sub
Public Sub play1()
DoEvents
SetCurrentDirectory App.Path + "\ProgramData3\"
  retVal = mciSendString("close songNumber1", vbNullString, 0, 0)
  retVal = mciSendString("open " & "click.mp3" & " type mpegvideo alias songNumber1", vbNullString, 0, 0)
        'Set a defailt volume of 50% using the hScroll value. A setting of 0 is lowest and 1000 is the highest.
      mciSendString "setaudio songNumber1 volume to " & 250, vbNullString, 0, 0
    'Start playing #1
    retVal = mciSendString("play songNumber1", vbNullString, 0, 0)
End Sub
Public Sub play2()
DoEvents
SetCurrentDirectory App.Path + "\ProgramData3\"
' retVal = mciSendString("close songNumber2", vbNullString, 0, 0)
  retVal = mciSendString("open " & "birds.mp3" & " type mpegvideo alias songNumber2", vbNullString, 0, 0)
        'Set a defailt volume of 50% using the hScroll value. A setting of 0 is lowest and 1000 is the highest.
      mciSendString "setaudio songNumber2 volume to " & 0, vbNullString, 0, 0
    'Start playing #1
    retVal = mciSendString("play songNumber2 repeat", vbNullString, 0, 0)
End Sub
Public Sub play3()
DoEvents
SetCurrentDirectory App.Path + "\ProgramData3\"
' retVal = mciSendString("close songNumber2", vbNullString, 0, 0)
  retVal = mciSendString("open " & "sea.mp3" & " type mpegvideo alias songNumber3", vbNullString, 0, 0)
        'Set a defailt volume of 50% using the hScroll value. A setting of 0 is lowest and 1000 is the highest.
      mciSendString "setaudio songNumber3 volume to " & 0, vbNullString, 0, 0
    'Start playing #1
    retVal = mciSendString("play songNumber3 repeat", vbNullString, 0, 0)
End Sub
Public Sub play4()
DoEvents
SetCurrentDirectory App.Path + "\ProgramData3\"
' retVal = mciSendString("close songNumber2", vbNullString, 0, 0)
  retVal = mciSendString("open " & "wind.mp3" & " type mpegvideo alias songNumber4", vbNullString, 0, 0)
        'Set a defailt volume of 50% using the hScroll value. A setting of 0 is lowest and 1000 is the highest.
      mciSendString "setaudio songNumber4 volume to " & 0, vbNullString, 0, 0
    'Start playing #1
    retVal = mciSendString("play songNumber4 repeat", vbNullString, 0, 0)
End Sub
Public Sub play5()
DoEvents
SetCurrentDirectory App.Path + "\ProgramData3\"
' retVal = mciSendString("close songNumber2", vbNullString, 0, 0)
  retVal = mciSendString("open " & "frogs.mp3" & " type mpegvideo alias songNumber5", vbNullString, 0, 0)
        'Set a defailt volume of 50% using the hScroll value. A setting of 0 is lowest and 1000 is the highest.
      mciSendString "setaudio songNumber5 volume to " & 0, vbNullString, 0, 0
    'Start playing #1
    retVal = mciSendString("play songNumber5 repeat", vbNullString, 0, 0)
End Sub
Public Sub play6()
DoEvents
SetCurrentDirectory App.Path + "\ProgramData3\"
' retVal = mciSendString("close songNumber2", vbNullString, 0, 0)
  retVal = mciSendString("open " & "magical_theme.mp3" & " type mpegvideo alias songNumber6", vbNullString, 0, 0)
        'Set a defailt volume of 50% using the hScroll value. A setting of 0 is lowest and 1000 is the highest.
      mciSendString "setaudio songNumber6 volume to " & 100, vbNullString, 0, 0
    'Start playing #1
    retVal = mciSendString("play songNumber6 repeat", vbNullString, 0, 0)
End Sub
Private Sub environmentsound()
Dim play2bvolume
Dim play3bvolume
Dim play4bvolume
Dim play5bvolume

play2bvolume = 0
play3bvolume = 0
play4bvolume = 0
play5bvolume = 0
'environment sound
Dim p
Dim q
On Error Resume Next
For p = 0 To 10
For q = 0 To 10
If bigmap(actualX + Xmap + p, actualZ + Zmap + q) < 3 Then   'forest sounds
play2bvolume = play2bvolume + 1
End If
If bigmap(actualX + Xmap + p, actualZ + Zmap + q) = 3 Then 'wind sounds
play3bvolume = play3bvolume + 1
End If
If bigmap(actualX + Xmap + p, actualZ + Zmap + q) = 4 Or bigmap(actualX + Xmap + p, actualZ + Zmap + q) = 18 Then 'swamp sounds
play4bvolume = play4bvolume + 1
End If
If bigmap(actualX + Xmap + p, actualZ + Zmap + q) > 4 And bigmap(actualX + Xmap + p, actualZ + Zmap + q) < 18 Then 'water sounds
play5bvolume = play5bvolume + 1
End If
Next
Next

 mciSendString "setaudio songNumber2 volume to " & Str((play2bvolume \ 4)), vbNullString, 0, 0 'forest
 mciSendString "setaudio songNumber3 volume to " & Str((play5bvolume)), vbNullString, 0, 0  'water
 mciSendString "setaudio songNumber4 volume to " & Str((play3bvolume * 2)), vbNullString, 0, 0 ' wind
 mciSendString "setaudio songNumber5 volume to " & Str((play4bvolume)), vbNullString, 0, 0 'swamp


End Sub
Private Sub monster_load()
DoEvents
Dim dataFORget As Integer
Dim freFILE2 As Integer
Dim gridCODE As Integer

svpos = 0
freFILE2 = FreeFile
Open App.Path + "\programdata2\mnst2FILE.DAT" For Random As freFILE2
For loopsvy = 0 To 500
For loopsvx = 0 To 500
'If monsters(loopsvy, loopsvy, 5) <> 0 Then
svpos = svpos + 1
Get freFILE2, svpos, dataFORget
monsters(loopsvx, 500 - loopsvy, 4) = dataFORget
Next loopsvx
Next loopsvy
Close freFILE2

svpos = 0
freFile = FreeFile
Open App.Path + "\programdata2\mnstFILE.DAT" For Random As freFile
For loopsvy = 0 To 500
For loopsvx = 0 To 500
Get freFile, svpos + 1, gridCODE
monsters(loopsvx, 500 - loopsvy, 5) = gridCODE
svpos = svpos + 1
Next loopsvx
Next loopsvy
Close freFile




MonstersLoaded = True










End Sub
Private Sub monsterShow(randommnst, xi, zi, posx, posz)
'If monsters(loopx + ACTUALgridX, loopy + ACTUALgridY, 5) = 123 Or (monsters(loopx + ACTUALgridX, loopy + ACTUALgridY, 5) >= 11000 And monsters(loopx + ACTUALgridX, loopy + ACTUALgridY, 5) < 13000) Then





If randommnst = 123 Or (randommnst >= 11000 And randommnst < 13000) Then 'raft
Mesh_Mnst(xi, zi, 1).LoadFromFile App.Path & "\programdata1\rft3d.x", 0, 0, Nothing, Nothing
Mesh_Mnst(xi, zi, 1).ScaleMesh 1.6, 1.6, 1.6
Frame_Mnst(xi, zi, 1).SetPosition Nothing, posx, -1.75, posz
Mesh_Mnst(xi, zi, 1).SetColorRGB 0.4, 0.3, 0.3
Frame_Mnst(xi, zi, 1).AddVisual Mesh_Mnst(xi, zi, 1)
End If

End Sub
Private Sub AnimateRaftsWaves()
HOVERINGX = HOVERINGX + 0.08
HOVERINGY = Sin(HOVERINGX) * 0.07
If HOVERINGX > 360 Then HOVERINGX = 0
Dim posx
Dim posz
On Error Resume Next
For xi = 1 To 11
For zi = 1 To 11


randommnst = monsters(actualX + Xmap + xi - 1, actualZ + Zmap + zi - 1, 5)
If randommnst = 123 Or (randommnst >= 11000 And randommnst < 13000) <> 0 Then 'raft
posx = ((actualX + xi - 1) * 10) + 0
posz = ((actualZ + zi - 1) * 10) + 0
Frame_Mnst(XReplaceSrfc(xi), ZReplaceSrfc(zi), 1).SetPosition Nothing, posx, -1.75 + HOVERINGY, posz
End If
randommap = bigmap(actualX + Xmap + xi - 1, actualZ + Zmap + zi - 1)
If randommap = 21 Or randommap = 211 Then
Animatebrd (actualX + xi - 1) * 10, (actualZ + zi - 1) * 10, 1 'brd
End If

'randommap = bigmap(actualX + Xmap + xi - 1, actualZ + Zmap + zi - 1, 5)
'f randommap = 4 Or randommap = 18 Then
'
'swampAnim1 = swampAnim1 + 1'
'
'
'
'If swampAnim1 > 650 Then
'swampAnim1 = 0
'If swampAnim2 Then
'swampAnim2 = False
'Else
'swampAnim2 = True
'End If
'End If




'If swampAnim2 = True Then Frame_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetRotation Nothing, 1, 0, 1, 0.002
'If swampAnim2 = False Then Frame_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetRotation Nothing, 1, 0, 1, -0.002




'End If

Next
Next


End Sub
Private Sub AnimateSwamp()

'Dim swampAnim1 As Integer
'Dim swampAnim2 As Boolean
'For xi = 1 To 11
'For zi = 1 To 11



'Next
'Next

End Sub
Private Sub RebuildMap()


On Error Resume Next
For xi = 1 To 11
XReplaceSrfc(xi) = xi - 1
ZReplaceSrfc(xi) = xi - 1
Next
For xi = 0 To 10
For zi = 0 To 10
Frame_surface((xi), (zi)).DeleteVisual Mesh_surface((xi), (zi))
Frame_Asset((xi), (zi)).DeleteVisual Mesh_Asset((xi), (zi))
Frame_Mnst((xi), (zi), 1).DeleteVisual Mesh_Mnst((xi), (zi), 1)
Mesh_Asset((xi), (zi)).Empty
Mesh_Mnst((xi), (zi), 0).Empty
Mesh_Mnst((xi), (zi), 1).Empty
'Mesh_surface((xi), (zi)).Empty
Next
Next







CameraPosAdX = ((500 - Xfrm) * 10) - 4
CameraPosAdZ = ((500 - Zfrm) * 10) - 11
Zdist = Vectors(90).z - CameraPosAdZ
Xdist = Vectors(90).x - CameraPosAdX



actualZ = Zfrm - 255
actualX = Xfrm - 255
If actualX < -250 Then
Xdist = Xdist + ((-250 - actualX) * 10)
actualX = -250
End If
If actualZ < -250 Then
Zdist = Zdist + ((-250 - actualZ) * 10)
actualZ = -250
End If

''If actualZ < -250 Then actualZ = -250

If actualX > 240 Then
Xdist = Xdist + ((240 - actualX) * 10)
actualX = 240
End If
If actualZ > 240 Then
Zdist = Zdist + ((240 - actualZ) * 10)
actualZ = 240
End If

''If actualZ > 240 Then actualZ = 240




Frame_frm.SetPosition Nothing, (XAbsolute + Xfrm) * 10, -1.7, (ZAbsolute + Zfrm) * 10


Dim CamSumX As Long
Dim CamSumZ As Long
CamSumZ = Vectors(90).z - CameraPosAdZ
CamSumX = Vectors(90).x - CameraPosAdX
'x > -2495 x < 2495 z < 2455 z > -2521.42
If CamSumX < -2495 Then CamSumX = -2495
If CamSumX > 2495 Then CamSumX = 2495
If CamSumZ < -2521.42 Then CamSumZ = -2521.42
If CamSumZ > 2455 Then CamSumZ = 2455

RMCanvas1.CameraFrame.SetPosition Nothing, CamSumX, Vectors(96).y, CamSumZ


RMCanvas1.CameraFrame.GetPosition Nothing, Vectors(93)
'Do While CameraPosAdZ > Zdist + 5
'actualZ = actualZ + 1
'Zdist = Zdist + 10
'Loop
'Do While CameraPosAdZ < Zdist - 5
'actualZ = actualZ - 1
'Zdist = Zdist - 10
'Loop
'Do While CameraPosAdX < Xdist - 5
'actualX = actualX - 1
'Xdist = Xdist - 10
'Loop
'Do While CameraPosAdX > Xdist + 5
'actualX = actualX + 1
'Xdist = Xdist + 10
'Loop







For xi = 1 To 11
For zi = 1 To 11





randommap = bigmap(actualX + Xmap + xi - 1, actualZ + Zmap + zi - 1)
randommnst = monsters(actualX + Xmap + xi - 1, actualZ + Zmap + zi - 1, 5)
If randommnst <> 0 Then 'raft
monsterShow randommnst, XReplaceSrfc(xi), ZReplaceSrfc(zi), ((actualX + xi - 1) * 10) + 0, ((actualZ + zi - 1) * 10) + 0
End If

If (randommap < 5 Or randommap > 17) And randommap <> 0 Then

If randommap = 1 Then
Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).LoadFromFile App.Path & "\programdata1\tree13ds2.x", 0, 0, Nothing, Nothing
Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).ScaleMesh 2, 2, 2
Frame_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetPosition Nothing, (actualX + xi - 1) * 10, 0.4, (actualZ + zi - 1) * 10
Frame_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).AddVisual Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi))
'Mesh_Asset(xi, zi).SetColorRGB 0.1, 0.4, 0.1
'Frame_Asset(xi, zi).AddLight light
End If
If randommap = 2 Then
Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).LoadFromFile App.Path & "\programdata1\tr33ds.x", 0, 0, Nothing, Nothing
Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).ScaleMesh 2.5, 2.5, 2.5
Frame_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetPosition Nothing, (actualX + xi - 1) * 10, 1, (actualZ + zi - 1) * 10
Frame_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).AddVisual Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi))
End If
If randommap = 3 Then
Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).LoadFromFile App.Path & "\programdata1\mount3ds.x", 0, 0, Nothing, Nothing
Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).ScaleMesh 2, 3.5, 3
Frame_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetPosition Nothing, (actualX + xi - 1) * 10, 1, (actualZ + zi - 1) * 10
''''Frame_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetOrientation Nothing, MNTvec(0).x, MNTvec(0).y, MNTvec(0).z, MNTvec(1).x, MNTvec(1).y, MNTvec(1).z
'RMCanvas1.CameraFrame.SetOrientation Nothing, Vectors(95).x, Vectors(95).y, Vectors(95).z, Vectors(94).x, Vectors(94).y, Vectors(94).z
Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetColorRGB 0.5, 0.5, 0.4
Frame_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).AddVisual Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi))

End If
If randommap = 4 Or randommap = 18 Then
Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).LoadFromFile App.Path & "\programdata1\swamp3.x", 0, 0, Nothing, Nothing
Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).ScaleMesh 1, 1, 1
Frame_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetPosition Nothing, (actualX + xi - 1) * 10, -1.5, (actualZ + zi - 1) * 10
Frame_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).AddVisual Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi))
End If
If randommap = 21 Or randommap = 211 Then
Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).LoadFromFile App.Path & "\programdata1\castle3ds.x", 0, 0, Nothing, Nothing
Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).ScaleMesh 3, 4, 4.5
Frame_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetPosition Nothing, (actualX + xi - 1) * 10, 1, (actualZ + zi - 1) * 10
Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetColorRGB 0.4, 0.4, 0.4 '
Frame_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).AddVisual Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi))
'Frame_Asset(xi, zi).AddLight light
Animatebrd (actualX + xi - 1) * 10, (actualZ + zi - 1) * 10, 0
End If
If randommap = 22 Or randommap = 221 Then

Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).LoadFromFile App.Path & "\programdata1\houses3ds.x", 0, 0, Nothing, Nothing
Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).ScaleMesh 1.5, 1.5, 1.5
Frame_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetPosition Nothing, (actualX + xi - 1) * 10, -0.5, (actualZ + zi - 1) * 10
Frame_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi)).AddVisual Mesh_Asset(XReplaceSrfc(xi), ZReplaceSrfc(zi))


End If

End If

If (randommap < 4 Or randommap > 18) And randommap <> 0 Then randommap = 0


'Set TEXTURE_surface(0, 0) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\" + Trim(Str(randommap)) + ".bmp")
Mesh_surface(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetTexture TEXTURE_surface(randommap)
'Mesh_surface(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetColorRGB 0.8, 0.8, 1 ' RGB'only when textured
Frame_surface(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetPosition Nothing, (actualX + xi - 1) * 10, -1.7, (actualZ + zi - 1) * 10
Frame_surface(XReplaceSrfc(xi), ZReplaceSrfc(zi)).AddVisual Mesh_surface(XReplaceSrfc(xi), ZReplaceSrfc(zi))






Next
Next


BlockedNewLine = False
End Sub
Public Sub bwx(PICTURETOGREY)


    Dim ca As COLORADJUSTMENT
    With PICTURETOGREY
       '' .AutoRedraw = True
        .ScaleMode = vbPixels
        SetStretchBltMode .hdc, HALFTONE
        GetColorAdjustment .hdc, ca
        ca.caContrast = 0
        ca.caRedGreenTint = 0
        ca.caBrightness = 0
        ca.caIlluminantIndex = 0
        ca.caColorfulness = -100 'No colors!
        ca.caReferenceBlack = 1000
        ca.caReferenceWhite = 9000
        ca.caBrightness = 5
        SetColorAdjustment .hdc, ca
        StretchBlt .hdc, 0, 0, .ScaleWidth, .ScaleHeight, .hdc, 0, 0, .ScaleWidth, .ScaleHeight, vbSrcCopy
       '' .Refresh
        .ScaleMode = 1
    End With
End Sub
Private Sub Animatebrd(xi, zi, strt)



On Error Resume Next
If strt = 0 Then Frame_brd.SetPosition Nothing, xi, 5, zi
If strt = 2 Then Frame_brd.SetPosition Nothing, Vectors(80).x, Vectors(80).y, Vectors(80).z
Dim brdVERT


If brdWNG = 1 Then
brdWNG3 = brdWNG3 + 1
If brdWNG3 > 55 Then brdWNG3 = 0
If brdWNG3 < 38 Then brdWNG2 = brdWNG2 + 1
If brdWNG2 = 7 Then
Frame_brd.DeleteVisual Mesh_brd(0)
Frame_brd.AddVisual Mesh_brd(1)
brdWNG = 0
brdWNG2 = 0

If Rnd * 20 > 5 Then
brdVERT = 0.01
Else
If Rnd * 20 < 15 Then
brdVERT = -0.01
End If
End If

End If
Else
brdWNG2 = brdWNG2 + 1
If brdWNG2 = 7 Then
Frame_brd.DeleteVisual Mesh_brd(1)
Frame_brd.AddVisual Mesh_brd(0)
brdWNG = 1
brdWNG2 = 0


If Rnd * 20 > 5 Then
brdVERT = 0.01
Else
If Rnd * 20 < 15 Then
brdVERT = -0.01
End If
End If


End If
End If
Frame_brd.GetPosition Nothing, Vectors(80)
If Vectors(80).y > 6 Then brdVERT = -0.01
If Vectors(80).y < 5 Then brdVERT = 0.01


Frame_brd.SetPosition Frame_brd, 0, brdVERT, -0.04




'Vectors (80)
End Sub
Private Sub SetFog()

RMCanvas1.SceneFrame.SetSceneFogColor RGB(255, 255, 255)
RMCanvas1.SceneFrame.SetSceneFogParams 45, 85, 55
RMCanvas1.SceneFrame.SetSceneFogMode D3DRMFOG_EXPONENTIALSQUARED
RMCanvas1.SceneFrame.SetSceneFogEnable D_TRUE
End Sub
Private Sub HandleMice5()
'Dim XcursorMice As Long
Dim I
'Dim ZcursorMice As Long
If handlemice(0) = 1 Then handlemice(0) = 20
RMCanvas1.CameraFrame.GetPosition Nothing, Vectors(93)
'XcursorMice = -(handlemice(1) - Me.Width \ 2) \ 3000
'ZcursorMice = -(handlemice(2) - Me.Height \ 2) \ 1200
'If handlemice(1) > Form1.Width \ 2 Then
'DoEvents
'For I = 1 To 20
'XcursorMice = XcursorMice + 0.005
'Frame_crs.SetOrientation Nothing, 1, 0, 1, 1, 1, 1
'Frame_crs.SetPosition RMCanvas1.CameraFrame, XcursorMice, -ZcursorMice, 5
'Next I
'End If
'If handlemice(1) < Form1.Width \ 2 Then
'DoEvents
'For I = 1 To 20
'XcursorMice = XcursorMice - 0.005
'Frame_crs.SetOrientation Nothing, 1, 0, 1, 1, 1, 1
'Frame_crs.SetPosition RMCanvas1.CameraFrame, XcursorMice, -ZcursorMice, 5
'Next
'End If
'If handlemice(2) > Form1.Height \ 2 Then
'DoEvents
'For I = 1 To 20
'ZcursorMice = ZcursorMice + 0.005
'Frame_crs.SetOrientation Nothing, 1, 0, 1, 1, 1, 1
'Frame_crs.SetPosition RMCanvas1.CameraFrame, XcursorMice, -ZcursorMice, 5
'Next
'End If
'If handlemice(2) < Form1.Height \ 2 Then
'DoEvents
'For I = 1 To 20
'ZcursorMice = ZcursorMice - 0.005
'Frame_crs.SetOrientation Nothing, 1, 0, 1, 1, 1, 1
'Frame_crs.SetPosition RMCanvas1.CameraFrame, XcursorMice, -ZcursorMice, 5
'Next
'End If


'(Vectors(93).x -(-2495))\10
'(Vectors(93).z -(-2521.42))\10
Label6.Caption = "3D cursor " + Str(capsON)


'Label6.Caption = "XcurM " + Str(XcursorMice) + " ZcurM " + Str(ZcursorMice) + vbCrLf
'Label6.Caption = Label6.Caption + Str(((Vectors(93).x - (-2495)) \ 10) - 254 + XcursorMice) + vbCrLf
'Label6.Caption = Label6.Caption + Str(((Vectors(93).z - (-2521.42)) \ 10) - 253 + ZcursorMice)
'If CamSumX < -2495 Then CamSumX = -2495
'If CamSumX > 2495 Then CamSumX = 2495
'If CamSumZ < -2521.42 Then CamSumZ = -2521.42
'If CamSumZ > 2455 Then CamSumZ = 2455



End Sub

Private Sub DayTimeLight()

'Dim NaWschod As Boolean
'Dim NaPolnoc As Boolean
'If NaWschod = False Then
'SunVector.x = SunVector.x - 0.4

'If SunVector.x >= 0 Then SunVector.y = SunVector.y - 0.4
'If SunVector.x > 0 Then SunVector.y = SunVector.y + 0.4

'If SunVector.x < -20 Then
'NaWschod = True
'End If
'Else
'SunVector.x = SunVector.x + 0.4

'If SunVector.x < 0 Then SunVector.y = SunVector.y + 0.4
'If SunVector.x >= 0 Then SunVector.y = SunVector.y - 0.4

'If SunVector.x > 20 Then
'NaWschod = False
'End If
'End If





'Godzina = Godzina - 0.1


  '  SunVector.x = 0:
  '  SunVector.y = 10:
  '  SunVector.z = -15



    SunRGBVector.x = 1: SunRGBVector.y = 1: SunRGBVector.z = 1
    
    
    RMCanvas1.AmbientLight.SetColorRGB SunRGBVector.x, SunRGBVector.y, SunRGBVector.z


    
    
    RMCanvas1.DirLightFrame.SetPosition RMCanvas1.CameraFrame, SunVector.x, SunVector.y, SunVector.z
    RMCanvas1.DirLightFrame.LookAt RMCanvas1.CameraFrame, Nothing, 0

















End Sub
Private Sub getFrameColor()
randommnst = monsters(Xfrm, Zfrm, 5)
randommap = bigmap(Xfrm, Zfrm)
If randommap = 21 Or randommap = 211 Or randommap = 22 Or randommap = 221 Or randommnst = 123 Or (randommnst >= 11000 And randommnst < 13000) Then
Mesh_frm.SetColorRGB 0.3, 0, 0.5
Else
Mesh_frm.SetColorRGB 1, 1, 0
End If



End Sub
'Private Sub Timer2_Timer()
'k values'gamepd check values'gamepd check values
'End Sub

Private Sub pad()


'Dim res
DoEvents
'MCanvas1.Update

'check keypressed on console'check keypressed on console
'check keypressed on console'check keypressed on console
'Dim loopkey
'Label1.Caption = "keys pressed:"
'For loopkey = 0 To 255
'If keypressed(loopkey) = True Then
'Label1.Caption = Label1.Caption + Str(loopkey)
'End If
'Next loopkey
'check keypressed on console'check keypressed on console
'check keypressed on console'check keypressed on console
'gamepd check values'gamepd check values'gamepd check values'gamepd check values'gamepd check values
'gamepd check values'gamepd check values'gamepd check values'gamepd check values'gamepd check values
'gamepd check values'gamepd check values'gamepd check values'gamepd check values'gamepd check values
'gamepd check values'gamepd check values'gamepd check values'gamepd check values'gamepd check values
Picture2.Cls
If joyGetPosEx(JNum, JI) <> 0 Then
    Picture2.Print "Joystick #"; CStr(JNum); " is not plugged in, or is not working."
Else
    With JI
        Picture2.Print "X = "; CStr(.dwXpos)
        Picture2.Print "Y = "; CStr(.dwYpos)
       ' Picture2.Print "Z = "; CStr(.dwZpos)
       ' Picture2.Print "R = "; CStr(.dwRpos)
       ' Picture2.Print "U = "; CStr(.dwUpos)
       ' Picture2.Print "V = "; CStr(.dwVpos)
        
        
        
        
     'WALK        'WALK  'WALK  'WALK  'WALK  'WALK  'WALK
          'WALK  'WALK  'WALK  'WALK  'WALK  'WALK  'WALK
         'WALK        'WALK  'WALK  'WALK  'WALK  'WALK  'WALK
          'WALK  'WALK  'WALK  'WALK  'WALK  'WALK  'WALK
          'WALK  'WALK  'WALK  'WALK  'WALK  'WALK  'WALK   'WALK  'WALK  'WALK  'WALK  'WALK  'WALK  'WALK
        
  'WALK        'WALK  'WALK  'WALK  'WALK  'WALK  'WALK
          'WALK  'WALK  'WALK  'WALK  'WALK  'WALK  'WALK
          'WALK  'WALK  'WALK  'WALK  'WALK  'WALK  'WALK
        
        
        
        
        
        
     '
     '   If HrFrapsCounter > 2 Then HrFrapsCounter = 0
     '   Hr = HrFrapsCounter
        
        '
        'Frame_hr(10).DeleteVisual Mesh_hr(10, Hr)
        On Error Resume Next
        Dim STEPED As Boolean
     '   Frame_hr(10).DeleteVisual Mesh_hr(10, HrFrapsCounter)
   If HrFrapsCounter = 0 Or HrFrapsCounter = 2 Then
   Frame_hr(10).DeleteVisual Mesh_hr(10, 0)
   End If
   If HrFrapsCounter = 1 Then
   Frame_hr(10).DeleteVisual Mesh_hr(10, 1)
   End If
   If HrFrapsCounter = 3 Then
   Frame_hr(10).DeleteVisual Mesh_hr(10, 2)
   End If
        
        
        Frame_hr(10).SetRotation Frame_hr(10), 0, 0, 0, 0
        
        If .dwXpos = 65535 Then
        
        'Frame_hr(10).SetPosition Frame_hr(10), 0.05, 0, 0
        Frame_hr(10).SetRotation Frame_hr(10), 0, 1, 0, 0.05

        
       If STEPED = False Then
       
       HrFrapsCounter2 = HrFrapsCounter2 + 1
       If HrFrapsCounter2 > 7 Then
       HrFrapsCounter2 = 0
       
       
       HrFrapsCounter = HrFrapsCounter + 1
             
        
        End If
       End If
        STEPED = True
        End If
        
        If .dwXpos = 0 Then
       Frame_hr(10).SetRotation Frame_hr(10), 0, -1, 0, 0.05
       ' Frame_hr(10).SetPosition Frame_hr(10), 0.05, 0, 0
        
       If STEPED = False Then
       
       HrFrapsCounter2 = HrFrapsCounter2 + 1
       If HrFrapsCounter2 > 7 Then
       HrFrapsCounter2 = 0
       
       
       HrFrapsCounter = HrFrapsCounter + 1
        End If
       End If
        STEPED = True
        End If
        
        
        If .dwYpos = 65535 Then
        Frame_hr(10).SetPosition Frame_hr(10), 0, 0, 0.05
       If STEPED = False Then
              HrFrapsCounter2 = HrFrapsCounter2 + 1
       If HrFrapsCounter2 > 7 Then
       HrFrapsCounter2 = 0
       HrFrapsCounter = HrFrapsCounter + 1
        End If
               End If
        STEPED = True
        End If

        If .dwYpos = 0 Then
        Frame_hr(10).SetPosition Frame_hr(10), 0, 0, -0.05
        If STEPED = False Then
               HrFrapsCounter2 = HrFrapsCounter2 + 1
       If HrFrapsCounter2 > 7 Then
       HrFrapsCounter2 = 0
        HrFrapsCounter = HrFrapsCounter + 1
         End If
     End If
        STEPED = True
        
        End If
      'If HrFrapsCounter > 3 Then HrFrapsCounter = 0
   If HrFrapsCounter > 3 Then HrFrapsCounter = 0
   If HrFrapsCounter = 0 Or HrFrapsCounter = 2 Then
   Frame_hr(10).AddVisual Mesh_hr(10, 0)
   End If
   If HrFrapsCounter = 1 Then
   Frame_hr(10).AddVisual Mesh_hr(10, 1)
   End If
   If HrFrapsCounter = 3 Then
   Frame_hr(10).AddVisual Mesh_hr(10, 2)
   End If
    
        
        
        
        
        
        
       ' Dim Frame_hr(10) As Direct3DRMFrame3
'Dim Vectors_hr(10, 2) As D3DVECTOR
        
        
        
        
   'WALK        'WALK  'WALK  'WALK  'WALK  'WALK  'WALK
       'WALK        'WALK  'WALK  'WALK  'WALK  'WALK  'WALK
          'WALK  'WALK  'WALK  'WALK  'WALK  'WALK  'WALK
          'WALK  'WALK  'WALK  'WALK  'WALK  'WALK  'WALK     'WALK  'WALK  'WALK  'WALK  'WALK  'WALK  'WALK
          'WALK  'WALK  'WALK  'WALK  'WALK  'WALK  'WALK
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
       ' If .dwPOV < &HFFFF& Then
       ' Picture2.Print "PovAngle = "; CStr(.dwPOV / 100)
       ' If .dwPOV / 100 = 270 Or .dwPOV / 100 = 315 Or .dwPOV / 100 = 225 Then
       ' Else
       ' End If
       ' If .dwPOV / 100 = 90 Or .dwPOV / 100 = 45 Or .dwPOV / 100 = 135 Then
       ' Else
       ' End If
       ' If .dwPOV / 100 = 0 Or .dwPOV / 100 = 315 Or .dwPOV / 100 = 45 Then
       ' Else
       ' End If
       ' If .dwPOV / 100 = 180 Or .dwPOV / 100 = 135 Or .dwPOV / 100 = 225 Then
       ' Else
       ' End If
       ' Else: Picture2.Print "PovCentered"
       ' End If
        
        
        Picture2.Print "ButtonsPressedCount = "; CStr(.dwButtonNumber)
        Picture2.Print "ButtonBinaryFlags = "; CStr(.dwButtons)
        If .dwButtons = 1 Or .dwButtons = 5 Or .dwButtons = 3 Or .dwButtons = 7 Or .dwButtons = 9 Or .dwButtons = 11 Or .dwButtons = 13 Or .dwButtons = 15 Then
        Else
        End If
        If .dwButtons = 8 Or .dwButtons = 12 Or .dwButtons = 14 Or .dwButtons = 9 Or .dwButtons = 10 Or .dwButtons = 11 Or .dwButtons = 13 Or .dwButtons = 15 Then
        Else
        End If
       If .dwButtons = 2 Or .dwButtons = 6 Or .dwButtons = 7 Or .dwButtons = 3 Or .dwButtons = 10 Or .dwButtons = 11 Or .dwButtons = 14 Or .dwButtons = 15 Then
       Else
       End If
       If .dwButtons = 4 Or .dwButtons = 5 Or .dwButtons = 7 Or .dwButtons = 6 Or .dwButtons = 12 Or .dwButtons = 13 Or .dwButtons = 14 Or .dwButtons = 15 Then
    
       Else
  
       End If
        Picture4.Cls
        Picture4.Circle (.dwXpos / &HFFFF& * (Picture4.ScaleWidth - 1), .dwYpos / &HFFFF& * (Picture4.ScaleHeight - 1)), 2
    End With
End If
'gamepd check values'gamepd check values'gamepd check values'gamepd check values'gamepd check values
'gamepd check values'gamepd check values'gamepd check values'gamepd check values'gamepd check values
'gamepd check values'gamepd check values'gamepd check values'gamepd check values'gamepd check values
'gamepd check values'gamepd check values'gamepd chec





End Sub






