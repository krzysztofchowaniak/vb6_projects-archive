VERSION 5.00
Object = "{08216199-47EA-11D3-9479-00AA006C473C}#2.1#0"; "rmcontrol.ocx"
Begin VB.Form Form1 
   AutoRedraw      =   -1  'True
   BackColor       =   &H00808080&
   BorderStyle     =   0  'None
   Caption         =   "KeyPad v1.0.1"
   ClientHeight    =   10170
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   17880
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   678
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   1192
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      ForeColor       =   &H80000008&
      Height          =   9975
      Left            =   2880
      ScaleHeight     =   665
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   1177
      TabIndex        =   2
      Top             =   2520
      Width           =   17655
      Begin VB.PictureBox Picture3 
         Appearance      =   0  'Flat
         AutoRedraw      =   -1  'True
         BackColor       =   &H00404040&
         BorderStyle     =   0  'None
         FillColor       =   &H00808080&
         ForeColor       =   &H00404040&
         Height          =   9975
         Left            =   720
         ScaleHeight     =   665
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   1177
         TabIndex        =   3
         Top             =   720
         Width           =   17655
         Begin RMControl7.RMCanvas RMCanvas1 
            Height          =   3495
            Index           =   0
            Left            =   1200
            TabIndex        =   4
            Top             =   1320
            Width           =   5295
            _ExtentX        =   9340
            _ExtentY        =   6165
         End
         Begin RMControl7.RMCanvas RMCanvas1 
            Height          =   3495
            Index           =   1
            Left            =   6960
            TabIndex        =   5
            Top             =   1320
            Width           =   5295
            _ExtentX        =   9340
            _ExtentY        =   6165
         End
      End
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      FillStyle       =   0  'Solid
      ForeColor       =   &H80000008&
      Height          =   2415
      Left            =   2880
      ScaleHeight     =   161
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   201
      TabIndex        =   0
      Top             =   0
      Width           =   3015
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   1080
      Top             =   2760
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Enabled         =   0   'False
      Height          =   2415
      Left            =   6000
      TabIndex        =   1
      Top             =   0
      Width           =   2535
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Mesh_OBJECT(55, 1) As Direct3DRMMeshBuilder3
Dim Frame_OBJECT(55, 1) As Direct3DRMFrame3


'Dim Mesh_star(40, 1) As Direct3DRMMeshBuilder3
'Dim Frame_star(40, 1) As Direct3DRMFrame3


Dim Frame_cam(1) As Direct3DRMFrame3
Dim TEXTURE_surface(0, 1) As Direct3DRMTexture3

Dim camStereo As D3DVECTOR
Dim camStereo11 As D3DVECTOR
Dim camStereo12 As D3DVECTOR
Dim camStereo21 As D3DVECTOR
Dim camStereo22 As D3DVECTOR
''''''''''key input''''''''''''''key input''''''''''''''key input''''''''''''''key input''''
''''''''''key input''''''''''''''key input''''''''''''''key input''''''''''''''key input''''
''''''''''key input''''''''''''''key input''''''''''''''key input''''''''''''''key input''''
Dim keypressed(255) As Boolean
''''''''''key input''''''''''''''key input''''''''''''''key input''''''''''''''key input''''
''''''''''key input''''''''''''''key input''''''''''''''key input''''''''''''''key input''''
''''''''''key input''''''''''''''key input''''''''''''''key input''''''''''''''key input''''
'///////////////////////////////////////////////////////////////////////////////////////////
'gamepad input''''''gamepad input''''''gamepad input''''''gamepad input'''''
'gamepad input''''''gamepad input''''''gamepad input''''''gamepad input'''''
'gamepad input''''''gamepad input''''''gamepad input''''''gamepad input'''''
Private Const JOY_RETURNBUTTONS As Long = &H80&
Private Const JOY_RETURNCENTERED As Long = &H400&
Private Const JOY_RETURNPOV As Long = &H40&
Private Const JOY_RETURNPOVCTS As Long = &H200&
Private Const JOY_RETURNR As Long = &H8&
Private Const JOY_RETURNRAWDATA As Long = &H100&
Private Const JOY_RETURNU As Long = &H10
Private Const JOY_RETURNV As Long = &H20
Private Const JOY_RETURNX As Long = &H1&
Private Const JOY_RETURNY As Long = &H2&
Private Const JOY_RETURNZ As Long = &H4&
Private Const JOY_RETURNALL As Long = (JOY_RETURNX Or JOY_RETURNY Or JOY_RETURNZ Or JOY_RETURNR Or JOY_RETURNU Or JOY_RETURNV Or JOY_RETURNPOV Or JOY_RETURNBUTTONS)
Private Type JOYINFOEX
    dwSize As Long ' size of structure
    dwFlags As Long ' flags to dicate what to return
    dwXpos As Long ' x position
    dwYpos As Long ' y position
    dwZpos As Long ' z position
    dwRpos As Long ' rudder/4th axis position
    dwUpos As Long ' 5th axis position
    dwVpos As Long ' 6th axis position
    dwButtons As Long ' button states
    dwButtonNumber As Long ' current button number pressed
    dwPOV As Long ' point of view state
    dwReserved1 As Long ' reserved for communication between winmm driver
    dwReserved2 As Long ' reserved for future expansion
End Type
Private Declare Function joyGetPosEx Lib "winmm.dll" (ByVal uJoyID As Long, ByRef pji As JOYINFOEX) As Long
Dim JI As JOYINFOEX
Const JNum As Long = 0
'Set this to the number of the joystick that
'you want to read (a value between 0 and 15).
'The first joystick plugged in is number 0.
'The API for reading joysticks supports up to
'16 simultaniously plugged in joysticks.
'Change this Const to a Dim if you want to set
'it at runtime.
'gamepad input''''''gamepad input''''''gamepad input''''''gamepad input'''''
'gamepad input''''''gamepad input''''''gamepad input''''''gamepad input'''''
'gamepad input''''''gamepad input''''''gamepad input''''''gamepad input'''''

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'keypress'keypress'keypress'keypress'keypress
keypressed(KeyCode) = True
'keypress'keypress'keypress'keypress'keypress
'exit'exit'exit'exit'exit'exit'exit
If KeyCode = 27 Then endProcedure
'exit'exit'exit'exit'exit'exit'exit
'show console'''show console'''show console''
If KeyCode = 192 Then
If Picture2.Visible = True Then
Picture2.Visible = False
Else
Picture2.Visible = True
End If
End If
'show console'''show console'''show console''
End Sub



Private Sub LoadTextures()
'TEXTURE_surface(10, 10) As Direct3DRMTexture3

Set TEXTURE_surface(0, 0) = RMCanvas1(0).D3DRM.LoadTexture(App.Path & "\textures\0.bmp")
Set TEXTURE_surface(0, 1) = RMCanvas1(0).D3DRM.LoadTexture(App.Path & "\textures\0.bmp")

'Set TEXTURE_surface(4) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\4.bmp")
'Set TEXTURE_surface(18) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\18.bmp")

'Set TEXTURE_surface(5) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\5.bmp")
'Set TEXTURE_surface(6) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\6.bmp")
'Set TEXTURE_surface(7) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\7.bmp")
'Set TEXTURE_surface(8) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\8.bmp")
's 'et TEXTURE_surface(9) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\9.bmp")
'Set TEXTURE_surface(10) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\10.bmp")
'Set TEXTURE_surface(11) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\11.bmp")
'Set TEXTURE_surface(12) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\12.bmp")
'Set TEXTURE_surface(13) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\13.bmp")
'Set TEXTURE_surface(14) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\14.bmp")
'Set TEXTURE_surface(15) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\15.bmp")
'Set TEXTURE_surface(16) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\16.bmp")
'Set TEXTURE_surface(17) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\programdata2\17.bmp")


End Sub








Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
'keypress-off'keypress-off'keypress-off
keypressed(KeyCode) = False
'keypress-off'keypress-off'keypress-off
End Sub

Private Sub Form_Load()
'gamepad'gamepad'gamepad'gamepad
JI.dwSize = Len(JI)
JI.dwFlags = JOY_RETURNALL
'gamepad'gamepad'gamepad'gamepad



End Sub

Private Sub Form_Resize()
'main screen-picbox resize
Picture2.Left = 0: Picture2.Width = ScaleWidth
Picture2.Top = 0: Picture2.Height = ScaleHeight
Picture3.Left = 0: Picture3.Width = ScaleWidth
Picture3.Top = 0: Picture3.Height = ScaleHeight

With RMCanvas1(0)
.Left = 0
.Top = 0
.Width = Form1.ScaleWidth \ 2
.Height = Form1.ScaleHeight
.StartWindowed
    .SceneFrame.SetSceneBackgroundRGB 0, 0.1, 0.3
    .Viewport.SetBack 4000
.AmbientLight.SetColorRGB 1, 1, 1
.Device.SetTextureQuality D3DRMTEXTURE_LINEAR
.DirLightFrame.SetPosition Nothing, 0, 422, 0


End With
With RMCanvas1(1)
.Left = Form1.ScaleWidth \ 2
.Top = 0
.Width = Form1.ScaleWidth \ 2
.Height = Form1.ScaleHeight
.StartWindowed
    .SceneFrame.SetSceneBackgroundRGB 0, 0.1, 0.3
    .Viewport.SetBack 4000
.AmbientLight.SetColorRGB 1, 1, 1
.Device.SetTextureQuality D3DRMTEXTURE_LINEAR
.DirLightFrame.SetPosition Nothing, 0, 422, 0

End With
LoadTextures
setCAMERASpositions
SETMESHES
End Sub
Private Sub setCAMERASpositions()
Set Frame_cam(0) = RMCanvas1(0).D3DRM.CreateFrame(RMCanvas1(0).SceneFrame)
Set Frame_cam(1) = RMCanvas1(1).D3DRM.CreateFrame(RMCanvas1(1).SceneFrame)
camStereo.x = 0
camStereo.y = 0.5
camStereo.z = -40
Frame_cam(0).SetPosition Nothing, camStereo.x, camStereo.y, camStereo.z
Frame_cam(1).SetPosition Nothing, camStereo.x, camStereo.y, camStereo.z

RMCanvas1(0).CameraFrame.SetPosition Frame_cam(0), -0.15, 0, 0
RMCanvas1(1).CameraFrame.SetPosition Frame_cam(1), 0.15, 0, 0

RMCanvas1(0).CameraFrame.GetOrientation Frame_cam(0), camStereo11, camStereo12
RMCanvas1(1).CameraFrame.GetOrientation Frame_cam(1), camStereo21, camStereo22
'Dim camStereo As D3DVECTOR
'Dim camStereo11 As D3DVECTOR
'Dim camStereo12 As D3DVECTOR
'Dim camStereo21 As D3DVECTOR
'Dim camStereo22 As D3DVECTOR







End Sub
Private Sub Picture1_KeyUp(KeyCode As Integer, Shift As Integer)
keypressed(KeyCode) = False
End Sub

Private Sub Picture2_KeyUp(KeyCode As Integer, Shift As Integer)
keypressed(KeyCode) = False
End Sub

Private Sub Timer1_Timer()
Dim zmove As Double
Dim rmove As Double

If CStr(JI.dwYpos) = 0 Then
zmove = 0.2
End If
If CStr(JI.dwYpos) = 65535 Then
zmove = -0.2
End If
If CStr(JI.dwXpos) = 0 Then
rmove = -0.03
End If
If CStr(JI.dwXpos) = 65535 Then
rmove = 0.03
End If



Frame_cam(0).SetRotation Frame_cam(0), 0, 1, 0, rmove
Frame_cam(1).SetRotation Frame_cam(1), 0, 1, 0, rmove
Frame_cam(0).SetPosition Frame_cam(0), 0, 0, zmove
Frame_cam(1).SetPosition Frame_cam(1), 0, 0, zmove





RMCanvas1(0).CameraFrame.SetPosition Frame_cam(0), -0.15, 0, 0
RMCanvas1(1).CameraFrame.SetPosition Frame_cam(1), 0.15, 0, 0

RMCanvas1(0).CameraFrame.SetOrientation Frame_cam(0), camStereo11.x, camStereo11.y, camStereo11.z, camStereo12.x, camStereo12.y, camStereo12.z
RMCanvas1(1).CameraFrame.SetOrientation Frame_cam(1), camStereo21.x, camStereo21.y, camStereo21.z, camStereo22.x, camStereo22.y, camStereo22.z





RMCanvas1(0).Update
RMCanvas1(1).Update
DoEvents


'check keypressed on console'check keypressed on console
'check keypressed on console'check keypressed on console
Dim loopkey
Label1.Caption = "keys pressed:"
For loopkey = 0 To 255
If keypressed(loopkey) = True Then
Label1.Caption = Label1.Caption + Str(loopkey)
End If
Next loopkey
'check keypressed on console'check keypressed on console
'check keypressed on console'check keypressed on console
'gamepd check values'gamepd check values'gamepd check values'gamepd check values'gamepd check values
'gamepd check values'gamepd check values'gamepd check values'gamepd check values'gamepd check values
'gamepd check values'gamepd check values'gamepd check values'gamepd check values'gamepd check values
'gamepd check values'gamepd check values'gamepd check values'gamepd check values'gamepd check values
Cls
If joyGetPosEx(JNum, JI) <> 0 Then
    Print "Joystick #"; CStr(JNum); " is not plugged in, or is not working."
Else
    With JI
        Print "X = "; CStr(.dwXpos)
        Print "Y = "; CStr(.dwYpos)
        Print "Z = "; CStr(.dwZpos)
        Print "R = "; CStr(.dwRpos)
        Print "U = "; CStr(.dwUpos)
        Print "V = "; CStr(.dwVpos)
        
        
        If .dwPOV < &HFFFF& Then
        Print "PovAngle = "; CStr(.dwPOV / 100)
        If .dwPOV / 100 = 270 Or .dwPOV / 100 = 315 Or .dwPOV / 100 = 225 Then
        Else
        End If
        If .dwPOV / 100 = 90 Or .dwPOV / 100 = 45 Or .dwPOV / 100 = 135 Then
        Else
        End If
        If .dwPOV / 100 = 0 Or .dwPOV / 100 = 315 Or .dwPOV / 100 = 45 Then
        Else
        End If
        If .dwPOV / 100 = 180 Or .dwPOV / 100 = 135 Or .dwPOV / 100 = 225 Then
        Else
        End If
        Else: Print "PovCentered"
        End If
        
        
        Print "ButtonsPressedCount = "; CStr(.dwButtonNumber)
        Print "ButtonBinaryFlags = "; CStr(.dwButtons)
        If .dwButtons = 1 Or .dwButtons = 5 Or .dwButtons = 3 Or .dwButtons = 7 Or .dwButtons = 9 Or .dwButtons = 11 Or .dwButtons = 13 Or .dwButtons = 15 Then
        Else
        End If
        If .dwButtons = 8 Or .dwButtons = 12 Or .dwButtons = 14 Or .dwButtons = 9 Or .dwButtons = 10 Or .dwButtons = 11 Or .dwButtons = 13 Or .dwButtons = 15 Then
        Else
        End If
       If .dwButtons = 2 Or .dwButtons = 6 Or .dwButtons = 7 Or .dwButtons = 3 Or .dwButtons = 10 Or .dwButtons = 11 Or .dwButtons = 14 Or .dwButtons = 15 Then
       Else
       End If
       If .dwButtons = 4 Or .dwButtons = 5 Or .dwButtons = 7 Or .dwButtons = 6 Or .dwButtons = 12 Or .dwButtons = 13 Or .dwButtons = 14 Or .dwButtons = 15 Then
    
       Else
  
       End If
        Picture1.Cls
        Picture1.Circle (.dwXpos / &HFFFF& * (Picture1.Width - 1), .dwYpos / &HFFFF& * (Picture1.Height - 1)), 2
    End With
End If
'gamepd check values'gamepd check values'gamepd check values'gamepd check values'gamepd check values
'gamepd check values'gamepd check values'gamepd check values'gamepd check values'gamepd check values
'gamepd check values'gamepd check values'gamepd check values'gamepd check values'gamepd check values
'gamepd check values'gamepd check values'gamepd check values'gamepd check values'gamepd check values
End Sub
Public Sub endProcedure()
'exit program'exit program'exit program'exit program
End
'exit program'exit program'exit program'exit program
End Sub


Private Sub SETMESHES()
Dim I
Dim Q
Dim rnd1
Dim rnd2

 Q = 0
'SURFACE
For I = 0 To 1
Set Frame_OBJECT(Q, I) = RMCanvas1(I).D3DRM.CreateFrame(RMCanvas1(I).SceneFrame)
Set Mesh_OBJECT(Q, I) = RMCanvas1(I).D3DRM.CreateMeshBuilder()
Mesh_OBJECT(Q, I).LoadFromFile App.Path & "\models\surface0.x", 0, 0, Nothing, Nothing
Mesh_OBJECT(Q, I).ScaleMesh 80, 0.0001, 80 '2.44, 0.0001, 3.62
Mesh_OBJECT(Q, I).SetTexture TEXTURE_surface(0, I)
Mesh_OBJECT(Q, I).SetColorRGB 0.1, 0.3, 0.1 ' RGB'only when textured
Frame_OBJECT(Q, I).SetPosition Nothing, 0, 0, 0
Frame_OBJECT(Q, I).AddVisual Mesh_OBJECT(Q, I)
Next
 Q = 1
'CASTLE
For I = 0 To 1
Set Frame_OBJECT(Q, I) = RMCanvas1(I).D3DRM.CreateFrame(RMCanvas1(I).SceneFrame)
Set Mesh_OBJECT(Q, I) = RMCanvas1(I).D3DRM.CreateMeshBuilder()
Mesh_OBJECT(Q, I).LoadFromFile App.Path & "\models\castle3ds.x", 0, 0, Nothing, Nothing
Mesh_OBJECT(Q, I).ScaleMesh 10, 10, 10 '2.44, 0.0001, 3.62
'Mesh_surface(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetTexture TEXTURE_surface(randommap)
Mesh_OBJECT(Q, I).SetColorRGB 0.4, 0.4, 0.4 ' RGB'only when textured
Frame_OBJECT(Q, I).SetPosition Nothing, 0, 5, 50
Frame_OBJECT(Q, I).AddVisual Mesh_OBJECT(Q, I)

RMCanvas1(I).DirLightFrame.LookAt Frame_OBJECT(Q, I), Nothing, 0

Next
'MOUNTAIN
Q = 2
For I = 0 To 1
Set Frame_OBJECT(Q, I) = RMCanvas1(I).D3DRM.CreateFrame(RMCanvas1(I).SceneFrame)
Set Mesh_OBJECT(Q, I) = RMCanvas1(I).D3DRM.CreateMeshBuilder()
Mesh_OBJECT(Q, I).LoadFromFile App.Path & "\models\mount3ds.x", 0, 0, Nothing, Nothing
Mesh_OBJECT(Q, I).ScaleMesh 25, 15, 15 '2.44, 0.0001, 3.62
'Mesh_surface(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetTexture TEXTURE_surface(randommap)
Mesh_OBJECT(Q, I).SetColorRGB 0.5, 0.5, 0.4 ' RGB'only when textured
Frame_OBJECT(Q, I).SetPosition Nothing, 5, 5, 70
Frame_OBJECT(Q, I).AddVisual Mesh_OBJECT(Q, I)
Next
Q = 3
'MOON
For I = 0 To 1
Set Frame_OBJECT(Q, I) = RMCanvas1(I).D3DRM.CreateFrame(RMCanvas1(I).SceneFrame)
Set Mesh_OBJECT(Q, I) = RMCanvas1(I).D3DRM.CreateMeshBuilder()
Mesh_OBJECT(Q, I).LoadFromFile App.Path & "\models\sphere.X", 0, 0, Nothing, Nothing
Mesh_OBJECT(Q, I).ScaleMesh 11, 11, 11 '2.44, 0.0001, 3.62
'Mesh_surface(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetTexture TEXTURE_surface(randommap)
'Mesh_OBJECT(Q, I).SetColorRGB 0.4, 0.4, 0.4 ' RGB'only when textured
Frame_OBJECT(Q, I).SetPosition Nothing, 5, 55, 180
Frame_OBJECT(Q, I).AddVisual Mesh_OBJECT(Q, I)
Next
For Q = 4 To 11
'TREES
rnd1 = (Rnd * 10) * 4
rnd2 = (Rnd * 10) * 4
For I = 0 To 1
Set Frame_OBJECT(Q, I) = RMCanvas1(I).D3DRM.CreateFrame(RMCanvas1(I).SceneFrame)
Set Mesh_OBJECT(Q, I) = RMCanvas1(I).D3DRM.CreateMeshBuilder()
Mesh_OBJECT(Q, I).LoadFromFile App.Path & "\models\tr33ds.x", 0, 0, Nothing, Nothing
Mesh_OBJECT(Q, I).ScaleMesh 3, 2.1, 3 '2.44, 0.0001, 3.62
'Mesh_surface(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetTexture TEXTURE_surface(randommap)
'Mesh_OBJECT(Q, I).SetColorRGB 0.4, 0.4, 0.4 ' RGB'only when textured
Frame_OBJECT(Q, I).SetPosition Nothing, -15 + rnd1, 2.2, 30 - rnd2
Frame_OBJECT(Q, I).AddVisual Mesh_OBJECT(Q, I)
Next
Next

Q = 12

For I = 0 To 1
Set Frame_OBJECT(Q, I) = RMCanvas1(I).D3DRM.CreateFrame(RMCanvas1(I).SceneFrame)
Set Mesh_OBJECT(Q, I) = RMCanvas1(I).D3DRM.CreateMeshBuilder()
Mesh_OBJECT(Q, I).LoadFromFile App.Path & "\models\sklt3ds.x", 0, 0, Nothing, Nothing
Mesh_OBJECT(Q, I).ScaleMesh 0.5, 0.55, 0.5 '2.44, 0.0001, 3.62
'Mesh_surface(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetTexture TEXTURE_surface(randommap)
Mesh_OBJECT(Q, I).SetColorRGB 0.5, 0.5, 0.4 ' RGB'only when textured
Frame_OBJECT(Q, I).SetPosition Nothing, 0, 0.5, -22
Frame_OBJECT(Q, I).AddVisual Mesh_OBJECT(Q, I)
Next


'stars front
For Q = 16 To 25
rnd1 = (Rnd * 400)
rnd2 = (Rnd * 100)
For I = 0 To 1
Set Frame_OBJECT(Q, I) = RMCanvas1(I).D3DRM.CreateFrame(RMCanvas1(I).SceneFrame)
Set Mesh_OBJECT(Q, I) = RMCanvas1(I).D3DRM.CreateMeshBuilder()
Mesh_OBJECT(Q, I).LoadFromFile App.Path & "\models\sphere.X", 0, 0, Nothing, Nothing
Mesh_OBJECT(Q, I).ScaleMesh 0.8, 0.8, 0.8 '2.44, 0.0001, 3.62
'Mesh_surface(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetTexture TEXTURE_surface(randommap)
'Mesh_OBJECT(Q, I).SetColorRGB 0.4, 0.4, 0.4 ' RGB'only when textured
Frame_OBJECT(Q, I).SetPosition Nothing, -200 + rnd1, 0 + rnd2, 200
Frame_OBJECT(Q, I).AddVisual Mesh_OBJECT(Q, I)
Next
Next
'Left
For Q = 26 To 35
rnd1 = (Rnd * 400)
rnd2 = (Rnd * 100)
For I = 0 To 1
Set Frame_OBJECT(Q, I) = RMCanvas1(I).D3DRM.CreateFrame(RMCanvas1(I).SceneFrame)
Set Mesh_OBJECT(Q, I) = RMCanvas1(I).D3DRM.CreateMeshBuilder()
Mesh_OBJECT(Q, I).LoadFromFile App.Path & "\models\sphere.X", 0, 0, Nothing, Nothing
Mesh_OBJECT(Q, I).ScaleMesh 0.8, 0.8, 0.8 '2.44, 0.0001, 3.62
'Mesh_surface(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetTexture TEXTURE_surface(randommap)
'Mesh_OBJECT(Q, I).SetColorRGB 0.4, 0.4, 0.4 ' RGB'only when textured
Frame_OBJECT(Q, I).SetPosition Nothing, 200, 0 + rnd2, -200 + rnd1
Frame_OBJECT(Q, I).AddVisual Mesh_OBJECT(Q, I)
Next
Next
'right
For Q = 36 To 45
rnd1 = (Rnd * 400)
rnd2 = (Rnd * 100)
For I = 0 To 1
Set Frame_OBJECT(Q, I) = RMCanvas1(I).D3DRM.CreateFrame(RMCanvas1(I).SceneFrame)
Set Mesh_OBJECT(Q, I) = RMCanvas1(I).D3DRM.CreateMeshBuilder()
Mesh_OBJECT(Q, I).LoadFromFile App.Path & "\models\sphere.X", 0, 0, Nothing, Nothing
Mesh_OBJECT(Q, I).ScaleMesh 0.8, 0.8, 0.8 '2.44, 0.0001, 3.62
'Mesh_surface(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetTexture TEXTURE_surface(randommap)
'Mesh_OBJECT(Q, I).SetColorRGB 0.4, 0.4, 0.4 ' RGB'only when textured
Frame_OBJECT(Q, I).SetPosition Nothing, -200, 0 + rnd2, -300 + rnd1
Frame_OBJECT(Q, I).AddVisual Mesh_OBJECT(Q, I)
Next
Next
'stars back
For Q = 46 To 55
rnd1 = (Rnd * 400)
rnd2 = (Rnd * 100)
For I = 0 To 1
Set Frame_OBJECT(Q, I) = RMCanvas1(I).D3DRM.CreateFrame(RMCanvas1(I).SceneFrame)
Set Mesh_OBJECT(Q, I) = RMCanvas1(I).D3DRM.CreateMeshBuilder()
Mesh_OBJECT(Q, I).LoadFromFile App.Path & "\models\sphere.X", 0, 0, Nothing, Nothing
Mesh_OBJECT(Q, I).ScaleMesh 0.8, 0.8, 0.8 '2.44, 0.0001, 3.62
'Mesh_surface(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetTexture TEXTURE_surface(randommap)
'Mesh_OBJECT(Q, I).SetColorRGB 0.4, 0.4, 0.4 ' RGB'only when textured
Frame_OBJECT(Q, I).SetPosition Nothing, -300 + rnd1, 0 + rnd2, -200
Frame_OBJECT(Q, I).AddVisual Mesh_OBJECT(Q, I)
Next
Next

'MOUNTAIN
Q = 13
For I = 0 To 1
Set Frame_OBJECT(Q, I) = RMCanvas1(I).D3DRM.CreateFrame(RMCanvas1(I).SceneFrame)
Set Mesh_OBJECT(Q, I) = RMCanvas1(I).D3DRM.CreateMeshBuilder()
Mesh_OBJECT(Q, I).LoadFromFile App.Path & "\models\mount3ds.x", 0, 0, Nothing, Nothing
Mesh_OBJECT(Q, I).ScaleMesh 25, 15, 25 '2.44, 0.0001, 3.62
'Mesh_surface(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetTexture TEXTURE_surface(randommap)
Mesh_OBJECT(Q, I).SetColorRGB 0.5, 0.5, 0.4 ' RGB'only when textured
Frame_OBJECT(Q, I).SetPosition Nothing, 75, 5, 5
Frame_OBJECT(Q, I).AddVisual Mesh_OBJECT(Q, I)
Next
Q = 14
For I = 0 To 1
Set Frame_OBJECT(Q, I) = RMCanvas1(I).D3DRM.CreateFrame(RMCanvas1(I).SceneFrame)
Set Mesh_OBJECT(Q, I) = RMCanvas1(I).D3DRM.CreateMeshBuilder()
Mesh_OBJECT(Q, I).LoadFromFile App.Path & "\models\mount3ds.x", 0, 0, Nothing, Nothing
Mesh_OBJECT(Q, I).ScaleMesh 25, 15, 25 '2.44, 0.0001, 3.62
'Mesh_surface(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetTexture TEXTURE_surface(randommap)
Mesh_OBJECT(Q, I).SetColorRGB 0.5, 0.5, 0.4 ' RGB'only when textured
Frame_OBJECT(Q, I).SetPosition Nothing, -75, 5, 5
Frame_OBJECT(Q, I).AddVisual Mesh_OBJECT(Q, I)
Next




 Q = 15
'house
For I = 0 To 1
Set Frame_OBJECT(Q, I) = RMCanvas1(I).D3DRM.CreateFrame(RMCanvas1(I).SceneFrame)
Set Mesh_OBJECT(Q, I) = RMCanvas1(I).D3DRM.CreateMeshBuilder()
Mesh_OBJECT(Q, I).LoadFromFile App.Path & "\models\houses3ds.x", 0, 0, Nothing, Nothing
Mesh_OBJECT(Q, I).ScaleMesh 2.1, 1.1, 2.1 '2.44, 0.0001, 3.62
'Mesh_surface(XReplaceSrfc(xi), ZReplaceSrfc(zi)).SetTexture TEXTURE_surface(randommap)
'Mesh_OBJECT(Q, I).SetColorRGB 0.4, 0.4, 0.4 ' RGB'only when textured
Frame_OBJECT(Q, I).SetPosition Nothing, 25, 1, -10
Frame_OBJECT(Q, I).AddVisual Mesh_OBJECT(Q, I)

'RMCanvas1(I).DirLightFrame.LookAt Frame_OBJECT(Q, I), Nothing, 0

Next



End Sub
