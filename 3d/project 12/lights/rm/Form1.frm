VERSION 5.00
Object = "{08216199-47EA-11D3-9479-00AA006C473C}#2.1#0"; "RMControl.ocx"
Begin VB.Form Form1 
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   6195
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   11145
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6195
   ScaleWidth      =   11145
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   10440
      Top             =   5280
   End
   Begin RMControl7.RMCanvas RMCanvas1 
      Height          =   5415
      Left            =   600
      TabIndex        =   0
      Top             =   240
      Width           =   9735
      _ExtentX        =   17171
      _ExtentY        =   9551
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Declare Function SetCurrentDirectory Lib "kernel32" _
Alias "SetCurrentDirectoryA" (ByVal lpPathName As String) As Long
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Const SW_SHOWNORMAL = 1
Private Declare Function GetSystemDirectory Lib "kernel32" Alias "GetSystemDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Long) As Long
'rmcontrol functions
Dim Mesh_OBJecT As Direct3DRMMeshBuilder3
Dim Frame_OBJecT As Direct3DRMFrame3
Dim TEXTURE_forObject As Direct3DRMTexture3
Dim Vectors(1) As D3DVECTOR
Dim TEXTURE_forBackground As Direct3DRMTexture3
Private Sub initObjects()

Set Frame_OBJecT = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
Set Mesh_OBJecT = RMCanvas1.D3DRM.CreateMeshBuilder()
Set TEXTURE_forObject = RMCanvas1.D3DRM.LoadTexture(App.Path & "\earthtexture.bmp")
Mesh_OBJecT.LoadFromFile App.Path & "\sphere.X", 0, 0, Nothing, Nothing
Mesh_OBJecT.ScaleMesh 1.7, 1.7, 1.7
Mesh_OBJecT.SetTexture TEXTURE_forObject
Frame_OBJecT.SetPosition Nothing, 0, 0, 13
'Frame_OBJCT(0).SetOrientation Nothing, 0.5, -0.2, -0.2, 0.28, 0.4, -0.4
Frame_OBJecT.SetRotation Nothing, 0, 1, 0, 0.005
Mesh_OBJecT.SetColorRGB 1, 0.5, 0.5 ' RGB
Frame_OBJecT.AddVisual Mesh_OBJecT


End Sub
Private Sub Form_Load()
' determine system directory and which system it is 32\64
' copy needed dll's (dirextx7) and ocx
' register files
Dim sSave As String, Ret As Long
sSave = Space(255)
Ret = GetSystemDirectory(sSave, 255)
sSave = Left$(sSave, Ret - 9)
Dim OSBits
OSBits = IIf(Len(Environ$("PROGRAMFILES(X86)")) > 0, 64, 32)
If OSBits = 64 Then
CopyFiles App.Path + "\dx7lib", sSave + "\syswow64"
SetCurrentDirectory sSave + "\syswow64"
Else
CopyFiles App.Path + "\dx7lib", sSave + "\System32"
SetCurrentDirectory sSave + "\System32"
End If
ShellExecute 0, "runas", "regsvr32", "RMControl.ocx /s", vbNullString, SW_SHOWNORMAL
ShellExecute 0, "runas", "regsvr32", "dx7vb.dll /s", vbNullString, SW_SHOWNORMAL
End Sub
Public Function CopyFiles(ByRef strSource As String, ByRef strDestination As String)
On Error Resume Next
Dim EndFile As String
Dim objfso
Set objfso = CreateObject("Scripting.FileSystemObject")
Dim strFile As String
If Right$(strSource, 1) <> "\" Then strSource = strSource & "\"
If Right$(strDestination, 1) <> "\" Then strDestination = strDestination & "\"
strFile = Dir(strSource & "*.*")
Do While Len(strFile)
With objfso
If Not .FolderExists(strDestination) Then .CreateFolder (strDestination)
If Right$(strFile, 2) = "l_" Then
EndFile = "l"
Else
EndFile = "x"
End If
.CopyFile strSource & strFile, Left$(strDestination & strFile, Len(strDestination & strFile) - 1) & EndFile
End With
strFile = Dir
Loop
Set objfso = Nothing
Exit Function
End Function
Private Sub initRM()
With RMCanvas1
    .StartWindowed
    .SceneFrame.SetSceneBackgroundRGB 0, 0, 0.5   'background color rgb seen when no backgroud texture
    .Viewport.SetBack 5000                        'view depth
    .CameraFrame.SetPosition Nothing, 0, 0, 0     'camera position xyz
End With
RMCanvas1.AmbientLight.SetColorRGB 0.7, 1, 0.8
'RMCanvas1.CameraFrame.SetOrientation Nothing, Vectors(0).X, Vectors(0).Y, Vectors(0).z, Vectors(1).X, Vectors(1).Y, Vectors(1).z
Set TEXTURE_forBackground = RMCanvas1.D3DRM.LoadTexture(App.Path + "/background.bmp")
RMCanvas1.SceneFrame.SetSceneBackgroundImage TEXTURE_forBackground
'fog parameters
RMCanvas1.SceneFrame.SetSceneFogColor RGB(143, 43, 21)
RMCanvas1.SceneFrame.SetSceneFogParams 11, 13, 117
RMCanvas1.SceneFrame.SetSceneFogMode D3DRMFOG_EXPONENTIAL
RMCanvas1.SceneFrame.SetSceneFogEnable D_TRUE

End Sub
Private Sub Form_Resize()
RMCanvas1.Left = 0: RMCanvas1.Top = 0: RMCanvas1.Width = Form1.ScaleWidth: RMCanvas1.Height = Form1.ScaleHeight
' initialise rmcontrol
initRM
' initialise objects
initObjects
End Sub
Private Sub RMCanvas1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
End
End Sub

Private Sub Timer1_Timer()
'update view
RMCanvas1.Update
End Sub
