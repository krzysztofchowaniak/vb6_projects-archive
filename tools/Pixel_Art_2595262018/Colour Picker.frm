VERSION 5.00
Begin VB.Form Colour_Picker 
   AutoRedraw      =   -1  'True
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " Colour Picker"
   ClientHeight    =   3060
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   5655
   BeginProperty Font 
      Name            =   "System"
      Size            =   9.75
      Charset         =   238
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   204
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   377
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Sel_Col_Frame 
      Caption         =   "Selected Colour"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   120
      TabIndex        =   20
      Top             =   2460
      Width           =   5415
      Begin VB.CommandButton Simplified_UI 
         Caption         =   "<"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   238
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Left            =   5150
         TabIndex        =   22
         ToolTipText     =   "Click to Simplify Interface"
         Top             =   120
         Width           =   255
      End
      Begin VB.PictureBox Show_Selected 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   310
         Left            =   1320
         ScaleHeight     =   285
         ScaleWidth      =   3795
         TabIndex        =   21
         ToolTipText     =   "Sample Colour"
         Top             =   140
         Width           =   3825
      End
      Begin VB.Image Image1 
         Height          =   360
         Left            =   480
         Picture         =   "Colour Picker.frx":0000
         Top             =   120
         Width           =   360
      End
   End
   Begin VB.CommandButton Apply 
      Caption         =   "&Apply"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   350
      Left            =   2400
      TabIndex        =   7
      ToolTipText     =   "Apply New Colour"
      Top             =   3120
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.Frame Palette_Frame 
      BackColor       =   &H00FFFFFF&
      Height          =   1650
      Index           =   0
      Left            =   120
      TabIndex        =   2
      Top             =   0
      Width           =   3135
      Begin VB.PictureBox Col_Palette 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   1250
         Index           =   0
         Left            =   130
         MouseIcon       =   "Colour Picker.frx":076A
         MousePointer    =   99  'Custom
         Picture         =   "Colour Picker.frx":0A74
         ScaleHeight     =   83
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   198
         TabIndex        =   3
         ToolTipText     =   "Click to pick from palette"
         Top             =   260
         Width           =   2975
      End
      Begin VB.Line Line2 
         BorderColor     =   &H8000000F&
         BorderWidth     =   15
         X1              =   0
         X2              =   3120
         Y1              =   0
         Y2              =   0
      End
   End
   Begin VB.Frame Frame2 
      Height          =   855
      Left            =   120
      TabIndex        =   6
      Top             =   1575
      Width           =   3135
      Begin VB.PictureBox B_In_Colour 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   150
         Left            =   2320
         ScaleHeight     =   120
         ScaleWidth      =   645
         TabIndex        =   19
         ToolTipText     =   "Sample Blue"
         Top             =   565
         Width           =   675
      End
      Begin VB.PictureBox G_In_Colour 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   150
         Left            =   1330
         ScaleHeight     =   120
         ScaleWidth      =   645
         TabIndex        =   18
         ToolTipText     =   "Sample Green"
         Top             =   565
         Width           =   675
      End
      Begin VB.PictureBox R_In_Colour 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   150
         Left            =   335
         ScaleHeight     =   120
         ScaleWidth      =   645
         TabIndex        =   17
         ToolTipText     =   "Sample Red"
         Top             =   565
         Width           =   675
      End
      Begin VB.VScrollBar Adjust_B 
         Height          =   315
         LargeChange     =   5
         Left            =   2750
         Max             =   0
         Min             =   255
         TabIndex        =   16
         Top             =   240
         Value           =   255
         Width           =   255
      End
      Begin VB.TextBox B_Value 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   238
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2325
         MaxLength       =   3
         TabIndex        =   14
         Text            =   "255"
         ToolTipText     =   "Blue Value"
         Top             =   240
         Width           =   400
      End
      Begin VB.VScrollBar Adjust_G 
         Height          =   315
         LargeChange     =   5
         Left            =   1755
         Max             =   0
         Min             =   255
         TabIndex        =   13
         Top             =   240
         Width           =   255
      End
      Begin VB.TextBox G_Value 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   238
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1335
         MaxLength       =   3
         TabIndex        =   11
         Text            =   "255"
         ToolTipText     =   "Green Value"
         Top             =   240
         Width           =   400
      End
      Begin VB.VScrollBar Adjust_R 
         Height          =   315
         LargeChange     =   5
         Left            =   750
         Max             =   0
         Min             =   255
         TabIndex        =   10
         Top             =   240
         Value           =   255
         Width           =   255
      End
      Begin VB.TextBox R_Value 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   238
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   325
         MaxLength       =   3
         TabIndex        =   8
         Text            =   "255"
         ToolTipText     =   "Red Value"
         Top             =   240
         Width           =   400
      End
      Begin VB.Label Label1 
         Caption         =   "B"
         Height          =   255
         Index           =   2
         Left            =   2125
         TabIndex        =   15
         Top             =   255
         Width           =   195
      End
      Begin VB.Label Label1 
         Caption         =   "G"
         Height          =   255
         Index           =   1
         Left            =   1145
         TabIndex        =   12
         Top             =   255
         Width           =   195
      End
      Begin VB.Label Label1 
         Caption         =   "R"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   9
         Top             =   250
         Width           =   195
      End
   End
   Begin VB.Frame Palette_Frame 
      BackColor       =   &H00FFFFFF&
      Height          =   2430
      Index           =   1
      Left            =   3285
      TabIndex        =   4
      Top             =   0
      Width           =   2265
      Begin VB.PictureBox Col_Palette 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   2055
         Index           =   1
         Left            =   110
         MouseIcon       =   "Colour Picker.frx":C578
         MousePointer    =   99  'Custom
         Picture         =   "Colour Picker.frx":C882
         ScaleHeight     =   137
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   137
         TabIndex        =   5
         ToolTipText     =   "Click to pick from palette"
         Top             =   225
         Width           =   2055
      End
      Begin VB.Line Line1 
         BorderColor     =   &H8000000F&
         BorderWidth     =   15
         X1              =   0
         X2              =   2280
         Y1              =   0
         Y2              =   0
      End
   End
   Begin VB.CommandButton OK 
      Caption         =   "&OK"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   350
      Left            =   4545
      TabIndex        =   1
      ToolTipText     =   "Close & Accept "
      Top             =   3060
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.CommandButton Cancel 
      Caption         =   "&Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   350
      Left            =   3480
      TabIndex        =   0
      ToolTipText     =   "Close"
      Top             =   3060
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.Menu mPopupSys 
      Caption         =   ""
      Visible         =   0   'False
   End
End
Attribute VB_Name = "Colour_Picker"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Declare Function GetPixel Lib "gdi32" (ByVal hdc As Long, ByVal X As Long, ByVal Y As Long) As Long
Private Declare Function SetWindowPos Lib "user32" (ByVal hwnd As Long, ByVal hWndInsertAfter As Long, ByVal X As Long, ByVal Y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long
Public Selected_R   As Byte
Public Selected_G   As Byte
Public Selected_B   As Byte
Public Selected_Col As Long
Private Const WM_CLOSE = &H10
Private Const HWND_BOTTOM = 1
Private Const HWND_NOTOPMOST = -2
Private Const HWND_TOP = 0
Private Const HWND_TOPMOST = -1
Private Const SWP_NOMOVE = &H2
Private Const SWP_NOSIZE = &H1

Private Sub Form_Activate()
   Set_Dialog_On_Top Me.hwnd
   Selected_R = 255
   Selected_G = 255
   Selected_B = 255
   R_In_Colour.BackColor = RGB(Selected_R, 0, 0)
   G_In_Colour.BackColor = RGB(0, Selected_G, 0)
   B_In_Colour.BackColor = RGB(0, 0, Selected_B)
End Sub

Private Sub Adjust_R_Change()
                                  Selected_R = Adjust_R.Value
   Selected_Col = RGB(Selected_R, Selected_G, Selected_B)
        Show_Selected.BackColor = Selected_Col
          R_Value.Text = Trim(Str(Selected_R))
      R_In_Colour.BackColor = RGB(Selected_R, 0, 0)

End Sub

Private Sub Adjust_G_Change()

                                  Selected_G = Adjust_G.Value
   Selected_Col = RGB(Selected_R, Selected_G, Selected_B)
          G_Value.Text = Trim(Str(Selected_G))
        Show_Selected.BackColor = Selected_Col
   G_In_Colour.BackColor = RGB(0, Selected_G, 0)
   
End Sub

Public Sub Adjust_B_Change()

                                  
                                  Selected_B = Adjust_B.Value
   Selected_Col = RGB(Selected_R, Selected_G, Selected_B)
        Show_Selected.BackColor = Selected_Col
          B_Value.Text = Trim(Str(Selected_B))
B_In_Colour.BackColor = RGB(0, 0, Selected_B)

End Sub

Private Sub R_Value_Change()
   

    
    If Val(R_Value.Text) > 255 Then
       Set_Dialog_Not_On_Top Me.hwnd
                                                    

         
       MsgBox "Invalid value. Range must be within (0-255).", vbOKOnly, "Can't Set Red"
       Set_Dialog_On_Top Me.hwnd
         
       R_Value.Text = Mid$(R_Value.Text, 1, 2)         'Keep first 2 digits
       R_Value.SelStart = Len(R_Value.Text)            'Move cursor to last
       Exit Sub                                        'Bail out
      End If
      
   '// Apply it
      
      Selected_R = Val(R_Value.Text)
                    Adjust_R.Value = Selected_R
      Selected_Col = RGB(Selected_R, Selected_G, Selected_B)
           Show_Selected.BackColor = Selected_Col
SelColDr = Selected_Col
End Sub

Private Sub R_Value_KeyPress(KeyAscii As Integer)
      
   '// Ensure numerical values only
   
   If Not ((KeyAscii >= Asc("0") And KeyAscii <= Asc("9"))) Then
      If KeyAscii <> 8 Then                            'Allow backspace
         KeyAscii = 0                                  'Force to zero
      End If                                           '(Field  will not update)
   End If

End Sub

Private Sub G_Value_Change()
   
   '// Allow user to manually key in value (check integrity of val first)
    
    If Val(G_Value.Text) > 255 Then
       Set_Dialog_Not_On_Top Me.hwnd                   'MsgBox needs foreground priority
                                                       '
   '// Prompt user with error msg
         
       MsgBox "Invalid value. Range must be within (0-255).", vbOKOnly, "Can't Set Green"
       Set_Dialog_On_Top Me.hwnd                       'Restore picker foreground priority
         
       G_Value.Text = Mid$(G_Value.Text, 1, 2)         'Keep first 2 digits
       G_Value.SelStart = Len(G_Value.Text)            'Move cursor to last
       Exit Sub                                        'Bail out
      End If
      
   '// Apply it
      
      Selected_G = Val(G_Value.Text)
                    Adjust_G.Value = Selected_G
      Selected_Col = RGB(Selected_R, Selected_G, Selected_B)
           Show_Selected.BackColor = Selected_Col
SelColDr = Selected_Col
End Sub

Private Sub G_Value_KeyPress(KeyAscii As Integer)
      
   '// Ensure numerical values only
   
   If Not ((KeyAscii >= Asc("0") And KeyAscii <= Asc("9"))) Then
      If KeyAscii <> 8 Then                            'Allow backspace
         KeyAscii = 0                                  'Force to zero
      End If                                           '(Field  will not update)
   End If

End Sub

Private Sub B_Value_Change()
   
   '// Allow user to manually key in value (check integrity of val first)
    
    If Val(B_Value.Text) > 255 Then
       Set_Dialog_Not_On_Top Me.hwnd                   'MsgBox needs foreground priority
                                                       '
   '// Prompt user with error msg
         
       MsgBox "Invalid value. Range must be within (0-255).", vbOKOnly, "Can't Set Blue"
       Set_Dialog_On_Top Me.hwnd                       'Restore picker foreground priority
         
       B_Value.Text = Mid$(R_Value.Text, 1, 2)         'Keep first 2 digits
       B_Value.SelStart = Len(B_Value.Text)            'Move cursor to last
       Exit Sub                                        'Bail out
      End If
      
   '// Apply it
      
      Selected_B = Val(B_Value.Text)
                    Adjust_B.Value = Selected_B
      Selected_Col = RGB(Selected_R, Selected_G, Selected_B)
           Show_Selected.BackColor = Selected_Col
SelColDr = Selected_Col
End Sub

Private Sub B_Value_KeyPress(KeyAscii As Integer)
      
   '// Ensure numerical values only
   
   If Not ((KeyAscii >= Asc("0") And KeyAscii <= Asc("9"))) Then
      If KeyAscii <> 8 Then                            'Allow backspace
         KeyAscii = 0                                  'Force to zero
      End If                                           '(Field  will not update)
   End If

End Sub

Private Sub Col_Palette_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
   
   '// (2 palletes)-Left button click to fetch col under cursor
   
   If Button = 1 Then
      Dim i As Long
      i = GetPixel(Col_Palette(Index).hdc, X - 10, Y)  'Sample palette
   
      If i <> -1 Then                                  'Error or unrecognizable = -1
         Show_Selected.BackColor = i                   'Show user the sample
         Convert_To_RGB (i)                            'Seperate
         Selected_Col = i
         SelColDr = i
         'Return val from picker
      End If
   End If

End Sub

Private Sub Col_Palette_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
   
   '// (2 palletes)-Left button held down constant update of col under cursor
   
   If Button = 1 Then
      Dim i As Long
      i = GetPixel(Col_Palette(Index).hdc, X - 10, Y)  'Sample palette
   
      If i <> -1 Then                                  'Error or unrecognizable = -1
         Show_Selected.BackColor = i                   'Show user the sample
         Convert_To_RGB (i)                            'Seperate
         Selected_Col = i                              'Return val from picker
      End If
   End If
   
End Sub

Public Sub Convert_To_RGB(Col_Val As Long)
       
   '// Sperate channels in col
   
   Selected_R = Col_Val Mod 256
   Selected_G = ((Col_Val And &HFF00) / 256&) Mod 256&
   Selected_B = (Col_Val And &HFF0000) / 65536
    
   '// Refresh scroll bars
   
   Adjust_R.Value = Selected_R
   Adjust_G.Value = Selected_G
   Adjust_B.Value = Selected_B
   
   '// Show samples of each channel
   
   R_In_Colour.BackColor = RGB(Selected_R, 0, 0)
   G_In_Colour.BackColor = RGB(0, Selected_G, 0)
   B_In_Colour.BackColor = RGB(0, 0, Selected_B)
   
   '// Values of channles (0-255)
   
   R_Value.Text = Trim(Str(Selected_R))
   G_Value.Text = Trim(Str(Selected_G))
   B_Value.Text = Trim(Str(Selected_B))
     
End Sub

Private Sub Simplified_UI_Click()
   
   '// Simplify / extend UI
   
   Dim i As Long
   
   Select Case Simplified_UI.Caption
   
   '// Simplified = (only fixed col palette is visible)
          
          Case "<"
               Palette_Frame(1).Visible = False
                    Colour_Picker.Width = 3465
                    Sel_Col_Frame.Width = 210
                    Show_Selected.Width = 1550
                     Simplified_UI.Left = 2865
                           ' Tip.Visible = False
                            Cancel.Left = 7
                             Apply.Left = 79
                                OK.Left = 151
                  Simplified_UI.Caption = ">"
               
   '// Restore to full sized UI
   
          Case ">"
               Palette_Frame(1).Visible = True
                    Colour_Picker.Width = 5745
                    Sel_Col_Frame.Width = 361
                    Show_Selected.Width = 3825
                     Simplified_UI.Left = 5150
                           ' Tip.Visible = True
                            Cancel.Left = 157
                             Apply.Left = 230
                                OK.Left = 303
                  Simplified_UI.Caption = "<"
   
   End Select
End Sub

Public Function Set_Picker_Colour(Set_Col As Long) As Boolean
         
   '// Use to force picker to show a col / start default col
   
   Show_Selected.BackColor = Set_Col                   'Show user the sample
   Convert_To_RGB (Set_Col)                            'Seperate
   Selected_Col = Set_Col                              'Return val from picker

End Function

Public Function Set_Picker_RGB(R As Byte, G As Byte, b As Byte) As Boolean
   
   '// Use to force picker to show a col / start default col
   
   Selected_R = R                                      'Set ret var
   Adjust_R.Value = R                                  'Scroll bar
   
   Selected_G = G                                      '^
   Adjust_G.Value = G                                  '^
    
   Selected_B = b                                      '^
   Adjust_B.Value = b                                  '^
   
   '// Show samples of each channel
   
   R_In_Colour.BackColor = RGB(Selected_R, 0, 0)       'Red
   G_In_Colour.BackColor = RGB(0, Selected_G, 0)       'Green
   B_In_Colour.BackColor = RGB(0, 0, Selected_B)       'Blue
      
   Selected_Col = RGB(R, G, b)                         'Ret var
   Show_Selected.BackColor = Selected_Col              'Preview sample
ColorPickedR = R
ColorPickedG = G
ColorPickedB = b
End Function

Public Sub Set_Dialog_On_Top(hwnd As Long)
   
   '// Dialog w/ foreground priority
   SetWindowPos hwnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE + SWP_NOSIZE
End Sub

Public Sub Set_Dialog_Not_On_Top(hwnd As Long)
   
   '// Resume w/out
   SetWindowPos hwnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE + SWP_NOSIZE
End Sub

Private Sub Cancel_Click()
   '// Insert the required code to suit application
   Me.Hide
End Sub

Private Sub OK_Click()
   '// Insert the required code to suit application
   Me.Hide
End Sub

Private Sub Form_Unload(Cancel As Integer)
   Set Colour_Picker = Nothing
End Sub
