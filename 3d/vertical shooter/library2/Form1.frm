VERSION 5.00
Object = "{08216199-47EA-11D3-9479-00AA006C473C}#2.1#0"; "rmcontrol.ocx"
Begin VB.Form Form1 
   BackColor       =   &H00000000&
   BorderStyle     =   0  'None
   Caption         =   "GAME"
   ClientHeight    =   10515
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   16110
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   701
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   1074
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.PictureBox Picture3 
      Appearance      =   0  'Flat
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      ForeColor       =   &H80000008&
      Height          =   7455
      Left            =   -720
      ScaleHeight     =   497
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   769
      TabIndex        =   39
      Top             =   8280
      Visible         =   0   'False
      Width           =   11535
      Begin VB.PictureBox Picture9 
         Appearance      =   0  'Flat
         BackColor       =   &H00800000&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   615
         Left            =   0
         ScaleHeight     =   41
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   721
         TabIndex        =   44
         Top             =   240
         Width           =   10815
         Begin VB.Image Image5 
            Height          =   495
            Left            =   9360
            Picture         =   "Form1.frx":98EA
            Stretch         =   -1  'True
            Top             =   0
            Width           =   615
         End
         Begin VB.Label Label13 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Label13"
            Height          =   195
            Left            =   8520
            TabIndex        =   47
            Top             =   120
            Width           =   570
         End
         Begin VB.Label Label12 
            Alignment       =   2  'Center
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Label12"
            Height          =   195
            Left            =   4320
            TabIndex        =   46
            Top             =   120
            Width           =   570
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Label11"
            Height          =   195
            Left            =   120
            TabIndex        =   45
            Top             =   120
            Width           =   570
         End
      End
      Begin RMControl7.RMCanvas RMCanvas1 
         Height          =   6495
         Left            =   840
         TabIndex        =   40
         Top             =   600
         Width           =   10215
         _ExtentX        =   18018
         _ExtentY        =   11456
      End
   End
   Begin VB.PictureBox Picture8 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      ForeColor       =   &H80000008&
      Height          =   2295
      Left            =   -3120
      ScaleHeight     =   2295
      ScaleWidth      =   5655
      TabIndex        =   38
      Top             =   2760
      Visible         =   0   'False
      Width           =   5655
      Begin VB.Image Image4 
         Height          =   1815
         Left            =   1200
         Picture         =   "Form1.frx":AC67
         Stretch         =   -1  'True
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
      End
      Begin VB.Image Image3 
         Enabled         =   0   'False
         Height          =   2175
         Left            =   -600
         Picture         =   "Form1.frx":D144
         Stretch         =   -1  'True
         Top             =   360
         Visible         =   0   'False
         Width           =   5655
      End
   End
   Begin VB.PictureBox Picture7 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   7215
      Left            =   2040
      ScaleHeight     =   7215
      ScaleWidth      =   9855
      TabIndex        =   36
      Top             =   3240
      Width           =   9855
      Begin VB.Timer Timer5 
         Interval        =   100
         Left            =   9360
         Top             =   2400
      End
      Begin VB.Label Label10 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackColor       =   &H00000000&
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Arial Narrow"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   435
         Index           =   4
         Left            =   5640
         TabIndex        =   48
         Top             =   2400
         Width           =   165
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         BackColor       =   &H00000000&
         BackStyle       =   0  'Transparent
         Caption         =   "HISCORE"
         BeginProperty Font 
            Name            =   "Arial Narrow"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   435
         Index           =   3
         Left            =   0
         TabIndex        =   43
         Top             =   2400
         Width           =   1305
      End
      Begin VB.Label Label10 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "START"
         BeginProperty Font 
            Name            =   "Arial Narrow"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000001&
         Height          =   615
         Index           =   0
         Left            =   0
         TabIndex        =   42
         Top             =   1200
         Width           =   6615
      End
      Begin VB.Label Label10 
         BackColor       =   &H00000000&
         BackStyle       =   0  'Transparent
         Caption         =   "EXIT"
         BeginProperty Font 
            Name            =   "ArcadeClassic"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   615
         Index           =   1
         Left            =   0
         TabIndex        =   41
         Top             =   4680
         Width           =   6735
      End
      Begin VB.Label Label10 
         BackColor       =   &H00000000&
         BackStyle       =   0  'Transparent
         Caption         =   "JOYSTICK"
         BeginProperty Font 
            Name            =   "ArcadeClassic"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   495
         Index           =   2
         Left            =   2280
         TabIndex        =   37
         Top             =   5280
         Width           =   3375
      End
   End
   Begin VB.PictureBox Picture6 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      ForeColor       =   &H80000008&
      Height          =   855
      Left            =   8400
      ScaleHeight     =   825
      ScaleWidth      =   825
      TabIndex        =   34
      Top             =   0
      Width           =   855
      Begin VB.Label Label9 
         BackStyle       =   0  'Transparent
         Caption         =   "J"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "ArcadeClassic"
            Size            =   27.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   615
         Left            =   240
         TabIndex        =   35
         Top             =   120
         Width           =   615
      End
   End
   Begin VB.PictureBox Picture4 
      Appearance      =   0  'Flat
      BackColor       =   &H00800080&
      ForeColor       =   &H80000008&
      Height          =   855
      Index           =   3
      Left            =   2880
      ScaleHeight     =   825
      ScaleWidth      =   825
      TabIndex        =   33
      Top             =   0
      Width           =   855
   End
   Begin VB.PictureBox Picture4 
      Appearance      =   0  'Flat
      BackColor       =   &H000000FF&
      ForeColor       =   &H80000008&
      Height          =   855
      Index           =   2
      Left            =   1920
      ScaleHeight     =   825
      ScaleWidth      =   825
      TabIndex        =   32
      Top             =   0
      Width           =   855
   End
   Begin VB.PictureBox Picture4 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      ForeColor       =   &H80000008&
      Height          =   855
      Index           =   1
      Left            =   960
      ScaleHeight     =   825
      ScaleWidth      =   825
      TabIndex        =   31
      Top             =   0
      Width           =   855
   End
   Begin VB.PictureBox Picture4 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FF00&
      ForeColor       =   &H80000008&
      Height          =   855
      Index           =   0
      Left            =   0
      ScaleHeight     =   825
      ScaleWidth      =   825
      TabIndex        =   30
      Top             =   0
      Width           =   855
   End
   Begin VB.PictureBox Picture5 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      ForeColor       =   &H80000008&
      Height          =   7335
      Left            =   12240
      ScaleHeight     =   7335
      ScaleWidth      =   13095
      TabIndex        =   29
      Top             =   360
      Visible         =   0   'False
      Width           =   13095
      Begin VB.Timer Timer4 
         Enabled         =   0   'False
         Interval        =   3500
         Left            =   1680
         Top             =   3480
      End
      Begin VB.Timer Timer3 
         Enabled         =   0   'False
         Interval        =   2
         Left            =   2760
         Top             =   2160
      End
      Begin VB.Image Image2 
         Enabled         =   0   'False
         Height          =   615
         Left            =   6840
         Picture         =   "Form1.frx":EA8D
         Stretch         =   -1  'True
         Top             =   4200
         Width           =   4005
      End
   End
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      Enabled         =   0   'False
      ForeColor       =   &H80000008&
      Height          =   3495
      Left            =   7200
      ScaleHeight     =   3465
      ScaleWidth      =   8025
      TabIndex        =   7
      Top             =   8640
      Visible         =   0   'False
      Width           =   8055
      Begin VB.TextBox Text1 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3120
         TabIndex        =   21
         Text            =   "p"
         Top             =   1920
         Width           =   615
      End
      Begin VB.TextBox Text2 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2400
         TabIndex        =   20
         Text            =   "a"
         Top             =   2250
         Width           =   615
      End
      Begin VB.TextBox Text3 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1680
         TabIndex        =   19
         Text            =   "o"
         Top             =   1920
         Width           =   615
      End
      Begin VB.TextBox Text4 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2400
         TabIndex        =   18
         Text            =   "q"
         Top             =   1440
         Width           =   615
      End
      Begin VB.TextBox Text5 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   5640
         TabIndex        =   17
         Text            =   "1"
         Top             =   1440
         Width           =   615
      End
      Begin VB.TextBox Text6 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   5040
         TabIndex        =   16
         Text            =   "2"
         Top             =   1080
         Width           =   615
      End
      Begin VB.TextBox Text7 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   6120
         TabIndex        =   15
         Text            =   "3"
         Top             =   1080
         Width           =   615
      End
      Begin VB.TextBox Text8 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   5640
         TabIndex        =   14
         Text            =   "4"
         Top             =   720
         Width           =   615
      End
      Begin VB.TextBox Text9 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   4200
         TabIndex        =   13
         Text            =   "Text9"
         Top             =   1080
         Width           =   615
      End
      Begin VB.TextBox Text10 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   285
         Left            =   2640
         TabIndex        =   12
         Text            =   "Text10"
         Top             =   1080
         Width           =   615
      End
      Begin VB.TextBox Text11 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   285
         Left            =   1200
         TabIndex        =   11
         Text            =   "Text11"
         Top             =   240
         Width           =   735
      End
      Begin VB.TextBox Text12 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   285
         Left            =   2040
         TabIndex        =   10
         Text            =   "Text12"
         Top             =   240
         Width           =   735
      End
      Begin VB.TextBox Text13 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   285
         Left            =   4680
         TabIndex        =   9
         Text            =   "Text13"
         Top             =   240
         Width           =   735
      End
      Begin VB.TextBox Text14 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   285
         Left            =   5520
         TabIndex        =   8
         Text            =   "Text14"
         Top             =   240
         Width           =   735
      End
      Begin VB.Image Image1 
         Height          =   3735
         Left            =   -240
         Picture         =   "Form1.frx":22DB7
         Stretch         =   -1  'True
         Top             =   0
         Width           =   8295
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "SELECT"
         Height          =   255
         Left            =   2640
         TabIndex        =   28
         Top             =   840
         Width           =   1095
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "START"
         Height          =   255
         Left            =   4200
         TabIndex        =   27
         Top             =   840
         Width           =   615
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "DIRECTIONS"
         Height          =   255
         Left            =   1320
         TabIndex        =   26
         Top             =   2280
         Width           =   975
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "BUTTONS 1-4"
         Height          =   255
         Left            =   5400
         TabIndex        =   25
         Top             =   1800
         Width           =   1695
      End
      Begin VB.Label Label6 
         BackStyle       =   0  'Transparent
         Caption         =   "BUTTONS RIGHT 1, 2"
         Enabled         =   0   'False
         Height          =   615
         Left            =   6360
         TabIndex        =   24
         Top             =   0
         Width           =   1455
      End
      Begin VB.Label Label7 
         Caption         =   "BUTTONS LEFT 1, 2"
         Enabled         =   0   'False
         Height          =   495
         Left            =   2880
         TabIndex        =   23
         Top             =   0
         Width           =   1335
      End
      Begin VB.Label Label8 
         Caption         =   "ASCII KEYS"
         Enabled         =   0   'False
         Height          =   735
         Left            =   0
         TabIndex        =   22
         Top             =   0
         Width           =   975
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Enabled         =   0   'False
      Height          =   255
      Index           =   3
      Left            =   3480
      TabIndex        =   6
      Top             =   7200
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Enabled         =   0   'False
      Height          =   255
      Index           =   2
      Left            =   2400
      TabIndex        =   5
      Top             =   7200
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   255
      Index           =   1
      Left            =   1440
      TabIndex        =   4
      Top             =   7200
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Enabled         =   0   'False
      Height          =   255
      Index           =   0
      Left            =   240
      TabIndex        =   3
      Top             =   7200
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Timer Timer2 
      Interval        =   1
      Left            =   120
      Top             =   6000
   End
   Begin VB.CheckBox Check1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000000&
      Caption         =   "Off - Edit - keys\ On - Save - joy"
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   120
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   1200
      Value           =   1  'Checked
      Width           =   3255
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H0080FFFF&
      BorderStyle     =   0  'None
      FillStyle       =   0  'Solid
      ForeColor       =   &H80000008&
      Height          =   2055
      Left            =   9960
      ScaleHeight     =   137
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   161
      TabIndex        =   0
      Top             =   480
      Width           =   2415
   End
   Begin VB.Timer Timer1 
      Interval        =   1
      Left            =   3840
      Top             =   1200
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Enabled         =   0   'False
      Height          =   2055
      Left            =   4920
      TabIndex        =   1
      Top             =   360
      Visible         =   0   'False
      Width           =   2175
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'MS_st3(LoopA).SetTexture TXENEMY_st3
'
Dim HitLight As Boolean
Dim HitLimit
Dim ExplLoaded  As Boolean
Dim HitLoaded As Boolean

Dim ShipHit As Boolean
'^Dim ExplLoaded  As Boolean
Dim SCOREH As Long
Dim ShipSpeed
Dim timeToStart As Long
Dim IloscPlatform

Dim Playing As Boolean
Dim SCORE As Long
Dim lIvEs As Integer
Dim Area As Integer
Dim TimerG1 As Long
Dim TimerG2 As Long
Dim TimerG3 As Long


Dim lastShot As Long
Dim AttackPressed As Boolean

Dim REDoff As Integer
'(;)Debug.Print "Option Explicit"



Dim arrowleftpres As Boolean
Dim arrowrightpres As Boolean
Dim arrowuppres As Boolean
Dim arrowdownpres As Boolean


Dim Mesh_EXPL(20, 2) As Direct3DRMMeshBuilder3
Dim Frame_EXPL(20, 2) As Direct3DRMFrame3
Dim TEXTURE_EXPL(20, 2) As Direct3DRMTexture3
Dim EXPLv(20) As D3DVECTOR
Dim eXPLb(20) As Boolean
Dim explTime(20) As Integer

Dim Mesh_SPHERE As Direct3DRMMeshBuilder3
Dim Frame_SPHERE As Direct3DRMFrame3
Dim TEXTURE_SPHERE(3) As Direct3DRMTexture3



Dim vertShot As Long

Dim Idle As Long
Dim Fade As Long

Dim green As Boolean

Dim GunEnabled5 As Boolean


Private Declare Function mciSendString Lib "winmm.dll" Alias "mciSendStringA" (ByVal lpstrCommand As String, ByVal lpstrReturnString As String, ByVal uReturnLength As Long, ByVal hwndCallback As Long) As Long
Dim retVal As Long
'
Dim ByKeyb As Boolean
'
Dim JoyEnabled As Boolean
Dim HOVERINGX As Double 'WAVES VARIABLES
Dim HOVERINGY As Double 'WAVES VARIABLES


Dim bCKgRN(2) As Direct3DRMTexture3



Dim TXENEMY_st3 As Direct3DRMTexture3
Dim TXENEMY_st3a As Direct3DRMTexture3





'ship
Dim ShipPaint
Dim Mesh_Ship As Direct3DRMMeshBuilder3
Dim Frame_Ship As Direct3DRMFrame3
Dim TEXTURE_Ship(3) As Direct3DRMTexture3
Dim ShipVector As D3DVECTOR


'daytimelight\laser
Dim SunVector As D3DVECTOR
Dim SunRGBVector As D3DVECTOR
Dim lightLoop1
Dim lightBol As Boolean

'shot
Dim PlayGun As Boolean
Dim Shotfrq
Dim rounds
Dim firepresed As Boolean
Dim Shoting(50) As Long
Dim FR_sho(50) As Direct3DRMFrame3
Dim MS_sho(50) As Direct3DRMMeshBuilder3
Dim VC_sho(50) As D3DVECTOR
Dim ShotB(50) As Boolean
Dim FIRED(50) As Boolean
'stars
Dim FR_st1(50) As Direct3DRMFrame3
Dim MS_st1(50) As Direct3DRMMeshBuilder3
'Dim TX_BS1(100) As Direct3DRMTexture3
Dim VC_st1(50) As D3DVECTOR

Dim FR_st2(50) As Direct3DRMFrame3
Dim MS_st2(50) As Direct3DRMMeshBuilder3
'Dim TX_BS1(100) As Direct3DRMTexture3
Dim VC_st2(50) As D3DVECTOR

Dim LoopA
'nebulas-platforms or enemys
Dim FR_st3(150) As Direct3DRMFrame3
Dim MS_st3(150) As Direct3DRMMeshBuilder3
Dim TX_st3(150) As Direct3DRMTexture3
Dim VC_st3(150) As D3DVECTOR
Dim EXPLODED(150) As Boolean
Dim toEXPLODE(150) As Integer
'enemys?
Dim FR_TURRETS3(10) As Direct3DRMFrame3
Dim MS_TURRETS3(10) As Direct3DRMMeshBuilder3
Dim TX_TURRETS3(10) As Direct3DRMTexture3
Dim VC_TURRETS3(10) As D3DVECTOR



Dim KEYpadkeys(14)
Const KEYEVENTF_EXTENDEDKEY = &H1
Const KEYEVENTF_KEYUP = &H2
Private Declare Sub keybd_event Lib "user32.dll" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Long, ByVal dwExtraInfo As Long)

Dim keypressed(255) As Boolean

Private Const JOY_RETURNBUTTONS As Long = &H80&
Private Const JOY_RETURNCENTERED As Long = &H400&
Private Const JOY_RETURNPOV As Long = &H40&
Private Const JOY_RETURNPOVCTS As Long = &H200&
Private Const JOY_RETURNR As Long = &H8&
Private Const JOY_RETURNRAWDATA As Long = &H100&
Private Const JOY_RETURNU As Long = &H10
Private Const JOY_RETURNV As Long = &H20
Private Const JOY_RETURNX As Long = &H1&
Private Const JOY_RETURNY As Long = &H2&
Private Const JOY_RETURNZ As Long = &H4&
Private Const JOY_RETURNALL As Long = (JOY_RETURNX Or JOY_RETURNY Or JOY_RETURNZ Or JOY_RETURNR Or JOY_RETURNU Or JOY_RETURNV Or JOY_RETURNPOV Or JOY_RETURNBUTTONS)

Private Type JOYINFOEX
    dwSize As Long ' size of structure
    dwFlags As Long ' flags to dicate what to return
    dwXpos As Long ' x position
    dwYpos As Long ' y position
    dwZpos As Long ' z position
    dwRpos As Long ' rudder/4th axis position
    dwUpos As Long ' 5th axis position
    dwVpos As Long ' 6th axis position
    dwButtons As Long ' button states
    dwButtonNumber As Long ' current button number pressed
    dwPOV As Long ' point of view state
    dwReserved1 As Long ' reserved for communication between winmm driver
    dwReserved2 As Long ' reserved for future expansion
End Type

Private Declare Function joyGetPosEx Lib "winmm.dll" (ByVal uJoyID As Long, ByRef pji As JOYINFOEX) As Long

Dim JI As JOYINFOEX

Const JNum As Long = 0



Private Declare Function AddFontResourceEx Lib "gdi32.dll" _
    Alias "AddFontResourceExA" (ByVal lpcstr As String, _
    ByVal dword As Long, ByRef DESIGNVECTOR) As Long
Private Const FR_PRIVATE As Long = &H10





'Private Declare Function AddFontResource Lib "gdi32" Alias "AddFontResourceA" (ByVal lpFileName As String) As Long
'Private Declare Function RemoveFontResource Lib "gdi32" Alias "RemoveFontResourceA" (ByVal lpFileName As String) As Long
'Dim AppPath As String



'Set this to the number of the joystick that
'you want to read (a value between 0 and 15).
'The first joystick plugged in is number 0.
'The API for reading joysticks supports up to
'16 simultaniously plugged in joysticks.
'Change this Const to a Dim if you want to set
'it at runtime.
Private Sub SetFonts()

'Install_TTF "Games", "Games.ttf", App.Path + "\library2\Font\"


'AddFontResourceEx App.Path + Trim("\library2\Font\Games.TTF"), FR_PRIVATE, 0&
'AddFontResourceEx App.Path + Trim("\library2\Font\StarForce.ttf"), FR_PRIVATE, 0&

'    AppPath = App.Path + "\library2\Font\"
    'If Right$(AppPath, 1) <> "\" Then AppPath = AppPath + "\"
    'Add the font to the Windows Font Table
'    AddFontResource AppPath + "Games.ttf"
    'Write something on the form
''    Me.AutoRedraw = True
''    Me.FontName = "myfont"
''    Me.Print "This is a test!"
Dim retvalue As Long
'Command1.Caption = "uninstall"
retvalue = AddFontResource(App.Path + "\Font\atari full.ttf")



Label10(0).Font = "Atari Font Full Version"
Label10(0).Font.Size = 18
Label10(0).Font.Italic = False
Label10(0).Font.Bold = False
Label10(0).Font.Underline = False
Label10(0).Font.Strikethrough = False
Label10(0).ForeColor = vbWhite ' RGB(50, 50, 50)
Label10(0).Caption = "PUSH START BUTTON"



Label10(3).Font = "Atari Font Full Version"
Label10(3).Font.Size = 18
Label10(3).Font.Italic = False
Label10(3).Font.Bold = False
Label10(3).Font.Underline = False
Label10(3).Font.Strikethrough = False
Label10(3).ForeColor = vbWhite ' RGB(50, 50, 50)
Label10(3).Caption = "HISCORE"
Label10(4).Font = "Atari Font Full Version"
Label10(4).Font.Size = 18
Label10(4).Font.Italic = False
Label10(4).Font.Bold = False
Label10(4).Font.Underline = False
Label10(4).Font.Strikethrough = False
Label10(4).ForeColor = vbWhite ' RGB(50, 50, 50)
Label10(4).Caption = "0"


Label10(1).Font = "Atari Font Full Version"
Label10(1).Font.Size = 18
Label10(1).Font.Italic = False
Label10(1).Font.Bold = False
Label10(1).Font.Underline = False
Label10(1).Font.Strikethrough = False
Label10(1).ForeColor = vbWhite ' RGB(50, 50, 50)
Label10(1).Caption = "END"



Label11.Font = "Atari Font Full Version"
Label11.Font.Size = 18
Label11.Font.Italic = False
Label11.Font.Bold = False
Label11.Font.Underline = False
Label11.Font.Strikethrough = False
Label11.ForeColor = vbWhite


Label12.Font = "Atari Font Full Version"
Label12.Font.Size = 18
Label12.Font.Italic = False
Label12.Font.Bold = False
Label12.Font.Underline = False
Label12.Font.Strikethrough = False
Label12.ForeColor = vbWhite

Label13.Font = "Atari Font Full Version"
Label13.Font.Size = 18
Label13.Font.Italic = False
Label13.Font.Bold = False
Label13.Font.Underline = False
Label13.Font.Strikethrough = False
Label13.ForeColor = vbWhite

'Label14.Font = "Atari Font Full Version"
'Label14.Font.Size = 18
'Label14.Font.Italic = False
'Label14.Font.Bold = False
'Label14.Font.Underline = False
'Label14.Font.Strikethrough = False
'Label14.ForeColor = vbWhite
End Sub


Private Sub Check1_Click()
SAVEcfg
allkeysUP

If Check1.Value = 1 Then
Picture2.Enabled = False
Picture2.Visible = False
Picture1.SetFocus
Else
If joyGetPosEx(JNum, JI) = 0 Then
Picture2.Enabled = True
Picture2.Visible = True
Picture1.SetFocus
Else
Picture2.Enabled = False
Picture2.Visible = False
Picture1.SetFocus
End If
End If

Picture1.SetFocus
'Picture6_Click
End Sub

Private Sub Command1_Click(Index As Integer)
Mesh_Ship.SetTexture TEXTURE_Ship(Index)
Picture1.SetFocus
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
keypressed(KeyCode) = True
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
keypressed(KeyCode) = False
End Sub
Private Sub SAVEcfg()
Dim freFILE
freFILE = FreeFile
Open App.Path + "\KeyPad.cfg" For Random As freFILE

If Text1.Text <> "" Then Put freFILE, 1, Text1.Text
If Text2.Text <> "" Then Put freFILE, 2, Text2.Text
If Text3.Text <> "" Then Put freFILE, 3, Text3.Text
If Text4.Text <> "" Then Put freFILE, 4, Text4.Text
If Text5.Text <> "" Then Put freFILE, 5, Text5.Text
If Text6.Text <> "" Then Put freFILE, 6, Text6.Text
If Text7.Text <> "" Then Put freFILE, 7, Text7.Text
If Text8.Text <> "" Then Put freFILE, 8, Text8.Text

If Text9.Text <> "" Then Put freFILE, 9, Text9.Text
If Text10.Text <> "" Then Put freFILE, 10, Text10.Text
If Text11.Text <> "" Then Put freFILE, 11, Text11.Text
If Text12.Text <> "" Then Put freFILE, 12, Text12.Text
If Text13.Text <> "" Then Put freFILE, 13, Text13.Text
If Text14.Text <> "" Then Put freFILE, 14, Text14.Text



Close freFILE
End Sub
Private Sub Form_Load()

HitLimit = 8
IloscPlatform = 50
ShipSpeed = 11
SCORE = 0
lIvEs = 3
Area = 1
TimerG1 = 0
TimerG2 = 0
TimerG3 = 0




Debug.Print "-------------------------------------"
Debug.Print "Start"
Debug.Print "\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"

Play
Play2
firepresed = False
keypressed(17) = False


Picture1.Top = -1000
Check1.Top = -1000

Picture6.Top = -1000

Label10(2).Top = -1000 'Label10(2).Top + 340'Gamepad
Picture2.Top = -1000


Label10(0).Top = Label10(0).Top - 150
'Label10(1).Top = -1000
'Label10(0).Top = -1000
'Label10(3).Top = -1000
Picture4(0).Top = -1000
Picture4(1).Top = -1000
Picture4(2).Top = -1000
Picture4(3).Top = -1000


'Picture6_Click
Idle = 1000






Text9.Text = " "
Text10.Text = " "
Text11.Text = " "
Text12.Text = " "
Text13.Text = " "
Text14.Text = " "




KEYpadkeys(1) = " "
KEYpadkeys(2) = " "
KEYpadkeys(3) = " "
KEYpadkeys(4) = " "
KEYpadkeys(5) = " "
KEYpadkeys(6) = " "
KEYpadkeys(7) = " "
KEYpadkeys(8) = " "

KEYpadkeys(9) = " "
KEYpadkeys(10) = " "
KEYpadkeys(11) = " "
KEYpadkeys(12) = " "
KEYpadkeys(13) = " "
KEYpadkeys(14) = " "




Dim VrL As String
Dim freFILE
freFILE = FreeFile
Open App.Path + "\KeyPad.cfg" For Random As freFILE
On Error Resume Next
Get freFILE, 1, VrL
Text1.Text = VrL
Get freFILE, 2, VrL
Text2.Text = VrL
Get freFILE, 3, VrL
Text3.Text = VrL
Get freFILE, 4, VrL
Text4.Text = VrL
Get freFILE, 5, VrL
Text5.Text = VrL
Get freFILE, 6, VrL
Text6.Text = VrL
Get freFILE, 7, VrL
Text7.Text = VrL
Get freFILE, 8, VrL
Text8.Text = VrL

Get freFILE, 9, VrL
Text9.Text = VrL
Get freFILE, 10, VrL
Text10.Text = VrL
Get freFILE, 11, VrL
Text11.Text = VrL
Get freFILE, 12, VrL
Text12.Text = VrL
Get freFILE, 13, VrL
Text13.Text = VrL
Get freFILE, 14, VrL
Text14.Text = VrL


Close freFILE

If Text1.Text <> "" Then KEYpadkeys(1) = Chr$(Text1.Text)
If Text2.Text <> "" Then KEYpadkeys(2) = Chr$(Text2.Text)
If Text3.Text <> "" Then KEYpadkeys(3) = Chr$(Text3.Text)
If Text4.Text <> "" Then KEYpadkeys(4) = Chr$(Text4.Text)
If Text5.Text <> "" Then KEYpadkeys(5) = Chr$(Text5.Text)
If Text6.Text <> "" Then KEYpadkeys(6) = Chr$(Text6.Text)
If Text7.Text <> "" Then KEYpadkeys(7) = Chr$(Text7.Text)
If Text8.Text <> "" Then KEYpadkeys(8) = Chr$(Text8.Text)

If Text9.Text <> "" Then KEYpadkeys(9) = Chr$(Text9.Text)
If Text10.Text <> "" Then KEYpadkeys(10) = Chr$(Text10.Text)
If Text11.Text <> "" Then KEYpadkeys(11) = Chr$(Text11.Text)
If Text12.Text <> "" Then KEYpadkeys(12) = Chr$(Text12.Text)
If Text13.Text <> "" Then KEYpadkeys(13) = Chr$(Text13.Text)
If Text14.Text <> "" Then KEYpadkeys(14) = Chr$(Text14.Text)



JI.dwSize = Len(JI)
JI.dwFlags = JOY_RETURNALL




SetFonts



End Sub
Private Sub allkeysUP()
If Label9.Caption = "J" Then
keybd_event Asc(UCase(KEYpadkeys(1))), 0, KEYEVENTF_KEYUP, 0  ' release
keybd_event Asc(UCase(KEYpadkeys(2))), 0, KEYEVENTF_KEYUP, 0  ' release
keybd_event Asc(UCase(KEYpadkeys(3))), 0, KEYEVENTF_KEYUP, 0  ' release
keybd_event Asc(UCase(KEYpadkeys(4))), 0, KEYEVENTF_KEYUP, 0  ' release
keybd_event Asc(UCase(KEYpadkeys(5))), 0, KEYEVENTF_KEYUP, 0  ' release
keybd_event Asc(UCase(KEYpadkeys(6))), 0, KEYEVENTF_KEYUP, 0  ' release
keybd_event Asc(UCase(KEYpadkeys(7))), 0, KEYEVENTF_KEYUP, 0  ' release
keybd_event Asc(UCase(KEYpadkeys(8))), 0, KEYEVENTF_KEYUP, 0  ' release

keybd_event Asc(UCase(KEYpadkeys(9))), 0, KEYEVENTF_KEYUP, 0  ' release
keybd_event Asc(UCase(KEYpadkeys(10))), 0, KEYEVENTF_KEYUP, 0  ' release
keybd_event Asc(UCase(KEYpadkeys(11))), 0, KEYEVENTF_KEYUP, 0  ' release
keybd_event Asc(UCase(KEYpadkeys(12))), 0, KEYEVENTF_KEYUP, 0  ' release
keybd_event Asc(UCase(KEYpadkeys(13))), 0, KEYEVENTF_KEYUP, 0  ' release
keybd_event Asc(UCase(KEYpadkeys(14))), 0, KEYEVENTF_KEYUP, 0  ' release


End If

End Sub

Private Sub Form_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

'Picture3.Visible = True
'Picture6.Visible = False


'Picture2.Visible = False

'Picture6.Visible = True
'Label10(0).Visible = True
'Label10(1).Visible = True

End Sub

Private Sub Form_Resize()

allkeysUP
Picture3.Top = 0: Picture3.Left = 0: Picture3.Width = Me.ScaleWidth: Picture3.Height = Me.ScaleHeight
RMCanvas1.Top = 0: RMCanvas1.Left = 0: RMCanvas1.Width = Picture3.Width: RMCanvas1.Height = Picture3.Height
Picture5.Top = 0: Picture5.Left = 0: Picture5.Width = Picture3.Width: Picture5.Height = Picture3.Height
Picture9.Top = 0: Picture9.Left = 0: Picture9.Width = Picture3.Width: Picture9.Height = 35


Label11.Left = 18
Label12.Left = Picture9.ScaleWidth \ 2 - Label12.Width \ 2
Label13.Left = Picture9.ScaleWidth - 90


InitCanva
'new\load
InitShip
InitStars
'Initlaser
RMCanvas1.AmbientLight.SetColorRGB 0.6, 0.6, 0.6
Picture4_Click (3)
'Picture6_Click



Picture7.Left = Me.ScaleWidth \ 2 - 220
Picture7.Refresh

End Sub

Private Sub Form_Terminate()
'Debug.Print "Form_Unload(Unload Succesfull)"
retVal = mciSendString("close songNumber1", vbNullString, 0, 0)
retVal = mciSendString("close songNumber2", vbNullString, 0, 0)
End Sub

Private Sub Form_Unload(Cancel As Integer)
retVal = mciSendString("close songNumber1", vbNullString, 0, 0)
retVal = mciSendString("close songNumber2", vbNullString, 0, 0)
allkeysUP

End Sub

Private Sub Image2_Click()
Picture3.Visible = True
Picture1.SetFocus
End Sub
Private Sub PrintLine()
'SCORE = 0
'lIvEs = 3
'Area = 1
'TimerG1 = 0
'TimerG2 = 0
'TimerG3 = 0
Label13.Caption = Trim(Str(lIvEs)) + "X"

Label11.Caption = "AREA " + Trim(Str(Area))
Label11.Left = 18


Label12.Caption = Trim(Str(SCORE))
Label12.Left = Picture9.ScaleWidth \ 2 - Label12.Width \ 2
Label13.Left = Picture9.ScaleWidth - 90
Image5.Left = Label13.Left + Label13.Width


End Sub

Private Sub Label10_Click(Index As Integer)



If Index = 0 And Picture3.Visible = False Then
timeToStart = 1
StartSound
ShipHit = False
Dim s
Dim a
Dim b
Dim c



For s = 0 To 50
a = -2500 + Rnd * 5000
b = (-1600 + Rnd * 3200) + 2500
c = 1783

'pozPocz wrogow?
FR_st3(s).SetPosition Nothing, a, b, c
Next


Frame_SPHERE.SetOrientation Nothing, 1, -0.5, 1, -1, 1, -1
Frame_SPHERE.SetRotation Frame_SPHERE, -1, 0, 0, 0
ShipVector.x = 0
ShipVector.y = -400
ShipVector.z = 1783
Frame_Ship.SetPosition Nothing, ShipVector.x, ShipVector.y, ShipVector.z
RMCanvas1.CameraFrame.SetPosition Nothing, ShipVector.x \ 3, 0, 0


SCORE = 0
lIvEs = 3
Area = 1
TimerG1 = 0
TimerG2 = 0
TimerG3 = 0

PrintLine



Picture2.Enabled = False
'Label10(0).Enabled = False
'Label10(1).Enabled = False
'Label10(0).Visible = False
'Label10(1).Visible = False
Picture7.Visible = False




Picture3.Visible = True






End If
If Index = 1 Then
'retVal = mciSendString("close songNumber1", vbNullString, 0, 0)
'retVal = mciSendString("close songNumber2", vbNullString, 0, 0)
retVal = mciSendString("close songNumber1", vbNullString, 0, 0)
'retVal = mciSendString("close songNumber2", vbNullString, 0, 0)
'retVal = mciSendString("close songNumber3", vbNullString, 0, 0)




ExitSub
End If

If Index = 2 Then
Picture6_Click

If Label9.Caption = "K" Then
Label10(2).Caption = "BIND (ADMIN)"
Else

Label10(2).Caption = "JOYSTICK"
End If



End If


End Sub

Private Sub Label2_Click()
'Dim res
'res = Shell("explorer.exe " & App.Path + "\donate_ paypal_USD.htm", vbNormalFocus)
End Sub

Private Sub Picture1_KeyDown(KeyCode As Integer, Shift As Integer)



If KeyCode = 13 And (Playing = False And timeToStart = 0) Then

Label10_Click (0)
End If


Dim Keyb As Boolean
If KeyCode = (17) And Playing Then
Command1_Click (3)

'AttackPressed = True




ChangeMainLight

'Idle = 200
If ShipHit = False Then mciSendString "setaudio songNumber2 volume to " & 230, vbNullString, 0, 0
If green = False Then
green = True
'Picture4_Click (2)


End If

firepresed = True
'Timer4.Enabled = True

'Timer4.Enabled = True


keypressed(17) = True
firepresed = True
If PlayGun = False Then
PlayGun = True
'Play2

End If
'keypressed(17) = False
Else


'mciSendString "setaudio songNumber2 volume to " & 0, vbNullString, 0, 0



'End If




End If

If KeyCode = 38 Or KeyCode = 40 Or KeyCode = 39 Or KeyCode = 37 And Label9.Caption = "K" Then Keyb = True


If ByKeyb = False And Label9.Caption = "J" Or KeyCode = 27 Or Keyb Then
'Check1.Value = 0
keypressed(KeyCode) = True
'End If
End If

'If KeyCode = 27 Then endProcedure
End Sub

Private Sub Picture1_KeyPress(KeyAscii As Integer)
If KeyAscii = (17) And Playing Then
AttackPressed = True
If ShipHit = False Then mciSendString "setaudio songNumber2 volume to " & 230, vbNullString, 0, 0
firepresed = True
'Timer4.Enabled = True
'Play2
'Timer4.Enabled = True


keypressed(17) = True

End If
End Sub

Private Sub Picture1_KeyUp(KeyCode As Integer, Shift As Integer)



If KeyCode = 17 Or KeyCode = 16 Then

On Error Resume Next
MS_st3(LoopA).SetTexture TXENEMY_st3

RMCanvas1.AmbientLight.SetColorRGB 1, 1, 1
AttackPressed = False
End If


keypressed(KeyCode) = False
Idle = 200


If KeyCode <> 17 Then

'keypressed(17) = False

Else

Command1_Click (0)




















'keypressed(17) = True















Fade = 150

If green = True Then
green = False

'Picture4(0).Visible = True
'Picture4_Click (0)
'Picture4(0).Visible = False

End If

End If




End Sub








Private Sub Picture3_KeyDown(KeyCode As Integer, Shift As Integer)
'Picture1_KeyDown
End Sub

Private Sub Picture3_KeyUp(KeyCode As Integer, Shift As Integer)
'Picture1_KeyUp
End Sub

Private Sub Picture4_Click(Index As Integer)
Command1_Click (Index)
REDoff = 1100
End Sub

Private Sub Picture6_Click()
If Label9.Caption = "J" Then

Check1.Value = 0
Label9.Caption = "K"
Else

Check1.Value = 1

Label9.Caption = "J"
End If
Picture1.SetFocus
End Sub

Private Sub Text1_KeyUp(KeyCode As Integer, Shift As Integer)
Text1.Text = KeyCode
KEYpadkeys(1) = Chr$(Text1.Text)
Text1.Text = chrto(Text1.Text)
End Sub

Private Sub Text15_Change()
'If Text15.Text <> "0" And Text15.Text <> "1" And Text15.Text <> "2" And Text15.Text <> "3" Then Text15.Text = "0"
End Sub

Private Sub Text5_KeyUp(KeyCode As Integer, Shift As Integer)
Text5.Text = KeyCode
KEYpadkeys(5) = Chr$(Text5.Text)
Text5.Text = chrto(Text5.Text)
End Sub
Private Sub Text2_KeyUp(KeyCode As Integer, Shift As Integer)
Text2.Text = KeyCode
KEYpadkeys(2) = Chr$(Text2.Text)
Text2.Text = chrto(Text2.Text)
End Sub
Private Sub Text3_KeyUp(KeyCode As Integer, Shift As Integer)
Text3.Text = KeyCode
KEYpadkeys(3) = Chr$(Text3.Text)
Text3.Text = chrto(Text3.Text)
End Sub
Private Sub Text4_KeyUp(KeyCode As Integer, Shift As Integer)
Text4.Text = KeyCode
KEYpadkeys(4) = Chr$(Text4.Text)
Text4.Text = chrto(Text4.Text)
End Sub
Private Sub Text6_KeyUp(KeyCode As Integer, Shift As Integer)
Text6.Text = KeyCode
KEYpadkeys(6) = Chr$(Text6.Text)
Text6.Text = chrto(Text6.Text)
End Sub
Private Sub Text7_KeyUp(KeyCode As Integer, Shift As Integer)
Text7.Text = KeyCode
KEYpadkeys(7) = Chr$(Text7.Text)
Text7.Text = chrto(Text7.Text)
End Sub
Private Sub Text8_KeyUp(KeyCode As Integer, Shift As Integer)
Text8.Text = KeyCode
KEYpadkeys(8) = Chr$(Text8.Text)
Text8.Text = chrto(Text8.Text)
End Sub
Private Sub Text9_KeyUp(KeyCode As Integer, Shift As Integer)
Text9.Text = KeyCode
KEYpadkeys(9) = Chr$(Text9.Text)
Text9.Text = chrto(Text9.Text)
End Sub
Private Sub Text10_KeyUp(KeyCode As Integer, Shift As Integer)
Text10.Text = KeyCode
KEYpadkeys(10) = Chr$(Text10.Text)
Text10.Text = chrto(Text10.Text)
End Sub
Private Sub Text11_KeyUp(KeyCode As Integer, Shift As Integer)
Text11.Text = KeyCode
KEYpadkeys(11) = Chr$(Text11.Text)
Text11.Text = chrto(Text11.Text)
End Sub
Private Sub Text12_KeyUp(KeyCode As Integer, Shift As Integer)
Text12.Text = KeyCode
KEYpadkeys(12) = Chr$(Text12.Text)
Text12.Text = chrto(Text12.Text)
End Sub
Private Sub Text13_KeyUp(KeyCode As Integer, Shift As Integer)
Text13.Text = KeyCode
KEYpadkeys(13) = Chr$(Text13.Text)
Text13.Text = chrto(Text13.Text)
End Sub
Private Sub Text14_KeyUp(KeyCode As Integer, Shift As Integer)
Text14.Text = KeyCode
KEYpadkeys(14) = Chr$(Text14.Text)
Text14.Text = chrto(Text14.Text)
End Sub









Private Sub Timer1_Timer()

If timeToStart > 0 Then
timeToStart = timeToStart + 1

'Dim returnData As String '= Space(128)
'returnData = Space(128)
 'Dim lngReturn As Long       '   Hold the value returned by the mciSendString
 'Dim intLength As Integer
 Dim strPosition As String * 255
'retVal = mciSendString("status songNumber3 mode", returnData, 128, 0)
 retVal = mciSendString("Status songNumber3 mode", strPosition, 255, 0)
 'intLength = InStr(strPosition, Chr(0))
 'position = Val(Left(strPosition, intLength - 1))


If Left$(strPosition, 7) = "stopped" Then
timeToStart = 0
Frame_SPHERE.SetRotation Frame_SPHERE, -1, 0, 0, 0.001
Playing = True
Play
retVal = mciSendString("play songNumber1 repeat", vbNullString, 0, 0)
End If

End If
'REDoff =1100
If REDoff < 1100 Then
If REDoff < 1000 Then REDoff = REDoff + 1
If REDoff = 1098 Then

'Picture4_Click 3
REDoff = REDoff + 1
End If
End If

If Fade > -1000 Then
Fade = -1000
'If Fade < -1000 Then

'Picture4_Click (0)
'End If

End If

If GunEnabled5 = False Or ShipHit = True Then
mciSendString "setaudio songNumber2 volume to " & 0, vbNullString, 0, 0


RMCanvas1.AmbientLight.SetColorRGB 0.6, 0.6, 0.6

Else

If ShipHit = False Then mciSendString "setaudio songNumber2 volume to " & 230, vbNullString, 0, 0

End If
'If Label9.Caption = "J" Then Check1.Value = 1

Cls
'keyscheck
Dim loopkey
Label1.Caption = "keys pressed(asc):"
For loopkey = 0 To 255
If keypressed(loopkey) = True Then
Label1.Caption = Label1.Caption + Str(loopkey) + "/ " + Chr$(loopkey)
End If
Next loopkey

If keypressed(17) And Playing And ShipHit = False Then







ChangeMainLight

If ShipHit = False Then mciSendString "setaudio songNumber2 volume to " & 230, vbNullString, 0, 0
firepresed = True
'Timer4.Enabled = True
'mciSendString "setaudio songNumber2 volume to " & 230, vbNullString, 0, 0
'Timer4.Enabled = True


keypressed(17) = True
If ShipHit = False Then mciSendString "setaudio songNumber2 volume to " & 230, vbNullString, 0, 0
If PlayGun = False Then
PlayGun = True
'Play2
End If


If ShipHit = False Then mciSendString "setaudio songNumber2 volume to " & 230, vbNullString, 0, 0


Else

'keybd_event Asc(UCase(KEYpadkeys(5))), 0, KEYEVENTF_KEYUP, 0  ' release






End If
'joycheck
If Check1.Value = 1 Then
If joyGetPosEx(JNum, JI) <> 0 Then
    Print "Joystick #"; CStr(JNum); " is not plugged in, or is not working."
    
    Image4.Visible = False
     Image3.Visible = True
    
    
    
    
Else

 Image4.Visible = True
     Image3.Visible = False

JOYsupport
'Label9.Caption = "K"


End If
End If





'controls
ControlsAction


If Playing Then

'TimerG1 = 0
'TimerG2 = 0
'TimerG3 = 0
End If


End Sub

Private Sub ExitSub()

End

End Sub


Public Sub endProcedure()
retVal = mciSendString("close songNumber1", vbNullString, 0, 0)
retVal = mciSendString("close songNumber2", vbNullString, 0, 0)
retVal = mciSendString("close songNumber3", vbNullString, 0, 0)

End
End Sub
Private Sub JOYsupport2(p, q)


If p = 1 And q = 512 Then
keypressed(13) = True
End If





End Sub

Private Function chrto(x)


chrto = x
Picture1.SetFocus

End Function
Private Sub JOYsupport()

If Label9.Caption = "J" Then



    With JI
        Print "X = "; CStr(.dwXpos)
        Print "Y = "; CStr(.dwYpos)
        Print "Z = "; CStr(.dwZpos)
        Print "R = "; CStr(.dwRpos)
        Print "U = "; CStr(.dwUpos)
        Print "V = "; CStr(.dwVpos)
        
        
        
      '  If .dwPOV < &HFFFF& Then
      '  Print "PovAngle = "; CStr(.dwPOV / 100)
       ' If Text1.Text <> "" Then KEYpadkeys(1) = Text1.Text
'If Text2.Text <> "" Then KEYpadkeys(2) = Text2.Text
'If Text3.Text <> "" Then KEYpadkeys(3) = Text3.Text
'If Text4.Text <> "" Then KEYpadkeys(4) = Text4.Text
        If .dwXpos = 65535 And .dwYpos = 0 Then
        
        JoyEnabled = True
        
        keybd_event Asc(UCase(KEYpadkeys(3))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(1))), 0, 0, 0  ' press
        keybd_event Asc(UCase(KEYpadkeys(2))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(4))), 0, 0, 0  ' press
        
        
        
        
        Else
        End If


        If .dwXpos = 65535 And .dwYpos = 65535 Then
        
        JoyEnabled = True
        
        keybd_event Asc(UCase(KEYpadkeys(3))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(1))), 0, 0, 0  ' press
        keybd_event Asc(UCase(KEYpadkeys(2))), 0, 0, 0  ' press
        keybd_event Asc(UCase(KEYpadkeys(4))), 0, KEYEVENTF_KEYUP, 0  ' release
        Else
        End If


        If .dwXpos = 0 And .dwYpos = 65535 Then
        
        JoyEnabled = True
        
        
        keybd_event Asc(UCase(KEYpadkeys(3))), 0, 0, 0  ' press
        keybd_event Asc(UCase(KEYpadkeys(1))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(2))), 0, 0, 0  ' press
        keybd_event Asc(UCase(KEYpadkeys(4))), 0, KEYEVENTF_KEYUP, 0  ' release
        Else
        End If

        If .dwXpos = 0 And .dwYpos = 0 Then
        
        
        JoyEnabled = True
        keybd_event Asc(UCase(KEYpadkeys(3))), 0, 0, 0  ' press
        keybd_event Asc(UCase(KEYpadkeys(1))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(2))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(4))), 0, 0, 0  ' press
        Else
        End If

        
        If .dwXpos = 65535 And .dwYpos = 32511 Then
        
        JoyEnabled = True
        keybd_event Asc(UCase(KEYpadkeys(3))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(1))), 0, 0, 0  ' press
        keybd_event Asc(UCase(KEYpadkeys(2))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(4))), 0, KEYEVENTF_KEYUP, 0  ' release
        Else
        End If
        
        If .dwXpos = 0 And .dwYpos = 32511 Then
        
        JoyEnabled = True
        keybd_event Asc(UCase(KEYpadkeys(3))), 0, 0, 0  ' press
        keybd_event Asc(UCase(KEYpadkeys(1))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(4))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(2))), 0, KEYEVENTF_KEYUP, 0  ' release
        Else
        End If
        
        If .dwXpos = 32511 And .dwYpos = 65535 Then
        JoyEnabled = True
       keybd_event Asc(UCase(KEYpadkeys(2))), 0, 0, 0  ' press
        keybd_event Asc(UCase(KEYpadkeys(4))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(1))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(3))), 0, KEYEVENTF_KEYUP, 0  ' release
        
        Else
        End If
        
        If .dwXpos = 32511 And .dwYpos = 0 Then
        JoyEnabled = True
        keybd_event Asc(UCase(KEYpadkeys(4))), 0, 0, 0  ' press
        keybd_event Asc(UCase(KEYpadkeys(2))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(1))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(3))), 0, KEYEVENTF_KEYUP, 0  ' release
        Else
        End If
        
      '   End If
      
  If .dwXpos = 32511 And .dwYpos = 32511 Then
  Print "Centered"
        keybd_event Asc(UCase(KEYpadkeys(3))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(1))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(2))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(4))), 0, KEYEVENTF_KEYUP, 0  ' release
        Else
       
         
         
        End If
        
        
      
        
        
        Print "ButtonsPressedCount = "; CStr(.dwButtonNumber)
        Print "ButtonBinaryFlags = "; CStr(.dwButtons)
        JOYsupport2 CStr(.dwButtonNumber), CStr(.dwButtons)
     '   If Label9.Caption = "J" Then
        
        If .dwButtons = 1 Or .dwButtons = 5 Or .dwButtons = 3 Or .dwButtons = 7 Or .dwButtons = 9 Or .dwButtons = 11 Or .dwButtons = 13 Or .dwButtons = 15 Then
       JoyEnabled = True
        keybd_event Asc(UCase(KEYpadkeys(8))), 0, 0, 0  ' press
        Else
        keybd_event Asc(UCase(KEYpadkeys(8))), 0, KEYEVENTF_KEYUP, 0  ' release
        End If
        
        If .dwButtons = 8 Or .dwButtons = 12 Or .dwButtons = 14 Or .dwButtons = 9 Or .dwButtons = 10 Or .dwButtons = 11 Or .dwButtons = 13 Or .dwButtons = 15 Then
     JoyEnabled = True
        keybd_event Asc(UCase(KEYpadkeys(6))), 0, 0, 0  ' press
        Else
        keybd_event Asc(UCase(KEYpadkeys(6))), 0, KEYEVENTF_KEYUP, 0  ' release
        End If
        
        
       If .dwButtons = 2 Or .dwButtons = 6 Or .dwButtons = 7 Or .dwButtons = 3 Or .dwButtons = 10 Or .dwButtons = 11 Or .dwButtons = 14 Or .dwButtons = 15 Then
     JoyEnabled = True
       keybd_event Asc(UCase(KEYpadkeys(7))), 0, 0, 0  ' press
       Else
       keybd_event Asc(UCase(KEYpadkeys(7))), 0, KEYEVENTF_KEYUP, 0  ' release
       End If
        
       If .dwButtons = 4 Or .dwButtons = 5 Or .dwButtons = 7 Or .dwButtons = 6 Or .dwButtons = 12 Or .dwButtons = 13 Or .dwButtons = 14 Or .dwButtons = 15 _
       Then
       JoyEnabled = True
       keybd_event Asc(UCase(KEYpadkeys(5))), 0, 0, 0  ' press
       Else
       keybd_event Asc(UCase(KEYpadkeys(5))), 0, KEYEVENTF_KEYUP, 0  ' release
       End If
        
      ' End If
        
        
        
        
        
        
        
        Picture1.Cls
        Picture1.Circle (.dwXpos / &HFFFF& * (Picture1.Width - 1), .dwYpos / &HFFFF& * (Picture1.Height - 1)), 2
    End With


End If
End Sub
Private Sub ControlsAction()
'ew

'If keypressed(Asc("j")) Or keypressed(Asc("J")) Then

'If Check1.Value = 0 Then

'Check1.Value = 1

'Else
'Check1.Value = 0

'End If


'keypressed(Asc("j")) = False
'keypressed(Asc("J")) = False
'End If



If keypressed(13) Then
Label10_Click (0)
keypressed(13) = False
End If





If keypressed(17) And Playing And ShipHit = False Then
'Picture4_Click (2)
GunEnabled5 = True





If ShipHit = False Then mciSendString "setaudio songNumber2 volume to " & 230, vbNullString, 0, 0
firepresed = True
'Timer4.Enabled = True
'mciSendString "setaudio songNumber2 volume to " & 230, vbNullString, 0, 0
'Play2
Else
GunEnabled5 = False
'retVal = mciSendString("pause songNumber2", vbNullString, 0, 0)
'keypressed(17) = False
'mciSendString "setaudio songNumber2 volume to " & 0, vbNullString, 0, 0
End If

If keypressed(27) Then
Debug.Print "\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"
Debug.Print "End"
Debug.Print "-------------------------------------"
If Picture3.Visible = True Then
timeToStart = 0
retVal = mciSendString("close songNumber1", vbNullString, 0, 0)
'retVal = mciSendString("close songNumber2", vbNullString, 0, 0)
retVal = mciSendString("close songNumber3", vbNullString, 0, 0)
Playing = False


Picture3.Visible = False

'reset laser
Dim a
Dim b
Dim c
Dim bLoop
For bLoop = 0 To 50
a = 0
b = -400
c = 1800
FR_sho(bLoop).SetPosition Nothing, a, b, c
Next

If Label9.Caption = "K" Then Picture2.Enabled = True

Picture7.Visible = True

Label10(0).Enabled = True
Label10(1).Enabled = True
Label10(0).Visible = True
Label10(1).Visible = True
Label10(2).Enabled = True
Label10(2).Visible = True


'ExitSub
Else



'Picture3.Visible = False
'Picture6.Visible = True
'Label10(0).Visible = True
'Label10(1).Visible = True
'Label10(2).Visible = True
'If Label9.Caption = "K" Then Picture2.Visible = True


Picture1.SetFocus



End If
'keypressed(192) = True '~

'keypressed(192) = False

keypressed(27) = False
'ExitSub
End If

'keypressed(192) = False
'If keypressed(192) Then
'If Picture3.Visible = True Then
'Picture3.Visible = False
'keypressed(192) = False
'Else
'Picture3.Visible = True
'keypressed(192) = False
'End If
'End If










mvShip













If keypressed(17) = False Then




keypressed(17) = False
End If













End Sub
Private Sub InitCanva()

With RMCanvas1
    .StartWindowed
    .SceneFrame.SetSceneBackgroundRGB 0, 0, 0
    .Viewport.SetBack 8000
    .CameraFrame.SetPosition Nothing, 0, 0, 0
End With


'Dim bCKgRN(2) As Direct3DRMTexture3
Set bCKgRN(2) = RMCanvas1.D3DRM.LoadTexture(App.Path + "/tEXTURES/62G.bmp")
'RMCanvas1.SceneFrame.SetSceneBackgroundImage backgroundtxtr
Set bCKgRN(1) = RMCanvas1.D3DRM.LoadTexture(App.Path + "/tEXTURES/62G2.bmp")
'RMCanvas1.SceneFrame.SetSceneBackgroundImage backgroundtxtr
Set bCKgRN(0) = RMCanvas1.D3DRM.LoadTexture(App.Path + "/tEXTURES/62G3.bmp")

RMCanvas1.SceneFrame.SetSceneBackgroundImage bCKgRN(0)




'RMCanvas1.AmbientLight.SetColorRGB 0.4, 0.4, 0.4
RMCanvas1.Device.SetTextureQuality D3DRMTEXTURE_LINEAR

'Light

    RMCanvas1.AmbientLight.SetColorRGB 0.4, 0.4, 0.4
    RMCanvas1.DirLightFrame.SetPosition RMCanvas1.CameraFrame, 0, -2250, -3250
    RMCanvas1.DirLightFrame.LookAt RMCanvas1.CameraFrame, Nothing, 0

End Sub
Private Sub InitShip()





'Dim Mesh_SPHERE As Direct3DRMMeshBuilder3
'Dim Frame_SPHERE As Direct3DRMFrame3
'Dim TEXTURE_SPHERE(3) As Direct3DRMTexture3
Set Frame_SPHERE = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
Set Mesh_SPHERE = RMCanvas1.D3DRM.CreateMeshBuilder()
Mesh_SPHERE.LoadFromFile App.Path & "\Models\SPHERE.x", 0, 0, Nothing, Nothing
Mesh_SPHERE.ScaleMesh 5420.4, 5420.4, 5420.4
Set TEXTURE_SPHERE(0) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\Textures\moontexture.bmp")
Mesh_SPHERE.SetTexture TEXTURE_SPHERE(0)
Frame_SPHERE.SetPosition Nothing, -500, 0, 9000



Frame_SPHERE.AddVisual Mesh_SPHERE
Mesh_SPHERE.SetColorRGB 1, 1, 1 ' RGB







'Dim Mesh_Ship As Direct3DRMMeshBuilder3
'Dim Frame_Ship As Direct3DRMFrame3
'Dim TEXTURE_Ship As Direct3DRMTexture3
Set Frame_Ship = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
Set Mesh_Ship = RMCanvas1.D3DRM.CreateMeshBuilder()






Mesh_Ship.LoadFromFile App.Path & "\Models\ShipFr.x", 0, 0, Nothing, Nothing
Mesh_Ship.ScaleMesh 0.4, 0.4, 0.4

Set TEXTURE_Ship(0) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\Textures\diffuse.bmp") 'diffuse2
Set TEXTURE_Ship(1) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\Textures\diffuse4.bmp")
Set TEXTURE_Ship(2) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\Textures\diffuse3.bmp")
Set TEXTURE_Ship(3) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\Textures\diffuse.bmp")
shippaintSel

ShipVector.x = 0
ShipVector.y = -400
ShipVector.z = 1783
Frame_Ship.SetPosition Nothing, ShipVector.x, ShipVector.y, ShipVector.z

Dim z
For z = 0 To 10
VC_sho(z).y = ShipVector.y
VC_sho(z).x = ShipVector.x
Next

Frame_Ship.SetOrientation Nothing, 0, -1, 0, 0.05, 2, -1
'Frame_Ship.SetRotation Frame_Ship, 1, 1, 0, 0.003
'Mesh_Ship.SetColorRGB 1, 0.5, 0.5 ' RGB
Frame_Ship.AddVisual Mesh_Ship



End Sub

Private Sub Timer2_Timer()






If Picture3.Visible = True Then

Picture2.Visible = False
Picture6.Visible = False
End If

'If Picture3.Visible = False Then
'Picture1.SetFocus
'ChangeMainLight
MveStars
'--------------------------------------------------------
'If Picture3.Visible = True Then

'Frame_Ship.SetPosition Nothing, ShipVector.X, ShipVector.Y + HOVERINGY * 10, ShipVector.z
RMCanvas1.CameraFrame.SetPosition Nothing, ShipVector.x \ 3, 0, 0


RMCanvas1.Update
End Sub
Private Sub ChangeMainLight()
    
    If ShipHit = False Then
    Dim r
    r = 0.6 + Rnd * 0.4
    RMCanvas1.AmbientLight.SetColorRGB r - 0.3, r - 0.3, 1
    If Rnd * 100 < 20 Then
    RMCanvas1.AmbientLight.SetColorRGB r - 0.3, r, 1
    End If
    vertShot = vertShot + 200
    RMCanvas1.DirLightFrame.SetPosition RMCanvas1.CameraFrame, 0, -4000 + vertShot, -2500
    If vertShot > 4000 Then vertShot = -4000
    RMCanvas1.DirLightFrame.LookAt Frame_Ship, Nothing, 0
    End If
    

End Sub


Private Sub shippaintSel()
'keypressed(39) = False
'If ShipPaint = 0 Then
Mesh_Ship.SetTexture TEXTURE_Ship(0)
'ShipPaint = 1
'ElseIf ShipPaint = 1 Then
'Mesh_Ship.SetTexture TEXTURE_Ship(1)
'ShipPaint = 2
'ElseIf ShipPaint = 2 Then
'Mesh_Ship.SetTexture TEXTURE_Ship(2)
'ShipPaint = 3
'Else
'Mesh_Ship.SetTexture TEXTURE_Ship(3)

'ShipPaint = 0
'End If



'keypressed(39) = False



End Sub
Private Sub mvShip()

If Playing Then
'LATE

'USED WITH
HOVERINGX = HOVERINGX + 0.11
HOVERINGY = Sin(HOVERINGX) * 0.5
If HOVERINGX > 360 Then HOVERINGX = 0




If keypressed(39) And ShipHit = False Then
ShipVector.x = ShipVector.x + ShipSpeed

If ShipVector.x > 1200 Then ShipVector.x = 1200

''''Frame_Ship.SetPosition Nothing, ShipVector.X, ShipVector.Y + HOVERINGY * 10, ShipVector.z




Dim a
Dim b
Dim c
a = ShipVector.x
b = ShipVector.y
c = 1883
'FR_sho(LoopA).SetPosition Nothing, ShipVector.X, ShipVector.Y + HOVERINGY * 10, c




'keypressed(39) = False
End If
If keypressed(37) And ShipHit = False Then
ShipVector.x = ShipVector.x - ShipSpeed

If ShipVector.x < -1200 Then ShipVector.x = -1200

''''Frame_Ship.SetPosition Nothing, ShipVector.X, ShipVector.Y + HOVERINGY * 10, ShipVector.z


a = ShipVector.x
b = ShipVector.y
c = 1883
'FR_sho(LoopA).SetPosition Nothing, ShipVector.X, ShipVector.Y + HOVERINGY * 10, c


'keypressed(37) = False
End If
If keypressed(38) And ShipHit = False Then
ShipVector.y = ShipVector.y + ShipSpeed

If ShipVector.y > 400 Then ShipVector.y = 400

''''Frame_Ship.SetPosition Nothing, ShipVector.X, ShipVector.Y + HOVERINGY * 10, ShipVector.z


a = ShipVector.x
b = ShipVector.y
c = 1883
'FR_sho(LoopA).SetPosition Nothing, ShipVector.X, ShipVector.Y + HOVERINGY * 10, c

'keypressed(38) = False
End If
If keypressed(40) And ShipHit = False Then
ShipVector.y = ShipVector.y - ShipSpeed

If ShipVector.y < -480 Then ShipVector.y = -480

'''Frame_Ship.SetPosition Nothing, ShipVector.X, ShipVector.Y + HOVERINGY * 10, ShipVector.z


'a = ShipVector.X
'b = ShipVector.Y
'c = 1883
'For LoopA = 0 To 10
'FR_sho(LoopA).SetPosition Nothing, ShipVector.X, ShipVector.Y + HOVERINGY * 10, c



'keypressed(40) = False
End If



If ShipHit = False Then
Frame_Ship.SetPosition Nothing, ShipVector.x, ShipVector.y + HOVERINGY * 10, ShipVector.z
Else
Frame_Ship.SetPosition Nothing, 20000, 20000, 0
End If
'a = ShipVector.X
'b = ShipVector.Y
'c = 1800

'For LoopA = 0 To 10
'FR_sho(LoopA).SetPosition Nothing, ShipVector.X, ShipVector.Y, c
'Next






End If
End Sub
Private Sub SimpleShot()






End Sub
Private Sub Play()
Dim FileName
FileName = Chr(34) & App.Path + "\sounds\chiploop.mp3" & Chr(34)
retVal = mciSendString("close songNumber1", vbNullString, 0, 0)
retVal = mciSendString("open " & FileName & " type mpegvideo alias songNumber1", vbNullString, 0, 0)
mciSendString "setaudio songNumber1 volume to " & 50, vbNullString, 0, 0
'retVal = mciSendString("play songNumber1 repeat", vbNullString, 0, 0)






End Sub
Private Sub Play2()
Dim FileName
FileName = Chr(34) & App.Path + "\sounds\sfx_wpn_machinegun_loop5.wav" & Chr(34)
retVal = mciSendString("close songNumber2", vbNullString, 0, 0)
retVal = mciSendString("open " & FileName & " type mpegvideo alias songNumber2", vbNullString, 0, 0)
mciSendString "setaudio songNumber2 volume to " & 0, vbNullString, 0, 0
retVal = mciSendString("play songNumber2 repeat", vbNullString, 0, 0)
'mciSendString "setaudio songNumber2 volume to " & 0, vbNullString, 0, 0





End Sub

Private Sub Timer4_Timer()
firepresed = False

lIvEs = lIvEs - 1
If lIvEs < 0 Then
Timer4.Enabled = False

timeToStart = 0

Playing = False
Picture3.Visible = False
'reset laser
Dim a
Dim b
Dim c
Dim bLoop
For bLoop = 0 To 50
a = 0
b = -400
c = 1800
FR_sho(bLoop).SetPosition Nothing, a, b, c
ShotB(bLoop) = False
Next
If Label9.Caption = "K" Then Picture2.Enabled = True
Picture7.Visible = True
Label10(0).Enabled = True
Label10(1).Enabled = True
Label10(0).Visible = True
Label10(1).Visible = True
Label10(2).Enabled = True
Label10(2).Visible = True


Exit Sub
End If
 
PrintLine

















keypressed(17) = False
timeToStart = 1
StartSound
ShipHit = False
Dim s
'Dim a
'Dim b
'Dim c
Playing = False
For s = 0 To 50
a = -2500 + Rnd * 5000
b = (-1600 + Rnd * 3200) + 2500
c = 1783
'pozPocz wrogow?
FR_st3(s).SetPosition Nothing, a, b, c
ShotB(s) = False
Next
Frame_SPHERE.SetOrientation Nothing, 1, -0.5, 1, -1, 1, -1
Frame_SPHERE.SetRotation Frame_SPHERE, -1, 0, 0, 0

ShipVector.x = 0
ShipVector.y = -400
ShipVector.z = 1783
Frame_Ship.SetPosition Nothing, ShipVector.x, ShipVector.y, ShipVector.z
RMCanvas1.CameraFrame.SetPosition Nothing, ShipVector.x \ 3, 0, 0




'k'eypressed(17) = False
Timer4.Enabled = False
'For Shotfrq = 0 To 600



'firepresed = False

'If Shotfrq = 600 Then Timer4.Enabled = False

'Next





End Sub
Private Sub StartSound()


Dim FileName
FileName = Chr(34) & App.Path + "\sounds\start.mp3" & Chr(34)
retVal = mciSendString("close songNumber3", vbNullString, 0, 0)
retVal = mciSendString("open " & FileName & " type mpegvideo alias songNumber3", vbNullString, 0, 0)
mciSendString "setaudio songNumber3 volume to " & 120, vbNullString, 0, 0
retVal = mciSendString("play songNumber3", vbNullString, 0, 0)


End Sub
Private Sub InitStars()
'stars

Set TXENEMY_st3a = RMCanvas1.D3DRM.LoadTexture(App.Path & "\Textures\62.bmp")
Set TXENEMY_st3 = RMCanvas1.D3DRM.LoadTexture(App.Path & "\Textures\diffuse4.bmp")



Dim a
Dim b
Dim c

'Dim LoopA
'For LoopA = 0 To 50







'Next

Dim k
'Dim FR_st1(100) As Direct3DRMFrame3
'Dim MS_st1(100) As Direct3DRMMeshBuilder3
'Dim VC_st1(100) As D3DVECTOR
For LoopA = 0 To 50

If LoopA <= 20 Then
For k = 0 To 2
Set Frame_EXPL(LoopA, k) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
Set Mesh_EXPL(LoopA, k) = RMCanvas1.D3DRM.CreateMeshBuilder()
Mesh_EXPL(LoopA, k).LoadFromFile App.Path & "\Models\sphere.X", 0, 0, Nothing, Nothing
Mesh_EXPL(LoopA, k).ScaleMesh 25.5 + k * 10, 25.5 + k * 10, 0.01
Mesh_EXPL(LoopA, k).SetColorRGB 1, 1, 1
Frame_EXPL(LoopA, k).SetPosition Nothing, 20000, 20000, 0
Frame_EXPL(LoopA, k).AddVisual Mesh_EXPL(LoopA, k)
Next
End If

'Init laser
If LoopA <= 50 Then
Set FR_sho(LoopA) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
Set MS_sho(LoopA) = RMCanvas1.D3DRM.CreateMeshBuilder()
MS_sho(LoopA).LoadFromFile App.Path & "\Models\backgroundb.X", 0, 0, Nothing, Nothing
MS_sho(LoopA).ScaleMesh 1.5, 8, 1.5
MS_sho(LoopA).SetColorRGB 0.5, 0.5, 1



a = 0
b = -400
c = 1800





FR_sho(LoopA).SetPosition Nothing, a, b, c
'MS_sho(LoopA).SetColorRGB 1, 1, 1 ' RGB
FR_sho(LoopA).AddVisual MS_sho(LoopA)

'shot
'Dim Shoting As Boolean
'Dim FR_sho(10) As Direct3DRMFrame3
'Dim MS_sho(10) As Direct3DRMMeshBuilder3


'platformy'platformy'platformy'platformy'platformy'platformy'platformy-albo wrogowie
End If





'platformy'platformy'platformy'platformy'platformy'platformy'platformy
'platformy'platformy'platformy'platformy'platformy'platformy'platformy
If LoopA <= IloscPlatform Then 'platformy'platformy'platformy'platformy'platformy
'150?
toEXPLODE(LoopA) = HitLimit


Set FR_st3(LoopA) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
Set MS_st3(LoopA) = RMCanvas1.D3DRM.CreateMeshBuilder()
MS_st3(LoopA).LoadFromFile App.Path & "\Models\backgrounda.X", 0, 0, Nothing, Nothing





MS_st3(LoopA).ScaleMesh 60, 40, 60




'Set TEXTURE_Ship(0) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\Textures\diffuse2.bmp")
'Set TEXTURE_Ship(1) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\Textures\diffuse3.bmp")
'Set TEXTURE_Ship(2) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\Textures\diffuse4.bmp")


Set TX_st3(LoopA) = RMCanvas1.D3DRM.LoadTexture(App.Path & "\Textures\62.bmp")



MS_st3(LoopA).SetTexture TX_st3(LoopA)

a = -2500 + Rnd * 5000
b = (-1600 + Rnd * 3200) + 2500
c = 1783

'pozPocz wrogow?
FR_st3(LoopA).SetPosition Nothing, a, b, c

FR_st3(LoopA).SetPosition Nothing, a, b, c

'FR_st3(LoopA).SetOrientation



MS_st3(LoopA).SetColorRGB 0.7, 0.7, 1 ' RGB
FR_st3(LoopA).AddVisual MS_st3(LoopA)
'platformy'platformy'platformy'platformy'platformy'platformy'platformy
'platformy'platformy'platformy'platformy'platformy'platformy'platformy
'platformy'platformy'platformy'platformy'platformy'platformy'platformy
'platformy'platformy'platformy'platformy'platformy'platformy'platformy
'platformy'platformy'platformy'platformy'platformy'platformy'platformy
'platformy'platformy'platformy'platformy'platformy'platformy'platformy
'platformy'platformy'platformy'platformy'platformy'platformy'platformy
'platformy'platformy'platformy'platformy'platformy'platformy'platformy

End If


Set FR_st1(LoopA) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
Set MS_st1(LoopA) = RMCanvas1.D3DRM.CreateMeshBuilder()

Set FR_st2(LoopA) = RMCanvas1.D3DRM.CreateFrame(RMCanvas1.SceneFrame)
Set MS_st2(LoopA) = RMCanvas1.D3DRM.CreateMeshBuilder()






'STARS

'Set TEXTURE_Ship = RM.D3DRM.LoadTexture(PATHFORFILES & "FILTERED.BMP")
MS_st1(LoopA).LoadFromFile App.Path & "\Models\sphere.X", 0, 0, Nothing, Nothing
MS_st1(LoopA).ScaleMesh 2.4, 2.4, 2.4

MS_st2(LoopA).LoadFromFile App.Path & "\Models\sphere.X", 0, 0, Nothing, Nothing
MS_st2(LoopA).ScaleMesh 1.9, 1.9, 1.9

'
a = -2400 + Rnd * 4800
b = -1200 + Rnd * 2400
c = 3483
FR_st1(LoopA).SetPosition Nothing, a, b, c
'Frame_Ship.SetOrientation Nothing, 0.5, -0.2, -0.2, 0.28, 0.4, -0.4
'Frame_Ship.SetRotation Nothing, 1, 0.5, 0.1, 0.003
a = -2400 + Rnd * 4800
b = -1200 + Rnd * 2400
c = 3490
FR_st2(LoopA).SetPosition Nothing, a, b, c




MS_st1(LoopA).SetColorRGB 1, 1, 1 ' RGB
FR_st1(LoopA).AddVisual MS_st1(LoopA)

MS_st2(LoopA).SetColorRGB 1, 1, 1 ' RGB
FR_st2(LoopA).AddVisual MS_st2(LoopA)







Next LoopA


End Sub
Private Function colision(p, q, c, d) As Boolean
Dim h
colision = False
If (p - 300 < q) And (p + 300 > q) And (c + 300 > d) And (c - 300 < d) Then

colision = True


End If

'EXPLODED 50 platfrm
'EXPLODED 50 platfrm
'EXPLODED 50 platfrm
'EXPLODED 50 platfrm
'EXPLODED 50 platfrm
'EXPLODED 50 platfrm
'EXPLODED 50 platfrm
'EXPLODED 50 platfrm

End Function
Private Sub DrawExplosions(x, y)
Dim q
For q = 0 To 20
If eXPLb(q) = False Then
eXPLb(q) = True
EXPLv(q).x = x
EXPLv(q).y = y
explTime(q) = 0

Exit Sub
End If
Next


'Dim Mesh_EXPL(20) As Direct3DRMMeshBuilder3
'Dim Frame_EXPL(20) As Direct3DRMFrame3
'Dim TEXTURE_EXPL(20) As Direct3DRMTexture3
'Dim EXPLv(20) As D3DVECTOR
'Dim eXPLb(20) As Boolean
'Dim explTime(20) As Integer

End Sub


Private Sub MveStars()
Dim positionwave As D3DVECTOR
If Playing Then



'DrawExplosions



Dim c
Dim d
Dim e
Dim f
Dim LoopR
Dim CanonVer As D3DVECTOR


If Idle >= 0 Then
Idle = Idle - 10
If Idle <= 0 Then
'Picture4_Click (3)
End If
End If
lastShot = lastShot + 15
If lastShot > 50 Then lastShot = 50

c = 1800
For LoopA = 0 To 50

'drawexplosions
If LoopA <= 20 Then
If eXPLb(LoopA) = False Then

'Mesh_EXPL(LoopA).ScaleMesh 1.5, 1.5, 1.5
'Mesh_EXPL(LoopA).SetColorRGB 1, 1, 1
Frame_EXPL(LoopA, 0).SetPosition Nothing, 20000, 20000, c
Frame_EXPL(LoopA, 1).SetPosition Nothing, 20000, 20000, c
Frame_EXPL(LoopA, 2).SetPosition Nothing, 20000, 20000, c
End If
If eXPLb(LoopA) Then

'Mesh_EXPL(LoopA).ScaleMesh   .ScaleMesh 1.5, 1.5, 1.5
'Mesh_EXPL(LoopA).Empty
'Mesh_EXPL(LoopA).LoadFromFile App.Path & "\Models\sphere.X", 0, 0, Nothing, Nothing
'Mesh_EXPL(LoopA).ScaleMesh 0, 0, 0
'Mesh_EXPL(LoopA).SetColorRGB 1, 1, 1
'Mesh_EXPL(LoopA).ScaleMesh 4 * explTime(LoopA), 4 * explTime(LoopA), 4 * explTime(LoopA)
If explTime(LoopA) = 0 Or explTime(LoopA) = 1 Then
Frame_EXPL(LoopA, 0).SetPosition Nothing, EXPLv(LoopA).x, EXPLv(LoopA).y, c
Frame_EXPL(LoopA, 1).SetPosition Nothing, 20000, 20000, c
Frame_EXPL(LoopA, 2).SetPosition Nothing, 20000, 20000, c
End If
If explTime(LoopA) = 2 Or explTime(LoopA) = 3 Then
Frame_EXPL(LoopA, 1).SetPosition Nothing, EXPLv(LoopA).x, EXPLv(LoopA).y, c
Frame_EXPL(LoopA, 0).SetPosition Nothing, 20000, 20000, c
Frame_EXPL(LoopA, 2).SetPosition Nothing, 20000, 20000, c
End If
'Frame_EXPL(LoopA).SetPosition Nothing, 20000, 20000, 0
'Frame_EXPL(LoopA).AddVisual Mesh_EXPL(LoopA)
If explTime(LoopA) = 4 Or explTime(LoopA) = 5 Then
Frame_EXPL(LoopA, 2).SetPosition Nothing, EXPLv(LoopA).x, EXPLv(LoopA).y, c
Frame_EXPL(LoopA, 1).SetPosition Nothing, 20000, 20000, c
Frame_EXPL(LoopA, 0).SetPosition Nothing, 20000, 20000, c
End If




explTime(LoopA) = explTime(LoopA) + 1
If explTime(LoopA) >= 12 Then eXPLb(LoopA) = False







End If
End If


If ShotB(LoopA) = False Then
If ((firepresed = True Or AttackPressed) And lastShot >= 30) And ShipHit = False Then
ShotB(LoopA) = True
VC_sho(LoopA).y = ShipVector.y
VC_sho(LoopA).x = ShipVector.x
Shoting(LoopA) = 0
lastShot = 0
AttackPressed = False
firepresed = False
End If

Else
Shoting(LoopA) = Shoting(LoopA) + 20
If Shoting(LoopA) > 1100 Then
Shoting(LoopA) = 0
ShotB(LoopA) = False
End If

End If


If ShotB(LoopA) = False Then

If ShipHit = False Then
FR_sho(LoopA).SetPosition Nothing, ShipVector.x, ShipVector.y, c
Else
FR_sho(LoopA).SetPosition Nothing, 20000, 20000, c
End If

Else
FR_sho(LoopA).SetPosition Nothing, VC_sho(LoopA).x, VC_sho(LoopA).y + Shoting(LoopA), c
End If









'enemy waves
If LoopA <= IloscPlatform Then


FR_st3(LoopA).GetPosition Nothing, positionwave


'shipcolision
'Frame_Ship.SetPosition Nothing, ShipVector.X, ShipVector.Y + HOVERINGY * 10, ShipVector.z

If ShipHit = False And (ShipVector.x - 55 < positionwave.x And ShipVector.x + 55 > positionwave.x And ShipVector.y - 55 < positionwave.y And ShipVector.y + 55 > positionwave.y) Then
ShipHit = True

pLAYexplosion

DrawExplosions ShipVector.x, ShipVector.y
''''''''''''''''positionwave.y = 3200
''''''''''''''''positionwave.x = -1200 + Rnd * 2400
''''''''''''''''FR_st3(LoopA).SetPosition Nothing, positionwave.x, positionwave.y, positionwave.z
shipLostLife
End If
'EXPLODED(150)?
For LoopR = 0 To 50

FR_sho(LoopR).GetPosition Nothing, CanonVer
'FR_st3(LoopA).GetPosition Nothing, positionwave
If ShotB(LoopR) = True Then
'-------------------------------------
'-------------------------------------
'-------------------------------------
If CanonVer.y < 480 And (CanonVer.x - 45 < positionwave.x And CanonVer.x + 45 > positionwave.x And CanonVer.y - 45 < positionwave.y And CanonVer.y + 45 > positionwave.y) Then




toEXPLODE(LoopA) = toEXPLODE(LoopA) - 1

If HitLight = True Then

HitLight = False

MS_st3(LoopA).SetTexture TXENEMY_st3

Else
HitLight = True

MS_st3(LoopA).SetTexture TXENEMY_st3a
End If



If toEXPLODE(LoopA) > 0 Then
pLAYhit
Else
pLAYexplosion
DrawExplosions positionwave.x, positionwave.y
'MS_st3(LoopA).SetTexture TXENEMY_st3
MS_st3(LoopA).SetTexture TXENEMY_st3

positionwave.y = 3200
positionwave.x = -1200 + Rnd * 2400
''''VC_st3(LoopA).Y = 3200
FR_st3(LoopA).SetPosition Nothing, positionwave.x, positionwave.y, positionwave.z
SCORE = SCORE + 10
Label12.Caption = Trim(Str(SCORE))
Label12.Left = Picture9.ScaleWidth \ 2 - Label12.Width \ 2




If SCORE > SCOREH Then SCOREH = SCORE
Label10(4).Caption = Str(SCOREH)
toEXPLODE(LoopA) = HitLimit
End If



'////////////////////////////////////////////////////////////////////
'FR_st3(LoopR).GetPosition Nothing, VC_st3(LoopA)

ShotB(LoopR) = False
'FR_sho(LoopR)
c = 1800

If ShipHit = False Then
FR_sho(LoopR).SetPosition Nothing, ShipVector.x, ShipVector.y, c
Else
FR_sho(LoopR).SetPosition Nothing, 20000, 20000, 0
End If
'MS_st3(LoopA).Empty
'EXPLODED(LoopA) = True
'sound
'points




End If



End If






'====================================





'positionwave
'CanonVer
'If colision(positionwave.X, positionwave.Y, CanonVer.X, CanonVer.Y) And ShotB(LoopR) Then

'EXPLODED(LoopA) = True
'MS_st3(LoopA).Empty
'MS_sho(LoopR).Empty
'For z = 0 To rounds'

'ShotB(LoopR) = False

'FIRED(LoopR) = True

'End If




Next



If EXPLODED(LoopA) = False Then
FR_st3(LoopA).SetPosition Nothing, positionwave.x, positionwave.y - 12, positionwave.z
End If









FR_st3(LoopA).SetRotation FR_st3(LoopA), 1, 0, 1, 0.2

FR_st3(LoopA).GetPosition Nothing, VC_st3(LoopA)
If VC_st3(LoopA).y < -3200 Then
toEXPLODE(LoopA) = HitLimit
'MS_st3(LoopA).SetTexture TXENEMY_st3
MS_st3(LoopA).SetTexture TXENEMY_st3

VC_st3(LoopA).x = -1200 + Rnd * 2400
VC_st3(LoopA).y = 3200
FR_st3(LoopA).SetPosition Nothing, VC_st3(LoopA).x, VC_st3(LoopA).y, VC_st3(LoopA).z
End If
End If
'stars field
FR_st1(LoopA).SetPosition FR_st1(LoopA), 0, -10, 0
FR_st1(LoopA).GetPosition Nothing, VC_st1(LoopA)
If VC_st1(LoopA).y < -1200 Then
VC_st1(LoopA).y = 1200
FR_st1(LoopA).SetPosition Nothing, VC_st1(LoopA).x, VC_st1(LoopA).y, VC_st1(LoopA).z
End If
'stars field
FR_st2(LoopA).SetPosition FR_st2(LoopA), 0, -5, 0
FR_st2(LoopA).GetPosition Nothing, VC_st2(LoopA)
If VC_st2(LoopA).y < -1200 Then
VC_st2(LoopA).y = 1200
FR_st2(LoopA).SetPosition Nothing, VC_st2(LoopA).x, VC_st2(LoopA).y, VC_st2(LoopA).z
End If


Next


End If

End Sub
Private Sub pLAYexplosion()
'GoTo przeskocz
'pLAYhit

'Exit Sub
'On Error Resume Next
Dim FileName

If ExplLoaded = False Then
FileName = Chr(34) & App.Path + "\sounds\sfx_exp2_short_soft10.mp3" & Chr(34)
retVal = mciSendString("close songNumber4", vbNullString, 0, 0)
retVal = mciSendString("open " & FileName & " type mpegvideo alias songNumber4", vbNullString, 0, 0)
mciSendString "setaudio songNumber4 volume to " & 550, vbNullString, 0, 0
ExplLoaded = True
End If
'retVal = mciSendString("stop songNumber4", vbNullString, 0, 0)
retVal = mciSendString("play songNumber4 from 0", vbNullString, 0, 0)

'przeskocz:



End Sub
Private Sub shipLostLife()


retVal = mciSendString("close songNumber1", vbNullString, 0, 0)
'retVal = mciSendString("close songNumber2", vbNullString, 0, 0)
retVal = mciSendString("close songNumber3", vbNullString, 0, 0)

















'Picture2.Enabled = False
'Label10(0).Enabled = False
'Label10(1).Enabled = False
'Label10(0).Visible = False
'Label10(1).Visible = False'
'Picture7.Visible = False




'Picture3.Visible = True









Timer4.Enabled = True
















End Sub

Private Sub pLAYhit()
'GoTo przeskocz
'On Error Resume Next
Dim FileName

If HitLoaded = False Then
FileName = Chr(34) & App.Path + "\sounds\hit7.wav" & Chr(34)
retVal = mciSendString("close songNumber7", vbNullString, 0, 0)
retVal = mciSendString("open " & FileName & " type mpegvideo alias songNumber7", vbNullString, 0, 0)
mciSendString "setaudio songNumber7 volume to " & 850, vbNullString, 0, 0
HitLoaded = True
End If
'retVal = mciSendString("stop songNumber4", vbNullString, 0, 0)
retVal = mciSendString("play songNumber7 from 15", vbNullString, 0, 0)

'przeskocz:



End Sub

Private Sub Timer5_Timer()


On Error Resume Next
Dim m


For m = 0 To 50
MS_st3(m).SetTexture TXENEMY_st3

Next

End Sub
