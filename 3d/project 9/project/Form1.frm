VERSION 5.00
Object = "{6BF52A50-394A-11D3-B153-00C04F79FAA6}#1.0#0"; "wmp.dll"
Object = "{08216199-47EA-11D3-9479-00AA006C473C}#2.1#0"; "RMControl.ocx"
Begin VB.Form Form1 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BackColor       =   &H00000000&
   BorderStyle     =   0  'None
   Caption         =   "no_name_yet_game"
   ClientHeight    =   8295
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   10500
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   MouseIcon       =   "Form1.frx":1CCA
   MousePointer    =   99  'Custom
   Moveable        =   0   'False
   NegotiateMenus  =   0   'False
   ScaleHeight     =   553
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   700
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.Timer Timer1 
      Interval        =   100
      Left            =   120
      Top             =   8040
   End
   Begin VB.PictureBox Picture6 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      ForeColor       =   &H80000008&
      Height          =   2055
      Left            =   4440
      ScaleHeight     =   137
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   273
      TabIndex        =   11
      Top             =   6120
      Visible         =   0   'False
      Width           =   4095
   End
   Begin VB.PictureBox Picture5 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      ForeColor       =   &H80000008&
      Height          =   3135
      Left            =   5040
      ScaleHeight     =   209
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   329
      TabIndex        =   10
      Top             =   4800
      Visible         =   0   'False
      Width           =   4935
   End
   Begin VB.PictureBox Picture4 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      ForeColor       =   &H80000008&
      Height          =   3855
      Left            =   6480
      ScaleHeight     =   257
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   249
      TabIndex        =   9
      Top             =   240
      Visible         =   0   'False
      Width           =   3735
   End
   Begin VB.PictureBox Picture3 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      ForeColor       =   &H80000008&
      Height          =   4935
      Left            =   2160
      Picture         =   "Form1.frx":1FD4
      ScaleHeight     =   329
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   233
      TabIndex        =   8
      Top             =   3960
      Visible         =   0   'False
      Width           =   3495
      Begin VB.Image Image2 
         Height          =   5760
         Left            =   2760
         Picture         =   "Form1.frx":3AD71
         Top             =   4320
         Visible         =   0   'False
         Width           =   7680
      End
      Begin VB.Image Image1 
         Height          =   5760
         Index           =   2
         Left            =   1080
         Picture         =   "Form1.frx":40C0B
         Top             =   2160
         Visible         =   0   'False
         Width           =   7680
      End
      Begin VB.Image Image1 
         Height          =   5760
         Index           =   1
         Left            =   480
         Picture         =   "Form1.frx":7868B
         Top             =   3240
         Visible         =   0   'False
         Width           =   7680
      End
      Begin VB.Image Image1 
         Height          =   5760
         Index           =   0
         Left            =   1920
         Picture         =   "Form1.frx":ABE01
         Top             =   1680
         Visible         =   0   'False
         Width           =   7680
      End
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      ForeColor       =   &H80000008&
      Height          =   6495
      Left            =   120
      MouseIcon       =   "Form1.frx":E1EFC
      MousePointer    =   99  'Custom
      ScaleHeight     =   433
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   601
      TabIndex        =   0
      Top             =   0
      Width           =   9015
      Begin VB.PictureBox Picture2 
         Appearance      =   0  'Flat
         BackColor       =   &H00000000&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         ForeColor       =   &H80000008&
         Height          =   5655
         Left            =   240
         ScaleHeight     =   377
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   545
         TabIndex        =   1
         Top             =   120
         Width           =   8175
         Begin RMControl7.RMCanvas RMCanvas1 
            Height          =   2295
            Index           =   0
            Left            =   240
            TabIndex        =   2
            Top             =   240
            Visible         =   0   'False
            Width           =   2655
            _ExtentX        =   4683
            _ExtentY        =   4048
         End
         Begin RMControl7.RMCanvas RMCanvas1 
            Height          =   2295
            Index           =   1
            Left            =   4920
            TabIndex        =   5
            Top             =   600
            Visible         =   0   'False
            Width           =   2655
            _ExtentX        =   4683
            _ExtentY        =   4048
         End
         Begin RMControl7.RMCanvas RMCanvas1 
            Height          =   2295
            Index           =   2
            Left            =   1080
            TabIndex        =   6
            Top             =   3120
            Visible         =   0   'False
            Width           =   2655
            _ExtentX        =   4683
            _ExtentY        =   4048
         End
         Begin RMControl7.RMCanvas RMCanvas1 
            Height          =   2295
            Index           =   3
            Left            =   3840
            TabIndex        =   7
            Top             =   2640
            Visible         =   0   'False
            Width           =   2655
            _ExtentX        =   4683
            _ExtentY        =   4048
         End
      End
   End
   Begin WMPLibCtl.WindowsMediaPlayer WindowsMediaPlayer1 
      Height          =   375
      Index           =   3
      Left            =   240
      TabIndex        =   13
      Top             =   6960
      Visible         =   0   'False
      Width           =   2295
      URL             =   ""
      rate            =   1
      balance         =   0
      currentPosition =   0
      defaultFrame    =   ""
      playCount       =   1
      autoStart       =   -1  'True
      currentMarker   =   0
      invokeURLs      =   -1  'True
      baseURL         =   ""
      volume          =   50
      mute            =   0   'False
      uiMode          =   "full"
      stretchToFit    =   0   'False
      windowlessVideo =   0   'False
      enabled         =   -1  'True
      enableContextMenu=   -1  'True
      fullScreen      =   0   'False
      SAMIStyle       =   ""
      SAMILang        =   ""
      SAMIFilename    =   ""
      captioningID    =   ""
      enableErrorDialogs=   0   'False
      _cx             =   4048
      _cy             =   661
   End
   Begin WMPLibCtl.WindowsMediaPlayer WindowsMediaPlayer1 
      Height          =   375
      Index           =   2
      Left            =   240
      TabIndex        =   12
      Top             =   7320
      Visible         =   0   'False
      Width           =   2295
      URL             =   ""
      rate            =   1
      balance         =   0
      currentPosition =   0
      defaultFrame    =   ""
      playCount       =   1
      autoStart       =   -1  'True
      currentMarker   =   0
      invokeURLs      =   -1  'True
      baseURL         =   ""
      volume          =   50
      mute            =   0   'False
      uiMode          =   "full"
      stretchToFit    =   0   'False
      windowlessVideo =   0   'False
      enabled         =   -1  'True
      enableContextMenu=   -1  'True
      fullScreen      =   0   'False
      SAMIStyle       =   ""
      SAMILang        =   ""
      SAMIFilename    =   ""
      captioningID    =   ""
      enableErrorDialogs=   0   'False
      _cx             =   4048
      _cy             =   661
   End
   Begin WMPLibCtl.WindowsMediaPlayer WindowsMediaPlayer1 
      Height          =   375
      Index           =   1
      Left            =   600
      TabIndex        =   4
      Top             =   7560
      Visible         =   0   'False
      Width           =   2295
      URL             =   ""
      rate            =   1
      balance         =   0
      currentPosition =   0
      defaultFrame    =   ""
      playCount       =   1
      autoStart       =   -1  'True
      currentMarker   =   0
      invokeURLs      =   -1  'True
      baseURL         =   ""
      volume          =   50
      mute            =   0   'False
      uiMode          =   "full"
      stretchToFit    =   0   'False
      windowlessVideo =   0   'False
      enabled         =   -1  'True
      enableContextMenu=   -1  'True
      fullScreen      =   0   'False
      SAMIStyle       =   ""
      SAMILang        =   ""
      SAMIFilename    =   ""
      captioningID    =   ""
      enableErrorDialogs=   0   'False
      _cx             =   4048
      _cy             =   661
   End
   Begin WMPLibCtl.WindowsMediaPlayer WindowsMediaPlayer1 
      Height          =   375
      Index           =   0
      Left            =   600
      TabIndex        =   3
      Top             =   7200
      Visible         =   0   'False
      Width           =   2295
      URL             =   ""
      rate            =   1
      balance         =   0
      currentPosition =   0
      defaultFrame    =   ""
      playCount       =   1
      autoStart       =   -1  'True
      currentMarker   =   0
      invokeURLs      =   -1  'True
      baseURL         =   ""
      volume          =   50
      mute            =   0   'False
      uiMode          =   "full"
      stretchToFit    =   0   'False
      windowlessVideo =   0   'False
      enabled         =   -1  'True
      enableContextMenu=   -1  'True
      fullScreen      =   0   'False
      SAMIStyle       =   ""
      SAMILang        =   ""
      SAMIFilename    =   ""
      captioningID    =   ""
      enableErrorDialogs=   0   'False
      _cx             =   4048
      _cy             =   661
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim pathDATAfolder0 As String
Dim LOOPvariable0 As Integer
Dim LOOPvariable1 As Integer
Dim MOUSEcoord(4) As Integer
Dim chanelsCHOIZ(3) As Integer
Dim timeLAG As Integer


Private Sub Form_Load()
pathDEKLARACJA
mediaplayersPOS
End Sub

Private Sub Form_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
'End
End Sub

Private Sub pathDEKLARACJA()
pathDATAfolder0 = App.Path + "\gamedata_0\"
Shell "regsvr32 " + pathDATAfolder0 + "RMControl.ocx" + " /s"
Form1.Picture3.Picture = Form1.Image2.Picture
Form1.Picture4.Picture = Form1.Image2.Picture
Form1.Picture5.Picture = Form1.Image2.Picture
Form1.Picture6.Picture = Form1.Image2.Picture
Form1.Picture3.Visible = True
Form1.Picture4.Visible = True
Form1.Picture5.Visible = True
Form1.Picture6.Visible = True
chanelsCHOIZ(0) = 1
chanelsCHOIZ(1) = 1
chanelsCHOIZ(2) = 1
chanelsCHOIZ(3) = 1
Form2.Label5(4).Caption = Str(chanelsCHOIZ(0))
Form2.Label5(5).Caption = Str(chanelsCHOIZ(1))
Form2.Label5(6).Caption = Str(chanelsCHOIZ(2))
Form2.Label5(7).Caption = Str(chanelsCHOIZ(3))

End Sub
Private Sub mediaplayersPOS()
For LOOPvariable0 = 0 To 3
WindowsMediaPlayer1(LOOPvariable0).Left = -1999
Next LOOPvariable0
'players init sound

WindowsMediaPlayer1(0).URL = pathDATAfolder0 + "switch.mp3"
WindowsMediaPlayer1(0).settings.setMode "loop", False '
'WindowsMediaPlayer1(0).settings.volume = 50
WindowsMediaPlayer1(0).Controls.stop
'WindowsMediaPlayer1(1).Controls.play
'WindowsMediaPlayer1(3).Controls.play
WindowsMediaPlayer1(1).URL = pathDATAfolder0 + "noize.wav"
WindowsMediaPlayer1(1).settings.setMode "loop", True
'WindowsMediaPlayer1(1).settings.volume = 0
WindowsMediaPlayer1(1).Controls.stop





End Sub




Private Sub Form_Resize()
Picture1.Left = 0: Picture2.Left = 0
Picture1.Top = 0: Picture2.Top = 0
Picture1.Height = Form1.ScaleHeight: Picture2.Height = Picture1.Height
Picture1.Width = Form1.ScaleWidth: Picture2.Width = Picture1.Width

For LOOPvariable1 = 0 To 1
RMCanvas1(LOOPvariable1).Top = 0
RMCanvas1(LOOPvariable1).Height = Form1.ScaleHeight \ 2
Next LOOPvariable1
For LOOPvariable1 = 2 To 3
RMCanvas1(LOOPvariable1).Top = Form1.ScaleHeight \ 2
RMCanvas1(LOOPvariable1).Height = Form1.ScaleHeight \ 2
Next LOOPvariable1
RMCanvas1(0).Left = 0: RMCanvas1(2).Left = 0
RMCanvas1(1).Left = Form1.ScaleWidth \ 2
RMCanvas1(3).Left = Form1.ScaleWidth \ 2
For LOOPvariable1 = 0 To 3
RMCanvas1(LOOPvariable1).Width = Form1.ScaleWidth \ 2
Next LOOPvariable1
Form2.Show
Picture3.Enabled = False
Picture3.Width = Form1.ScaleWidth \ 2
Picture3.Height = Form1.ScaleHeight \ 2
Picture3.Left = 0: Picture3.Top = 0


Picture4.Enabled = False
Picture4.Width = Form1.ScaleWidth \ 2
Picture4.Height = Form1.ScaleHeight \ 2
Picture4.Left = Form1.ScaleWidth \ 2: Picture4.Top = 0


Picture5.Enabled = False
Picture5.Width = Form1.ScaleWidth \ 2
Picture5.Height = Form1.ScaleHeight \ 2
Picture5.Left = 0: Picture5.Top = Form1.ScaleHeight \ 2


Picture6.Enabled = False
Picture6.Width = Form1.ScaleWidth \ 2
Picture6.Height = Form1.ScaleHeight \ 2
Picture6.Left = Form1.ScaleWidth \ 2: Picture6.Top = Form1.ScaleHeight \ 2

Form1.Picture3.Visible = True
Form1.Picture4.Visible = True
Form1.Picture5.Visible = True
Form1.Picture6.Visible = True














End Sub

Private Sub Picture1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
MOUSEcoord(4) = X
MOUSEcoord(3) = Y


If MOUSEcoord(4) < Form1.ScaleWidth \ 2 And MOUSEcoord(3) < Form1.ScaleHeight \ 2 Then
Form2.Label5(0).Enabled = True

chanelsCHOIZ(0) = chanelsCHOIZ(0) + 1
Form2.HScroll2(0).Value = Form2.HScroll2(0).Value + 1




'If Form2.Label5(0).Enabled = True Then
Picture3.Visible = True
WindowsMediaPlayer1(1).Controls.play

'End If
End If

If MOUSEcoord(4) >= Form1.ScaleWidth \ 2 And MOUSEcoord(3) < Form1.ScaleHeight \ 2 Then
Form2.Label5(1).Enabled = True
Picture4.Visible = True
WindowsMediaPlayer1(1).Controls.play
chanelsCHOIZ(1) = chanelsCHOIZ(1) + 1
Form2.HScroll2(1).Value = Form2.HScroll2(1).Value + 1

'End If
End If

If MOUSEcoord(4) < Form1.ScaleWidth \ 2 And MOUSEcoord(3) >= Form1.ScaleHeight \ 2 Then
Form2.Label5(2).Enabled = True
Picture5.Visible = True
WindowsMediaPlayer1(1).Controls.play
chanelsCHOIZ(2) = chanelsCHOIZ(2) + 1
Form2.HScroll2(2).Value = Form2.HScroll2(2).Value + 1

'End If
End If


If MOUSEcoord(4) >= Form1.ScaleWidth \ 2 And MOUSEcoord(3) >= Form1.ScaleHeight \ 2 Then
Form2.Label5(3).Enabled = True
Picture6.Visible = True
WindowsMediaPlayer1(1).Controls.play
chanelsCHOIZ(3) = chanelsCHOIZ(3) + 1
Form2.HScroll2(3).Value = Form2.HScroll2(3).Value + 1







End If

If chanelsCHOIZ(0) > 10 Then chanelsCHOIZ(0) = 1
If chanelsCHOIZ(1) > 10 Then chanelsCHOIZ(1) = 1
If chanelsCHOIZ(2) > 10 Then chanelsCHOIZ(2) = 1
If chanelsCHOIZ(3) > 10 Then chanelsCHOIZ(3) = 1

Form2.Label5(4).Caption = Str(chanelsCHOIZ(0))
Form2.Label5(5).Caption = Str(chanelsCHOIZ(1))
Form2.Label5(6).Caption = Str(chanelsCHOIZ(2))
Form2.Label5(7).Caption = Str(chanelsCHOIZ(3))







End Sub

Private Sub Picture1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
Form2.Label7.Caption = Trim(Str(X)) + Chr(13) + Trim(Str(Y)) + Chr(13) + Trim(Str(Button))
End Sub

Private Sub Timer1_Timer()


 timeLAG = timeLAG + 1
If timeLAG > 50 Then
Form2.Label5(0).Enabled = True
Picture3.Visible = True
Form1.RMCanvas1(0).Visible = True
Form1.RMCanvas1(1).Visible = True
Form1.RMCanvas1(2).Visible = True
Form1.RMCanvas1(3).Visible = True
WindowsMediaPlayer1(1).Controls.play
Picture1.Enabled = True
Timer1.Enabled = False
End If



End Sub

