VERSION 5.00
Begin VB.Form Form3 
   BackColor       =   &H00004000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Read Site"
   ClientHeight    =   9300
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   13125
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "Form1.frx":0000
   ScaleHeight     =   9300
   ScaleWidth      =   13125
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command11 
      BackColor       =   &H00808080&
      Caption         =   "Save fragment"
      BeginProperty Font 
         Name            =   "Press Start 2P"
         Size            =   8.25
         Charset         =   238
         Weight          =   500
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3360
      Style           =   1  'Graphical
      TabIndex        =   40
      Top             =   5880
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.CommandButton Command10 
      BackColor       =   &H00808080&
      Caption         =   "color"
      DisabledPicture =   "Form1.frx":53F16
      BeginProperty Font 
         Name            =   "Press Start 2P"
         Size            =   8.25
         Charset         =   238
         Weight          =   500
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4800
      Style           =   1  'Graphical
      TabIndex        =   39
      Top             =   5880
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.CheckBox Check3 
      BackColor       =   &H00808080&
      Caption         =   "Lock"
      DisabledPicture =   "Form1.frx":5CB67
      BeginProperty Font 
         Name            =   "Press Start 2P"
         Size            =   8.25
         Charset         =   238
         Weight          =   500
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   355
      Left            =   6240
      Style           =   1  'Graphical
      TabIndex        =   38
      Top             =   5880
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton Command9 
      BackColor       =   &H00808080&
      Caption         =   "End"
      BeginProperty Font 
         Name            =   "Press Start 2P"
         Size            =   8.25
         Charset         =   238
         Weight          =   500
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   11640
      Style           =   1  'Graphical
      TabIndex        =   37
      Top             =   5880
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.ListBox List8 
      BeginProperty Font 
         Name            =   "Press Start 2P"
         Size            =   8.25
         Charset         =   238
         Weight          =   500
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   5880
      TabIndex        =   33
      Top             =   5880
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.CommandButton Command8 
      BackColor       =   &H00808080&
      Caption         =   "Save  counterSTAT.txt"
      DisabledPicture =   "Form1.frx":657B8
      BeginProperty Font 
         Name            =   "Press Start 2P"
         Size            =   8.25
         Charset         =   238
         Weight          =   500
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8160
      Style           =   1  'Graphical
      TabIndex        =   32
      Top             =   5880
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00808080&
      ForeColor       =   &H80000008&
      Height          =   9135
      Left            =   120
      TabIndex        =   31
      Top             =   0
      Width           =   12855
      Begin VB.TextBox Text8 
         Height          =   7935
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   44
         Top             =   1080
         Width           =   12615
      End
      Begin VB.CommandButton Command12 
         Caption         =   "READ"
         Height          =   255
         Left            =   120
         TabIndex        =   42
         Top             =   720
         Width           =   2055
      End
      Begin VB.TextBox Text7 
         Height          =   285
         Left            =   120
         TabIndex        =   41
         Top             =   240
         Width           =   12135
      End
      Begin VB.PictureBox Picture2 
         Appearance      =   0  'Flat
         BackColor       =   &H00808080&
         ForeColor       =   &H80000008&
         Height          =   5295
         Left            =   -4200
         Picture         =   "Form1.frx":6E409
         ScaleHeight     =   5265
         ScaleWidth      =   12465
         TabIndex        =   35
         Top             =   8640
         Visible         =   0   'False
         Width           =   12495
         Begin VB.Label Label9 
            BackStyle       =   0  'Transparent
            BeginProperty Font 
               Name            =   "Press Start 2P"
               Size            =   12
               Charset         =   238
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C0C0C0&
            Height          =   5175
            Left            =   240
            TabIndex        =   36
            Top             =   480
            Width           =   12375
            WordWrap        =   -1  'True
         End
      End
      Begin VB.Label Label11 
         BackStyle       =   0  'Transparent
         Height          =   7935
         Left            =   120
         TabIndex        =   43
         Top             =   1080
         Width           =   12615
      End
   End
   Begin VB.TextBox Text6 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   600
      TabIndex        =   30
      Text            =   "5"
      Top             =   5160
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.CommandButton Command7 
      Caption         =   "Refresh"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   11520
      TabIndex        =   29
      Top             =   5280
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.TextBox Text5 
      Height          =   4335
      Left            =   12600
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   28
      Text            =   "Form1.frx":19B6DA
      Top             =   6000
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.TextBox Text1 
      Height          =   3135
      Left            =   240
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   27
      Text            =   "Form1.frx":19B6E0
      Top             =   6360
      Visible         =   0   'False
      Width           =   12015
   End
   Begin VB.CommandButton Command6 
      Caption         =   "Delete Site"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   355
      Left            =   9960
      TabIndex        =   26
      Top             =   5280
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.CommandButton Command5 
      Caption         =   "Add Site"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   355
      Left            =   8400
      TabIndex        =   25
      Top             =   5280
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.TextBox Text4 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5280
      TabIndex        =   24
      Top             =   4800
      Visible         =   0   'False
      Width           =   7695
   End
   Begin VB.ListBox List7 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4365
      Left            =   5280
      Style           =   1  'Checkbox
      TabIndex        =   23
      Top             =   240
      Visible         =   0   'False
      Width           =   7695
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Reset Visiblility"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   355
      Left            =   1800
      TabIndex        =   22
      Top             =   5160
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Clear Stats"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   355
      Left            =   6840
      TabIndex        =   21
      Top             =   5280
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.PictureBox Picture1 
      Height          =   8175
      Left            =   4920
      ScaleHeight     =   8115
      ScaleWidth      =   9675
      TabIndex        =   6
      Top             =   8760
      Visible         =   0   'False
      Width           =   9735
      Begin VB.CommandButton Command1 
         Caption         =   "Next"
         Enabled         =   0   'False
         Height          =   255
         Left            =   10080
         TabIndex        =   14
         Top             =   5280
         Visible         =   0   'False
         Width           =   1455
      End
      Begin VB.TextBox Text2 
         Enabled         =   0   'False
         Height          =   1335
         Left            =   1320
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   13
         Top             =   2040
         Visible         =   0   'False
         Width           =   6735
      End
      Begin VB.ListBox List1 
         BackColor       =   &H00C0C0C0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   238
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4140
         Left            =   2040
         TabIndex        =   12
         Top             =   0
         Visible         =   0   'False
         Width           =   2415
      End
      Begin VB.ListBox List2 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   238
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000003&
         Height          =   6060
         Left            =   2520
         TabIndex        =   11
         Top             =   0
         Visible         =   0   'False
         Width           =   1935
      End
      Begin VB.ListBox List3 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   238
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6060
         Left            =   1920
         TabIndex        =   10
         Top             =   0
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.CommandButton Command3 
         Caption         =   "Speach"
         Enabled         =   0   'False
         Height          =   355
         Left            =   9960
         TabIndex        =   9
         Top             =   5400
         Visible         =   0   'False
         Width           =   1455
      End
      Begin VB.TextBox Text3 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   238
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4575
         Left            =   10200
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   8
         Top             =   1560
         Width           =   5775
      End
      Begin VB.ListBox List6 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   238
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   3600
         Left            =   11160
         Style           =   1  'Checkbox
         TabIndex        =   7
         Top             =   120
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.Label Label1 
         BackColor       =   &H00000080&
         BackStyle       =   0  'Transparent
         Caption         =   "website adress..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   238
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C0C0C0&
         Height          =   1335
         Left            =   5880
         TabIndex        =   20
         Top             =   3000
         Visible         =   0   'False
         Width           =   10695
         WordWrap        =   -1  'True
      End
      Begin VB.Label Label2 
         BackColor       =   &H00000080&
         BackStyle       =   0  'Transparent
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   238
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C0C0C0&
         Height          =   975
         Left            =   0
         TabIndex        =   19
         Top             =   1080
         Visible         =   0   'False
         Width           =   5775
      End
      Begin VB.Label Label3 
         Caption         =   "probe 21"
         Height          =   495
         Left            =   10560
         TabIndex        =   18
         Top             =   5160
         Visible         =   0   'False
         Width           =   3375
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "App.Path + ""\SITES.txt"""
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   238
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C0C0C0&
         Height          =   975
         Left            =   0
         TabIndex        =   17
         Top             =   1560
         Visible         =   0   'False
         Width           =   5775
      End
      Begin VB.Label Label7 
         BackStyle       =   0  'Transparent
         Caption         =   "number"
         Height          =   495
         Left            =   3720
         TabIndex        =   16
         Top             =   1920
         Visible         =   0   'False
         Width           =   1455
      End
      Begin VB.Image Image1 
         Height          =   495
         Left            =   6840
         Top             =   3000
         Width           =   1215
      End
      Begin VB.Label Label8 
         Caption         =   "Label8"
         Height          =   495
         Left            =   8760
         TabIndex        =   15
         Top             =   1200
         Visible         =   0   'False
         Width           =   3135
      End
   End
   Begin VB.CheckBox Check2 
      BackColor       =   &H00808080&
      Caption         =   "Boot"
      DisabledPicture =   "Form1.frx":19B6E6
      BeginProperty Font 
         Name            =   "Press Start 2P"
         Size            =   8.25
         Charset         =   238
         Weight          =   500
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   355
      Left            =   240
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   5880
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.ListBox List5 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4875
      Left            =   1800
      Style           =   1  'Checkbox
      TabIndex        =   4
      Top             =   240
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.ListBox List4 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   4875
      Left            =   600
      Style           =   1  'Checkbox
      TabIndex        =   3
      Top             =   240
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.CheckBox Check1 
      BackColor       =   &H00808080&
      Caption         =   "Run"
      DisabledPicture =   "Form1.frx":1A4337
      BeginProperty Font 
         Name            =   "Press Start 2P"
         Size            =   8.25
         Charset         =   238
         Weight          =   500
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   355
      Left            =   1800
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   5880
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   4000
      Left            =   8880
      Top             =   6120
   End
   Begin VB.Label Label10 
      Caption         =   "Label10"
      BeginProperty Font 
         Name            =   "Press Start 2P"
         Size            =   8.25
         Charset         =   238
         Weight          =   500
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9960
      TabIndex        =   34
      Top             =   5880
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "word"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   238
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1440
      TabIndex        =   2
      Top             =   2040
      Visible         =   0   'False
      Width           =   2415
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   "position on list"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   238
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   360
      TabIndex        =   1
      Top             =   1560
      Visible         =   0   'False
      Width           =   1455
   End
End
Attribute VB_Name = "Form3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Declare Function AddFontResourceEx Lib "gdi32.dll" _
    Alias "AddFontResourceExA" (ByVal lpcstr As String, _
    ByVal dword As Long, ByRef DESIGNVECTOR) As Long
Private Const FR_PRIVATE As Long = &H10


Dim popularPos As Long
Dim popularPosCount As Long
Dim sitesLOADED As Boolean
Dim second_run As Boolean
Dim Upload() As String
Dim iUpl As Long
Dim filename1 As String
Dim filename2 As String
Dim bootB As Boolean
Dim sciezka_dop As String
Private Declare Function URLDownloadToFile Lib "urlmon" Alias "URLDownloadToFileA" (ByVal pCaller As Long, ByVal szURL As String, ByVal szFileName As String, ByVal dwReserved As Long, ByVal lpfnCB As Long) As Long
Private Declare Function InternetOpen Lib "wininet" Alias "InternetOpenA" (ByVal sAgent As String, ByVal lAccessType As Long, ByVal sProxyName As String, ByVal sProxyBypass As String, ByVal lFlags As Long) As Long
Private Declare Function InternetCloseHandle Lib "wininet" (ByVal hInet As Long) As Integer
Const FreFile = 1
Dim URLline As Long
Dim Titles() As String
Dim ProbeStart
Dim ProbeTLIST(20000) As String
Dim probeCount(20000) As Long
Dim probeB(20000) As Boolean
Dim probeB2(50) As Long
Dim Fromlist As Long
Dim h2
Dim STEPnr As Long
Dim STASstep(30) As Boolean
'
Dim retVal As Long

'Will hold the path of the audio to open.
Dim filename As String
Dim Filename5 As String
Private Declare Function mciSendString Lib "winmm.dll" Alias "mciSendStringA" (ByVal lpstrCommand As String, ByVal lpstrReturnString As String, ByVal uReturnLength As Long, ByVal hwndCallback As Long) As Long
Public Function GetBodyContent(HTMLContent As String) As String
  Dim Pos1, Pos2
      Pos1 = InStr(1, HTMLContent, "<body", vbTextCompare)
   If Pos1 = 0 Then GetBodyContent = HTMLContent: Exit Function
      Pos2 = InStr(Pos1, HTMLContent, "</body>", vbTextCompare)
   If Pos2 = 0 Then GetBodyContent = HTMLContent: Exit Function
   'cut-out the body
   GetBodyContent = Mid$(HTMLContent, Pos1, Pos2 - Pos1 + Len("</body>"))
   'comment out potentially still contained scriptsections
   GetBodyContent = Replace(Replace(GetBodyContent, "<script", "<!--<script"), "</script>", "</script>-->")
End Function

Private Sub ReadLines()
List7.Clear
Text5.Text = ""
On Error Resume Next
Dim MyLine As String
Open sciezka_dop + "\sites.TXT" For Input As #FreFile
    Do While Not EOF(1)
    If Text1.Text <> "" Then Text1.Text = Text1.Text + vbCrLf
    If Text5.Text <> "" Then Text5.Text = Text5.Text + vbCrLf
    
        Line Input #FreFile, MyLine
        
        If Trim(MyLine) <> "" Then
        Text1.Text = Text1.Text + MyLine
        Text5.Text = Text5.Text + MyLine
        List7.AddItem MyLine
        List7.Selected(List7.ListCount - 1) = True
        List7.ListIndex = URLline
        End If
        
    Loop
Close #FreFile


sitesLOADED = True





End Sub

Private Sub PrepareList()
Text5.Text = ""
Text1.Text = ""
Dim q
For q = 0 To List7.ListCount - 1

If Text5.Text <> "" Then Text5.Text = Text5.Text + vbCrLf
If Text1.Text <> "" Then Text1.Text = Text1.Text + vbCrLf

Text5.Text = Text5.Text + List7.List(q)
If List7.Selected(q) Then Text1.Text = Text1.Text + List7.List(q)
Next q

Text5.Text = Trim(Text5.Text)
Text1.Text = Trim(Text1.Text)

End Sub


Private Sub SaveLinesUrls()
Dim FreFile1
FreFile1 = FreeFile
Open sciezka_dop + "\sites.TXT" For Output As FreFile1
Print #FreFile1, Text5.Text
Close FreFile1

End Sub




Private Sub Check1_Click()
DoEvents
On Error Resume Next
If Check1.Value = 1 Then
'Timer1.Enabled = True
Command1.Enabled = False

Command2.Enabled = False
Command4.Enabled = False
Command5.Enabled = False
Command6.Enabled = False
Command7.Enabled = False
List7.Enabled = False
Text6.Enabled = False
Command8.Enabled = False
Else
Timer1.Enabled = False
Command1.Enabled = True

Command2.Enabled = True
Command4.Enabled = True
Command5.Enabled = True
Command6.Enabled = True
Command7.Enabled = True
List7.Enabled = True
Text6.Enabled = True
Command8.Enabled = True
End If
End Sub

Private Sub Check2_Click()
If Check2.Value = 1 Then
Frame1.Visible = False
Else
Frame1.Visible = True
End If


End Sub



Private Sub Check3_Click()
'
End Sub

Private Sub Command10_Click()
Dim RgBx(2) As Double
RgBx(0) = 256 * Rnd
RgBx(1) = 256 * Rnd
RgBx(2) = 256 * Rnd
Label9.ForeColor = RGB(RgBx(0), RgBx(1), RgBx(2))
Frame1.BackColor = RGB(RgBx(1), RgBx(2), RgBx(0))
End Sub

Private Sub Command11_Click()
Dim HTML_string
Dim FreFile
HTML_string = Trim(Label9.Caption)
FreFile = FreeFile
Open App.Path + "\fragment.txt" For Output As FreFile
Print #FreFile, HTML_string
Close FreFile

End Sub

Private Sub Command12_Click()
InternetGetFile Text7.Text, App.Path + "\filename.txt"



Dim intFile As Integer
Dim strData As String
intFile = FreeFile
Open App.Path + "\filename.txt" For Input As intFile
strData = Input(LOF(intFile), intFile)
'put strData in a textbox, a listbox, or any other control that will contain text values
Close intFile
Form1.TextField.Text = GetBodyContent(HtmlToText(strData))
Text8.Text = Form1.TextField.Text





Form1.Timer5.Enabled = True









End Sub





Private Function HtmlToText(HTMLContent As String) As String
  With CreateObject("HtmlFile") 'the ProgID for the MS-HTML-DocObject is just: "HtmlFile"
    .Write HTMLContent
    HtmlToText = .Body.innertext
  End With
End Function






















Private Sub Command2_Click()
Dim d
For d = 0 To 20000
probeB(d) = True
Next



End Sub

Private Sub Command3_Click()
On Error Resume Next
Form1.TextField.Text = ""
Dim S
For S = 0 To 32
Form1.TextField.Text = Form1.TextField.Text + " " + List5.List(S)
Next
Form1.TextField.Text = Trim(Form1.TextField.Text) + "?"
Label9.Caption = Trim(Form1.TextField.Text)
Form1.Timer5.Enabled = True

'Declare the SpVoice object.
'Dim Voice As SpVoice
'Note - Applications that require handling of SAPI events should declair the
'SpVoice as follows:
'Dim WithEvents Voice As SpVoice




End Sub

Private Sub Command4_Click()
On Error Resume Next
Kill sciezka_dop + "\" + Filename5
Kill sciezka_dop + "\" + "count.TXT"
MsgBox "Counter is empty. Restart program"
End Sub

Private Sub Command5_Click()
If Text4.Text <> "" Then
List7.AddItem Text4.Text, 0
List7.Selected(0) = True
Text4.Text = ""
Check1.Enabled = False
End If
End Sub

Private Sub Command6_Click()
Dim q
For q = 0 To List7.ListCount - 1
If List7.ListIndex = q Then List7.RemoveItem (q)
Check1.Enabled = False
Next q
End Sub

Private Sub Command7_Click()
If sitesLOADED Then
PrepareList
SaveLinesUrls
StatTitlesLetters
URLline = 0
List7.ListIndex = URLline
Check1.Enabled = True
End If
End Sub

Private Sub Command8_Click()
Check1.Enabled = False
Check2.Enabled = False
Command8.Enabled = False
Label10.Visible = True

Label10.Caption = "WAIT"
List8.Clear

Dim SaveFile As String
Dim q
Dim QL
Dim POSSET As Boolean

List8.AddItem 0, 0
For q = 1 To 1550
Label10.Caption = "ADDING WORD" + Str(q)
POSSET = False
DoEvents
For QL = 0 To List8.ListCount - 1

If ProbeTLIST(q) <> "" And probeCount(q) <> 0 Then
If probeCount(q) > probeCount(List8.List(QL)) And ProbeTLIST(q) <> ProbeTLIST(List8.List(QL)) Then
List8.AddItem q, QL
POSSET = True
Exit For
End If
End If


Next QL


If POSSET = False And ProbeTLIST(q) <> "" And probeCount(q) <> 0 Then List8.AddItem q


Next q

DoEvents

For QL = 0 To List8.ListCount - 1
Label10.Caption = "CREATING FILE"
If SaveFile <> "" Then SaveFile = SaveFile + vbCrLf

SaveFile = SaveFile + ProbeTLIST(List8.List(QL)) + vbTab + Str(probeCount(List8.List(QL)))

Next
'If SaveFile <> "" Then SaveFile = SaveFile + vbCrLf
'List5.AddItem ProbeTLIST(h), h2 '":(", h2 'ProbeTLIST(h), h2
'List4.AddItem probeCount(h), h2

'SaveFile = SaveFile + ProbeTLIST(q) + vbTab + Str(probeCount(q))
'For h = 0 To 20000
'For h2 = 0 To 32

'If probeCount(h) > List4.List(h2) And ProbeTLIST(h) <> List5.List(h2) Then




Label10.Caption = "SAVED"
Dim FreFile1
FreFile1 = FreeFile
Open App.Path + "\counterSTAT.txt" For Output As FreFile1
Print #FreFile1, SaveFile
Close FreFile1









Command8.Enabled = True
Check1.Enabled = True
Check2.Enabled = True
End Sub

Private Sub Command9_Click()
End
End Sub

Private Sub Form_Load()
On Error Resume Next
Exit Sub


Randomize Rnd
'
AddFontResourceEx App.Path + Trim("\atari full.ttf"), FR_PRIVATE, 0&
Label9.Font = "Atari Font Full Version"
Label9.Font.Size = 19
Label9.Font.Italic = False
Label9.Font.Bold = True
Label9.Font.Underline = False
Label9.Font.Strikethrough = False
Randomize
Dim RgBx(2) As Double
RgBx(0) = 256 * Rnd
RgBx(1) = 256 * Rnd
RgBx(2) = 256 * Rnd
Label9.ForeColor = RGB(RgBx(0), RgBx(1), RgBx(2))
Frame1.BackColor = RGB(RgBx(1), RgBx(2), RgBx(0))
'Label9.Caption = "TEXT"

Check2.Font = "Atari Font Full Version"
Check2.Font.Size = 9
Check2.Font.Italic = False
Check2.Font.Bold = True
Check2.Font.Underline = False
Check2.Font.Strikethrough = False
Check2.ForeColor = vbGreen

Check1.Font = "Atari Font Full Version"
Check1.Font.Size = 9
Check1.Font.Italic = False
Check1.Font.Bold = True
Check1.Font.Underline = False
Check1.Font.Strikethrough = False
Check1.ForeColor = vbGreen


Command8.Font = "Atari Font Full Version"
Command8.Font.Size = 9
Command8.Font.Italic = False
Command8.Font.Bold = True
Command8.Font.Underline = False
Command8.Font.Strikethrough = False
'Command8.ForeColor = vbBlue


Command9.Font = "Atari Font Full Version"
Command9.Font.Size = 9
Command9.Font.Italic = False
Command9.Font.Bold = True
Command9.Font.Underline = False
Command9.Font.Strikethrough = False























popularPos = -1
MkDir App.Path + "\protocols.txt"
sciezka_dop = App.Path + "\protocols.txt"

Filename5 = Format(Date, "MMMM") & Format(Date, "DDDD") & Format(Date, "YYYY") & Format(Time, "HH") & Format(Time, "NN") & Format(Time, "SS") & ".txt"
Filename5 = "archive.TXT"
Form3.Caption = "20000 keywords"










filename1 = Trim(Date$) + Trim(DatePart("m", Now)) + Trim("_list.txt")
filename2 = Trim(Date$) + Trim(DatePart("m", Now)) + Trim("_count.txt")









retVal = mciSendString("open " & "hull.mp3" & " type mpegvideo alias songNumber1", vbNullString, 0, 0)
retVal = mciSendString("play songNumber1 repeat", vbNullString, 0, 0)
mciSendString "setaudio songNumber1 volume to " & 500, vbNullString, 0, 0




DoEvents
On Error Resume Next
'Form1.Show


LOADfirst


Text1.Text = ""
Label2.Caption = ""

ReadLines
 ' Debug.Print HtmlToText("<html><body>Some Body-Text</body</html>")
  
 ' Debug.Print HtmlToText("<div>div-text with umlauts &auml;&ouml;&uuml;</div>")

StatTitlesLetters

Check1.Value = 1



'Check1_Click
End Sub
Function InternetGetFile(sURLFileName As String, sSaveToFile As String, Optional bOverwriteExisting As Boolean = False) As Boolean
   DoEvents
On Error Resume Next
    Dim lRet As Long
    Const S_OK As Long = 0, E_OUTOFMEMORY = &H8007000E
    Const INTERNET_OPEN_TYPE_PRECONFIG = 0, INTERNET_FLAG_EXISTING_CONNECT = &H20000000
    Const INTERNET_OPEN_TYPE_DIRECT = 1, INTERNET_OPEN_TYPE_PROXY = 3
    Const INTERNET_FLAG_RELOAD = &H80000000
    
    On Error Resume Next
    'Create an internet connection
    lRet = InternetOpen("", INTERNET_OPEN_TYPE_DIRECT, vbNullString, vbNullString, 0)
    
    If bOverwriteExisting Then
        If Len(Dir$(sSaveToFile)) Then
            VBA.Kill sSaveToFile
        End If
    End If
    'Check file doesn't already exist
    If Len(Dir$(sSaveToFile)) = 0 Then
        'Download file
        lRet = URLDownloadToFile(0&, sURLFileName, sSaveToFile, 0&, 0)
        If Len(Dir$(sSaveToFile)) Then
            'File successfully downloaded
            InternetGetFile = True
        Else
            'Failed to download file
            If lRet = E_OUTOFMEMORY Then
                Debug.Print "The buffer length is invalid or there was insufficient memory to complete the operation."
            Else
                'Debug.Assert False
                Debug.Print "Error occurred " & lRet & " (this is probably a proxy server error)."
            End If
            InternetGetFile = False
        End If
    End If
    On Error GoTo 0
    
End Function




Private Sub StatTitlesLetters()
Dim c_a As Long
Dim c_b As Long
Dim c_c As Long
Dim c_d As Long
Dim c_e As Long
Dim c_f As Long
Dim c_g As Long
Dim c_h As Long







Titles() = Split(Trim(Text1.Text), vbCrLf)









Dim i
For i = 0 To UBound(Titles)
Dim q
For q = 1 To Len(Titles(i))





'add letter and count

Next
Next


End Sub

Private Sub Form_Unload(Cancel As Integer)
''''''Shell App.Path + "\LONGEVITY STATS.exe" 'dziala!
retVal = mciSendString("close songNumber1", vbNullString, 0, 0)
End
End Sub

Private Sub List5_ItemCheck(Item As Integer)
    'Dim i As Long
On Error Resume Next
    'For i = 0 To List5.ListCount - 1    'loop through the items in the ListBox
        If List5.Selected(Item) = False Then
        List4.Selected(Item) = False
        List6.Selected(Item) = False
        
        ' if the item is selected(checked)
        probeB(List6.List(Item)) = False
        Else
        List4.Selected(Item) = True
        probeB(List6.List(Item)) = True
        List6.Selected(Item) = True
          '  MsgBox List5.List(i)        ' display the item
        End If
    'Next
    'List5.Enabled = False
End Sub

Private Sub List7_Click()
'Check1.Enabled = False
End Sub

Private Sub List7_ItemCheck(Item As Integer)
'Check1.Enabled = False
End Sub

Private Sub Timer1_Timer()

DoEvents
On Error Resume Next
'LoadList


If STASstep(2) = True And STASstep(3) = False Then
Dim intFile As Integer
Dim strData As String
intFile = FreeFile
Open sciezka_dop + "\filename.txt" For Input As intFile
strData = Input(LOF(intFile), intFile)
'put strData in a textbox, a listbox, or any other control that will contain text values
Close intFile
STASstep(3) = True
End If
If STASstep(1) = True And STASstep(2) = False Then


InternetGetFile Titles(URLline), sciezka_dop + "\filename.txt"


STASstep(2) = True



End If
If STASstep(0) = True And STASstep(1) = False Then
On Error Resume Next
Kill sciezka_dop + "\filename.txt"
STASstep(1) = True
End If
If STASstep(0) = False Then

If Titles(URLline) <> "" Then

Randomize ProbeStart
Label1.Caption = Titles(URLline) + vbCrLf + Str(URLline)
STASstep(0) = True

Else

STASstep(5) = True
End If



End If




If STASstep(3) = True And STASstep(4) = False Then
'If URLline <> 17 And URLline <> 0 And URLline <> 39 Then
strData = HtmlToText(strData)
STASstep(4) = True
End If










If STASstep(4) = True And STASstep(5) = False Then
Text2.Text = strData
STASstep(5) = True
End If


If STASstep(5) = True And STASstep(6) = False Then

If List7.List(URLline) <> "" Then
List7.ListIndex = URLline
Dim f

For f = 0 To List7.ListCount - 1

If List7.List(f) = Titles(URLline) Then List7.Selected(f) = True

Next f

ocurencesSTR
STASstep(6) = True
End If


URLline = URLline + 1


If URLline > UBound(Titles) Then URLline = 0








End If

If STASstep(6) = True Then
Dim r

For r = 0 To 6
STASstep(r) = False
Next
End If


End Sub
Private Sub ocurencesSTR()
DoEvents
On Error Resume Next
Dim words() As String
Dim ProbeT(30) As String

'LoadList
'Next

List1.Clear
List2.Clear
List3.Clear
List4.Clear
List5.Clear
List6.Clear
'List4.Clear
'For h2 = 0 To 30
'List4.AddItem 0
'List5.AddItem "XXXXX"
'''''''Text2.Text

If Not second_run Then

'LoadList'LoadList'LoadList'LoadList
LoadList

second_run = True
End If
words() = Split(Trim(Text2.Text), " ")

Label2.Caption = "/" & Str(UBound(words)) '+ " total words on 1 website"


ProbeStart = (Rnd * (UBound(words) - 30))
Dim h
Dim o



Dim k
k = -1



'LoadList





Dim pozycja As Boolean
For o = 0 To 30
ProbeT(o) = words(ProbeStart + o)
ProbeT(o) = LCase(ProbeT(o))
'Text3.Text = Text3.Text + ProbeT(o) + vbCrLf
List1.AddItem ProbeT(o) '[X[]>
 pozycja = False
 
 
 
 
 
 
 'LoadList
 
 
 
 
 
 
 
 
For h = 0 To 20000
DoEvents














If ((ProbeTLIST(h) = ProbeT(o)) Or ProbeTLIST(h) = "") And pozycja = False Then

If ProbeTLIST(h) = ProbeT(o) Then
probeCount(h) = probeCount(h) + 1
End If

If ProbeTLIST(h) = "" Then
If Len(ProbeT(o)) < 17 Then
ProbeTLIST(h) = ProbeT(o)
probeB(h) = True
probeCount(h) = 1
End If
End If

k = h
pozycja = True
End If



Next h






'////////////////////////////////////////////////
List2.AddItem k '<[]X]
If pozycja <> True Then List3.AddItem 0
If pozycja = True Then List3.AddItem probeCount(k)





Next




'Dim managed3 As Boolean


For h = 0 To 20000
For h2 = 0 To 32
DoEvents
If List5.ListCount - 1 > 32 Then
If List4.ListCount - 1 > 32 Then List4.RemoveItem (List4.ListCount - 1)
List5.RemoveItem (List5.ListCount - 1)
End If
If probeCount(h) > List4.List(h2) And ProbeTLIST(h) <> List5.List(h2) Then


If probeB(h) = True Then
probeB2(h2) = h
List5.AddItem ProbeTLIST(h), h2 '":(", h2 'ProbeTLIST(h), h2
List4.AddItem probeCount(h), h2
List6.AddItem h, h2
List5.Selected(h2) = True
List4.Selected(h2) = True
List6.Selected(h2) = True
List5.ListIndex = -1
List4.ListIndex = -1
List6.ListIndex = -1



End If
'If List5.ListCount > 20 Then
'List4.RemoveItem (List4.ListCount - 1)
'List5.RemoveItem (List5.ListCount - 1)
'managed3 = True
'End If
'If List4.ListCount > 10 Then

'If List4.List(0) = 0 Or List4.List(0) = "0" Then
'List4.RemoveItem (0)
'List5.RemoveItem (0)
'List4.RemoveItem (List4.ListCount)
'List4.Refresh
'End If

'End If






Exit For
'Else


'If ProbeTLIST(h) <> List5.List(h2) And probeCount(h) < List4.List(h2) Then
'List4.AddItem probeCount(h), h2 + 1
'List5.AddItem ProbeTLIST(h), h2 + 1
'End If
End If



Next h2




Next h

List5.Enabled = True

STEPnr = STEPnr + 1
Label2 = Str(STEPnr) + Label2
Label8.Caption = List5.ListCount
Label1.Caption = Label1.Caption + vbCrLf + "OK"
'/////////////////////
On Error Resume Next
Form1.TextField.Text = ""
Dim S
For S = 0 To 32
Form1.TextField.Text = Form1.TextField.Text + " " + List5.List(S)
Next
Form1.TextField.Text = Trim(Form1.TextField.Text) ' + " ?"
Form1.TextField.Text = Replace(Form1.TextField.Text, (vbCrLf + vbCrLf), vbCrLf)
Form1.TextField.Text = Replace(Form1.TextField.Text, vbTab, "")
Label9.Caption = Trim(Form1.TextField.Text)





'SAVE
Dim FreFile1 As Integer
Dim FreFile2 As Integer
Dim HTML_string1
'HTML_string1 = Join(ProbeTLIST(), " ")
'HTML_string1 = Trim(HTML_string1)
'HTML_string1 = Replace(HTML_string1, vbCrLf + vbCrLf, " ")
'HTML_string1 = Replace(HTML_string1, vbCrLf, " ")
'HTML_string1 = Replace(HTML_string1, vbTab, " ")
'HTML_string1 = Replace(HTML_string1, "  ", " ")

'SAVEList
Dim HTML_string2
Dim N
For N = 0 To 20000

'If ProbeTLIST(N) <> "" And ProbeTLIST(N) <> " " And ProbeTLIST(N) <> vbCrLf And ProbeTLIST(N) <> vbCrLf + vbCrLf And ProbeTLIST(N) <> vbTab Then

HTML_string1 = HTML_string1 + (ProbeTLIST(N)) + "&+_"
HTML_string2 = HTML_string2 + Str(probeCount(N)) + "&+_"
'End If

Next
HTML_string2 = Trim(HTML_string2)
HTML_string1 = Trim(HTML_string1)


FreFile1 = FreeFile
Open sciezka_dop + "\" + Filename5 For Output As FreFile1
Print #FreFile1, HTML_string1
Close FreFile1
FreFile1 = FreeFile
Open sciezka_dop + "\" + "count.TXT" For Output As FreFile1
Print #FreFile1, HTML_string2
Close FreFile1
'SAVEListCount






Form1.Timer5.Enabled = True



mostPopular


End Sub
Private Sub LoadList()
'SAVE
Dim FreFile1 As Integer

'Dim FreFile2 As Integer
Dim HTML_string1
'
FreFile1 = FreeFile
'Open sciezka_dop + "\" + Filename5 For Input As FreFile1
'HTML_string1 = Input(LOF(FreFile1), FreFile1)
'Close FreFile1
'HTML_string1 = Join(ProbeTLIST(), " ")
'HTML_string1 = Trim(HTML_string1)
'HTML_string1 = Replace(HTML_string1, vbCrLf + vbCrLf, " ")
'HTML_string1 = Replace(HTML_string1, vbCrLf, " ")
'HTML_string1 = Replace(HTML_string1, vbTab, " ")
'HTML_string1 = Replace(HTML_string1, "  ", " ")
'FreFile1 = FreeFile
'Open sciezka_dop + "\" + Filename5 For Output As FreFile1
'Print #FreFile1, HTML_string1
'Close FreFile1
'HTML_string1 = Replace(HTML_string1, "&+_", " ")

'SAVE
'LAODING

'Text1.Text = HTML_string2
'Dim FreFile1 As Integer
'Dim FreFile2 As Integer
'Dim HTML_string1
'Dim HTML_string2




'Dim Upload(20000) As String
'Dim iUpl As Long

'List5.AddItem ProbeTLIST(h), h2 '":(", h2 'ProbeTLIST(h), h2
'List4.AddItem probeCount(h), h2
'HTML_string = Join(List5.ListIndex(), " ")
'SAVINGList5.AddItem ProbeTLIST(h), h2 '":(", h2 'ProbeTLIST(h), h2
'''List4.AddItem probeCount(h), h2
'''HTML_string = Trim(Text1.Text


'FreFile1 = FreeFile
'Open sciezka_dop + "\list.txt" For Input As FreFile1
'HTML_string1 = Input(LOF(FreFile1), FreFile1)
'Close FreFile1


'FreFile2 = FreeFile
'Open sciezka_dop + "\count.txt" For Input As FreFile2
'HTML_string2 = Input(LOF(FreFile2), FreFile2)
'Close FreFile2



'FreFile2 = FreeFile
'Open App.Path + "\count.txt" For Output As FreFile2
'Print #FreFile2, HTML_string2
'\\\\\\\\\\\\\\\\\\\\\\'Close FreFile2
'.X.D.X.


'Dim f
'Dim HTML1SPLIT()
'Dim HTML2SPLIT()
'HTML1SPLIT() = Split(HTML_string1, " ")
'HTML2SPLIT() = Split(HTML_string2, " ")
'For f = 0 To 20000

'ProbeTLIST(f) = HTML1SPLIT(f)
'probeCount(f) = HTML2SPLIT(f)

'HTML_string2 = HTML_string2 + Str(probeCount(f)) + " "
'Next f




'HTML_string1 = Join(ProbeTLIST(), " ")
'HTML_string2 = Join(probeCount(), " ")

'HTML_string1 = Trim(HTML_string1)
'HTML_string2 = Trim(HTML_string2)




'.X.D.X.
'\\\\\\\\\\\\\\\\\\\\\\
'''WebBrowser1.Navigate file_path
'LAODING
'FreFile = FreeFile
'Open Trim(App.Path) + Trim("\WEBSITE.HTM") For Input As FreFile
'HTML_string2 = Input(LOF(FreFile), FreFile)
'Close FreFile
'Text1.Text = HTML_string2

'load'load'load'load'load'load'load'load'load'load
'load'load'load'load'load'load'load'load'load'load

Text2.Text = HTML_string1
Text3.Text = HTML_string1

'Upload = Split(HTML_string1, " ")

'For iUpl = 0 To UBound(Upload)
'ProbeTLIST(iUpl) = Upload(iUpl) '":(", h2 'ProbeTLIST(h), h2
'probeCount(iUpl) = 25

'Next iUpl
'load'load'load'load'load'load'load'load'load'load
'load'load'load'load'load'load'load'load'load'load
End Sub
Private Sub LOADfirst()

Dim FreFile1 As Integer
Dim HTML_string1
Dim HTML_string2
Dim STRINGsplit1() As String
Dim STRINGsplit2() As String
On Error Resume Next
FreFile1 = FreeFile
Open sciezka_dop + "\" + Filename5 For Input As FreFile1
HTML_string1 = Input(LOF(FreFile1), FreFile1)
Close FreFile1
FreFile1 = FreeFile
Open sciezka_dop + "\" + "count.TXT" For Input As FreFile1
HTML_string2 = Input(LOF(FreFile1), FreFile1)
Close FreFile1



STRINGsplit1() = Split(Trim(HTML_string1), "&+_")
STRINGsplit2() = Split(Trim(HTML_string2), "&+_")

Dim g
For g = 0 To UBound(STRINGsplit1())

'If STRINGsplit1(g) <> "" And STRINGsplit1(g) <> " " Then
ProbeTLIST(g) = STRINGsplit1(g)
probeCount(g) = Val(STRINGsplit2(g))
probeB(g) = True
'End If
Next

End Sub


Private Sub mostPopular()
If popularPos = List6.List(0) Then
'Dim popularPos As Long
popularPosCount = popularPosCount + 1
Else
popularPos = List6.List(0)
popularPosCount = 1
End If

If popularPosCount > Val(Text6.Text) And Check3.Value = 0 Then probeB(List6.List(0)) = False

End Sub


Private Sub read()







DoEvents
On Error Resume Next
'LoadList


If STASstep(2) = True And STASstep(3) = False Then
Dim intFile As Integer
Dim strData As String
intFile = FreeFile
'Open sciezka_dop + "\filename.txt" For Input As intFile
'strData = Input(LOF(intFile), intFile)
'put strData in a textbox, a listbox, or any other control that will contain text values
'Close intFile
STASstep(3) = True
End If
If STASstep(1) = True And STASstep(2) = False Then


InternetGetFile Text7.Text, sciezka_dop + "\filename.txt"


STASstep(2) = True



End If
If STASstep(0) = True And STASstep(1) = False Then
On Error Resume Next
'Kill sciezka_dop + "\filename.txt"
STASstep(1) = True
End If
If STASstep(0) = False Then

If Titles(URLline) <> "" Then

Randomize ProbeStart
Label1.Caption = Titles(URLline) + vbCrLf + Str(URLline)
STASstep(0) = True

Else

STASstep(5) = True
End If



End If




If STASstep(3) = True And STASstep(4) = False Then
'If URLline <> 17 And URLline <> 0 And URLline <> 39 Then
strData = HtmlToText(strData)
STASstep(4) = True
End If










If STASstep(4) = True And STASstep(5) = False Then
Text2.Text = strData
STASstep(5) = True
End If


If STASstep(5) = True And STASstep(6) = False Then

If List7.List(URLline) <> "" Then
List7.ListIndex = URLline
Dim f

For f = 0 To List7.ListCount - 1

If List7.List(f) = Titles(URLline) Then List7.Selected(f) = True

Next f

ocurencesSTR
STASstep(6) = True
End If


URLline = URLline + 1


If URLline > UBound(Titles) Then URLline = 0








End If

If STASstep(6) = True Then
Dim r

For r = 0 To 6
STASstep(r) = False
Next
End If














End Sub


