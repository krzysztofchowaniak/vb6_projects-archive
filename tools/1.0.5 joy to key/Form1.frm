VERSION 5.00
Begin VB.Form Form1 
   AutoRedraw      =   -1  'True
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "KeyPad v1.0.5"
   ClientHeight    =   5790
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   8355
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   386
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   557
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox Check1 
      BackColor       =   &H00808080&
      Caption         =   "Off - Edit\ On - Save"
      Height          =   375
      Left            =   120
      TabIndex        =   11
      Top             =   1920
      Value           =   1  'Checked
      Width           =   3255
   End
   Begin VB.PictureBox Picture2 
      Enabled         =   0   'False
      Height          =   3255
      Left            =   120
      ScaleHeight     =   3195
      ScaleWidth      =   7995
      TabIndex        =   2
      Top             =   2400
      Width           =   8055
      Begin VB.TextBox Text8 
         Height          =   285
         Left            =   5160
         TabIndex        =   10
         Text            =   "4"
         Top             =   720
         Width           =   375
      End
      Begin VB.TextBox Text7 
         Height          =   285
         Left            =   5640
         TabIndex        =   9
         Text            =   "3"
         Top             =   1080
         Width           =   375
      End
      Begin VB.TextBox Text6 
         Height          =   285
         Left            =   4680
         TabIndex        =   8
         Text            =   "2"
         Top             =   1080
         Width           =   375
      End
      Begin VB.TextBox Text5 
         Height          =   285
         Left            =   5160
         TabIndex        =   7
         Text            =   "1"
         Top             =   1440
         Width           =   375
      End
      Begin VB.TextBox Text4 
         Height          =   285
         Left            =   2400
         TabIndex        =   6
         Text            =   "q"
         Top             =   1320
         Width           =   375
      End
      Begin VB.TextBox Text3 
         Height          =   285
         Left            =   1920
         TabIndex        =   5
         Text            =   "o"
         Top             =   1800
         Width           =   375
      End
      Begin VB.TextBox Text2 
         Height          =   285
         Left            =   2400
         TabIndex        =   4
         Text            =   "a"
         Top             =   2250
         Width           =   375
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Left            =   3000
         TabIndex        =   3
         Text            =   "p"
         Top             =   1770
         Width           =   375
      End
      Begin VB.Image Image1 
         Height          =   3255
         Left            =   120
         Picture         =   "Form1.frx":324A
         Stretch         =   -1  'True
         Top             =   120
         Width           =   6735
      End
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      FillStyle       =   0  'Solid
      ForeColor       =   &H80000008&
      Height          =   2055
      Left            =   5760
      ScaleHeight     =   137
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   161
      TabIndex        =   0
      Top             =   240
      Width           =   2415
   End
   Begin VB.Timer Timer1 
      Interval        =   1
      Left            =   3840
      Top             =   1200
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   2055
      Left            =   3480
      TabIndex        =   1
      Top             =   240
      Width           =   2175
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim KEYpadkeys(8)
Const KEYEVENTF_EXTENDEDKEY = &H1
Const KEYEVENTF_KEYUP = &H2
Private Declare Sub keybd_event Lib "user32.dll" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Long, ByVal dwExtraInfo As Long)

Dim keypressed(255) As Boolean

Private Const JOY_RETURNBUTTONS As Long = &H80&
Private Const JOY_RETURNCENTERED As Long = &H400&
Private Const JOY_RETURNPOV As Long = &H40&
Private Const JOY_RETURNPOVCTS As Long = &H200&
Private Const JOY_RETURNR As Long = &H8&
Private Const JOY_RETURNRAWDATA As Long = &H100&
Private Const JOY_RETURNU As Long = &H10
Private Const JOY_RETURNV As Long = &H20
Private Const JOY_RETURNX As Long = &H1&
Private Const JOY_RETURNY As Long = &H2&
Private Const JOY_RETURNZ As Long = &H4&
Private Const JOY_RETURNALL As Long = (JOY_RETURNX Or JOY_RETURNY Or JOY_RETURNZ Or JOY_RETURNR Or JOY_RETURNU Or JOY_RETURNV Or JOY_RETURNPOV Or JOY_RETURNBUTTONS)

Private Type JOYINFOEX
    dwSize As Long ' size of structure
    dwFlags As Long ' flags to dicate what to return
    dwXpos As Long ' x position
    dwYpos As Long ' y position
    dwZpos As Long ' z position
    dwRpos As Long ' rudder/4th axis position
    dwUpos As Long ' 5th axis position
    dwVpos As Long ' 6th axis position
    dwButtons As Long ' button states
    dwButtonNumber As Long ' current button number pressed
    dwPOV As Long ' point of view state
    dwReserved1 As Long ' reserved for communication between winmm driver
    dwReserved2 As Long ' reserved for future expansion
End Type

Private Declare Function joyGetPosEx Lib "winmm.dll" (ByVal uJoyID As Long, ByRef pji As JOYINFOEX) As Long

Dim JI As JOYINFOEX

Const JNum As Long = 0
'Set this to the number of the joystick that
'you want to read (a value between 0 and 15).
'The first joystick plugged in is number 0.
'The API for reading joysticks supports up to
'16 simultaniously plugged in joysticks.
'Change this Const to a Dim if you want to set
'it at runtime.



Private Sub Check1_Click()
SAVEcfg
allkeysUP

If Check1.Value = 1 Then
Picture2.Enabled = False
Form1.Caption = "KeyPad v1.0.5"
Picture1.SetFocus
Else
Picture2.Enabled = True
Form1.Caption = "KeyPad v1.0.5[paused]"
End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
keypressed(KeyCode) = True
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
keypressed(KeyCode) = False
End Sub
Private Sub SAVEcfg()
Dim freFILE
freFILE = FreeFile
Open App.Path + "\KeyPad.cfg" For Random As freFILE

If Text1.Text <> "" Then Put freFILE, 1, Text1.Text
If Text2.Text <> "" Then Put freFILE, 2, Text2.Text
If Text3.Text <> "" Then Put freFILE, 3, Text3.Text
If Text4.Text <> "" Then Put freFILE, 4, Text4.Text
If Text5.Text <> "" Then Put freFILE, 5, Text5.Text
If Text6.Text <> "" Then Put freFILE, 6, Text6.Text
If Text7.Text <> "" Then Put freFILE, 7, Text7.Text
If Text8.Text <> "" Then Put freFILE, 8, Text8.Text

Close freFILE
End Sub
Private Sub Form_Load()

KEYpadkeys(1) = " "
KEYpadkeys(2) = " "
KEYpadkeys(3) = " "
KEYpadkeys(4) = " "
KEYpadkeys(5) = " "
KEYpadkeys(6) = " "
KEYpadkeys(7) = " "
KEYpadkeys(8) = " "

Dim VrL As String
Dim freFILE
freFILE = FreeFile
Open App.Path + "\KeyPad.cfg" For Random As freFILE
On Error Resume Next
Get freFILE, 1, VrL
Text1.Text = VrL
Get freFILE, 2, VrL
Text2.Text = VrL
Get freFILE, 3, VrL
Text3.Text = VrL
Get freFILE, 4, VrL
Text4.Text = VrL
Get freFILE, 5, VrL
Text5.Text = VrL
Get freFILE, 6, VrL
Text6.Text = VrL
Get freFILE, 7, VrL
Text7.Text = VrL
Get freFILE, 8, VrL
Text8.Text = VrL
Close freFILE

If Text1.Text <> "" Then KEYpadkeys(1) = Text1.Text
If Text2.Text <> "" Then KEYpadkeys(2) = Text2.Text
If Text3.Text <> "" Then KEYpadkeys(3) = Text3.Text
If Text4.Text <> "" Then KEYpadkeys(4) = Text4.Text
If Text5.Text <> "" Then KEYpadkeys(5) = Text5.Text
If Text6.Text <> "" Then KEYpadkeys(6) = Text6.Text
If Text7.Text <> "" Then KEYpadkeys(7) = Text7.Text
If Text8.Text <> "" Then KEYpadkeys(8) = Text8.Text


JI.dwSize = Len(JI)
JI.dwFlags = JOY_RETURNALL
End Sub
Private Sub allkeysUP()

keybd_event Asc(UCase(KEYpadkeys(1))), 0, KEYEVENTF_KEYUP, 0  ' release
keybd_event Asc(UCase(KEYpadkeys(2))), 0, KEYEVENTF_KEYUP, 0  ' release
keybd_event Asc(UCase(KEYpadkeys(3))), 0, KEYEVENTF_KEYUP, 0  ' release
keybd_event Asc(UCase(KEYpadkeys(4))), 0, KEYEVENTF_KEYUP, 0  ' release
keybd_event Asc(UCase(KEYpadkeys(5))), 0, KEYEVENTF_KEYUP, 0  ' release
keybd_event Asc(UCase(KEYpadkeys(6))), 0, KEYEVENTF_KEYUP, 0  ' release
keybd_event Asc(UCase(KEYpadkeys(7))), 0, KEYEVENTF_KEYUP, 0  ' release
keybd_event Asc(UCase(KEYpadkeys(8))), 0, KEYEVENTF_KEYUP, 0  ' release

End Sub
Private Sub Form_Resize()

allkeysUP



End Sub

Private Sub Form_Unload(Cancel As Integer)
allkeysUP
End Sub

Private Sub Label2_Click()
Dim res
res = Shell("explorer.exe " & App.Path + "\donate_ paypal_USD.htm", vbNormalFocus)
End Sub

Private Sub Picture1_KeyDown(KeyCode As Integer, Shift As Integer)
keypressed(KeyCode) = True
'If KeyCode = 27 Then endProcedure
End Sub

Private Sub Picture1_KeyUp(KeyCode As Integer, Shift As Integer)
keypressed(KeyCode) = False
End Sub

Private Sub Text1_Change()
If Len(Text1.Text) > 1 Then Text1.Text = Mid(Text1.Text, Len(Text1.Text) - 1, 1)
If Text1.Text <> "" Then KEYpadkeys(1) = Text1.Text

End Sub

Private Sub Text2_Change()
If Len(Text2.Text) > 1 Then Text2.Text = Mid(Text2.Text, Len(Text2.Text) - 1, 1)
If Text2.Text <> "" Then KEYpadkeys(2) = Text2.Text

End Sub

Private Sub Text3_Change()
If Len(Text3.Text) > 1 Then Text3.Text = Mid(Text3.Text, Len(Text3.Text) - 1, 1)
If Text3.Text <> "" Then KEYpadkeys(3) = Text3.Text

End Sub

Private Sub Text4_Change()
If Len(Text4.Text) > 1 Then Text4.Text = Mid(Text4.Text, Len(Text4.Text) - 1, 1)
If Text4.Text <> "" Then KEYpadkeys(4) = Text4.Text

End Sub

Private Sub Text5_Change()
If Len(Text5.Text) > 1 Then Text5.Text = Mid(Text5.Text, Len(Text5.Text) - 1, 1)
If Text5.Text <> "" Then KEYpadkeys(5) = Text5.Text

End Sub

Private Sub Text6_Change()
If Len(Text6.Text) > 1 Then Text6.Text = Mid(Text6.Text, Len(Text6.Text) - 1, 1)
If Text6.Text <> "" Then KEYpadkeys(6) = Text6.Text

End Sub

Private Sub Text7_Change()
If Len(Text7.Text) > 1 Then Text7.Text = Mid(Text7.Text, Len(Text7.Text) - 1, 1)
If Text7.Text <> "" Then KEYpadkeys(7) = Text7.Text

End Sub

Private Sub Text8_Change()
If Len(Text8.Text) > 1 Then Text8.Text = Mid(Text8.Text, Len(Text8.Text) - 1, 1)
If Text8.Text <> "" Then KEYpadkeys(8) = Text8.Text

End Sub

Private Sub Timer1_Timer()
Cls

Dim loopkey
Label1.Caption = "keys pressed(asc):"
For loopkey = 0 To 255
If keypressed(loopkey) = True Then
Label1.Caption = Label1.Caption + Str(loopkey) + "/ " + Chr$(loopkey)
End If
Next loopkey


If Check1.Value = 1 Then
If joyGetPosEx(JNum, JI) <> 0 Then
    Print "Joystick #"; CStr(JNum); " is not plugged in, or is not working."
Else
    With JI
        Print "X = "; CStr(.dwXpos)
        Print "Y = "; CStr(.dwYpos)
        Print "Z = "; CStr(.dwZpos)
        Print "R = "; CStr(.dwRpos)
        Print "U = "; CStr(.dwUpos)
        Print "V = "; CStr(.dwVpos)
        
        
        
      '  If .dwPOV < &HFFFF& Then
      '  Print "PovAngle = "; CStr(.dwPOV / 100)
       ' If Text1.Text <> "" Then KEYpadkeys(1) = Text1.Text
'If Text2.Text <> "" Then KEYpadkeys(2) = Text2.Text
'If Text3.Text <> "" Then KEYpadkeys(3) = Text3.Text
'If Text4.Text <> "" Then KEYpadkeys(4) = Text4.Text
        If .dwXpos = 65535 And .dwYpos = 0 Then
        keybd_event Asc(UCase(KEYpadkeys(3))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(1))), 0, 0, 0  ' press
        keybd_event Asc(UCase(KEYpadkeys(2))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(4))), 0, 0, 0  ' press
        Else
        End If


        If .dwXpos = 65535 And .dwYpos = 65535 Then
        keybd_event Asc(UCase(KEYpadkeys(3))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(1))), 0, 0, 0  ' press
        keybd_event Asc(UCase(KEYpadkeys(2))), 0, 0, 0  ' press
        keybd_event Asc(UCase(KEYpadkeys(4))), 0, KEYEVENTF_KEYUP, 0  ' release
        Else
        End If


        If .dwXpos = 0 And .dwYpos = 65535 Then
        keybd_event Asc(UCase(KEYpadkeys(3))), 0, 0, 0  ' press
        keybd_event Asc(UCase(KEYpadkeys(1))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(2))), 0, 0, 0  ' press
        keybd_event Asc(UCase(KEYpadkeys(4))), 0, KEYEVENTF_KEYUP, 0  ' release
        Else
        End If

        If .dwXpos = 0 And .dwYpos = 0 Then
        keybd_event Asc(UCase(KEYpadkeys(3))), 0, 0, 0  ' press
        keybd_event Asc(UCase(KEYpadkeys(1))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(2))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(4))), 0, 0, 0  ' press
        Else
        End If

        
        If .dwXpos = 65535 And .dwYpos = 32511 Then
        keybd_event Asc(UCase(KEYpadkeys(3))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(1))), 0, 0, 0  ' press
        keybd_event Asc(UCase(KEYpadkeys(2))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(4))), 0, KEYEVENTF_KEYUP, 0  ' release
        Else
        End If
        
        If .dwXpos = 0 And .dwYpos = 32511 Then
        keybd_event Asc(UCase(KEYpadkeys(3))), 0, 0, 0  ' press
        keybd_event Asc(UCase(KEYpadkeys(1))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(4))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(2))), 0, KEYEVENTF_KEYUP, 0  ' release
        Else
        End If
        
        If .dwXpos = 32511 And .dwYpos = 65535 Then
       keybd_event Asc(UCase(KEYpadkeys(2))), 0, 0, 0  ' press
        keybd_event Asc(UCase(KEYpadkeys(4))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(1))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(3))), 0, KEYEVENTF_KEYUP, 0  ' release
        
        Else
        End If
        
        If .dwXpos = 32511 And .dwYpos = 0 Then
        keybd_event Asc(UCase(KEYpadkeys(4))), 0, 0, 0  ' press
        keybd_event Asc(UCase(KEYpadkeys(2))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(1))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(3))), 0, KEYEVENTF_KEYUP, 0  ' release
        Else
        End If
        
      '   End If
      
  If .dwXpos = 32511 And .dwYpos = 32511 Then
  Print "PovCentered"
        keybd_event Asc(UCase(KEYpadkeys(3))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(1))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(2))), 0, KEYEVENTF_KEYUP, 0  ' release
        keybd_event Asc(UCase(KEYpadkeys(4))), 0, KEYEVENTF_KEYUP, 0  ' release
        Else
       
         
         
        End If
        
        
      
        
        
        Print "ButtonsPressedCount = "; CStr(.dwButtonNumber)
        Print "ButtonBinaryFlags = "; CStr(.dwButtons)
        
        If .dwButtons = 1 Or .dwButtons = 5 Or .dwButtons = 3 Or .dwButtons = 7 Or .dwButtons = 9 Or .dwButtons = 11 Or .dwButtons = 13 Or .dwButtons = 15 Then
       keybd_event Asc(UCase(KEYpadkeys(8))), 0, 0, 0  ' press
        Else
        keybd_event Asc(UCase(KEYpadkeys(8))), 0, KEYEVENTF_KEYUP, 0  ' release
        End If
        
        If .dwButtons = 8 Or .dwButtons = 12 Or .dwButtons = 14 Or .dwButtons = 9 Or .dwButtons = 10 Or .dwButtons = 11 Or .dwButtons = 13 Or .dwButtons = 15 Then
        keybd_event Asc(UCase(KEYpadkeys(6))), 0, 0, 0  ' press
        Else
        keybd_event Asc(UCase(KEYpadkeys(6))), 0, KEYEVENTF_KEYUP, 0  ' release
        End If
        
        
       If .dwButtons = 2 Or .dwButtons = 6 Or .dwButtons = 7 Or .dwButtons = 3 Or .dwButtons = 10 Or .dwButtons = 11 Or .dwButtons = 14 Or .dwButtons = 15 Then
       keybd_event Asc(UCase(KEYpadkeys(7))), 0, 0, 0  ' press
       Else
       keybd_event Asc(UCase(KEYpadkeys(7))), 0, KEYEVENTF_KEYUP, 0  ' release
       End If
        
       If .dwButtons = 4 Or .dwButtons = 5 Or .dwButtons = 7 Or .dwButtons = 6 Or .dwButtons = 12 Or .dwButtons = 13 Or .dwButtons = 14 Or .dwButtons = 15 Then
       keybd_event Asc(UCase(KEYpadkeys(5))), 0, 0, 0  ' press
       Else
       keybd_event Asc(UCase(KEYpadkeys(5))), 0, KEYEVENTF_KEYUP, 0  ' release
       End If
        
        
        
        
        
        
        
        
        
        Picture1.Cls
        Picture1.Circle (.dwXpos / &HFFFF& * (Picture1.Width - 1), .dwYpos / &HFFFF& * (Picture1.Height - 1)), 2
    End With
End If
End If
End Sub
Public Sub endProcedure()
End
End Sub


